package cat.ereza.customactivityoncrash.helper;

import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import cat.ereza.customactivityoncrash.BuildConfig;
import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.Encryptor.Encrypts;
import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import cat.ereza.customactivityoncrash.model.User;
import cz.msebera.android.httpclient.Header;

public class GlobalHelper {
//    WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabasePath(),
//            GlobalVars.DB_MASTER,
//            GlobalVars.getDatabasePass());
//    WaspHash tblUser = db.openOrCreateHash(GlobalVars.SELECTED_USER);
//    final User user = tblUser.get(GlobalVars.SELECTED_USER);

    public static final String EXTERNAL_DIR_FILES = "/AMSApp/files";
    //no SQL
    public static final String DB_MASTER = "DBMasterAMS";
    public static final String DB_PASS = "AMS12345PALM";
    public static final String SELECTED_USER = "User";
    public static final String CrashReport = "CrashReport";


    public static User getUser(){
        Gson gson = new Gson();
        //Log.e("Encryp",Encrypts.encrypt(SELECTED_USER));
        if(isFileContentAvailable(Encrypts.encrypt(SELECTED_USER))){
            User user = null;
            user = gson.fromJson(
                    Encrypts.decrypt(getFileContent(Encrypts.encrypt(SELECTED_USER))),
                    User.class);

            return user;
        }
        return null;
    }

    public static boolean isFileContentAvailable(String fileName){
        File file = new File(getDatabasePath(),fileName);
        return file.exists();
    }

    public static String getFileContent(String fileName){
        File file = new File(getDatabasePath(),fileName);
        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine()) !=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDatabasePath(){
//        Log.e("Storage", Environment.getDataDirectory() + EXTERNAL_DIR_FILES +"/");
        String path = "";
        if(BuildConfig.BUILD_VARIANT.equals("dev")){
            path = Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/dev/db/";
            Log.e("Storage",path);
        }else{
            path = Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/";
            Log.e("Storage",path);
        }

        return path;
//        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/";

    }

    public static String getDatabasePass(){
        return Encrypts.encrypt(DB_PASS);
    }



//    public void noSqlInit(){
//        user = GlobalHelper.getUser();
//    }

    public static void isOffline( CrashDetailAnalyticInfo crashDetail){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                GlobalHelper.DB_MASTER,
                GlobalHelper.getDatabasePass());
        final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
        int i=0;
        if(crashTbl.getAllValues().size()==0){
            i=1;
        }else{
            i = crashTbl.getAllValues().size()+1;
        }
        crashTbl.put(i,crashDetail);
//        if(loadingDialog.isShowing()){
//            loadingDialog.dismiss();
//        }

        Log.i("Offline Add", "berhasil simpan log crash : "+crashTbl.getAllValues().size());
    }
}
