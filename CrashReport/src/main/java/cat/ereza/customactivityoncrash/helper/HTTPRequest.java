package cat.ereza.customactivityoncrash.helper;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import cat.ereza.customactivityoncrash.util.CrashUtil;

public class HTTPRequest {
//    protected final static String baseUrl = "http://172.30.1.122/Android_Service/";
//    protected final static String baseUrl = "http://147.139.162.146:33520/Android_Service/";
    protected final static String baseUrl = "https://app.gis-div.com/Android_Service/";
    public final static AsyncHttpClient client = new AsyncHttpClient();


    public static void reportCrash(Context context, CrashDetailAnalyticInfo model,
                                                 AsyncHttpResponseHandler handler){
        String URL = baseUrl + "api/user/CrashReport";
        RequestParams params = new RequestParams();
        params.add("androidID",model.getAndroidID());
        params.add("packageName",model.getPackageName());
        params.add("crashDate", CrashUtil.getStringFromDate(model.getCrashDate()));
        params.add("userID", ""+model.getUserID());
        params.add("brand",model.getBrand());
        params.add("board",model.getBoard());
        params.add("display",model.getDisplay());
        params.add("hardware",model.getHardware());
        params.add("product",model.getProduct());
        params.add("manufacturer",model.getManufacturer());
        params.add("id",model.getId());
        params.add("type",model.getType());
        params.add("androidOS",model.getAndroidOS());
        params.add("androidOS_code",model.getAndroidOS_code());
        params.add("deviceRAM", ""+model.getDeviceRAM());
        params.add("deviceStorage",""+model.getDeviceStorage());
        params.add("deviceResolution",model.getDeviceResolution());
        params.add("deviceScreenSize",""+model.getDeviceScreenSize());
        params.add("crashIP",model.getCrashIP());
        params.add("macAddress",model.getMacAddress());
        params.add("crashDetail",model.getCrashDetail());

        client.post(context,URL,params,handler);
    }



    public static void getHelpDescription(Context context,AsyncHttpResponseHandler handler){
        String URL = baseUrl + "api/user/GetHelpDescription?packageName=id.co.ams_plantation.amsadminapps";
        client.get(context,URL,handler);
    }


}
