package cat.ereza.customactivityoncrash.model;

import java.util.Date;

public class CrashDetailAnalyticInfo {

    int idCrashHistory;
    String androidID ;
    String packageName;
    Date crashDate;
    int userID;
    String brand;
    String board;
    String display;
    String hardware;
    String manufacturer;
    String id;
    String product;
    String type;
    String androidOS;
    String androidOS_code;
    long deviceRAM;
    long deviceStorage;
    String deviceResolution;
    double deviceScreenSize;
    String crashIP;
    String macAddress;
    String crashDetail;
    int hasSync;

    public int getIdCrashHistory() {
        return idCrashHistory;
    }

    public void setIdCrashHistory(int idCrashHistory) {
        this.idCrashHistory = idCrashHistory;
    }

    public String getAndroidID() {
        return androidID;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Date getCrashDate() {
        return crashDate;
    }

    public void setCrashDate(Date crashDate) {
        this.crashDate = crashDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String harddware) {
        this.hardware = harddware;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAndroidOS() {
        return androidOS;
    }

    public void setAndroidOS(String androidOS) {
        this.androidOS = androidOS;
    }

    public String getAndroidOS_code() {
        return androidOS_code;
    }

    public void setAndroidOS_code(String androidOS_code) {
        this.androidOS_code = androidOS_code;
    }

    public long getDeviceRAM() {
        return deviceRAM;
    }

    public void setDeviceRAM(long deviceRAM) {
        this.deviceRAM = deviceRAM;
    }

    public long getDeviceStorage() {
        return deviceStorage;
    }

    public void setDeviceStorage(long deviceStorage) {
        this.deviceStorage = deviceStorage;
    }

    public String getDeviceResolution() {
        return deviceResolution;
    }

    public void setDeviceResolution(String deviceResolution) {
        this.deviceResolution = deviceResolution;
    }

    public double getDeviceScreenSize() {
        return deviceScreenSize;
    }

    public void setDeviceScreenSize(double deviceScreenSize) {
        this.deviceScreenSize = deviceScreenSize;
    }

    public String getCrashIP() {
        return crashIP;
    }

    public void setCrashIP(String crashIP) {
        this.crashIP = crashIP;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getCrashDetail() {
        return crashDetail;
    }

    public void setCrashDetail(String crashDetail) {
        this.crashDetail = crashDetail;
    }

    public int getHasSync(){
        return hasSync;
    }

    private void setHasSync(int hasSync){
        this.hasSync = hasSync;
    }

    public String toString(){

        return "androidID: "+ androidID+" packageName: "+packageName+" crashDate: "+crashDate.toString()+" userID: "
                + userID+" brand: "+brand+" Board: "+board+" Display : "+display+" Hardware: "+hardware
                +" Manufacturer: "+manufacturer+" id: "+id+" Product: "+product+" Type: "+type+" androdiOS : "+androidOS
                +" AndroidOS_COde: "+androidOS_code+" deviceRAM: "+deviceRAM+" DeviceStorage: "+deviceStorage+" DeviceResolution: "+deviceResolution
                +" deviceScreenSize: "+deviceScreenSize+ "CrashIP: "+crashIP+" macAddress: "+macAddress+" crashDetail: "+crashDetail+" hasSync: "+hasSync;

    }
}
