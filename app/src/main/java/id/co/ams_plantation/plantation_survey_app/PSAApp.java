package id.co.ams_plantation.plantation_survey_app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.ViewModelProvider;
import androidx.multidex.MultiDex;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;

import java.io.File;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import id.co.ams_plantation.plantation_survey_app.data.AppDatabase;
import id.co.ams_plantation.plantation_survey_app.data.view_model.AssetViewModel;
import id.co.ams_plantation.plantation_survey_app.ui.BaseActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;

/**
 * Created by Mafrukhin on 08/02/2023.
 */

public class PSAApp extends Application {
    private static Context context;
    public static final String CONNECTION_PREF = "CONNECTION_PREF";
    protected static AssetViewModel mAssetViewModel;
    protected static AppDatabase appDb;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Prefs.init(this);
        MultiDex.install(this);
        PSAApp.context = getApplicationContext();
        Log.e("OncreatePSA","true");
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(this, config);
        try {
            GlobalHelper.overrideFont(getContext(),"DEFAULT",GlobalHelper.FONT_CORBERT);
            GlobalHelper.overrideFont(getContext(),"SERIF",GlobalHelper.FONT_CORBERT);
        } catch (Exception e) {
            Log.e("Can not set custom font","Fail");
        }
        CustomActivityOnCrash.install(this);

//        mAssetViewModel = new ViewModelProvider().get(AssetViewModel.class);
//        appDb = AppDatabase.getDatabase(this);

    }

    public static Context getContext(){
        return PSAApp.context;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            Log.e("deleted cache",dir.getAbsolutePath());
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
                Log.e("deleted cache","true");
//                context.getCacheDir().
            }
        } catch (Exception e) {
            Log.e("deleted cache",e.getMessage());

        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
