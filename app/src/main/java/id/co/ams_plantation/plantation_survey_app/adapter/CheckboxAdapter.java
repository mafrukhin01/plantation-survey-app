package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.plantation_survey_app.R;

public class CheckboxAdapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<String> items;

    public CheckboxAdapter(Context context, ArrayList<String> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_employee, parent, false);
            holder = new ViewHolder();
            holder.checkBox = convertView.findViewById(R.id.checkbox);
            holder.textView = convertView.findViewById(R.id.text_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final String item = items.get(position);
        holder.textView.setText(item);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tanggapan saat kotak centang diklik
                CheckBox checkBox = (CheckBox) v;
                if (checkBox.isChecked()) {
                    // Checkbox ditandai
                    // Tambahkan logika Anda di sini
                } else {
                    // Checkbox tidak ditandai
                    // Tambahkan logika Anda di sini
                }
            }
        });

        return convertView;
    }

    static class ViewHolder {
        CheckBox checkBox;
        TextView textView;
    }

}
