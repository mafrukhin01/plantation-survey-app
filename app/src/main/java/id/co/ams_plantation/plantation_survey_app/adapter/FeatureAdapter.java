package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.ViewHolder> {
    List<EditFeature> mData;
    Set<String> keys;
    private LayoutInflater mInflater;
    MapReplantingActivity activity;
    List<EditFeature>editFeatures;
    private ItemClickListener mClickListener;
    public FeatureAdapter(Context context, List<EditFeature>  data){
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        editFeatures = new ArrayList<>();

//        this.keys =  mData.keySet();
        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;
        }
    }

    public List<EditFeature> getEditedFeature(){
        Log.e("getEditedFeature", String.valueOf(editFeatures.size()));
        return editFeatures;
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        TextView value;

        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_key);
            value = itemView.findViewById(R.id.et_value);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_feature, parent, false);


        return new ViewHolder(view);
    }

    public void bind(ViewHolder holder, final int position){
        final EditFeature itemEdit = mData.get(position);
        holder.value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("beforeChange "+holder.tvName.getText(),charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                EditFeature item = new EditFeature(holder.tvName.getText().toString(),holder.value.getText().toString());
                editFeatures.add(item);
                Log.e("afterChange "+editFeatures.size()+item.getKey(),item.getValue());
            }
        });

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(GlobalHelper.getNameAttribute(mData.get(position).getKey()));
        holder.value.setText(mData.get(position).getValue());
        final ViewHolder mHolder = (ViewHolder) holder;
        bind(mHolder,position);
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        mClickListener = itemClickListener;
    }
    @Override
    public int getItemCount() {
        return mData.size();
    }

}
