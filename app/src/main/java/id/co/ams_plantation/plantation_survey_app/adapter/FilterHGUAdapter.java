package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Optionlayer;
import id.co.ams_plantation.plantation_survey_app.ui.MapHguActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;

public class FilterHGUAdapter extends RecyclerView.Adapter<FilterHGUAdapter.ViewHolder> implements Filterable {
        TextView tvName;
        private List<Optionlayer> mData;
        private LayoutInflater mInflater;
        private ItemClickListener mClickListener;
        CheckBox cb;
        SeekBar sb;
        MapHguActivity activity3;
        private contextOf mState;
        //        ImageView iv;

    public FilterHGUAdapter(Context context, List<Optionlayer> data){
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        if(context instanceof MapHguActivity){
            activity3=(MapHguActivity) context;
        }


//
    }

    @Override
    public Filter getFilter() {
        return null;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        CheckBox cb;
        SeekBar sb;
        //        ImageView iv;
        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            cb = itemView.findViewById(R.id.cb_item);
            sb = itemView.findViewById(R.id.seekBar);
//            iv = itemView.findViewById(R.id.icon_legend);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_option_layer, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.iv.setImageBitmap(bitmapList.get(position));
        holder.tvName.setText(mData.get(position).getName());

        int p = position;

        if (mData.get(position).isChecked()){
            holder.cb.setChecked(true);
        }
//        else {
//            holder.cb.setChecked(true);
//        }
        holder.cb.setOnCheckedChangeListener((compoundButton, b) -> {
            Log.e(String.valueOf(mData.get(p).name),String.valueOf(b));
            if(b){
                    mData.get(p).setChecked(true);
            }
            else {
                    mData.get(p).setChecked(false);
            }
            activity3.applyDefinition();

        });
    }


    @Override
    public int getItemCount() {
//        Log.e("itemCount",String.valueOf(mData.size()));
        return mData.size();
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    enum contextOf {
        Inventory,
        Replanting,
        HGU
    }
}
