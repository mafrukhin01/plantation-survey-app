package id.co.ams_plantation.plantation_survey_app.adapter;

import static androidx.viewpager.widget.PagerAdapter.POSITION_NONE;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.R;

/**
 * Created by user on 11/15/2017.
 */

public class ImageViewPagerPinchToZoomAdapter extends PagerAdapter {
    private List<File> drawables;
    private String urlI;
    public ImageViewPagerPinchToZoomAdapter(List<File> drawables, String urlI) {
        this.urlI = urlI;
        this.drawables = drawables;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Context context = container.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.page_image, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);

        Bitmap myBitmap = null;
//        container.addView(view);
        myBitmap = loadImageFromUrl(urlI);

        if (myBitmap != null) {
            imageView.setImageBitmap(myBitmap);
            ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(context);
            imageView.setOnTouchListener(imageMatrixTouchHandler);
            container.addView(view); // Tambahkan view setelah gambar dimuat
        } else {
            Toast.makeText(context, "Gagal memuat gambar", Toast.LENGTH_SHORT).show();
        }

        return view;
//        if(drawables.get(position).toString().startsWith("http")){
//            String surl = drawables.get(position).toString();
//            if(drawables.get(position).toString().startsWith("http://")){
//
//            }else if(drawables.get(position).toString().startsWith("http:/")){
//                surl =surl.replace("http:/","http://");
//            }else if(drawables.get(position).toString().startsWith("https://")){
//
//            }else if(drawables.get(position).toString().startsWith("https:/")){
//                surl =surl.replace("https:/","https://");
//            }
////            try {
////                URL urlConnection = new URL(urlI);
////                Log.e("StringURL",urlI);
////                HttpURLConnection connection = (HttpURLConnection) urlConnection
////                        .openConnection();
////                connection.setDoInput(true);
////                connection.connect();
////                InputStream input = connection.getInputStream();
////                myBitmap = BitmapFactory.decodeStream(input);
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
//        }else {
//            myBitmap = BitmapFactory.decodeFile(drawables.get(position).getAbsolutePath());
//        }
//        if(myBitmap != null) {
//            imageView.setImageBitmap(myBitmap);
//            ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(context);
//            imageView.setOnTouchListener(imageMatrixTouchHandler);
//        }else{
//            Toast.makeText(PSAApp.getContext(),"Gagal Load Foto",Toast.LENGTH_SHORT);
//        }
//        return view;
    }

//    @Override
//    public Object instantiateItem(View container, int position) {
//        Context context = container.getContext();
//        LayoutInflater layoutInflater = LayoutInflater.from(context);
//        View view = layoutInflater.inflate(R.layout.page_image, null);
//
//        ImageView imageView = (ImageView) view.findViewById(R.id.image);
//        Bitmap myBitmap = null;
//        try {
//            URL urlConnection = new URL(urlI);
//            Log.e("StringURL",urlI);
//            HttpURLConnection connection = (HttpURLConnection) urlConnection
//                    .openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            myBitmap = BitmapFactory.decodeStream(input);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        if(drawables.get(position).toString().startsWith("http")){
////            String surl = drawables.get(position).toString();
////            if(drawables.get(position).toString().startsWith("http://")){
////
////            }else if(drawables.get(position).toString().startsWith("http:/")){
////                surl =surl.replace("http:/","http://");
////            }else if(drawables.get(position).toString().startsWith("https://")){
////
////            }else if(drawables.get(position).toString().startsWith("https:/")){
////                surl =surl.replace("https:/","https://");
////            }
//////            try {
//////                URL urlConnection = new URL(urlI);
//////                Log.e("StringURL",urlI);
//////                HttpURLConnection connection = (HttpURLConnection) urlConnection
//////                        .openConnection();
//////                connection.setDoInput(true);
//////                connection.connect();
//////                InputStream input = connection.getInputStream();
//////                myBitmap = BitmapFactory.decodeStream(input);
//////            } catch (Exception e) {
//////                e.printStackTrace();
//////            }
////        }else {
////            myBitmap = BitmapFactory.decodeFile(drawables.get(position).getAbsolutePath());
////        }
//        if(myBitmap != null) {
//            Log.e("StringURLnootnull",urlI);
//            imageView.setImageBitmap(myBitmap);
//            ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(context);
//            imageView.setOnTouchListener(imageMatrixTouchHandler);
//        }else{
//            Toast.makeText(PSAApp.getContext(),"Gagal Load Foto",Toast.LENGTH_SHORT);
//        }
//        return view;
//    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(0);

        container.removeView(view);
    }

    @Override
    public int getCount() {
        return drawables.isEmpty() ? 1 : drawables.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition (Object object) {
        return POSITION_NONE;
    }
    private Bitmap loadImageFromUrl(String imageUrl) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
