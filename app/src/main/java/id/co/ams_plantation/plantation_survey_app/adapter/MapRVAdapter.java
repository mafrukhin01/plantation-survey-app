package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.ArrayList;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.services.model.AppInfo;
import id.co.ams_plantation.plantation_survey_app.ui.DownloadActivity;
import id.co.ams_plantation.plantation_survey_app.utils.DownloadMapHelper;

/**
 * Created on : 09,May,2022
 * Author     : Afid
 */

public class MapRVAdapter extends RecyclerView.Adapter<MapRVAdapter.Holder>{
    Context context;
    ArrayList<AppInfo> originLists;
    public MapRVAdapter(Context context, ArrayList<AppInfo> originLists) {
        this.context = context;
        this.originLists = originLists;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.download_item,viewGroup,false);
        return new Holder(v);
    }
    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setValue(originLists.get(position),position);
    }

    @Override
    public int getItemCount() {
        return originLists!=null ? originLists.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        public ImageView map_iv_icon;
        public TextView map_tv_namafile;
        public TextView map_tv_kebun;
        public TextView map_tv_version;
        public TextView map_tv_filesize;
        public Button btnDownload;

        public Holder(View itemView) {
            super(itemView);
            map_iv_icon = itemView.findViewById(R.id.map_iv_icon);
            map_tv_namafile = itemView.findViewById(R.id.map_tv_namafile);
            map_tv_kebun = itemView.findViewById(R.id.map_tv_kebun);
            map_tv_version = itemView.findViewById(R.id.map_tv_version);
            map_tv_filesize = itemView.findViewById(R.id.map_tv_filesize);
            btnDownload = itemView.findViewById(R.id.btnDownload);
        }

        public void setValue(AppInfo appInfo,int position) {
            switch (appInfo.getMaps().getFileType()){
                case "KML_BLOCK":
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            MaterialDesignIconic.Icon.gmi_layers
                    ).sizeDp(40).colorRes(R.color.Gray));
                    break;
                case "ZIP_MAP":
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            MaterialDesignIconic.Icon.gmi_map
                    ).sizeDp(40).colorRes(R.color.Gray));
                    break;
                case "KML_LANDUSE":
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            MaterialDesignIconic.Icon.gmi_nature
                    ).sizeDp(40).colorRes(R.color.Gray));
                    break;
                case "PDF_MAP":
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            MaterialDesignIconic.Icon.gmi_collection_pdf
                    ).sizeDp(40).colorRes(R.color.Gray));
                    break;
                case "FOTO_UDARA_MAP":
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            FontAwesome.Icon.faw_file_archive_o
                    ).sizeDp(40).colorRes(R.color.Gray));
                    break;
                default:
                    map_iv_icon.setImageDrawable(new IconicsDrawable(context).icon(
                            MaterialDesignIconic.Icon.gmi_file
                    ).sizeDp(40).colorRes(R.color.Gray));
            }

            map_tv_namafile.setText(appInfo.getMaps().getFilename().split("\\.")[0]);
            map_tv_kebun.setText(appInfo.getMaps().getCompanyShortName()+ " : "+
                    DownloadMapHelper.allCapsConverter(appInfo.getMaps().getEstName()));
            map_tv_version.setText(DownloadMapHelper.versionRead(Long.parseLong(appInfo.getMaps().getLastModified())));
            map_tv_filesize.setText(DownloadMapHelper.fileSizeShorter(String.valueOf(appInfo.getMaps().getFileLengthBytes()),true));

            btnDownload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof DownloadActivity){
                        ((DownloadActivity) context).rowMapItemAction(appInfo,position);
                    }
                }
            });

        }
    }

}