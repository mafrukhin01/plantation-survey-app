package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.ui.SyncBlockActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class MasterBlockAdapter extends BaseAdapter implements Filterable {
    private final Context context;
//    private List<MasterBlock> blocks;
    private ArrayList<MasterBlock> blocks;
    private ArrayList<MasterBlock> mOriginalValues;
    private ArrayList<MasterBlock> blockTemp;
    private MasterBlock blockModel;
    String estateCode,userID, url;
    SyncBlockActivity activity;
    private LayoutInflater layoutInflater;
    public MasterBlockAdapter(Context context, ArrayList<MasterBlock> blocks, ViewModelStoreOwner owner) {
        this.context = context;
        this.blocks = blocks;
        estateCode = GlobalHelper.getEstate().getEstCode();
        userID = GlobalHelper.getUser().getUserID();
        activity = (SyncBlockActivity) context;
//        mBlockViewModel = new ViewModelProvider(owner).get(MasterBlockViewModel.class);

        layoutInflater= LayoutInflater.from(context);

//        Log.e("blockTemp",String.valueOf(blockTemp.size()));
    }
    @Override
    public int getCount() {
        return blocks.size();
    }

    @Override
    public Object getItem(int position) {
        return blocks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        Log.e("getView","true");
        if (rowView == null) {
            rowView = layoutInflater.inflate(R.layout.block_adapter, parent, false);
        }

        TextView blockName = rowView.findViewById(R.id.blockName);
        TextView haGross = rowView.findViewById(R.id.tv_hagross);
        TextView ancak = rowView.findViewById(R.id.tv_ancak);
        TextView tph = rowView.findViewById(R.id.tv_tph);
        TextView mandor = rowView.findViewById(R.id.tv_mandor);
        CheckBox checkBox = rowView.findViewById(R.id.checkbox);
        Button download = rowView.findViewById(R.id.bt_download);
        Button open = rowView.findViewById(R.id.bt_open);
        mandor.setText(blocks.get(position).mandor);
        blockName.setText(blocks.get(position).getBlock());
        haGross.setText(blocks.get(position).getHaGross());
        blockModel = blocks.get(position);
        checkBox.setChecked(blockModel.isSelected());
        checkBox.setTag(position);
        download.setOnClickListener(v -> {
            activity.showIsCheckout(blocks.get(position).getBlock());
        });

        checkBox.setOnClickListener(v -> {
            int currentPos = (int) v.getTag();
//            boolean isChecked = false;
            if (!blocks.get(currentPos).isSelected()) {
                blocks.get(currentPos).setSelected(true);
//                if(blockTemp.stream().filter(MasterBlock::isSelected).count() >= 3){
//                    Toast.makeText(context, "Hanya 3 block yang boleh dipilih", Toast.LENGTH_LONG).show();
//                    blocks.get(currentPos).setSelected(false);
//                }
//                else {
//                    blocks.get(currentPos).setSelected(true);
//                    blockTemp.add(blocks.get(currentPos));
//                }
            }
            else {
                blocks.get(currentPos).setSelected(false);
//                blockTemp.remove(blocks.get(currentPos));
            }
//            blocks.stream().filter(MasterBlock::isSelected).forEach(block -> block.setSelected(false));
//            blockTemp.forEach(b->{
//                blocks.stream().filter(block -> block.getBlock().equals(b.getBlock())).forEach(block -> block.setSelected(true));
//            });
//            mBlockViewModel.resetBlock();
//            blockTemp.forEach(b->{
//                mBlockViewModel.updateBlock(b.getBlockName());
//            });

//                mBlockViewModel.updateBlock(blocks.get(currentPos).getBlockName());
            notifyDataSetChanged();
        });

//        try {
//            status.setText(GlobalHelper.getStatusCensusBlock(blockModel.getStatusSensus()));
//            duplicate.setText(blockModel.getTotalDuplicate());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        checkBox.setChecked(blockModel.isSelected());
//        checkBox.setTag(position);
//        download.setTag(position);
//        open.setTag(position);
//        download.setOnClickListener(v -> {
//            int currentPos = (int) v.getTag();
//            downloadReport(activity,blocks.get(currentPos).getBlock());
//        });
//        open.setOnClickListener(v -> {
//            int currentPos = (int) v.getTag();
//            openReport(blocks.get(currentPos).getBlock());
//        });

//        checkBox.setOnClickListener(v -> {
//            int currentPos = (int) v.getTag();
////            boolean isChecked = false;
//            if (!blocks.get(currentPos).isSelected()) {
//                if(blockTemp.stream().filter(MasterBlock::isSelected).count() >= 3){
//                    Toast.makeText(context, "Hanya 3 block yang boleh dipilih", Toast.LENGTH_LONG).show();
//                    blocks.get(currentPos).setSelected(false);
//                }
//                else {
//                    blocks.get(currentPos).setSelected(true);
//                    blockTemp.add(blocks.get(currentPos));
//                }
//            }
//            else {
//                blocks.get(currentPos).setSelected(false);
//                blockTemp.remove(blocks.get(currentPos));
//            }
//            blocks.stream().filter(MasterBlock::isSelected).forEach(block -> block.setSelected(false));
//            blockTemp.forEach(b->{
//                blocks.stream().filter(block -> block.getBlock().equals(b.getBlock())).forEach(block -> block.setSelected(true));
//            });
////            mBlockViewModel.resetBlock();
////            blockTemp.forEach(b->{
////                mBlockViewModel.updateBlock(b.getBlockName());
////            });
//
////                mBlockViewModel.updateBlock(blocks.get(currentPos).getBlockName());
//            notifyDataSetChanged();
//        });
        return rowView;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {
                blocks = (ArrayList<MasterBlock>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<MasterBlock> FilteredArrList = new ArrayList<MasterBlock>();
                if (mOriginalValues == null) {
                    mOriginalValues = blocks; // saves the original data in mOriginalValues
                }
                if (constraint == null || constraint.length() == 0) {
                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String data = mOriginalValues.get(i).getBlock();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}
