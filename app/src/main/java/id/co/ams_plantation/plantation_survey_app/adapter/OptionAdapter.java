package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Optionlayer;
import id.co.ams_plantation.plantation_survey_app.ui.MapHguActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

/**
 * Created by Mafrukhin on 26/06/2023.
 */
public class OptionAdapter extends RecyclerView.Adapter<OptionAdapter.ViewHolder> implements Filterable {

    private List<Optionlayer> mData;
    private LayoutInflater mInflater;
    private SelectionAdapter.ItemClickListener mClickListener;
    MapReplantingActivity activity;
    MapInventoryActivity activity2;
    MapHguActivity activity3;
    private contextOf mState;
    private List<Bitmap>bitmapList;


    public OptionAdapter(Context context, List<Optionlayer> data){
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
//        bitmapList = bitmaps;
        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;
            mState = contextOf.Replanting;
        }
        else if (context instanceof MapInventoryActivity){
            activity2 = (MapInventoryActivity) context;
            mState = contextOf.Inventory;
        }else {
            activity3=(MapHguActivity) context;
            mState = contextOf.HGU;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        CheckBox cb;
        SeekBar sb;
//        ImageView iv;
        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            cb = itemView.findViewById(R.id.cb_item);
            sb = itemView.findViewById(R.id.seekBar);
//            iv = itemView.findViewById(R.id.icon_legend);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    @Override
    public Filter getFilter() {
        return null;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_option_layer, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionAdapter.ViewHolder holder, int position) {
//        holder.iv.setImageBitmap(bitmapList.get(position));
        holder.tvName.setText(mData.get(position).getName());

        int p = position;
        final  Optionlayer item = mData.get(position);
        if (mData.get(position).isChecked()){
            holder.cb.setChecked(true);
        }
//        else {
//            holder.cb.setChecked(true);
//        }
        holder.cb.setOnCheckedChangeListener((compoundButton, b) -> {
//            mData.get(p).setSelected(b);
            if(b){
                if(mState == contextOf.Replanting){
                    activity.setVisible(p);
                }
                else {
                    mData.get(p).setChecked(true);
                    activity2.applyDefinition();
                }
            }
            else {
                if(mState == contextOf.Replanting) {
                    activity.setInVisible(p);
                }
                else {
                    mData.get(p).setChecked(false);
                    activity2.applyDefinition();
//                    activity2.setInVisible(p);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
//        Log.e("itemCount",String.valueOf(mData.size()));
        return mData.size();
    }
    public void setClickListener(SelectionAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    String getItem(int id) {
        return mData.get(id).getName();
    }

    enum contextOf {
        Inventory,
        Replanting,
        HGU
    }
}
