package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.ui.DownloadActivity;
import id.co.ams_plantation.plantation_survey_app.ui.HomeActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapHguActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;
import id.co.ams_plantation.plantation_survey_app.ui.ProjectsActivity;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder>implements Filterable {
    private final Context context;
    ProjectsActivity activity;
    private List<UserProject> projects;
    private SelectionAdapter.ItemClickListener mClickListener;
//    private MasterBlockViewModel mBlockViewModel;
    private LayoutInflater layoutInflater;

    String estateCode,userID, url;
    public ProjectAdapter(Context context, List<UserProject> blocks) {
        activity = (ProjectsActivity) context;
        this.context = context;
        this.projects = blocks;
//        mBlockViewModel = new ViewModelProvider(owner).get(MasterBlockViewModel.class);
        layoutInflater= LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName,tvDesc;
        TextView tvEstate;
        Button bt_open;
        TextView tvStart;
        TextView tvEnd;
        ImageView iv;

        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_project);
            tvDesc = itemView.findViewById(R.id.tv_desc);
            tvEstate= itemView.findViewById(R.id.tv_estate);
            tvStart = itemView.findViewById(R.id.tv_periode);
            iv = itemView.findViewById(R.id.bimg_project);
            bt_open = itemView.findViewById(R.id.bt_open);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {

        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_project, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int p = position;
        String[] parts = GlobalHelper.getEstate().getEstName().split(" ");
        holder.tvName.setText(projects.get(position).description);
        holder.tvDesc.setText(projects.get(position).description);
        holder.tvEstate.setText(parts[0]);
        holder.tvStart.setText(DateTime.toStringTemplate(projects.get(position).getStartDate())+" : "+DateTime.toStringTemplate(projects.get(position).getEndDate()));
        holder.bt_open.setOnClickListener(v -> {
            if (projects.get(position).description.equals("Asset Water Management")){
                String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
                String path2 = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
                File file2 = new File(path2);
                File file = new File(path);
                if(!file.exists()||!file2.exists()){
                    Intent i = new Intent(context, DownloadActivity.class);
                    context.startActivity(i);
                    activity.backup();
                }
                else {
                    if (onPeriod(projects.get(p).getEndDate())){
                        Intent i = new Intent(context, MapInventoryActivity.class);
//                        Intent i = new Intent(context, HomeActivity.class);
                        context.startActivity(i);
                    } else {
                        activity.showAlertExpired();
//                        Toast.makeText(context,"Project Period Expired!",Toast.LENGTH_LONG).show();
                    }
                }
            }else if (projects.get(position).description.equals("Survey Patok Batas")) {
                String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
                String path2 = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
                File file2 = new File(path2);
                File file = new File(path);
                if(!file.exists()||!file2.exists()){
                    Intent i = new Intent(context, DownloadActivity.class);
                    context.startActivity(i);
                    activity.backup();
                }else {
                    if (onPeriod(projects.get(p).getEndDate())){
                        Intent i = new Intent(context, MapHguActivity.class);
                        context.startActivity(i);
                    } else {
                        activity.showAlertExpired();
//                        Toast.makeText(context,"Project Period Expired!",Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
//                String path2 = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
//                File file2 = new File(path2);
                File file = new File(path);
                if(!file.exists()){
                    Intent i = new Intent(context, DownloadActivity.class);
                    context.startActivity(i);
                    activity.backup();
                }else {
                    if (onPeriod(projects.get(p).getEndDate())){
                        Intent i = new Intent(context, MapReplantingActivity.class);
                        context.startActivity(i);
                    } else {
                        activity.showAlertExpired();
//                        Toast.makeText(context,"Project Period Expired!",Toast.LENGTH_LONG).show();
                    }
                }
            }


        });
        if (projects.get(position).description.equals("Asset Water Management"))
        {holder.iv.setImageResource(R.drawable.pas);}
        else if (projects.get(position).description.equals("Survey Ancak & TPH")){
            holder.iv.setImageResource(R.drawable.icon_ancak);
        }else {
            holder.iv.setImageResource(R.drawable.group_50);
        }

    }

    private boolean onPeriod(long until){
        long millisecondsPerDay = 24 * 60 * 60 * 1000;
        long difference = (until-DateTime.timeStampCalendar().getTimeInMillis())/millisecondsPerDay;
        Log.e("long diff",String.valueOf(difference));
        if (difference>=0){
            return true;
        }
        else {
            return false;
        }
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

    @Override
    public int getItemCount() {
        return projects.size();
    }


    private boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
