package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.ui.MapHguActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;


public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.ViewHolder> implements Filterable{

    private List<LayerMap> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    MapReplantingActivity activity;
    MapInventoryActivity activity2;
    MapHguActivity activity3;
    private contextOf mState;
    private List<Bitmap>bitmapList;


    public SelectionAdapter(Context context, List<LayerMap> data, List<Bitmap> bitmaps){
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        bitmapList = bitmaps;
        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;
            mState = contextOf.Replanting;
        }
        else if(context instanceof MapInventoryActivity){
            activity2 = (MapInventoryActivity) context;
            mState = contextOf.Inventory;
        }else {
            activity3 = (MapHguActivity) context;
            mState = contextOf.HGU;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName;
        CheckBox cb;
        SeekBar sb;
        ImageView iv;
        ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            cb = itemView.findViewById(R.id.cb_item);
            sb = itemView.findViewById(R.id.seekBar);
            iv = itemView.findViewById(R.id.icon_legend);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    @Override
    public Filter getFilter() {
        return null;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_selection, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.iv.setImageBitmap(bitmapList.get(position));
            holder.tvName.setText(GlobalHelper.getNameObject(mData.get(position).getLayerName()));
            int p = position;
            final  LayerMap item = mData.get(position);
            if (mData.get(position).isSelected()){
                holder.cb.setChecked(true);
            }
            else {
                holder.cb.setChecked(true);
            }
            holder.cb.setOnCheckedChangeListener((compoundButton, b) -> {
                mData.get(p).setSelected(b);
                if(b){
                    if(mState == contextOf.Replanting){
                        activity.setVisible(p);
                    }
                    else {
                        activity2.setVisible(p);
                    }
                }
                else {
                    if(mState == contextOf.Replanting) {
                        activity.setInVisible(p);
                    }
                    else {
                        activity2.setInVisible(p);
                    }
                }
            });
            holder.sb.setMax(10);
            holder.sb.setProgress(10);
            holder.sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    int a = 10;
                    float result = (float) i/a;
                    Log.e("opacity",String.valueOf(result));
                    if(mState == contextOf.Replanting) {
                        activity.setOpacity(p, result);
                    }
                    else {
                        activity2.setOpacity(p, result);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
    }


    @Override
    public int getItemCount() {
//        Log.e("itemCount",String.valueOf(mData.size()));
        return mData.size();
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    String getItem(int id) {
        return mData.get(id).getLayerName();
    }

    enum contextOf {
        Inventory,
        Replanting,
        HGU
    }
}

