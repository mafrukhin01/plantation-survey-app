package id.co.ams_plantation.plantation_survey_app.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.ui.ResultCensusActivity;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;

/**
 * Created by Mafrukhin on 02/06/2022.
 */
public class SessionListAdapter extends BaseAdapter  {
    private Context context;
    private List<Session> sessions;
    private LayoutInflater layoutInflater;
    private Session sessionModel;


    public SessionListAdapter(Context context, List<Session> sessions) {
        this.context = context;
        this.sessions = sessions;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return sessions.size();
    }

    @Override
    public Object getItem(int position) {
        return  sessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ResultCensusActivity activity = (ResultCensusActivity) context;
        View rowView=convertView;
        if (rowView==null){
            rowView=layoutInflater.inflate(R.layout.linear_session,parent,false);
        }

        CardView content = rowView.findViewById(R.id.content_linear);
        TextView sessionName = rowView.findViewById(R.id.l_session_name);
        TextView sessionDate = rowView.findViewById(R.id.l_date);
        TextView sessionBlock = rowView.findViewById(R.id.l_block);
        TextView sessionEstate = rowView.findViewById(R.id.l_estate);
        ImageView iconView = rowView.findViewById(R.id.icon_session);
//        TextView sessionCensus = rowView.findViewById(R.id.l_census);
//        TextView sessionGanoderma = rowView.findViewById(R.id.l_ganoderma);
//        ImageView imageView = rowView.findViewById(R.id.iv_session_status);

        sessionModel = sessions.get(position);
        long a = sessionModel.getCreateDate().getTime();
        long b = Calendar.getInstance().getTime().getTime();
        long diff = b-a;
        long duration = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        String status = sessionModel.getStatus();
        if (duration >=2 || status.equals("2")){
            iconView.setImageResource(R.drawable.ic_disable);
        }
        sessionName.setText(sessionModel.getSessionName());
        sessionDate.setText(DateTime.toStringTemplate(sessionModel.getCreateDate()));
//        sessionBlock.setText(sessionModel.getBlock());
        sessionEstate.setText(sessionModel.getEstCode());
//        imageView.setTag(position);
        content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setResult(Activity.RESULT_OK);
//                activity.showContinueSessionDialog(sessions.get(position).getSessionId());
//                activity.startSession(sessions.get(position).getSessionId());
                Log.e("SessionName ", sessions.get(position).getSessionName());
//            if(Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
//                activity.startSession(sessionModel.getSessionId());
//            }else{
////                Prefs.setPrefs()
////                        .putString(Prefs.SESSION_ID, sessionModel.getSessionName())
////                        .putBoolean(Prefs.ACTIVE_SESSION,true).apply();
//
//            }
//                notifyDataSetChanged();
            }
        });
        return rowView;
    }

}
