package id.co.ams_plantation.plantation_survey_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.plantation_survey_app.R;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.SummaryHGU;
import id.co.ams_plantation.plantation_survey_app.data.view_model.CensusCellViewHolder;
import id.co.ams_plantation.plantation_survey_app.data.view_model.CensusRowHeaderViewHolder;
import id.co.ams_plantation.plantation_survey_app.data.view_model.ColumnHeaderViewHolder;
import id.co.ams_plantation.plantation_survey_app.data.view_model.SummaryViewModel;
import id.co.ams_plantation.plantation_survey_app.data.view_model.TableViewCell;
import id.co.ams_plantation.plantation_survey_app.data.view_model.TableViewColumnHeader;
import id.co.ams_plantation.plantation_survey_app.data.view_model.TableViewRowHeader;

public class SummaryHguAdapter extends AbstractTableAdapter<TableViewColumnHeader, TableViewRowHeader, TableViewCell> {

    private final SummaryViewModel tableViewModel;
    private final LayoutInflater mInflater;
    public SummaryHguAdapter(Context context, SummaryViewModel tableViewModel) {
        super();
        this.mInflater = LayoutInflater.from(context);
        this.tableViewModel = tableViewModel;
    }

    @Override
    public int getColumnHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getRowHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCellItemViewType(int position) {
        return 0;
    }

    @NonNull
    @Override
    public AbstractViewHolder onCreateCellViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout;
        layout = mInflater.inflate(R.layout.table_view_cell_layout, parent, false);
        return new CensusCellViewHolder(layout);
    }

    @Override
    public void onBindCellViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewCell cellItemModel, int columnPosition, int rowPosition) {
        TableViewCell cell = (TableViewCell) cellItemModel;
        CensusCellViewHolder viewHolder = (CensusCellViewHolder) holder;
        viewHolder.setCell(cell);
    }

    @NonNull
    @Override
    public AbstractViewHolder onCreateColumnHeaderViewHolder(@NonNull ViewGroup parent, int viewType) {
        View corner = mInflater.inflate(R.layout.table_view_column_header_layout, parent, false);
        return new ColumnHeaderViewHolder(corner, getTableView());
    }

    @Override
    public void onBindColumnHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewColumnHeader columnHeaderItemModel, int columnPosition) {
        TableViewColumnHeader columnHeader = (TableViewColumnHeader) columnHeaderItemModel;
        ColumnHeaderViewHolder columnHeaderViewHolder = (ColumnHeaderViewHolder) holder;
        columnHeaderViewHolder.setColumnHeader(columnHeader);

    }

    @NonNull
    @Override
    public AbstractViewHolder onCreateRowHeaderViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = mInflater.inflate(R.layout.table_view_row_header_layout, parent, false);
        return new CensusRowHeaderViewHolder(layout);

    }

    @Override
    public void onBindRowHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewRowHeader rowHeaderItemModel, int rowPosition) {
        TableViewRowHeader rowHeader = (TableViewRowHeader) rowHeaderItemModel;
        CensusRowHeaderViewHolder rowHeaderViewHolder = (CensusRowHeaderViewHolder) holder;
        rowHeaderViewHolder.setCell(rowHeader);
    }

    @NonNull
    @Override
    public View onCreateCornerView(@NonNull ViewGroup parent) {
        View corner = mInflater.inflate(R.layout.table_view_corner_layout, null);
        TextView tvCorner = corner.findViewById(R.id.tvCorner);
        tvCorner.setText("Tipe");
//        corner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SortState sortState = PemanenAdapter.this.getTableView()
//                        .getRowHeaderSortingStatus();
//                if (sortState != SortState.ASCENDING) {
//                    Log.d("TableViewAdapter", "Order Ascending");
//                    PemanenAdapter.this.getTableView().sortRowHeader(SortState.ASCENDING);
//                } else {
//                    Log.d("TableViewAdapter", "Order Descending");
//                    PemanenAdapter.this.getTableView().sortRowHeader(SortState.DESCENDING);
//                }
//            }
//        });
        return corner;
    }
}
