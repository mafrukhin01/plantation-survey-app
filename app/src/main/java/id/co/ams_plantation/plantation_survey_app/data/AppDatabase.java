package id.co.ams_plantation.plantation_survey_app.data;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.RenameTable;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import id.co.ams_plantation.plantation_survey_app.data.dao.EmployeesDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.EstateMappingDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.HistoryAssetDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.ImageUploadDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.MasterBlockDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.SessionDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.SessionSurveysDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.TPHDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.UserActivityDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.UserProjectDao;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.PostEndPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SessionSurveys;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserAppUsage;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PointEntity;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.SequenceAncak;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.utils.Converters;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

/**
 * Created by Mafrukhin on 07/03/2023.
 */
@Database(entities = {
        UserAppUsage.class,
        Session.class,
        ImagesUpload.class,
        SessionSurveys.class,
        UserProject.class,
        EstateMapping.class,
        PostEndPR.class,
        Employee.class,
        TPH.class,
        MasterBlock.class,
        SequenceAncak.class,
        PointEntity.class,
        ExtendEstate.class
       },
//        views = {PostCensusObservation.class},
//        autoMigrations = {@AutoMigration(from = 1,to = 2
//        ,spec = AppDatabase.MyAutoMigration.class)},
        version = 10,
        exportSchema = true
)

@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
//       @RenameTable(fromTableName = "User", toTableName = "AppUser")
//       static class MyAutoMigration implements AutoMigrationSpec { }
       public abstract SessionDao sessionDao();
       public abstract ImageUploadDao imageUploadDao();
       public abstract SessionSurveysDao sessionSurveysDao();
       public abstract UserProjectDao userProjectDao();
       public abstract EstateMappingDao estateMappingDao();
       public abstract UserActivityDao userActivityDao();
       public abstract EmployeesDao employeesDao();
       public abstract TPHDao tphDao();
       public abstract MasterBlockDao masterBlockDao();
       private static volatile AppDatabase INSTANCE;
       private static final int NUMBER_OF_THREADS = 6;
       public static final ExecutorService databaseWriteExecutor =
               Executors.newFixedThreadPool(NUMBER_OF_THREADS);

       public static AppDatabase getDatabase(final Context context){
              if (INSTANCE == null) {
                     synchronized (AppDatabase.class) {
                            if (INSTANCE == null) {
                                   INSTANCE = createDatabase(context);
                            }
                     }
              }
              return INSTANCE;
       }

       private static AppDatabase createDatabase(Context context) {
              File customFolder = new File(Environment.getExternalStorageDirectory() + GlobalHelper.SQLITE_LOCATION);
              if (!customFolder.exists()) {
                     customFolder.mkdirs();
              }

              File databaseFile = new File(customFolder, "psa.db");
              INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                              AppDatabase.class, databaseFile.getAbsolutePath())
                      .allowMainThreadQueries()
                      .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5,MIGRATION_5_6,MIGRATION_6_7,MIGRATION_7_8, MIGRATION_8_9,MIGRATION_9_10)
                      .build();

              return INSTANCE;
       }


       static final Migration MIGRATION_1_2 = new Migration(1, 2) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_EstateMapping` (`estCode` TEXT NOT NULL,`plantation` TEXT,`grouping` TEXT,`groupCompanyName` TEXT,`companyCode` TEXT,`estNewCode` TEXT,`estCodeSAP` TEXT,`newEstName` TEXT, PRIMARY KEY(`estCode`) )");
              }
       };

       static final Migration MIGRATION_2_3 = new Migration(2, 3) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL("DROP TABLE IF EXISTS `T_History_BRD1`");
                     database.execSQL("DROP TABLE IF EXISTS `PSA_EndPR`");
                     database.execSQL("ALTER TABLE PSA_Session ADD COLUMN appVersion TEXT");
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_PostPR` " +
                             "(`globalID` TEXT PRIMARY KEY NOT NULL, " +
                             "`objType` TEXT, " +
                             "`datePR` TEXT, " +
                             "`isUpload` INTEGER NOT NULL DEFAULT 0)");
//
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_UAL` (" +
                             "'id' INTEGER PRIMARY KEY NOT NULL, " +
                             "'userId' TEXT, " +
                             "'moduleCode' TEXT, " +
                             "'appId' TEXT, " +
                             "'activityType' TEXT, " +
                             "'platform' TEXT, " +
                             "'pageId' TEXT, " +
                             "'elementId' TEXT, " +
                             "'connectionInfo' TEXT, " +
                             "'ipAddress' TEXT, " +
                             "'startTime' TEXT, " +
                             "'endTime' TEXT, " +
                             "'remarks' TEXT, " +
                             "'androidId' TEXT, " +
                             "'idAndroid' TEXT, " +
                             "'versionApp' TEXT, " +
                             "'createDate' TEXT" +
                             ")");

              }
       };

       static final Migration MIGRATION_3_4 = new Migration(3, 4) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL("CREATE TABLE IF NOT EXISTS PSA_Employees (" +
                             "empCode TEXT PRIMARY KEY NOT NULL," +
                             "estCode TEXT," +
                             "zProfile TEXT," +
                             "NIK TEXT," +
                             "Nama TEXT," +
                             "NoKTP TEXT," +
                             "TanggalLahir TEXT," +  // Store date as long value
                             "Email TEXT," +
                             "NoHP TEXT," +
                             "Alamat TEXT," +
                             "Status TEXT," +
                             "Gang TEXT," +
                             "Divisi TEXT," +
                             "Grade TEXT," +
                             "Lvl TEXT," +
                             "Posisi TEXT," +
                             "Jabatan TEXT," +
                             "Area TEXT," +
                             "Company TEXT," +
                             "Gender TEXT," +
                             "SourceData TEXT," +
                             "LastUpdate TEXT," +  // Store date as long value
                             "Department TEXT," +
                             "lastUpdateBy TEXT," +
                             "lastUpdateStaging TEXT," +  // Store date as long value
                             "lastTimeStaging TEXT," +  // Store date as long value
                             "flag TEXT," +
                             "isActive TEXT," +
                             "VALID_FROM TEXT," +
                             "VALID_TO TEXT," +
                             "WS_GROUP TEXT," +
                             "ENTRY_DATE TEXT," +
                             "RESIGN_DATE TEXT" +
                             ")");
              }
       };
       static final Migration MIGRATION_4_5 = new Migration(4, 5) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL("  CREATE TABLE IF NOT EXISTS `PSA_TPH` (" +
                             "`noTph` TEXT PRIMARY KEY NOT NULL," +
                             "`Latitude` REAL NOT NULL," +
                             "`Longitude` REAL NOT NULL," +
                             "`afdeling` TEXT," +
                             "`ancak` TEXT," +
                             "`block` TEXT,\n" +
                             "`createBy` INTEGER NOT NULL," +
                             "`createDate` INTEGER NOT NULL," +
                             "`estCode` TEXT," +
                             "`namaTph` TEXT," +
                             "`status` INTEGER NOT NULL," +
                             "`updateBy` INTEGER NOT NULL," +
                             "`updateDate` INTEGER NOT NULL," +
                             "`resurvey` INTEGER NOT NULL," +
                             "`jumlahPokok` INTEGER NOT NULL," +
                             "`panen3Bulan` INTEGER NOT NULL," +
                             "`ancakFlag` INTEGER NOT NULL DEFAULT 2," +
                             "`foto` TEXT)");
              }
       };
       static final Migration MIGRATION_5_6 = new Migration(5, 6) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_MasterBlock` (" +
                             "`block` TEXT PRIMARY KEY NOT NULL, " +
                             "`afd` TEXT, " +
                             "`haGross` TEXT, " +
                             "`tAncak` INTEGER NOT NULL, " +
                             "`tTPH` INTEGER NOT NULL, " +
                             "`tAncakReject` INTEGER NOT NULL, " +
                             "`tAncakApprove` INTEGER NOT NULL, " +
                             "`lastCheckout` INTEGER NOT NULL, " +
                             "`remarks` TEXT, " +
                             "`status` INTEGER NOT NULL, " +
                             "`selected` INTEGER NOT NULL DEFAULT 0)");

                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_AncakSequence` (" +
                             "`block` TEXT PRIMARY KEY NOT NULL, " +
                             "`afd` TEXT, " +
                             "`haGross` TEXT, " +
                             "`tAncak` INTEGER NOT NULL, " +
                             "`tTPH` INTEGER NOT NULL, " +
                             "`tAncakReject` INTEGER NOT NULL, " +
                             "`tAncakApprove` INTEGER NOT NULL, " +
                             "`lastCheckout` INTEGER NOT NULL, " +
                             "`remarks` TEXT, " +
                             "`status` INTEGER NOT NULL, " +
                             "`selected` INTEGER NOT NULL DEFAULT 0)");

              }
       };

       static final Migration MIGRATION_6_7 = new Migration(6, 7) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
//                     database.execSQL("DROP TABLE IF EXISTS `PSA_AncakSequence`");
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_PointBackup`(" +
                             "    id INTEGER PRIMARY KEY NOT NULL," +
                             "    latitude REAL NOT NULL," +
                             "    longitude REAL NOT NULL" +
                             ")"
                     );
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_AncakSeq`(" +
                             "    `idSequence` TEXT PRIMARY KEY NOT NULL)"
                     );
                     database.execSQL("ALTER TABLE PSA_MasterBlock ADD COLUMN mandor TEXT");
                     database.execSQL("ALTER TABLE PSA_MasterBlock ADD COLUMN isMultiple INTEGER NOT NULL");

              }
       };
       static final Migration MIGRATION_7_8 = new Migration(7, 8) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
//                     database.execSQL("DROP TABLE IF EXISTS `PSA_AncakSequence`");
                     database.execSQL("CREATE TABLE IF NOT EXISTS `PSA_ExtendEstate` (" +
                             "`MinX` TEXT PRIMARY KEY NOT NULL, " +
                             "`MinY` TEXT, " +
                             "`MaxX` TEXT, " +
                             "`MaxY` TEXT )");

              }
       };
       static final Migration MIGRATION_8_9 = new Migration(8, 9) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
//                     database.execSQL("DROP TABLE IF EXISTS `PSA_AncakSequence`");
                     database.execSQL("ALTER TABLE PSA_TPH ADD COLUMN idx INTEGER NOT NULL DEFAULT 0");
                     database.execSQL("ALTER TABLE PSA_TPH ADD COLUMN userId TEXT");
                     database.execSQL("ALTER TABLE PSA_TPH ADD COLUMN versionApp TEXT");

              }
       };
       static final Migration MIGRATION_9_10 = new Migration(9, 10) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
//                     database.execSQL("DROP TABLE IF EXISTS `PSA_AncakSequence`");
                     database.execSQL("ALTER TABLE PSA_MasterBlock ADD COLUMN isOpen INTEGER NOT NULL DEFAULT 0");


              }
       };





       static final Migration SampleAlterColumn = new Migration(2,3) {
              @Override
              public void migrate(SupportSQLiteDatabase database) {
                     database.execSQL(
                             "CREATE TABLE IF NOT EXISTS VehicleDetailsEntityTmp (vehicleId TEXT NOT NULL, updatedOn TEXT, updatedBy TEXT,vehicleClientId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL )");
                     // Copy the data
                     database.execSQL(
                             "INSERT INTO VehicleDetailsEntityTmp (vehicleId, updatedOn, updatedBy ,vehicleClientId) SELECT vehicleId, updatedOn, updatedBy ,vehicleClientId FROM VehicleDetailsEntity ");
                     // Remove the old table
                     database.execSQL("DROP TABLE VehicleDetailsEntity");
                     // Change the table name to the correct one
                     database.execSQL("ALTER TABLE VehicleDetailsEntityTmp RENAME TO VehicleDetailsEntity");
              }
       };


}
