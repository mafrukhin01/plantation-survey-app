package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EmployeeNIK;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;

@Dao
public interface EmployeesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Employee param);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Employee> param);
    @Update
    void updateList(List<Employee> objects);
    @Query("SELECT * from PSA_Employees ")
    List<Employee> getAllEstateMapping();
    @Query("select Nama as name, NIK from PSA_Employees")
    List<EmployeeNIK>getEmployeeNIK();

    @Query("SELECT name || '-' || NIK AS nameNik FROM (SELECT Nama AS name, NIK FROM PSA_Employees)")
    List<String> getEmployeeNameNikList();


}
