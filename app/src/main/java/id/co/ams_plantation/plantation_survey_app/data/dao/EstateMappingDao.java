package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

/**
 * Created by Mafrukhin on 11/07/2023.
 */
@Dao
public interface EstateMappingDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(EstateMapping param);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<EstateMapping> param);
    @Update
    void updateList(List<EstateMapping> objects);
    @Query("SELECT * from PSA_EstateMapping ")
    List<EstateMapping> getAllEstateMapping();
    @Query("SELECT * from PSA_EstateMapping where estCode = :estCode")
    List<EstateMapping> getEstateMappingByEstCode(String estCode);
    @Query("SELECT * from PSA_EstateMapping where estCode = :estCode limit 1")
    EstateMapping getMappingByEstCode(String estCode);

    @Query("SELECT groupCompanyName from PSA_EstateMapping where estCode = :param")
    String getGroupCompanyName(String param);

    @Transaction
    default EstateMapping getEstateMapping(){
        String est = GlobalHelper.getEstate().getEstCode();
        return getMappingByEstCode(est);
    }
}
