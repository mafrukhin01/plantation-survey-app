package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Images;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;

/**
 * Created by Mafrukhin on 03/06/2023.
 */
@Dao
public interface ImageAssetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Images param);
    @Update
    void updateList(List<Images> objects);
    @Query("SELECT * from PSA_Images where status = '1' ")
    List<Images> getAllImageUpload();
//    @Query("SELECT * from PSA_Images where globalId = :globalId")
//    List<Images> getAllImageByGlobalId(String globalId);
}
