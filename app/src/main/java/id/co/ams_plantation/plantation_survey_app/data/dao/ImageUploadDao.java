package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;

/**
 * Created by Mafrukhin on 05/06/2023.
 */
@Dao
public interface ImageUploadDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ImagesUpload param);
    @Update
    void updateList(List<ImagesUpload> objects);
    @Query("SELECT * from PSA_ImagesUpload where isUpload = 1 and sessionSurvey not null and sessionSurvey!=''")
    List<ImagesUpload> getAllImageUpload();
    @Query("SELECT * from PSA_ImagesUpload where globalId = :globalId")
    List<ImagesUpload> getAllImageByGlobalId(String globalId);
}
