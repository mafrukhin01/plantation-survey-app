package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.esri.arcgisruntime.geometry.Point;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PointEntity;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.SequenceAncak;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;

@Dao
public interface MasterBlockDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MasterBlock> param);
    @Query("Delete from PSA_ExtendEstate")
    void clearExtend();
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertExtend(List<ExtendEstate> param);

    @Query("SELECT * from PSA_MasterBlock order by selected desc, block ")
    List<MasterBlock> getAllMasterBlock();
    @Query("Delete from PSA_MasterBlock")
    void clearMasterBlock();
    @Query("SELECT * from PSA_MasterBlock where selected = 1 order by block asc limit 1")
    MasterBlock getSelectedBlock();
    @Query("Update PSA_MasterBlock set selected =0")
    void setFalseBlock();
    @Query("Update PSA_MasterBlock set isOpen =1 where block in (:blocks)")
    void setBlockOpen(List<String> blocks);
    @Query("Update PSA_MasterBlock set selected =1 where block = :block")
    void setSelectedBlock(String block);
    @Query("SELECT block from PSA_MasterBlock where selected = 1 order by block asc")
    List<String> getSelectedBlocks();
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllPoint(List<PointEntity> param);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPoint(PointEntity param);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSequenceAncak(SequenceAncak param);
    @Query("SELECT * from PSA_PointBackup ")
    List<PointEntity> getPointBackups();
    @Query("SELECT COUNT(*) FROM PSA_PointBackup")
    int getCount();
    @Query("SELECT isOpen FROM PSA_MasterBlock where block=:block ")
    boolean isOpen(String block);
    @Query("Delete from PSA_PointBackup")
    void clearPointsBackup();
    @Query("Delete from PSA_PointBackup where id=:param ")
    void undoPointsBackup(long param);
    @Query("SELECT * FROM PSA_PointBackup order by id desc limit 1")
    PointEntity getLast();
    @Query("SELECT * FROM PSA_ExtendEstate  limit 1")
    ExtendEstate getExtend();
    @Transaction
    default void insertPointBackup(Point point) {
        // Buat ID baru dengan format SENddMMyyXXXX
        long id = System.currentTimeMillis();
        // Buat dan simpan PointEntity baru
        PointEntity newPoint = new PointEntity(id,point.getY(),point.getX());
        insertPoint(newPoint);
    }

    @Transaction
    default void insertSequenceAncak() {
        long id = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        SequenceAncak sequenceAncak = new SequenceAncak("");
        insertSequenceAncak(sequenceAncak);
    }
    @Transaction
    default void undoPoint() {
        PointEntity lastPoint = getLast();
        long id = lastPoint.getId();
        undoPointsBackup(id);

    }
    @Transaction
    default void inputExtent(List<ExtendEstate> param) {
        clearExtend();
        insertExtend(param);
    }



}
