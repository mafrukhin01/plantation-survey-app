package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.PostEndPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.ToUploadPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;

/**
 * Created by Mafrukhin on 05/06/2023.
 */
@Dao
public interface SessionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Session session);
    @Query("SELECT * from PSA_Session")
    List<Session> getAllSession();
    @Query("UPDATE PSA_Session set finishDate = :finishDate where sessionId = :sessionId ")
    void updateEndSession(String sessionId, long finishDate);
    @Query("SELECT * from PSA_Session where status ='1' order by sessionName asc")
    List<Session> getAllSessionToUpload();
    @Query("SELECT count(sessionId) from PSA_Session ")
    LiveData<Integer> getSequence();
    @Query("SELECT * from PSA_Session where sessionId = :sessionId limit 1")
    Session getSessionBySessionId(String sessionId);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPr(PostEndPR pr);
    @Query("SELECT globalID, objType, datePR from PSA_PostPR")
    List<ToUploadPR> getAllPr();
    @Query("SELECT sessionName from PSA_Session order by createDate DESC limit 1 ")
    String getLastSessionName();
}

