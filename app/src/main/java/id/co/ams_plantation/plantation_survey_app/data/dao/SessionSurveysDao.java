package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SessionSurveys;

/**
 * Created by Mafrukhin on 06/06/2023.
 */
@Dao
public interface SessionSurveysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(SessionSurveys session);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertList(List<SessionSurveys> session);
    @Query("SELECT * from PSA_SessionSurveys")
    List<SessionSurveys> getAllSessionSessionSurvey();


}
