package id.co.ams_plantation.plantation_survey_app.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;

@Dao
public interface TPHDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TPH param);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<TPH> param);

    @Query("SELECT * from PSA_TPH where status = 1 and namaTph <> '999'")
    List<TPH> getAllTPH();
    @Query("SELECT * FROM PSA_TPH where status = 1 and namaTph <> '999'")
    LiveData<List<TPH>> watchAllTPH();
    @Query("Update PSA_TPH set status = 1 and ancakFlag = 3 where ancakFlag =9")
    void updateDoneUpload();
    @Query("Update PSA_TPH set foto =:param where noTph =:id")
    void updateFoto(List<String> param, String id);

    @Query("SELECT PSA_TPH.*, PSA_MasterBlock.* FROM PSA_TPH\n" +
            "LEFT JOIN PSA_MasterBlock ON UPPER(PSA_TPH.block) = UPPER(PSA_MasterBlock.block)\n" +
            "WHERE PSA_TPH.status = 1 \n" +
            "AND PSA_TPH.namaTph <> '999'\n" +
            "AND ( \n" +
            "CASE WHEN EXISTS (SELECT 1 FROM PSA_MasterBlock WHERE selected = 1) \n" +
            "THEN PSA_TPH.block IN (SELECT block FROM PSA_MasterBlock WHERE selected = 1)\n" +
            "ELSE PSA_TPH.block IN ('')\n" +
            "END )\n")
    LiveData<List<TPH>> watchAllTPH2old();
    @Query("SELECT PSA_TPH.*, PSA_MasterBlock.* FROM PSA_TPH\n" +
            "LEFT JOIN PSA_MasterBlock ON UPPER(PSA_TPH.block) = UPPER(PSA_MasterBlock.block)\n" +
            "WHERE PSA_TPH.status = 1 \n" +
            "AND PSA_TPH.namaTph <> '999'\n" +
            "AND PSA_MasterBlock.selected = 1\n" +
            "AND UPPER(PSA_TPH.block) IN (SELECT UPPER(block) FROM PSA_MasterBlock WHERE selected = 1)\n")
    LiveData<List<TPH>> watchAllTPH2();


    @Query("SELECT * from PSA_TPH where status = 1 and namaTph <> '999' and block =:block")
    List<TPH> getAllTPHInBlock(String block);
    @Query("SELECT * from PSA_TPH where status = 1 and namaTph <> '999' and ancakFlag ='9' ")
    List<TPH> getAllTPHToUpload();
    @Query("SELECT namaTph from PSA_TPH where status = 1 and namaTph <> '999' and block =:block")
    List<String> getAllNameTPH(String block);
    @Query("SELECT noTph from PSA_TPH where status = 1 and namaTph <> '999' and block =:block and namaTph =:tphName")
    String getIdTPH(String block,String tphName);
    @Query("Update PSA_TPH set ancakFlag = 1, ancak = :ancak where noTph =:code")
    void updateFlagJoin(String code, String ancak);

    @Query("SELECT * FROM PSA_TPH WHERE createDate BETWEEN :startOfDay AND :endOfDay ORDER BY noTph DESC LIMIT 1")
    TPH getLatestIDTph(long startOfDay, long endOfDay);
    @Transaction
    default String createNewTph(String userId,  TPH newTPH) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long startOfDay = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        long endOfDay = calendar.getTimeInMillis();

        // Format currentDate to 'yyMMdd'
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyy", Locale.getDefault());
        String currentDate = dateFormat.format(new Date());

        // Get the latest entry for today
        TPH latestEntry = getLatestIDTph(startOfDay, endOfDay);

        String newRunningNumber;
        if (latestEntry == null) {
            // If no entry exists for the current date, start with T0001
            newRunningNumber = "T0001";
        } else {
            // Extract the number from the latest idTph and increment
            String latestIdTph = latestEntry.getNoTph().substring(1, 5); // Assuming T0007 -> 0007
            int nextNumber = Integer.parseInt(latestIdTph) + 1;
            newRunningNumber = String.format("T%04d", nextNumber); // Format to T000X
        }

        // Create the new IDTph
        String newIDTph = newRunningNumber + currentDate + userId;

        // Insert the new record
//        TPH newRecord = new TPH(newIDTph,0,0,"","",);
//        newRecord; = newIDTph;
//        newRecord.date = currentDate;
//        newRecord.userId = userId;
        newTPH.setNoTph(newIDTph);
        insert(newTPH);

        return newIDTph;
    }


}
