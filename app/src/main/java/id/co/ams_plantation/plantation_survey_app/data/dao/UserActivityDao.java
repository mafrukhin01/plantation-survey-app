package id.co.ams_plantation.plantation_survey_app.data.dao;

import android.util.Log;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.aspsine.multithreaddownload.util.L;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.GetUAL;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserAppUsage;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

@Dao
public interface UserActivityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserAppUsage ual);
    @Query("SELECT * from PSA_UAL")
    List<UserAppUsage> getAllUAL();

    @Query("select count(id) from PSA_UAL")
    Integer getCount();

    @Transaction
    default void insertUAL(UserAppUsage userAppUsage){
        int count = getCount();
        Log.e("CountID",String.valueOf(count));
        userAppUsage.setId(count+1);
        insert(userAppUsage);
    }
}
