package id.co.ams_plantation.plantation_survey_app.data.dao;

import android.util.Log;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;

/**
 * Created by Mafrukhin on 09/06/2023.
 */
@Dao
public interface UserProjectDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(UserProject param);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserProject> param);

    @Query("SELECT * from PSA_UserProject ")
    List<UserProject> getAllUserProject();

    @Query("SELECT * from PSA_UserProject where estCode = :estCode")
    List<UserProject> getUserProjectByEstate(String estCode);
    @Query("DELETE from PSA_UserProject")
    void truncate();

    @Transaction
    default void truncateAndInsertProject(List<UserProject> projects){
        Log.e("project lengts", String.valueOf(projects.size()));
        truncate();
        insertAll(projects);
    }


}
