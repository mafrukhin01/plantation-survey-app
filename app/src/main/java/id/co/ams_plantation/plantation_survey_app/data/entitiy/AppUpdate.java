package id.co.ams_plantation.plantation_survey_app.data.entitiy;

/**
 * Created by Mafrukhin on 24/07/2023.
 */
public class AppUpdate {
    public String versionCode;
    public String versionName;
    public String description;
    public short isMandatory;

    public AppUpdate(String versionCode, String versionName, String description, short isMandatory) {
        this.versionCode = versionCode;
        this.versionName = versionName;
        this.description = description;
        this.isMandatory = isMandatory;
    }
    public String getVersionCode() {
        return versionCode;
    }
    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }
    public String getVersionName() {
        return versionName;
    }
    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public short getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(short isMandatory) {
        this.isMandatory = isMandatory;
    }
}
