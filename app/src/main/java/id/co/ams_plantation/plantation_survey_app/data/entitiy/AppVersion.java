package id.co.ams_plantation.plantation_survey_app.data.entitiy;

/**
 * Created by Mafrukhin on 13/07/2023.
 */
public class AppVersion {

        public String versionCode;
        public String version;

        public AppVersion(String versionCode, String version) {
                this.versionCode = versionCode;
                this.version = version;
        }

        public String getVersionCode() {
                return versionCode;
        }

        public void setVersionCode(String versionCode) {
                this.versionCode = versionCode;
        }

        public String getVersion() {
                return version;
        }

        public void setVersion(String version) {
                this.version = version;
        }
}
