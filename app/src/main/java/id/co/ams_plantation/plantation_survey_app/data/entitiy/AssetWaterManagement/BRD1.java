package id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by Mafrukhin on 03/07/2023.
 */
@Entity(tableName = "T_BRD1")
public class BRD1 {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "survey_ID")
    private String survey_ID;
    @ColumnInfo(name = "Shape")
    private String Shape;
    @ColumnInfo(name = "BRD_ID")
    private String BRD_ID;
    @ColumnInfo(name = "ESTNR")
    private String ESTNR;
    @ColumnInfo(name = "block")
    private String block;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "condition")
    private String condition;
    @ColumnInfo(name = "length")
    private String length;
    @ColumnInfo(name = "width")
    private String width;

}
