package id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement;

/**
 * Created by Mafrukhin on 14/07/2023.
 */
public class CountObject {
    public int totalBRD1;
    public int totalCVT1;
    public int totalPUM1;
    public int totalWGT1;
    public int totalDAM1;
    public int totalBUN1;
    public int totalZIP1;

    public CountObject(int totalBRD1, int totalCVT1, int totalPUM1, int totalWGT1, int totalDAM1, int totalBUN1, int totalZIP1) {
        this.totalBRD1 = totalBRD1;
        this.totalCVT1 = totalCVT1;
        this.totalPUM1 = totalPUM1;
        this.totalWGT1 = totalWGT1;
        this.totalDAM1 = totalDAM1;
        this.totalBUN1 = totalBUN1;
        this.totalZIP1 = totalZIP1;
    }

    public int getTotalBRD1() {
        return totalBRD1;
    }

    public void setTotalBRD1(int totalBRD1) {
        this.totalBRD1 = totalBRD1;
    }

    public int getTotalCVT1() {
        return totalCVT1;
    }

    public void setTotalCVT1(int totalCVT1) {
        this.totalCVT1 = totalCVT1;
    }

    public int getTotalPUM1() {
        return totalPUM1;
    }

    public void setTotalPUM1(int totalPUM1) {
        this.totalPUM1 = totalPUM1;
    }

    public int getTotalWGT1() {
        return totalWGT1;
    }

    public void setTotalWGT1(int totalWGT1) {
        this.totalWGT1 = totalWGT1;
    }

    public int getTotalDAM1() {
        return totalDAM1;
    }

    public void setTotalDAM1(int totalDAM1) {
        this.totalDAM1 = totalDAM1;
    }

    public int getTotalBUN1() {
        return totalBUN1;
    }

    public void setTotalBUN1(int totalBUN1) {
        this.totalBUN1 = totalBUN1;
    }

    public int getTotalZIP1() {
        return totalZIP1;
    }

    public void setTotalZIP1(int totalZIP1) {
        this.totalZIP1 = totalZIP1;
    }


}
