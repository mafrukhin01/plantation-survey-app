package id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PSA_PostPR")
public class PostEndPR {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "globalID")
    public String globalID;
    @ColumnInfo(name = "objType")
    public String objType;
    @ColumnInfo(name = "datePR")
    public String datePR;
    @ColumnInfo(name = "isUpload")
    public int isUpload = 0;

    public PostEndPR(@NonNull String globalID, String objType, String datePR) {
        this.globalID = globalID;
        this.objType = objType;
        this.datePR = datePR;
    }

    @NonNull
    public String getGlobalID() {
        return globalID;
    }

    public void setGlobalID(@NonNull String globalID) {
        this.globalID = globalID;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getDatePR() {
        return datePR;
    }

    public void setDatePR(String datePR) {
        this.datePR = datePR;
    }

    public int getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(int isUpload) {
        this.isUpload = isUpload;
    }
}
