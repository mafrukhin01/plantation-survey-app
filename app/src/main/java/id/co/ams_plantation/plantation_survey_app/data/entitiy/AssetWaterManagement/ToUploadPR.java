package id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement;

public class ToUploadPR {
    public String globalID;
    public String objType;
    public String datePR;

    public ToUploadPR(String globalID, String objType, String datePR) {
        this.globalID = globalID;
        this.objType = objType;
        this.datePR = datePR;
    }

    public String getGlobalID() {
        return globalID;
    }

    public void setGlobalID(String globalID) {
        this.globalID = globalID;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getDatePR() {
        return datePR;
    }

    public void setDatePR(String datePR) {
        this.datePR = datePR;
    }
}
