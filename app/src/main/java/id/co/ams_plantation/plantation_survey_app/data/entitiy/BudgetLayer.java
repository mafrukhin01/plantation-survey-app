package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class BudgetLayer {
    public int index;
    public String name;
    public boolean isChecked ;

    public BudgetLayer(int index, String name, boolean isChecked) {
        this.index = index;
        this.name = name;
        this.isChecked = isChecked;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
