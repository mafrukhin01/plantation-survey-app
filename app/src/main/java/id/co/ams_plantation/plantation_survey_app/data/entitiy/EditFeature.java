package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class EditFeature {
    String key;
    String value;

    public EditFeature(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
