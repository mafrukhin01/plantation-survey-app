package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "PSA_Employees")
public class Employee {
    @NonNull
    @PrimaryKey()
    @ColumnInfo(name = "empCode")
    private String empCode;
    @ColumnInfo(name = "estCode")
    private String estCode;
    @ColumnInfo(name = "zProfile")
    private String zProfile;
    @ColumnInfo(name = "NIK")
    private String NIK;
    @ColumnInfo(name = "Nama")
    private String Nama;
    @ColumnInfo(name = "NoKTP")
    private String NoKTP;
    @ColumnInfo(name = "TanggalLahir")
    private String TanggalLahir;
    @ColumnInfo(name = "Email")
    private String Email;
    @ColumnInfo(name = "NoHP")
    private String NoHP;
    @ColumnInfo(name = "Alamat")
    private String Alamat;
    @ColumnInfo(name = "Status")
    private String Status;
    @ColumnInfo(name = "Gang")
    private String Gang;
    @ColumnInfo(name = "Divisi")
    private String Divisi;
    @ColumnInfo(name = "Grade")
    private String Grade;
    @ColumnInfo(name = "Lvl")
    private String Lvl;
    @ColumnInfo(name = "Posisi")
    private String Posisi;
    @ColumnInfo(name = "Jabatan")
    private String Jabatan;
    @ColumnInfo(name = "Area")
    private String Area;
    @ColumnInfo(name = "Company")
    private String Company;
    @ColumnInfo(name = "Gender")
    private String Gender;
    @ColumnInfo(name = "SourceData")
    private String SourceData;
    @ColumnInfo(name = "LastUpdate")
    private String LastUpdate;
    @ColumnInfo(name = "Department")
    private String Department;
    @ColumnInfo(name = "lastUpdateBy")
    private String lastUpdateBy;
    @ColumnInfo(name = "lastUpdateStaging")
    private String lastUpdateStaging;
    @ColumnInfo(name = "lastTimeStaging")
    private String lastTimeStaging;
    @ColumnInfo(name = "flag")
    private String flag;
    @ColumnInfo(name = "isActive")
    private String isActive;
    @ColumnInfo(name = "VALID_FROM")
    private String VALID_FROM;
    @ColumnInfo(name = "VALID_TO")
    private String VALID_TO;
    @ColumnInfo(name = "WS_GROUP")
    private String WS_GROUP;
    @ColumnInfo(name = "ENTRY_DATE")
    private String ENTRY_DATE;
    @ColumnInfo(name = "RESIGN_DATE")
    private String RESIGN_DATE;

    public Employee() {
    }

    @Ignore
    public Employee(String empCode, String estCode, String zProfile, String NIK, String nama, String noKTP, String tanggalLahir, String email, String noHP, String alamat, String status, String gang, String divisi, String grade, String lvl, String posisi, String jabatan, String area, String company, String gender, String sourceData, String lastUpdate, String department, String lastUpdateBy, String lastUpdateStaging, String lastTimeStaging, String flag, String isActive, String VALID_FROM, String VALID_TO, String WS_GROUP, String ENTRY_DATE, String RESIGN_DATE) {
        this.empCode = empCode;
        this.estCode = estCode;
        this.zProfile = zProfile;
        this.NIK = NIK;
        this.Nama = nama;
        this.NoKTP = noKTP;
        this.TanggalLahir = tanggalLahir;
        this.Email = email;
        this.NoHP = noHP;
        this.Alamat = alamat;
        this.Status = status;
        this.Gang = gang;
        this.Divisi = divisi;
        this.Grade = grade;
        this.Lvl = lvl;
        this.Posisi = posisi;
        this.Jabatan = jabatan;
        this.Area = area;
        this.Company = company;
        this.Gender = gender;
        this.SourceData = sourceData;
        this.LastUpdate = lastUpdate;
        this.Department = department;
        this.lastUpdateBy = lastUpdateBy;
        this.lastUpdateStaging = lastUpdateStaging;
        this.lastTimeStaging = lastTimeStaging;
        this.flag = flag;
        this.isActive = isActive;
        this.VALID_FROM = VALID_FROM;
        this.VALID_TO = VALID_TO;
        this.WS_GROUP = WS_GROUP;
        this.ENTRY_DATE = ENTRY_DATE;
        this.RESIGN_DATE = RESIGN_DATE;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getZProfile() {
        return zProfile;
    }

    public void setZProfile(String zProfile) {
        this.zProfile = zProfile;
    }

    public String getNIK() {
        return NIK;
    }

    public void setNIK(String NIK) {
        this.NIK = NIK;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getNoKTP() {
        return NoKTP;
    }

    public void setNoKTP(String noKTP) {
        NoKTP = noKTP;
    }

    public String getTanggalLahir() {
        return TanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        TanggalLahir = tanggalLahir;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNoHP() {
        return NoHP;
    }

    public void setNoHP(String noHP) {
        NoHP = noHP;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getGang() {
        return Gang;
    }

    public void setGang(String gang) {
        Gang = gang;
    }

    public String getDivisi() {
        return Divisi;
    }

    public void setDivisi(String divisi) {
        Divisi = divisi;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getLvl() {
        return Lvl;
    }

    public void setLvl(String lvl) {
        Lvl = lvl;
    }

    public String getPosisi() {
        return Posisi;
    }

    public void setPosisi(String posisi) {
        Posisi = posisi;
    }

    public String getJabatan() {
        return Jabatan;
    }

    public void setJabatan(String jabatan) {
        Jabatan = jabatan;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getSourceData() {
        return SourceData;
    }

    public void setSourceData(String sourceData) {
        SourceData = sourceData;
    }

    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public String getLastUpdateStaging() {
        return lastUpdateStaging;
    }

    public void setLastUpdateStaging(String lastUpdateStaging) {
        this.lastUpdateStaging = lastUpdateStaging;
    }

    public String getLastTimeStaging() {
        return lastTimeStaging;
    }

    public void setLastTimeStaging(String lastTimeStaging) {
        this.lastTimeStaging = lastTimeStaging;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getVALID_FROM() {
        return VALID_FROM;
    }

    public void setVALID_FROM(String VALID_FROM) {
        this.VALID_FROM = VALID_FROM;
    }

    public String getVALID_TO() {
        return VALID_TO;
    }

    public void setVALID_TO(String VALID_TO) {
        this.VALID_TO = VALID_TO;
    }

    public String getWS_GROUP() {
        return WS_GROUP;
    }

    public void setWS_GROUP(String WS_GROUP) {
        this.WS_GROUP = WS_GROUP;
    }

    public String getENTRY_DATE() {
        return ENTRY_DATE;
    }

    public void setENTRY_DATE(String ENTRY_DATE) {
        this.ENTRY_DATE = ENTRY_DATE;
    }

    public String getRESIGN_DATE() {
        return RESIGN_DATE;
    }

    public void setRESIGN_DATE(String RESIGN_DATE) {
        this.RESIGN_DATE = RESIGN_DATE;
    }
}
