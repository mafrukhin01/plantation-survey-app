package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class EmployeeNIK {
    String name;
    String NIK;

    public EmployeeNIK(String name, String NIK) {
        this.name = name;
        this.NIK = NIK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNIK() {
        return NIK;
    }

    public void setNIK(String NIK) {
        this.NIK = NIK;
    }
}
