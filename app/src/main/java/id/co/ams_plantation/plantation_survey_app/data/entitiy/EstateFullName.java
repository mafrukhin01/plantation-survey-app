package id.co.ams_plantation.plantation_survey_app.data.entitiy;

/**
 * Created by user on 4/9/2018.
 */

public class EstateFullName {
    String companyShortName;
    String companyFullName;

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }
}
