package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


/**
 * Created by Mafrukhin on 11/07/2023.
 */
@Entity(tableName = "PSA_EstateMapping")
public class EstateMapping {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "estCode")
    public String estCode;
    @ColumnInfo(name = "plantation")
    public String plantation;
    @ColumnInfo(name = "grouping")
    public String grouping;
    @ColumnInfo(name = "groupCompanyName")
    public String groupCompanyName;
    @ColumnInfo(name = "companyCode")
    public String companyCode;
    @ColumnInfo(name = "estNewCode")
    public String estNewCode;
    @ColumnInfo(name = "estCodeSAP")
    public String estCodeSAP;
    @ColumnInfo(name = "newEstName")
    public String newEstName;
    @Ignore
    public EstateMapping() {
    }
    public EstateMapping(@NonNull String estCode, String plantation, String grouping, String groupCompanyName, String companyCode, String estNewCode, String estCodeSAP, String newEstName) {
        this.estCode = estCode;
        this.plantation = plantation;
        this.grouping = grouping;
        this.groupCompanyName = groupCompanyName;
        this.companyCode = companyCode;
        this.estNewCode = estNewCode;
        this.estCodeSAP = estCodeSAP;
        this.newEstName = newEstName;
    }

    @NonNull
    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(@NonNull String estCode) {
        this.estCode = estCode;
    }

    public String getPlantation() {
        return plantation;
    }

    public void setPlantation(String plantation) {
        this.plantation = plantation;
    }

    public String getGrouping() {
        return grouping;
    }

    public void setGrouping(String grouping) {
        this.grouping = grouping;
    }

    public String getGroupCompanyName() {
        return groupCompanyName;
    }

    public void setGroupCompanyName(String groupCompanyName) {
        this.groupCompanyName = groupCompanyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getEstNewCode() {
        return estNewCode;
    }

    public void setEstNewCode(String estNewCode) {
        this.estNewCode = estNewCode;
    }

    public String getEstCodeSAP() {
        return estCodeSAP;
    }

    public void setEstCodeSAP(String estCodeSAP) {
        this.estCodeSAP = estCodeSAP;
    }

    public String getNewEstName() {
        return newEstName;
    }

    public void setNewEstName(String newEstName) {
        this.newEstName = newEstName;
    }
}
