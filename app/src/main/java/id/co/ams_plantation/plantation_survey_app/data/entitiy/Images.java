package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Mafrukhin on 11/05/2022.
 */

@Entity(tableName = "PSA_Images")
public class Images {
    @NonNull
    public String getImageId() {
        return imageId;
    }

    public void setImageId(@NonNull String imageId) {
        this.imageId = imageId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getImageAlias() {
        return imageAlias;
    }

    public void setImageAlias(String imageAlias) {
        this.imageAlias = imageAlias;
    }


    public Images(@NonNull String imageId, String imageAlias, String location, String status, Date createDate) {
        this.imageId = imageId;
        this.imageAlias = imageAlias;
        this.location = location;
        this.status = status;
        this.createDate = createDate;
    }
    @Ignore
    public Images(){
    }

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "imageId")
    private String imageId;
    @ColumnInfo(name = "imageAlias")
    private String imageAlias;
    @ColumnInfo(name = "location")
    private String location;
    @ColumnInfo(name = "status")
    private String status;
    @ColumnInfo(name = "createDate")
    private Date createDate;
}
