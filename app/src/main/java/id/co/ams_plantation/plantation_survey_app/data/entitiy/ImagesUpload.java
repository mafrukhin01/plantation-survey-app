package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Mafrukhin on 03/06/2023.
 */
@Entity(tableName = "PSA_ImagesUpload")
public class ImagesUpload {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "imageId")
    private String imageId;
    @ColumnInfo(name = "globalId")
    private String globalId;
    @ColumnInfo(name = "location")
    private String location;
    @ColumnInfo(name = "sessionSurvey")
    private String sessionSurvey;
    @ColumnInfo(name = "createDate")
    private Date createDate;
    @ColumnInfo(name = "isUpload")
    private int isUpload;

    public ImagesUpload(@NonNull String imageId, String globalId, String location, String sessionSurvey, Date createDate, int isUpload) {
        this.imageId = imageId;
        this.globalId = globalId;
        this.location = location;
        this.sessionSurvey = sessionSurvey;
        this.createDate = createDate;
        this.isUpload = isUpload;
    }

    @NonNull
    public String getImageId() {
        return imageId;
    }

    public void setImageId(@NonNull String imageId) {
        this.imageId = imageId;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSessionSurvey() {
        return sessionSurvey;
    }

    public void setSessionSurvey(String sessionSurvey) {
        this.sessionSurvey = sessionSurvey;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(int isUpload) {
        this.isUpload = isUpload;
    }
}
