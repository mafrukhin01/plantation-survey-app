package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class LayerMap {
    int value;
    boolean selected;
    String layerName;

    public LayerMap(int value, boolean selected, String layerName) {
        this.value = value;
        this.selected = selected;
        this.layerName = layerName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }
}
