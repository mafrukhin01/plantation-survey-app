package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class Project {
    String ProjectId;
    String ProjectName;
    String ProjectDesc;
    String StartDate;
    String EndDate;
    String EstateName;

    public Project(String projectId, String projectName, String projectDesc, String startDate, String endDate, String estateName) {
        ProjectId = projectId;
        ProjectName = projectName;
        ProjectDesc = projectDesc;
        StartDate = startDate;
        EndDate = endDate;
        EstateName = estateName;
    }

    public String getProjectId() {
        return ProjectId;
    }

    public void setProjectId(String projectId) {
        ProjectId = projectId;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }

    public String getProjectDesc() {
        return ProjectDesc;
    }

    public void setProjectDesc(String projectDesc) {
        ProjectDesc = projectDesc;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getEstateName() {
        return EstateName;
    }

    public void setEstateName(String estateName) {
        EstateName = estateName;
    }
}
