package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import java.util.List;

public class ProjectsNMapping {
    public List<UserProject> projects;
    public EstateMapping mapping;
    public ProjectsNMapping(List<UserProject> projects, EstateMapping mapping) {
        this.projects = projects;
        this.mapping = mapping;
    }
    public List<UserProject> getProjects() {
        return projects;
    }
    public void setProjects(List<UserProject> projects) {
        this.projects = projects;
    }
    public EstateMapping getMapping() {
        return mapping;
    }
    public void setMapping(EstateMapping mapping) {
        this.mapping = mapping;
    }
}
