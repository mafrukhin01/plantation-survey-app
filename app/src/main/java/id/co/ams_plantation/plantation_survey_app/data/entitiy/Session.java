package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Mafrukhin on 19/05/2022.
 */
@Entity(tableName = "PSA_Session")
public class Session {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "sessionId")
    private String sessionId;
    @ColumnInfo(name = "sessionName")
    private String sessionName;
    @ColumnInfo(name = "createBy")
    private String createBy;
    @ColumnInfo(name = "createDate")
    private Date createDate;
    @ColumnInfo(name = "updateBy")
    private String updateBy;
    @ColumnInfo(name = "updateDate")
    private Date updateDate;
    @ColumnInfo(name = "estCode")
    private String estCode;
    @ColumnInfo(name = "startDate")
    private Date startDate;
    @ColumnInfo(name = "finishDate")
    private Date finishDate;
//    @ColumnInfo(name = "status")
//    private String status;
    @ColumnInfo(name = "remarks")
    private String remarks;
    @ColumnInfo(name = "officer")
    private String officer;
    @ColumnInfo(name = "status", defaultValue = "1")
    private String status;
    @ColumnInfo(name = "appVersion")
    private String appVersion;

    public Session(@NonNull String sessionId, String sessionName, String createBy, Date createDate, String updateBy, Date updateDate, String estCode, Date startDate, Date finishDate, String remarks, String officer, String status, String appVersion) {
        this.sessionId = sessionId;
        this.sessionName = sessionName;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.estCode = estCode;
        this.startDate = startDate;
        this.finishDate = finishDate;
//        this.status = status;
        this.remarks = remarks;
        this.officer = officer;
        this.status = status;
        this.appVersion = appVersion;
    }

    @NonNull
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(@NonNull String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOfficer() {
        return officer;
    }

    public void setOfficer(String officer) {
        this.officer = officer;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
