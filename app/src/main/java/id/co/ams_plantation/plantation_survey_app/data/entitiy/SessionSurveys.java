package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Mafrukhin on 06/06/2023.
 */
@Entity(tableName = "PSA_SessionSurveys")
public class SessionSurveys {
    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "sessionId")
    private String sessionId;
    @ColumnInfo(name = "createdBy")
    private String createdBy;
    @ColumnInfo(name = "createdDate")
    private Date createdDate;
    @ColumnInfo(name = "globalId")
    private String globalId;
    @ColumnInfo(name = "action")
    private String action;
    @ColumnInfo(name = "objCode")
    private String objCode;
    @ColumnInfo(name = "statusQC")
    private String statusQC;
    @ColumnInfo(name = "status")
    private String status;

    public SessionSurveys( String sessionId, String createdBy, Date createdDate, String globalId, String action, String objCode, String statusQC, String status) {

        this.sessionId = sessionId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.globalId = globalId;
        this.action = action;
        this.objCode = objCode;
        this.statusQC = statusQC;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getObjCode() {
        return objCode;
    }

    public void setObjCode(String objCode) {
        this.objCode = objCode;
    }

    public String getStatusQC() {
        return statusQC;
    }

    public void setStatusQC(String statusQC) {
        this.statusQC = statusQC;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
