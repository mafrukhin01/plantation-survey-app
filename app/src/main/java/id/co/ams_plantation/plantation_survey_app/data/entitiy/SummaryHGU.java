package id.co.ams_plantation.plantation_survey_app.data.entitiy;

public class SummaryHGU {
    String type;
    int baik;
    int rusak;
    int hilang;
    int bp;
    int tbi;
    int tbs;
    int total;

    public SummaryHGU(String type, int baik, int rusak, int hilang, int bp, int tbi, int tbs, int total) {
        this.type = type;
        this.baik = baik;
        this.rusak = rusak;
        this.hilang = hilang;
        this.bp = bp;
        this.tbi = tbi;
        this.tbs = tbs;
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBaik() {
        return baik;
    }

    public void setBaik(int baik) {
        this.baik = baik;
    }

    public int getRusak() {
        return rusak;
    }

    public void setRusak(int rusak) {
        this.rusak = rusak;
    }

    public int getHilang() {
        return hilang;
    }

    public void setHilang(int hilang) {
        this.hilang = hilang;
    }

    public int getBp() {
        return bp;
    }

    public void setBp(int bp) {
        this.bp = bp;
    }

    public int getTbi() {
        return tbi;
    }

    public void setTbi(int tbi) {
        this.tbi = tbi;
    }

    public int getTbs() {
        return tbs;
    }

    public void setTbs(int tbs) {
        this.tbs = tbs;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
