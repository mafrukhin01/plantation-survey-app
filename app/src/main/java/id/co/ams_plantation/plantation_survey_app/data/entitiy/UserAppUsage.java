package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "PSA_UAL")
public class UserAppUsage {
    public static final String HIT = "hit";
    public static final String SCREEN_TIME = "screen time";
    public static final String REQUEST_DATA = "request data";
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private long id;
    @ColumnInfo(name = "userId")
    private String userId;
    @ColumnInfo(name = "moduleCode")
    private String moduleCode;
    @ColumnInfo(name = "appId")
    private String appId;
    @ColumnInfo(name = "activityType")
    private String activityType;
    @ColumnInfo(name = "platform")
    private String platform;
    @ColumnInfo(name = "pageId")
    private String pageId;
    @ColumnInfo(name = "elementId")
    private String elementId;
    @ColumnInfo(name = "connectionInfo")
    private String connectionInfo;
    @ColumnInfo(name = "ipAddress")
    private String ipAddress;
    @ColumnInfo(name = "startTime")
    private String startTime;
    @ColumnInfo(name = "endTime")
    private String endTime;
    @ColumnInfo(name = "remarks")
    private String remarks;
    @ColumnInfo(name = "androidId")
    private String androidId;
    @ColumnInfo(name = "idAndroid")
    private String idAndroid;
    @ColumnInfo(name = "versionApp")
    private String versionApp;
    @ColumnInfo(name = "createDate")
    private String createDate;


    public UserAppUsage(String userId, String moduleCode, String appId, String activityType, String platform, String pageId, String elementId, String connectionInfo, String ipAddress, String startTime, String endTime, String remarks, String androidId, String idAndroid, String versionApp, String createDate) {
        this.userId = userId;
        this.moduleCode = moduleCode;
        this.appId = appId;
        this.activityType = activityType;
        this.platform = platform;
        this.pageId = pageId;
        this.elementId = elementId;
        this.connectionInfo = connectionInfo;
        this.ipAddress = ipAddress;
        this.startTime = startTime;
        this.endTime = endTime;
        this.remarks = remarks;
        this.androidId = androidId;
        this.idAndroid = idAndroid;
        this.versionApp = versionApp;
        this.createDate = createDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(String connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getIdAndroid() {
        return idAndroid;
    }

    public void setIdAndroid(String idAndroid) {
        this.idAndroid = idAndroid;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
