package id.co.ams_plantation.plantation_survey_app.data.entitiy;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by Mafrukhin on 08/06/2023.
 */
@Entity(tableName = "PSA_UserProject")
public class UserProject {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;
    @ColumnInfo(name = "estCode")
    public String estCode;
    @ColumnInfo(name = "companyCode")
    public String companyCode;
    @ColumnInfo(name = "description")
    public String description;
    @ColumnInfo(name = "userID")
    public int userID;
    @ColumnInfo(name = "statusEdit")
    public int statusEdit;
    @ColumnInfo(name = "startDate")
    public long startDate;
    @ColumnInfo(name = "endDate")
    public long endDate;


    public UserProject(String estCode, String companyCode, String description, int userID, int statusEdit, long startDate, long endDate) {
        this.estCode = estCode;
        this.companyCode = companyCode;
        this.description = description;
        this.userID = userID;
        this.statusEdit = statusEdit;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getStatusEdit() {
        return statusEdit;
    }

    public void setStatusEdit(int statusEdit) {
        this.statusEdit = statusEdit;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }
}
