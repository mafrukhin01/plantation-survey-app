package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "PSA_ExtendEstate")
public class ExtendEstate {
    @PrimaryKey
    @NonNull
    @SerializedName("minX")
    @ColumnInfo(name = "MinX")
    public String MinX;
    @SerializedName("minY")
    @ColumnInfo(name = "MinY")
    public String MinY;

    @SerializedName("maxX")
    @ColumnInfo(name = "MaxX")
    public String MaxX;

    @SerializedName("maxY")
    @ColumnInfo(name = "MaxY")
    public String MaxY;

    public ExtendEstate(){
    }

    @Ignore
    public ExtendEstate(String minX, String minY, String maxX, String maxY) {
        MinX = minX;
        MinY = minY;
        MaxX = maxX;
        MaxY = maxY;
    }

    public String getMinX() {
        return MinX;
    }

    public void setMinX(String minX) {
        MinX = minX;
    }

    public String getMinY() {
        return MinY;
    }

    public void setMinY(String minY) {
        MinY = minY;
    }

    public String getMaxX() {
        return MaxX;
    }

    public void setMaxX(String maxX) {
        MaxX = maxX;
    }

    public String getMaxY() {
        return MaxY;
    }

    public void setMaxY(String maxY) {
        MaxY = maxY;
    }
}
