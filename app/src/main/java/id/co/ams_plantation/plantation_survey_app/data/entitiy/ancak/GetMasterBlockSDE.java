package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

public class GetMasterBlockSDE {
    public String block;
    public String estnr;
    public String division;
    public String companyCode;
    public double ha_Gross;
    public double ha_GIS;

    public GetMasterBlockSDE(String block, String estnr, String division, String companyCode, double ha_Gross, double ha_GIS) {
        this.block = block;
        this.estnr = estnr;
        this.division = division;
        this.companyCode = companyCode;
        this.ha_Gross = ha_Gross;
        this.ha_GIS = ha_GIS;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getEstnr() {
        return estnr;
    }

    public void setEstnr(String estnr) {
        this.estnr = estnr;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public double getHa_Gross() {
        return ha_Gross;
    }

    public void setHa_Gross(double ha_Gross) {
        this.ha_Gross = ha_Gross;
    }

    public double getHa_GIS() {
        return ha_GIS;
    }

    public void setHa_GIS(double ha_GIS) {
        this.ha_GIS = ha_GIS;
    }
}
