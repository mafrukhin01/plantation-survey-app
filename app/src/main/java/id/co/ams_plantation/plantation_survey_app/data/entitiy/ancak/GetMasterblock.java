package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import java.util.Date;

public class GetMasterblock {

        public String estCode;
        public String division;
        public String block;
        public int multiAncak;
        public double ha_Gross;
        public double ha_GIS;
        public String mandor;
        public long startPeriode;
        public long endPeriode;
        public int isOpen;

    public GetMasterblock(String estCode, String division, String block, int multiAncak, double ha_Gross, double ha_GIS, String mandor, long startPeriode, long endPeriode, int isOpen) {
        this.estCode = estCode;
        this.division = division;
        this.block = block;
        this.multiAncak = multiAncak;
        this.ha_Gross = ha_Gross;
        this.ha_GIS = ha_GIS;
        this.mandor = mandor;
        this.startPeriode = startPeriode;
        this.endPeriode = endPeriode;
        this.isOpen = isOpen;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public int getMultiAncak() {
        return multiAncak;
    }

    public void setMultiAncak(int multiAncak) {
        this.multiAncak = multiAncak;
    }

    public double getHa_Gross() {
        return ha_Gross;
    }

    public void setHa_Gross(double ha_Gross) {
        this.ha_Gross = ha_Gross;
    }

    public double getHa_GIS() {
        return ha_GIS;
    }

    public void setHa_GIS(double ha_GIS) {
        this.ha_GIS = ha_GIS;
    }

    public String getMandor() {
        return mandor;
    }

    public void setMandor(String mandor) {
        this.mandor = mandor;
    }

    public long getStartPeriode() {
        return startPeriode;
    }

    public void setStartPeriode(long startPeriode) {
        this.startPeriode = startPeriode;
    }

    public long getEndPeriode() {
        return endPeriode;
    }

    public void setEndPeriode(long endPeriode) {
        this.endPeriode = endPeriode;
    }

    public GetMasterblock(int isOpen) {
        this.isOpen = isOpen;
    }
}
