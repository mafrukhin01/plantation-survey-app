package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import com.esri.arcgisruntime.mapping.view.Graphic;

public class MapPointTPH {
    Graphic graphic;
    TPH tph;

    public MapPointTPH(Graphic graphic, TPH tph) {
        this.graphic = graphic;
        this.tph = tph;
    }

    public Graphic getGraphic() {
        return graphic;
    }

    public void setGraphic(Graphic graphic) {
        this.graphic = graphic;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }
}
