package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PSA_MasterBlock")
public class MasterBlock {
    @PrimaryKey
    @NonNull
    public String block;
    public String afd;
    public String haGross;
    public String mandor;
    public  int isMultiple;
    public int tAncak;
    public int tTPH;
    public int tAncakReject;
    public int tAncakApprove;
    public long lastCheckout;
    public String remarks;
    public int status;
    @ColumnInfo(defaultValue = "0")
    public boolean selected;
    @ColumnInfo(defaultValue = "0")
    public boolean isOpen;


    public MasterBlock(String block, String afd, String haGross, int tAncak, int tTPH, long lastCheckout, String remarks, int status, boolean isOpen) {
        this.block = block;
        this.afd = afd;
        this.haGross = haGross;
        this.tAncak = tAncak;
        this.tTPH = tTPH;
        this.lastCheckout = lastCheckout;
        this.remarks = remarks;
        this.status = status;
        this.isOpen = isOpen;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getAfd() {
        return afd;
    }

    public void setAfd(String afd) {
        this.afd = afd;
    }

    public String getHaGross() {
        return haGross;
    }

    public void setHaGross(String haGross) {
        this.haGross = haGross;
    }

    public int gettAncak() {
        return tAncak;
    }

    public void settAncak(int tAncak) {
        this.tAncak = tAncak;
    }

    public int gettTPH() {
        return tTPH;
    }

    public void settTPH(int tTPH) {
        this.tTPH = tTPH;
    }

    public long getLastCheckout() {
        return lastCheckout;
    }

    public void setLastCheckout(long lastCheckout) {
        this.lastCheckout = lastCheckout;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public int gettAncakReject() {
        return tAncakReject;
    }

    public void settAncakReject(int tAncakReject) {
        this.tAncakReject = tAncakReject;
    }

    public int gettAncakApprove() {
        return tAncakApprove;
    }

    public void settAncakApprove(int tAncakApprove) {
        this.tAncakApprove = tAncakApprove;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getMandor() {
        return mandor;
    }

    public void setMandor(String mandor) {
        this.mandor = mandor;
    }

    public int getIsMultiple() {
        return isMultiple;
    }

    public void setIsMultiple(int isMultiple) {
        this.isMultiple = isMultiple;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
