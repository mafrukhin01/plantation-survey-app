package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PSA_PointBackup")
public class PointEntity {
    @PrimaryKey()
    public long id;
    public double latitude;
    public double longitude;

    public PointEntity(long id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
