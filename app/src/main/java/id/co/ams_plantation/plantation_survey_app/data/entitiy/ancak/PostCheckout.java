package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

public class PostCheckout {
    public String estCode;
    public String block;
    public String userActive;
    public String status;

    public PostCheckout(String estCode, String block, String userActive, String status) {
        this.estCode = estCode;
        this.block = block;
        this.userActive = userActive;
        this.status = status;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getUserActive() {
        return userActive;
    }

    public void setUserActive(String userActive) {
        this.userActive = userActive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
