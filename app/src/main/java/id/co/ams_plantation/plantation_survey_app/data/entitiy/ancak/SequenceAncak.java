package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "PSA_AncakSeq")
public class SequenceAncak {
    @PrimaryKey
    @NonNull
    String idSequence;

    public SequenceAncak(@NonNull String idSequence) {
        this.idSequence = idSequence;
    }

    @NonNull
    public String getIdSequence() {
        return idSequence;
    }

    public void setIdSequence(@NonNull String idSequence) {
        this.idSequence = idSequence;
    }
}
