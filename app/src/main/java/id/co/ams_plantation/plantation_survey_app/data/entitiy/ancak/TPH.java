package id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "PSA_TPH")
public class TPH {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "noTph")
    private String noTph;
    @NonNull
    @ColumnInfo(name = "Latitude")
    private Double Latitude;
    @NonNull
    @ColumnInfo(name = "Longitude")
    private Double Longitude;
    @ColumnInfo(name = "afdeling")
    private String afdeling;
    @ColumnInfo(name = "ancak")
    private String ancak;
    @ColumnInfo(name = "block")
    private String block;
    @ColumnInfo(name = "createBy")
    private int createBy;

    @ColumnInfo(name = "createDate")
    private long createDate;

    @ColumnInfo(name = "estCode")
    private String estCode;

    @ColumnInfo(name = "namaTph")
    private String namaTph;

    @ColumnInfo(name = "status")
    private int status;

    @ColumnInfo(name = "updateBy")
    private int updateBy;

    @ColumnInfo(name = "updateDate")
    private long updateDate;

    @ColumnInfo(name = "resurvey")
    private int resurvey;

    @ColumnInfo(name = "jumlahPokok")
    private int jumlahPokok;

    @ColumnInfo(name = "panen3Bulan")
    private int panen3Bulan;

    @ColumnInfo(name = "foto")
    private List<String> foto;

    @ColumnInfo(name = "ancakFlag", defaultValue = "2")
    public int ancakFlag;

    @ColumnInfo(name = "idx", defaultValue = "0")
    public int idx = 0;
    @ColumnInfo(name = "userId")
    private String userId;

    @ColumnInfo(name = "versionApp")
    private String versionApp;

    public TPH() {
    }

    @Ignore
    public TPH(@NonNull String noTph, Double latitude, Double longitude, String afdeling, String ancak, String block, int createBy, long createDate, String estCode, String namaTph, int status, int updateBy, long updateDate, int resurvey, int jumlahPokok, int panen3Bulan, List<String> foto) {
        this.noTph = noTph;
        Latitude = latitude;
        Longitude = longitude;
        this.afdeling = afdeling;
        this.ancak = ancak;
        this.block = block;
        this.createBy = createBy;
        this.createDate = createDate;
        this.estCode = estCode;
        this.namaTph = namaTph;
        this.status = status;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.resurvey = resurvey;
        this.jumlahPokok = jumlahPokok;
        this.panen3Bulan = panen3Bulan;
        this.foto = foto;
    }

    @Ignore
    public TPH(@NonNull  Double latitude, Double longitude, String afdeling, String block, int createBy, long createDate, String estCode,  int status, int updateBy, long updateDate, String userId, String versionApp) {
        Latitude = latitude;
        Longitude = longitude;
        this.afdeling = afdeling;
        this.block = block;
        this.createBy = createBy;
        this.createDate = createDate;
        this.estCode = estCode;
        this.namaTph = namaTph;
        this.status = status;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.versionApp = versionApp;
        this.userId = userId;
    }

    @NonNull
    public String getNoTph() {
        return noTph;
    }

    public void setNoTph(@NonNull String noTph) {
        this.noTph = noTph;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getAncak() {
        return ancak;
    }

    public void setAncak(String ancak) {
        this.ancak = ancak;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getNamaTph() {
        return namaTph;
    }

    public void setNamaTph(String namaTph) {
        this.namaTph = namaTph;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public int getResurvey() {
        return resurvey;
    }

    public void setResurvey(int resurvey) {
        this.resurvey = resurvey;
    }

    public int getJumlahPokok() {
        return jumlahPokok;
    }

    public void setJumlahPokok(int jumlahPokok) {
        this.jumlahPokok = jumlahPokok;
    }

    public int getPanen3Bulan() {
        return panen3Bulan;
    }

    public void setPanen3Bulan(int panen3Bulan) {
        this.panen3Bulan = panen3Bulan;
    }

    public List<String> getFoto() {
        return foto;
    }

    public void setFoto(List<String> foto) {
        this.foto = foto;
    }

    public int getAncakFlag() {
        return ancakFlag;
    }

    public void setAncakFlag(int ancakFlag) {
        this.ancakFlag = ancakFlag;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }
}
