package id.co.ams_plantation.plantation_survey_app.data.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.aspsine.multithreaddownload.util.ListUtils;
import com.esri.arcgisruntime.geometry.Point;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.AppDatabase;
import id.co.ams_plantation.plantation_survey_app.data.dao.EmployeesDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.EstateMappingDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.HistoryAssetDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.ImageAssetDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.ImageUploadDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.MasterBlockDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.SessionDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.SessionSurveysDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.TPHDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.UserActivityDao;
import id.co.ams_plantation.plantation_survey_app.data.dao.UserProjectDao;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.PostEndPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.ToUploadPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EmployeeNIK;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
//import id.co.ams_plantation.plantation_survey_app.data.entitiy.H_BRD1;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Images;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SessionSurveys;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserAppUsage;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PointEntity;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;

/**
* Created by Mafrukhin on 19/05/2023.
*/
public class AssetRepository {
//    private final HistoryAssetDao mHistoryAssetDao;
//    private final LiveData<List<H_BRD1>> streamHistoryBRD1;
    private SessionDao mSessionDao;
    private ImageUploadDao imagesUploadDao;
    private ImageAssetDao imagesDao;
    private SessionSurveysDao sessionSurveysDao;
    private UserProjectDao userProjectDao;
    private EstateMappingDao estateMappingDao;
    private UserActivityDao userActivityDao;
    private EmployeesDao employeesDao;
    private TPHDao tphDao;
    private MasterBlockDao masterBlockDao;
    public AssetRepository(Application application){
        AppDatabase db = AppDatabase.getDatabase(application);
//        mHistoryAssetDao = db.historyAssetDao();
        mSessionDao = db.sessionDao();
        imagesUploadDao = db.imageUploadDao();
        sessionSurveysDao = db.sessionSurveysDao();
        userProjectDao = db.userProjectDao();
        estateMappingDao = db.estateMappingDao();
        userActivityDao = db.userActivityDao();
        employeesDao = db.employeesDao();
        tphDao = db.tphDao();
        masterBlockDao = db.masterBlockDao();
//        streamHistoryBRD1 = mHistoryAssetDao.streamBRD1();
    }

//    public LiveData<List<H_BRD1>> streamHistoryBRD11(){
//        return streamHistoryBRD1;
//    }
    public void insertSession(Session param){
//        AppDatabase.databaseWriteExecutor.execute(()->{
            mSessionDao.insert(param);
//        });
    }
    public void insertPR(PostEndPR pr){
        mSessionDao.insertPr(pr);
    }
    public void insertImageUploads(ImagesUpload param){
//        AppDatabase.databaseWriteExecutor.execute(()->{
            imagesUploadDao.insert(param);
//        });
    }
    public void updateList(List<ImagesUpload> objects){
        imagesUploadDao.updateList(objects);
    }
    public void insertSessionSurveys(SessionSurveys param){
        sessionSurveysDao.insert(param);
    }
    public void insertListSessionSurveys(List<SessionSurveys> param){
        sessionSurveysDao.insertList(param);
    }
    public void insertListUserProject(List<UserProject> param){
        userProjectDao.insertAll(param);
    }
    public void insertUserProject(UserProject param){
        userProjectDao.insert(param);
    }
    public List<UserProject> getAllUserProject(){
        return userProjectDao.getAllUserProject();
    }
    public List<UserProject> getUserProjectByEstate(String estCode){
        return userProjectDao.getUserProjectByEstate(estCode);
    }
    public void truncateUserProjects(){
         userProjectDao.truncate();
    }
    public void truncateAndInsertProjects(List<UserProject> projects){
        userProjectDao.truncateAndInsertProject(projects);
    }
    public List<SessionSurveys> getAllSessionSurveys(){
        return sessionSurveysDao.getAllSessionSessionSurvey();
    }
    public List<ImagesUpload> getAllImageUpload(){
        return imagesUploadDao.getAllImageUpload();
    }
    public List<ImagesUpload> getAllImageByGlobalId(String globalId){
        return imagesUploadDao.getAllImageByGlobalId(globalId);
    }
    public LiveData<Integer> watchSequenceSession(){return  mSessionDao.getSequence();}
    public List<Session> getAllSession(){
        return mSessionDao.getAllSession();
    }
    public List<ToUploadPR> getAllPR(){
        return mSessionDao.getAllPr();
    }
    public String getSessionName(){
        return  mSessionDao.getLastSessionName();
    }

    public List<Session> getAllSessionToUpload(){
        return mSessionDao.getAllSessionToUpload();
    }
    public Session getSessionBySessionId(String sessionId){
        return mSessionDao.getSessionBySessionId(sessionId);
    }
    public void insertListEstateMapping(List<EstateMapping> param){
        estateMappingDao.insertAll(param);
    }
    public void insertEstateMapping(EstateMapping param){
        estateMappingDao.insert(param);
    }
    public List<EstateMapping> getAllEstateMapping(){
        return estateMappingDao.getAllEstateMapping();
    }
    public List<EstateMapping> getAllEstateMappingByEstCode(String estCode){
        return estateMappingDao.getEstateMappingByEstCode(estCode);
    }
    public EstateMapping getEstateMapping(){
        return estateMappingDao.getEstateMapping();
    }
    public void insertUal(UserAppUsage ual){
        userActivityDao.insertUAL(ual);
    }

    public void insertListEmployee(List<Employee> param){
        employeesDao.insertAll(param);
    }

    public List<EmployeeNIK>getEmployeeNIK(){
        return employeesDao.getEmployeeNIK();
    }

    public List<String>getListUser(){
        return employeesDao.getEmployeeNameNikList();
    }

    public void  insertAllTPH(List<TPH> param){
        tphDao.insertAll(param);
    }
    public List<TPH> getAllTPH(){
       return tphDao.getAllTPH();
    }
    public LiveData<List<TPH>> watchAllTPH(){
        return tphDao.watchAllTPH();
    }
    public void updateDoneUpload(){
        tphDao.updateDoneUpload();
    }
    public  void updateFoto(List<String> param, String id){
        tphDao.updateFoto(param, id);
    }
    public LiveData<List<TPH>> watchAllTPH2(){
        return tphDao.watchAllTPH2();
    }
    public void insertAllMasterBlock(List<MasterBlock> param){
        masterBlockDao.insertAll(param);
    }
    public List<MasterBlock> getAllMasterBlock(){
       return masterBlockDao.getAllMasterBlock();
    }

    public void truncateMasteBlock(){
        masterBlockDao.clearMasterBlock();
    }
    public void insertExtend(List<ExtendEstate> param){
        masterBlockDao.insertExtend(param);
    }
    public void inputExtend(List<ExtendEstate> param){
        masterBlockDao.inputExtent(param);
    }
    public MasterBlock getSelectedBlock(){
        return masterBlockDao.getSelectedBlock();
    }
    public List<String> getSelectedBlocks(){
        return  masterBlockDao.getSelectedBlocks();
    }
    public  boolean isOpen(String block){
        return masterBlockDao.isOpen(block);
    }
    public void setBlockOpen(List<String> blockOpen){
        masterBlockDao.setBlockOpen(blockOpen);
    }
    public void setFalseBlock(){
        masterBlockDao.setFalseBlock();
    }
    public  void setSelectedBlock(String block){
        masterBlockDao.setSelectedBlock(block);
    }
    public String getIdTPH(String block,String tphName){
        return  tphDao.getIdTPH(block, tphName);
    }
    public void updateFlagJoin(String code, String ancak){
        tphDao.updateFlagJoin(code,ancak);
    }
    public  String createNewTph(String userId,  TPH newTPH){
        return tphDao.createNewTph(userId, newTPH);
    }
    public List<TPH> getAllTPHInBlock(String block){
        return tphDao.getAllTPHInBlock(block);
    }
    public List<TPH> getAllTPHToUpload(){
        return tphDao.getAllTPHToUpload();
    }
    public List<String> getAllNameTPH(String block){
        return tphDao.getAllNameTPH(block);
    }

    public void insertAllPoint(List<PointEntity> param){
        masterBlockDao.insertAllPoint(param);
    }
    public void insertPointBackup(Point param){
        masterBlockDao.insertPointBackup(param);
    }
    public void clearPointsBackup(){
        masterBlockDao.clearPointsBackup();
    }
    public List<PointEntity> getPointBackups(){
        return masterBlockDao.getPointBackups();
    }
    public ExtendEstate getExtend(){
        return masterBlockDao.getExtend();
    }
    public void undoPoint(){
        masterBlockDao.undoPoint();
    }



}
