package id.co.ams_plantation.plantation_survey_app.data.view_model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.esri.arcgisruntime.geometry.Point;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.dao.ImageAssetDao;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.PostEndPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.ToUploadPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EmployeeNIK;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
//import id.co.ams_plantation.plantation_survey_app.data.entitiy.H_BRD1;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SessionSurveys;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserAppUsage;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PointEntity;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.data.repository.AssetRepository;

/**
 * Created by Mafrukhin on 19/05/2023.
 */
public class AssetViewModel extends AndroidViewModel {
    private final AssetRepository assetRepository;
//    private final LiveData<List<H_BRD1>> streamBRD1;
    public AssetViewModel(Application application) {
        super(application);
        assetRepository = new AssetRepository(application);
//        streamBRD1 = assetRepository.streamHistoryBRD11();
    }

//    public LiveData<List<H_BRD1>> streamHistoryBRD11(){
//        return streamBRD1;
//    }
    public LiveData<Integer> watchSequenceSession(){return  assetRepository.watchSequenceSession();}
    public void insertSession(Session param){assetRepository.insertSession(param);}
    public void insertImageUpload(ImagesUpload param){assetRepository.insertImageUploads(param);}
    public void updateList(List<ImagesUpload> objects){
        assetRepository.updateList(objects);
    }
    public List<Session> getAllSession(){
        return assetRepository.getAllSession();}

    public List<ToUploadPR> getAllPR(){
        return assetRepository.getAllPR();
    }

    public String getSessionName(){
        return assetRepository.getSessionName();
    }

    public void insertPrDate(PostEndPR pr){
        assetRepository.insertPR(pr);
    }
    public List<ImagesUpload> getAllImageUpload(){
        return assetRepository.getAllImageUpload();
    }
    public List<ImagesUpload> getAllImageByGlobalId(String globalId){
        return assetRepository.getAllImageByGlobalId(globalId);
    }
    public List<Session> getAllSessionToUpload(){
        return assetRepository.getAllSessionToUpload();
    }
    public void insertSessionSurveys(SessionSurveys param){
        assetRepository.insertSessionSurveys(param);
    }
    public void insertListSessionSurveys(List<SessionSurveys> param){
        assetRepository.insertListSessionSurveys(param);
    }

    public List<SessionSurveys> getAllSessionSurveys(){
        return assetRepository.getAllSessionSurveys();
    }
    public void insertListUserProject(List<UserProject> param){
        assetRepository.insertListUserProject(param);
    }
    public void insertUserProject(UserProject param){
        assetRepository.insertUserProject(param);
    }
    public List<UserProject> getAllUserProject(){
        return assetRepository.getAllUserProject();
    }
    public List<UserProject> getUserProjectByEstate(String estCode){
        return assetRepository.getUserProjectByEstate(estCode);
    }
    public void truncateUserProjects(){
        assetRepository.truncateUserProjects();
    }
    public void truncateAndInsertProjects(List<UserProject> projects){
        assetRepository.truncateAndInsertProjects(projects);
    }
    public Session getSessionBySessionId(String sessionId){
        return  assetRepository.getSessionBySessionId(sessionId);
    }

    public void insertListEstateMapping(List<EstateMapping> param){
        assetRepository.insertListEstateMapping(param);
    }
    public void insertEstateMapping(EstateMapping param){
        assetRepository.insertEstateMapping(param);
    }
    public List<EstateMapping> getAllEstateMapping(){
        return assetRepository.getAllEstateMapping();
    }
    public List<EstateMapping> getAllEstateMappingByEstCode(String estCode){
        return assetRepository.getAllEstateMappingByEstCode(estCode);
    }
    public EstateMapping getEstateMapping(){
        return assetRepository.getEstateMapping();
    }
    public void insertUal(UserAppUsage ual){
        assetRepository.insertUal(ual);
    }
    public void insertListEmployee(List<Employee> param){
        assetRepository.insertListEmployee(param);
    }
    public List<EmployeeNIK>getEmployeeNIK(){
        return assetRepository.getEmployeeNIK();
    }
    public List<String> getListUser(){
        return assetRepository.getListUser();
    }
    public void  insertAllTPH(List<TPH> param){
        assetRepository.insertAllTPH(param);
    }
    public List<TPH> getAllTPH(){
        return assetRepository.getAllTPH();
    }
    public LiveData<List<TPH>> watchAllTPH(){
        return assetRepository.watchAllTPH();
    }
    public void updateDoneUpload(){
        assetRepository.updateDoneUpload();
    }
    public  void updateFoto(List<String> param, String id){
        assetRepository.updateFoto(param, id);
    }
    public LiveData<List<TPH>> watchAllTPH2(){
        return assetRepository.watchAllTPH2();
    }

    public void insertAllMasterBlock(List<MasterBlock> param){
        assetRepository.insertAllMasterBlock(param);
    }
    public List<MasterBlock> getAllMasterBlock(){
        return assetRepository.getAllMasterBlock();
    }
    public void truncateMasteBlock(){
        assetRepository.truncateMasteBlock();
    }
    public void insertExtend(List<ExtendEstate> param){
        assetRepository.insertExtend(param);
    }
    public void inputExtend(List<ExtendEstate> param){
        assetRepository.inputExtend(param);
    }
    public MasterBlock getSelectedBlock(){
        return assetRepository.getSelectedBlock();
    }
    public  boolean isOpen(String block){
        return assetRepository.isOpen(block);
    }
    public void setBlockOpen(List<String> blockOpen){
        assetRepository.setBlockOpen(blockOpen);
    }
    public List<String> getSelectedBlocks(){
        return assetRepository.getSelectedBlocks();
    }
    public void setFalseBlock(){
        assetRepository.setFalseBlock();
    }
    public void setSelectedBlock(String block){
        assetRepository.setSelectedBlock(block);
    }
    public String getIdTPH(String block,String tphName){
        return  assetRepository.getIdTPH(block, tphName);
    }
    public void updateFlagJoin(String code, String ancak){
        assetRepository.updateFlagJoin(code, ancak);
    }
    public  String createNewTph(String userId,  TPH newTPH){
        return assetRepository.createNewTph(userId, newTPH);
    }
    public List<TPH> getAllTPHInBlock(String block){
        return assetRepository.getAllTPHInBlock(block);
    }
    public List<TPH> getAllTPHToUpload(){
        return assetRepository.getAllTPHToUpload();
    }
    public List<String> getAllNameTPH(String block){
        return assetRepository.getAllNameTPH(block);
    }
    public void insertPointBackup(Point param){
        assetRepository.insertPointBackup(param);
    }
    public  void clearPointsBackup(){
        assetRepository.clearPointsBackup();
    }
    public List<PointEntity> getPointBackups(){
        return assetRepository.getPointBackups();
    }
    public ExtendEstate getExtend(){
        return assetRepository.getExtend();
    }
    public void undoPoint(){
        assetRepository.undoPoint();
    }

//    public void insertAllPoint(List<PointEntity> param){
//        assetRepository.insertAllPoint(param);
//    }
//    public void insertPoint(PointEntity param){
//        assetRepository.insertPoint(param);
//    }

}
