package id.co.ams_plantation.plantation_survey_app.data.view_model;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.plantation_survey_app.R;

/**
 * Created by Mafrukhin on 27/07/2022.
 */
public class CensusCellViewHolder extends AbstractViewHolder {
    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public CensusCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;

        String [] id = cell.getId().split("-");
//        try {
//            int urut = Integer.parseInt(id[2]);
//            cell_container.setBackgroundColor(Pemanen.COLOR_DEFIND);
//        }catch (Exception e){
//            cell_container.setBackgroundColor(Pemanen.COLOR_UNDEFIND);
//        }

        cell_textview.requestLayout();
    }
}
