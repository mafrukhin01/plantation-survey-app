package id.co.ams_plantation.plantation_survey_app.data.view_model;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import java.util.ArrayList;
import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.SummaryHGU;


public class SummaryViewModel {
    private final Context mContext;
    private  ArrayList<SummaryHGU> data;

//    private ArrayList<DataBlockSummary> censuses;
    String [] HeaderColumn = {"Baik", "Rusak", "Hilang","Belum Dipasang","Tidak bisa Inspeksi","Belum di Ispeksi","Total"};
//    ApprovalViewModel mApprovalViewModel;
//    MasterBlockViewModel mMasterBlockViewModel;
//    GCensusViewModel mGCensusViewModel;
    public SummaryViewModel(Context context, SummaryHGU hgu,SummaryHGU riparian){
        data = new ArrayList<>();
//        SummaryHGU riparian = new SummaryHGU("Riparian",40,20,20,80);
//        SummaryHGU hgu = new SummaryHGU("HGU",41,10,0,90);
        data.add(riparian);
        data.add(hgu);
//        mApprovalViewModel = new ViewModelProvider(owner).get(ApprovalViewModel.class);
//        mGCensusViewModel = new ViewModelProvider(owner).get(GCensusViewModel.class);
//        mMasterBlockViewModel = new ViewModelProvider(owner).get(MasterBlockViewModel.class);
        this.mContext = context;
//        this.censuses = censuses;
    }
    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
//            String id = i + "-" + censuses.get(i).getGanoId() + "-"+ censuses.get(i).getBlock();
            String id = data.get(i).getType();
            TableViewRowHeader header = new TableViewRowHeader(id, data.get(i).getType());
            list.add(header);
        }
        return list;
    }

    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();
        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }
        return list;
    }

    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j +"";
                TableViewCell cell = null;
                switch (j){
                    case 0:{
                        cell = new TableViewCell(id, data.get(i).getBaik());
                        break;
                    }
                    case 1:{
                        cell = new TableViewCell(id, data.get(i).getRusak());
                        break;
                    }
                    case 2:{
                        cell = new TableViewCell(id, data.get(i).getHilang());
                        break;
                    }
                    case 3:{
                       cell = new TableViewCell(id, data.get(i).getBp());
                        break;
                    }
                    case 4:{
                        cell = new TableViewCell(id, data.get(i).getTbi());
                        break;
                    }
                    case 5:{
                        cell = new TableViewCell(id, data.get(i).getTbs());
                        break;
                    }
                    case 6:{
                        cell = new TableViewCell(id, data.get(i).getTotal());
                        break;
                    }
//                    case 6:{
//                        cell = new TableViewCell(id, data.get(i).getTotal());
//                        break;
//                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }
    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
