package id.co.ams_plantation.plantation_survey_app.data.view_model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.evrencoskun.tableview.filter.IFilterableModel;
import com.evrencoskun.tableview.sort.ISortableModel;

/**
 * Created by Mafrukhin on 26/07/2022.
 */
public class TableViewCell implements ISortableModel, IFilterableModel {
    private final String mId;
    private Object mData;
    private String mFilterKeyword;
    private int color;

    public TableViewCell(String id) {
        this.mId = id;
    }

    public TableViewCell(String id, Object data) {
        this.mId = id;
        this.mData = data;
        this.mFilterKeyword = String.valueOf(data);
    }


    @NonNull
    @Override
    public String getFilterableKeyword() {
        return mFilterKeyword;
    }

    @NonNull
    @Override
    public String getId() {
        return mId;
    }

    public Object getData() {
        return mData;
    }

    @Nullable
    @Override
    public Object getContent() {
        return mData;
    }
}
