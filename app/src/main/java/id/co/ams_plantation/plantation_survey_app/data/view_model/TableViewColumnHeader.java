package id.co.ams_plantation.plantation_survey_app.data.view_model;

/**
 * Created by Mafrukhin on 26/07/2022.
 */
public class TableViewColumnHeader extends TableViewCell{
    public TableViewColumnHeader(String id) {
        super(id);
    }

    public TableViewColumnHeader(String id, Object data) {
        super(id, data);
    }
}
