package id.co.ams_plantation.plantation_survey_app.data.view_model;

/**
 * Created by Mafrukhin on 26/07/2022.
 */
public class TableViewRowHeader extends TableViewCell{
    public TableViewRowHeader(String id) {
        super(id);
    }

    public TableViewRowHeader(String id, String data) {
        super(id, data);
    }
}
