package id.co.ams_plantation.plantation_survey_app.services;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.plantation_survey_app.services.model.SimpleGetResponse;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mafrukhin on 16/02/2023.
 */
public class ApiClient {
    private static Retrofit retrofit = null;
    private static HttpLoggingInterceptor logging;

    private static final OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
//            .addInterceptor(fileLoggerInterceptor)
            .addNetworkInterceptor(new StethoInterceptor())
            .connectTimeout(7, TimeUnit.MINUTES)
            .writeTimeout(7, TimeUnit.MINUTES)
            .readTimeout(7, TimeUnit.MINUTES)
            .build();


    public static Retrofit getClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(Prefs.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getClientPDC(){
        retrofit = new Retrofit.Builder()
                .baseUrl(Prefs.BASE_URL_PDC)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


    public static Retrofit getClientHMS() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Prefs.BASE_URL_HMS)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Retrofit getClientAdminApps() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Prefs.BASE_URL_ADMIN_APPS)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    public static JSONObject castToJsonObject(SimpleGetResponse simpleGetResponse){
        Gson gson = new Gson();
        try {
            JSONObject returnedObject = new JSONObject(gson.toJson(simpleGetResponse,SimpleGetResponse.class));
            return returnedObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }




}
