package id.co.ams_plantation.plantation_survey_app.services;

import java.util.ArrayList;


public class ApiResponses<T> {

    public boolean success;
    public String message;
    public T data;

    public ApiResponses(boolean success, String message, T data) {
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

