package id.co.ams_plantation.plantation_survey_app.services;

import androidx.annotation.NonNull;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.AppUpdate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AppVersion;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.CountObject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.ToUploadPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ProjectsNMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.GetMasterBlockSDE;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.GetMasterblock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PostCheckout;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.services.model.AdminAppsResponse;
import id.co.ams_plantation.plantation_survey_app.services.model.GetProjects;
import id.co.ams_plantation.plantation_survey_app.services.model.HMSResponse;
import id.co.ams_plantation.plantation_survey_app.services.model.PostImage;
import id.co.ams_plantation.plantation_survey_app.services.model.PostTPH;
import id.co.ams_plantation.plantation_survey_app.services.model.SimpleGetResponse;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Mafrukhin on 16/02/2023.
 */
public interface ApiServices {
    @NonNull
    @POST("postImages")
    Call<ResponseBody> postImages(@Body List<PostImage> post);
    @NonNull
    @POST("postEndPR")
    Call<ResponseBody> postPr(@Body List<ToUploadPR> post);


    @NonNull
    @GET("getProjectsByUser")
    Call<ApiResponses<List<UserProject>>> GetProjectByUserEstate(@Query("userId") String estCode, @Query("estCode") String block);
//    @NonNull
//    @GET("getProjectsByUserEstate")
//    Call<ApiResponses<ProjectsNMapping>> GetProjectNMapping(@Query("userId") String estCode, @Query("estCode") String block);
    @NonNull
    @GET("getProjectsByUserEstate2")
    Call<ApiResponses<ProjectsNMapping>> GetProjectNMapping(@Query("userId") String estCode, @Query("estCode") String block);
    @NonNull
    @GET("getAppVersion")
    Call<List<AppVersion>> GetVersionApp(@Query("package") String packageName);
    @NonNull
    @GET("CheckUpdateApp")
    Call<List<AppUpdate>> CheckUpdateApp();
    @NonNull
    @GET("getEstateMappingByEstCode")
    Call<ApiResponses<List<EstateMapping>>> GetEstateMappingByEstCode(@Query("estCode") String estCode);
    @NonNull
    @GET("getAllEstateMapping")
    Call<ApiResponses<List<EstateMapping>>> GetAllEstateMapping();
    @NonNull
    @GET("map/GetAllFileTypeUrlMapping")
    Call<SimpleGetResponse> GetAllFileTypeUrlMapping();
    @NonNull
    @GET("map/GetAllMapFilesByEstate")
    Call<SimpleGetResponse> GetAllMapFilesByEstate(@Query("userid") String userid, @Query("username") String username, @Query("estate") String estate);
    @NonNull
    @GET("getCountObjectSDE")
    Call<List<CountObject>> getCountObjectSDE();
    @NonNull
    @GET("user/GetAllUserIPLASByEstate")
    Call<AdminAppsResponse> getAllEmployee(@Query("estCode") String escode);
    @NonNull
    @GET("TPH/GetAllTPHByEstateSP")
    Call<HMSResponse<List<TPH>>> getAllTPHByAfdeling(@Query("estCode") String estCode , @Query("afd") String afd);
    @NonNull
    @POST("tph/insertTPHnew")
    Call<ResponseBody> postTPH(@Body List<TPH> post, @Query("estCode") String estCode);

    @NonNull
    @POST("tph/InsertImageTPH")
    Call<ResponseBody> postImageTPH(@Body RequestBody param, @Query("estCode") String estCode);

    @NonNull
    @GET("GetMasterBlockSDE")
    Call<List<GetMasterBlockSDE>>getMasterBlockSDE(@Query("estnr") String estCode);
    @NonNull
    @POST("HMS/PostBlockCheckInOut")
    Call<ResponseBody> postCheckoutBlock(@Body PostCheckout post);
    @NonNull
    @GET("GetExtend")
    Call<List<ExtendEstate>>getExtend(@Query("estnr") String estCode);
    @NonNull
    @GET("GetMasterBlocksAncak")
    Call<ApiResponses<List<GetMasterblock>>>getMasterBLockAncak(@Query("estnr") String estCode);

}
