package id.co.ams_plantation.plantation_survey_app.services.model;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Employee;

public class AdminAppsResponse {
    private String code;
    private String message;
    private List<Employee> data;

    public AdminAppsResponse(String code, String message, List<Employee> data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }
}
