package id.co.ams_plantation.plantation_survey_app.services.model;

/**
 * Created by Mafrukhin on 28/03/2023.
 */
public class FileTypeUrlMapping {
    String fileType;
    String url;

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
