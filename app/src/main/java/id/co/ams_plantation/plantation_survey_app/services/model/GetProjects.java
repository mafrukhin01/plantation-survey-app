package id.co.ams_plantation.plantation_survey_app.services.model;

import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;

/**
 * Created by Mafrukhin on 08/06/2023.
 */
public class GetProjects {
    public List<UserProject> projects;

    public GetProjects(List<UserProject> projects) {
        this.projects = projects;
    }

    public List<UserProject> getProjects() {
        return projects;
    }

    public void setProjects(List<UserProject> projects) {
        this.projects = projects;
    }
}
