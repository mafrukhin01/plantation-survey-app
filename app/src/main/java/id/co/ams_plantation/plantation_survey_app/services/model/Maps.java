package id.co.ams_plantation.plantation_survey_app.services.model;



public class Maps {
    private String companyCode;
    private String estCode;
    private String estName;
    private String divisiId;
    private String blockId;
    private String filename;
    private String fileLengthBytes;
    private String lastModified;
    private String fileType;
    private String prefixName;
    private String companyShortName;
    private String regionCode;
    private String regionName;
    private String countryCode;
    private String countryName;

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    /**
     *
     * @return
     * The estCode
     */
    public String getEstCode() {
        return estCode;
    }

    /**
     *
     * @param estCode
     * The estCode
     */
    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    /**
     *
     * @return
     * The estName
     */
    public String getEstName() {
        return estName;
    }

    /**
     *
     * @param estName
     * The estName
     */
    public void setEstName(String estName) {
        this.estName = estName;
    }

    /**
     *
     * @return
     * The divisiId
     */
    public String getDivisiId() {
        return divisiId;
    }

    /**
     *
     * @param divisiId
     * The divisiId
     */
    public void setDivisiId(String divisiId) {
        this.divisiId = divisiId;
    }

    /**
     *
     * @return
     * The blockId
     */
    public String getBlockId() {
        return blockId;
    }

    /**
     *
     * @param blockId
     * The blockId
     */
    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    /**
     *
     * @return
     * The filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     *
     * @param filename
     * The filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     *
     * @return
     * The fileLengthBytes
     */
    public String getFileLengthBytes() {
        return fileLengthBytes;
    }

    /**
     *
     * @param fileLengthBytes
     * The fileLengthBytes
     */
    public void setFileLengthBytes(String fileLengthBytes) {
        this.fileLengthBytes = fileLengthBytes;
    }

    /**
     *
     * @return
     * The lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     *
     * @param lastModified
     * The lastModified
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     *
     * @return
     * The fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     *
     * @param fileType
     * The fileType
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     *
     * @return
     * The prefixName
     */
    public String getPrefixName() {
        return prefixName;
    }

    /**
     *
     * @param prefixName
     * The prefixName
     */
    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    /**
     *
     * @return
     * The companyShortName
     */
    public String getCompanyShortName() {
        return companyShortName;
    }

    /**
     *
     * @param companyShortName
     * The companyShortName
     */
    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    /**
     *
     * @return
     * The regionCode
     */
    public String getRegionCode() {
        return regionCode;
    }

    /**
     *
     * @param regionCode
     * The regionCode
     */
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    /**
     *
     * @return
     * The regionName
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     *
     * @param regionName
     * The regionName
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /**
     *
     * @return
     * The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     *
     * @param countryCode
     * The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     *
     * @return
     * The countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     *
     * @param countryName
     * The countryName
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}

