package id.co.ams_plantation.plantation_survey_app.services.model;

/**
 * Created by Mafrukhin on 05/06/2023.
 */
public class PostImage {
    public String name;
    public String sessionSurveyId;
    public String image;
    public String globalID;

    public PostImage(String name, String sessionSurveyId, String image, String globalID) {
        this.name = name;
        this.sessionSurveyId = sessionSurveyId;
        this.image = image;
        this.globalID = globalID;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSessionSurveyId() {
        return sessionSurveyId;
    }

    public void setSessionSurveyId(String sessionSurveyId) {
        this.sessionSurveyId = sessionSurveyId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGlobalID() {
        return globalID;
    }

    public void setGlobalID(String globalID) {
        this.globalID = globalID;
    }
}
