package id.co.ams_plantation.plantation_survey_app.services.model;

import java.util.ArrayList;

public class PostTPH {
    public double latitude;
    public double longitude;
    public String afdeling;
    public String ancak;
    public String block;
    public int createBy;
    public Object createDate;
    public String estCode;
    public ArrayList<String> foto;
    public String namaTph;
    public String noTph;
    public int panen3Bulan;
    public int resurvey;
    public int status;
    public int updateBy;
    public double updateDate;
    public int idx;
    public String userId;
    public String versionApp;
    public int ancakFlag;

    public PostTPH(double latitude, double longitude, String afdeling, String ancak, String block, int createBy, Object createDate, String estCode, ArrayList<String> foto, String namaTph, String noTph, int panen3Bulan, int resurvey, int status, int updateBy, double updateDate, int idx, String userId, String versionApp, int ancakFlag) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.afdeling = afdeling;
        this.ancak = ancak;
        this.block = block;
        this.createBy = createBy;
        this.createDate = createDate;
        this.estCode = estCode;
        this.foto = foto;
        this.namaTph = namaTph;
        this.noTph = noTph;
        this.panen3Bulan = panen3Bulan;
        this.resurvey = resurvey;
        this.status = status;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.idx = idx;
        this.userId = userId;
        this.versionApp = versionApp;
        this.ancakFlag = ancakFlag;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getAncak() {
        return ancak;
    }

    public void setAncak(String ancak) {
        this.ancak = ancak;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public Object getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Object createDate) {
        this.createDate = createDate;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public ArrayList<String> getFoto() {
        return foto;
    }

    public void setFoto(ArrayList<String> foto) {
        this.foto = foto;
    }

    public String getNamaTph() {
        return namaTph;
    }

    public void setNamaTph(String namaTph) {
        this.namaTph = namaTph;
    }

    public String getNoTph() {
        return noTph;
    }

    public void setNoTph(String noTph) {
        this.noTph = noTph;
    }

    public int getPanen3Bulan() {
        return panen3Bulan;
    }

    public void setPanen3Bulan(int panen3Bulan) {
        this.panen3Bulan = panen3Bulan;
    }

    public int getResurvey() {
        return resurvey;
    }

    public void setResurvey(int resurvey) {
        this.resurvey = resurvey;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public double getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(double updateDate) {
        this.updateDate = updateDate;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public int getAncakFlag() {
        return ancakFlag;
    }

    public void setAncakFlag(int ancakFlag) {
        this.ancakFlag = ancakFlag;
    }
}
