package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.esri.arcgisruntime.LicenseInfo;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.portal.Portal;
import com.esri.arcgisruntime.security.UserCredential;

import java.io.File;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.AppDatabase;
import id.co.ams_plantation.plantation_survey_app.data.view_model.AssetViewModel;
import id.co.ams_plantation.plantation_survey_app.services.ApiClient;
import id.co.ams_plantation.plantation_survey_app.services.ApiServices;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;

/**
 * Created by Mafrukhin on 19/05/2023.
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected static AppDatabase appDb;
    protected ApiServices apiService = ApiClient.getClient().create(ApiServices.class);;
    protected ApiServices adminAppsService = ApiClient.getClientAdminApps().create(ApiServices.class);
    protected ApiServices hmsAppsServices = ApiClient.getClientHMS().create(ApiServices.class);
    protected ApiServices pdcServices = ApiClient.getClientPDC().create(ApiServices.class);
//    protected ApiServices devHmsAppService = ApiClient.g
    protected static AssetViewModel mAssetViewModel;
    Dialog sweetAlertDialog ;
    Dialog sweetAlertLoading;
    protected void initDB(){
        mAssetViewModel = new ViewModelProvider(this).get(AssetViewModel.class);
        appDb = AppDatabase.getDatabase(this);
    }
    protected Date addTimeStamp(){
        return new Date();
    }
    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void getLicense(){
        UserCredential credential = new UserCredential(getString(R.string.user), getString(R.string.password));
        Portal portal = new Portal("https://app.gis-div.com/portal");
        portal.setCredential(credential);
        portal.loadAsync();
        Log.e("license read",Prefs.getLicense(Prefs.LICENSE));
        portal.addDoneLoadingListener(() -> {
            // check that the portal loaded correctly
            if (portal.getLoadStatus() == LoadStatus.LOADED) {
                // get license info from the portal
                ListenableFuture<LicenseInfo> licenseFuture = portal.fetchLicenseInfoAsync();
                // listen for the license info from the server
                licenseFuture.addDoneListener(() -> {
                    try {
                        LicenseInfo licenseInfo = licenseFuture.get();
                        // Get the license as a json string
                        String licenseJson = licenseInfo.toJson();
                        Log.e("license jsOn",licenseJson);
//                        ArcGISRuntimeEnvironment.setLicense(licenseInfo);
                        Prefs.setPrefs()
                                .putString(Prefs.LICENSE,licenseJson)
                                .apply();
                    } catch (InterruptedException | ExecutionException e) {
                        // error code goes here
                    }
                });
            }
        });
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    protected void showAlert(String title, String message,int type){
        sweetAlertDialog = new SweetAlertDialog(this, type)
                .hideConfirmButton()
                .setTitleText(title).setContentText(message);
        sweetAlertDialog.show();
        (new Handler()).postDelayed(this::closeAlert, 2000);
    }

    protected void closeAlert(){
        sweetAlertDialog.dismiss();
    }
    protected void alertDialog(boolean param){
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(BaseActivity.this);
        builder.setMessage("Aktifkan koneksi Internet dahulu");
        builder.setTitle("Offline Network");
        builder.setPositiveButton("Oke", (dialog, which) -> {
            dialog.cancel();
        });
//        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        AlertDialog alertDialog = builder.create();
        if (param) {
            alertDialog.show();
        } else {
            alertDialog.dismiss();
        }
    }

    protected void showLoadingAlert(String title, String message){
        sweetAlertLoading = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .hideConfirmButton();
        sweetAlertLoading.setCancelable(false);
        sweetAlertLoading.show();
    }

    protected void hideLoadingAlert(){
        if(sweetAlertLoading.isShowing()){
            sweetAlertLoading.dismiss();
        }

    }


    protected void backupGDB(){
        try {
            File gdb = new File(getCacheDir() + "/file_inventory.geodatabase");
            File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION+"backup");
            if (gdb.exists()){
                GlobalHelper.copyFile(gdb,fileLoc,GlobalHelper.getUser().getUserID());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    protected void copyGDB(){
        try {
            File gdb = new File(getCacheDir() + "/file_inventory.geodatabase");
            File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION);
            File fileDGB = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION+"/file_inventory.geodatabase");

            if (gdb.exists()&&!fileDGB.exists()){
                GlobalHelper.copyFile(gdb,fileLoc,"file_inventory");
                Log.e("movingFile","True");
            }
            else {
                Log.e("movingFile","False");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
