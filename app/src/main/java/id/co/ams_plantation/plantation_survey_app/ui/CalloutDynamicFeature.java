package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.layers.FeatureLayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FeatureAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.FeatureEditAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class CalloutDynamicFeature {


    @SuppressLint("SetTextI18n")
    public static ViewGroup setInfoFeature(Context context, Feature mData, String objName){
        MapReplantingActivity mapActivity = (MapReplantingActivity) context;
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup returnedView = (ViewGroup) inflater.inflate(R.layout.callout_dynamic,null);
        RecyclerView rv = returnedView.findViewById(R.id.rv);
        Button bt_pass = returnedView.findViewById(R.id.bt_pass);
        Button button = returnedView.findViewById(R.id.action_record);
        FeatureAdapter adapter;
//        int stat_object = (int) feature.getAttributes().get("status_object");
        List<String> setAttr = new ArrayList<>();
        setAttr = GlobalHelper.attributesAncak();
        List<EditFeature> list = new ArrayList<>();
        Map<String, Object> attr = mData.getAttributes();
        Set<String> keys = attr.keySet();
        Log.e("content", mData.getFeatureTable().getTableName());

        for ( String key :keys){
            Object value = attr.get(key);
            if (value == null){
                value = "";
            }
            Log.e(key,value.toString());
        }
        if (Objects.equals(mData.getFeatureTable().getTableName(), "LUS3")){
            for (String _attr : setAttr) {
            for (String key : keys) {
                if (_attr.equals(key)) {
                    if (mData.getFeatureTable().getField(key).isEditable()) {
                        Object value = attr.get(key);
                        Log.e("key", key + " | type " + mData.getFeatureTable().getField(key).getDomain() + "\n");
                        if (mData.getFeatureTable().getField(key).getDomain() != null) {
                            CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                            List<CodedValue> values = codedValue.getCodedValues();
                            for (CodedValue fieldValue : values) {
                                if (fieldValue.getCode().equals(fieldValue)) {
                                    Log.e("key", key + " | type " + fieldValue.getName() + "\n");
                                    break;
                                }
                            }
                        }
                        if (value != null) {
                            EditFeature f = new EditFeature(key, value.toString());
                            list.add(f);
                            Log.e("content", key + " | " + value + "\n");
                        }
                    }
                }
            }
            }
        }
        else {
            for (String _attr : setAttr) {
                for (String key : keys) {
                    if (_attr.equals(key)) {
                        if (mData.getFeatureTable().getField(key).isEditable()) {
                            Object value = attr.get(key);
                            Log.e("key", key + " | type " + mData.getFeatureTable().getField(key).getDomain() + "\n");
                            if (mData.getFeatureTable().getField(key).getDomain() != null) {
                                CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                List<CodedValue> values = codedValue.getCodedValues();
                                for (CodedValue fieldValue : values) {
                                    if (fieldValue.getCode().equals(fieldValue)) {
                                        Log.e("key", key + " | type " + fieldValue.getName() + "\n");
                                        break;
                                    }
                                }
                            }


//                mData.getFeatureTable().getField(key).getDomain();
//            EditFeature f = new EditFeature(key,String.valueOf(value));
//            list.add(f);
                            if (value != null) {
                                EditFeature f = new EditFeature(key, value.toString());
                                list.add(f);
                                Log.e("content", key + " | " + value + "\n");
                            }
                        }
                    }
//            calloutContent.append(key + " | " + value + "\n");
                }
            }
        }


        adapter = new FeatureAdapter(context, list);
//        adapter.setClickListener((FeatureEditAdapter.ItemClickListener) context);
//                adapter.setData(layers);
//        Log.e("adapter size", String.valueOf(adapter.getItemCount()));
        rv.setAdapter(adapter);
        bt_pass.setOnClickListener(v ->{

            FeatureLayer layer = (FeatureLayer) mData.getFeatureTable().getLayer();
            mapActivity.closeCallout();
//            mapActivity.deleteFeature(mData);
//            mapActivity.updateToFinal(feature);
        });
        return returnedView;
    }

}
