package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.MapView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.ui.MapHguActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class CalloutHGU {
    @SuppressLint("SetTextI18n")
    public static ViewGroup setInfoFeature(Context context, Feature feature, int param){
        MapHguActivity mapActivity = (MapHguActivity) context;
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup returnedView = (ViewGroup) inflater.inflate(R.layout.callout_object_awm,null);
        TextView title = returnedView.findViewById(R.id.title);
        TextView condition = returnedView.findViewById(R.id.condition);
        TextView description = returnedView.findViewById(R.id.description);
        LinearLayout l_prev_census = returnedView.findViewById(R.id.l_next_cen);
        TextView tv_condition = returnedView.findViewById(R.id.tv_condition);
        Button bt_pass = returnedView.findViewById(R.id.bt_pass);
        Button button = returnedView.findViewById(R.id.action_record);
        String patokId = Objects.requireNonNull(feature.getAttributes().get("PatokID")).toString();
        String comp = GlobalHelper.getValueCoded(feature,"typePatok");
        String  block = Objects.requireNonNull(feature.getAttributes().get("Block")).toString();
        description.setText(comp);
        condition.setText("Block : " +block);


        if (param == 1){
            button.setVisibility(View.VISIBLE);
            button.setText("Survey");
            button.setOnClickListener(view -> {
                mapActivity.showBottomSheet3(0,feature);
            });
        }else
        {
            button.setText("Check");
            if(GlobalHelper.getValueCoded(feature,"condition")!=""){
                tv_condition.setText(GlobalHelper.getValueCoded(feature,"condition"));
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(view -> {
                    mapActivity.showBottomSheet3(1,feature);
                });
            }
            else {
                tv_condition.setText("Tidak Ada Data Survey");
                button.setVisibility(View.GONE);
            }

        }

        bt_pass.setOnClickListener(view -> {
            mapActivity.closeCallout();
        });


//        int check = activity.checkEventPatok(patokId);
//        Log.e("check event", String.valueOf(check));
        List<String> setAttr = new ArrayList<>();
//        int stat_object = (int) feature.getAttributes().get("status_object");
        Map<String, Object> attr = feature.getAttributes();
        Set<String> keys = attr.keySet();
        for ( String key :keys){
            Object value = attr.get(key);
            Field.Type typeObject = feature.getFeatureTable().getField(key).getFieldType();
            Log.e("key",key + " | FieldType " + typeObject+ "\n");
            if(feature.getFeatureTable().getField(key).getDomain() != null) {
                CodedValueDomain codedValue = (CodedValueDomain) feature.getFeatureTable().getField(key).getDomain();
                List<CodedValue> values = codedValue.getCodedValues();
                for (CodedValue fieldValue : values) {
                    Log.e("key",key + " | values " + fieldValue.getName()+ "\n");
                }

            }

            if (value == null){
                value = "";
            }
            if (key ==  "GlobalID" ){
//                globalId = String.valueOf(value);
            }
            Log.e(key,value.toString());
        }

        title.setText(patokId);


        //        tv_condition.setText("globalId : "+ feature.getAttributes().get("GlobalID"));

//        int stat_object = (int) feature.getAttributes().get("status_object");
//        short status_asset = (short) feature.getAttributes().get("status_asset");
//        if (stat_object==1){
//            condition.setText("Kondisi : "+GlobalHelper.getValueCoded(feature,"condition"));
//            button.setOnClickListener(v ->mapActivity.showBottomSheet3(1));
//        }
//        else if (stat_object==3&&status_asset==5) {
//            condition.setText("Status : Final Budget");
//            button.setText("Realisasi");
//            button.setOnClickListener(v ->mapActivity.showRealizeForm(feature));
//        }else {
//            condition.setText("Status : "+GlobalHelper.getValueCoded(feature,"status_object"));
//            button.setOnClickListener(v ->mapActivity.showBottomSheet3(1));
//        }
//        title.setText(objName);
//        description.setText("Block : "+ feature.getAttributes().get("block"));
//        tv_condition.setText("globalId : "+ feature.getAttributes().get("GlobalID"));
//        l_prev_census.setVisibility(View.VISIBLE);
//        bt_pass.setEnabled(true);
//        bt_pass.setOnClickListener(v ->{
//            mapActivity.closeCallout();
////            mapActivity.updateToFinal(feature);
//        });


        return returnedView;
    }


}
