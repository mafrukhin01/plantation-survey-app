package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esri.arcgisruntime.data.Feature;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;


public class CalloutInfoFeature {
    @SuppressLint("SetTextI18n")
    public static ViewGroup setInfoFeature(Context context, Feature feature, String objName){
        MapInventoryActivity mapActivity = (MapInventoryActivity) context;
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup returnedView = (ViewGroup) inflater.inflate(R.layout.callout_object_awm,null);
        TextView title = returnedView.findViewById(R.id.title);
        TextView condition = returnedView.findViewById(R.id.condition);
        TextView description = returnedView.findViewById(R.id.description);
        LinearLayout l_prev_census = returnedView.findViewById(R.id.l_next_cen);
        TextView tv_condition = returnedView.findViewById(R.id.tv_condition);
        Button bt_pass = returnedView.findViewById(R.id.bt_pass);
        Button button = returnedView.findViewById(R.id.action_record);
//        int stat_object = (int) feature.getAttributes().get("status_object");
        Map<String, Object> attr = feature.getAttributes();
        Set<String> keys = attr.keySet();
        for ( String key :keys){
            Object value = attr.get(key);
            if (value == null){
                value = "";
            }
            Log.e(key,value.toString());
        }
        int stat_object = (int) feature.getAttributes().get("status_object");
        short status_asset = (short) feature.getAttributes().get("status_asset");
        if (stat_object==1){
            condition.setText("Kondisi : "+GlobalHelper.getValueCoded(feature,"condition"));
            button.setOnClickListener(v ->mapActivity.showBottomSheet3(1));
        }
        else if (stat_object==3&&status_asset==5) {
            condition.setText("Status : Final Budget");
            button.setText("Realisasi");
            button.setOnClickListener(v ->mapActivity.showRealizeForm(feature));
        }else {
            condition.setText("Status : "+GlobalHelper.getValueCoded(feature,"status_object"));
            button.setOnClickListener(v ->mapActivity.showBottomSheet3(1));
        }
        title.setText(objName);
        description.setText("Block : "+ feature.getAttributes().get("block"));
        tv_condition.setText("globalId : "+ feature.getAttributes().get("GlobalID"));
        l_prev_census.setVisibility(View.VISIBLE);
        bt_pass.setEnabled(true);
        bt_pass.setOnClickListener(v ->{
            mapActivity.closeCallout();
//            mapActivity.updateToFinal(feature);
        });


        return returnedView;
    }

}
