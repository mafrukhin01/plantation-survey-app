package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esri.arcgisruntime.data.Feature;

import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class CalloutTPH {

    @SuppressLint("SetTextI18n")
    public static ViewGroup setInfoFeature(Context context, TPH tph){
        MapReplantingActivity mapActivity = (MapReplantingActivity) context;
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup returnedView = (ViewGroup) inflater.inflate(R.layout.callout_tph,null);
        TextView title = returnedView.findViewById(R.id.title);
        TextView condition = returnedView.findViewById(R.id.condition);
        TextView description = returnedView.findViewById(R.id.description);
        TextView status = returnedView.findViewById(R.id.status);
        Button bt_close = returnedView.findViewById(R.id.bt_pass);
        Button bt_join = returnedView.findViewById(R.id.join);
        title.setText("No : " +tph.getNamaTph());
        description.setText("Block : " +tph.getBlock());
        condition.setText("Status : Aktif");
        status.setText(tph.getNoTph());
        bt_close.setOnClickListener(v -> mapActivity.closeCallout());
        bt_join.setOnClickListener(v -> {
            mapActivity.joinTPH(tph.getNoTph(), tph.getBlock());
            Log.e("TPH code", tph.getNoTph());

        });

//        int stat_object = (int) feature.getAttributes().get("status_object");


        return returnedView;
    }
}
