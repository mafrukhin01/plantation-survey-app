package id.co.ams_plantation.plantation_survey_app.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.co.ams_plantation.plantation_survey_app.R;

public class CustomViewGroup extends LinearLayout {
    public CustomViewGroup(Context context) {
        super(context);

        // Inflate the custom layout file and add it to the custom ViewGroup
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_view_point,null);
        ViewGroup customItemView = view.findViewById(R.id.view_group);
        TextView textView = new TextView(context);
        textView.setText("Hello, World!");
        customItemView.addView(textView);


    }
}