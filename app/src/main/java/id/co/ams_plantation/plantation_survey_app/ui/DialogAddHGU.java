package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;

public class DialogAddHGU extends DialogFragment {
    private static Context context;
    RelativeLayout brd1,cvt1,wgt1,pum1,dam1,bun1,zip1,flp1;
    DialogAddHGU(){}
    public static DialogChooseObject newInstance(Context _context){
        DialogChooseObject frag = new DialogChooseObject();
        context = _context;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_object, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        MapInventoryActivity activity = (MapInventoryActivity) context;
        MapInventoryActivity activity = (MapInventoryActivity) context;
        brd1 = view.findViewById(R.id.bt_BRD1);
        cvt1 = view.findViewById(R.id.bt_CVT1);
        wgt1 = view.findViewById(R.id.bt_WGT1);
        bun1 = view.findViewById(R.id.bt_BUN1);
        pum1 = view.findViewById(R.id.bt_PUM1);
        dam1 = view.findViewById(R.id.bt_DAM1);
        zip1 = view.findViewById(R.id.bt_ZIP1);
        flp1 = view.findViewById(R.id.bt_FLP1);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        brd1.setOnClickListener(v -> {
//            activity.showWarning();
//            activity.addNewObject("BRD1");
            activity.checkDuplicateObject("BRD1");
            getDialog().dismiss();
        });
        cvt1.setOnClickListener(v -> {
            activity.checkDuplicateObject("CVT1");
//            activity.addNewObject("CVT1");
            getDialog().dismiss();
        });
        wgt1.setOnClickListener(v -> {
            activity.checkDuplicateObject("WGT1");
//            activity.addNewObject("WGT1");
            getDialog().dismiss();
        });
        pum1.setOnClickListener(v -> {
            activity.checkDuplicateObject("PUM1");
//            activity.addNewObject("PUM1");
            getDialog().dismiss();
        });
        dam1.setOnClickListener(v -> {
            activity.checkDuplicateObject("DAM1");
//            activity.addNewObject("DAM1");
            getDialog().dismiss();
        });
        bun1.setOnClickListener(v -> {
            activity.checkDuplicateObject("BUN1");
//            activity.addNewObject("BUN1");
            getDialog().dismiss();
        });
        zip1.setOnClickListener(v -> {
            activity.checkDuplicateObject("ZIP1");
//            activity.addNewObject("ZIP1");
            getDialog().dismiss();
        });
        flp1.setOnClickListener(v -> {
            activity.checkDuplicateObject("FLP1");
//            activity.addNewObject("FLP1");
            getDialog().dismiss();
        });

    }
}
