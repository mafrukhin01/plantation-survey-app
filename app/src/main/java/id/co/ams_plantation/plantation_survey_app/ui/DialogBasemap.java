package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;
/**
 * Created by Mafrukhin on 14/07/2022.
 */
public class DialogBasemap extends DialogFragment {
    private static Context context;
    RelativeLayout mVtpk, mFum;
    DialogBasemap(){}
    public static DialogBasemap newInstance(Context _context){
        DialogBasemap frag = new DialogBasemap();
        context = _context;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_basemap, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapReplantingActivity activity = (MapReplantingActivity) context;
        mVtpk = view.findViewById(R.id.vtpk_btn);
        mFum = view.findViewById(R.id.fum_btn);
//        mmpk = view.findViewById(R.id.mmpk_btn);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);

        mVtpk.setOnClickListener(v -> {
                activity.setVectorTPK();
            getDialog().dismiss();
        });

        mFum.setOnClickListener(v -> {
                activity.setAerialPhoto();
            getDialog().dismiss();
        });
//        mmpk.setOnClickListener(v ->{
//            activity.setMmpkMap();
//            getDialog().dismiss();
//        });

    }
}
