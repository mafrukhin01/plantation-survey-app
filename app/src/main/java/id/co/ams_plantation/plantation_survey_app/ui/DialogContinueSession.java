package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;

/**
 * Created by Mafrukhin on 03/06/2022.
 */
public class DialogContinueSession extends DialogFragment {
    private TextView mSessionId, mDate, mTime, mUser, mEstate,mSessionName, mBlock, mNormal, mRingan, mSedang, mBerat, mMati, mMatiNon, mTotal;
    Button bt_save, bt_cancel;
    private static Context context;
    private static Date date;
    private static String sessionId, sessionName, time, user, block, estate, normal, ringan, sedang, berat, mati, matiN, total, status;
    public DialogContinueSession(){
    }
    public static DialogContinueSession newInstance(Context context1, String _sessionId, String _sessionName, Date _date, String _time, String _user,
                                                    String _normal, String _ringan, String _sedang, String _berat, String _mati, String _matiN, String _total, String _status){
        DialogContinueSession frag = new DialogContinueSession();
        context = context1;
        sessionId = _sessionId;
        date = _date;
        time = _time;
        user = _user;
        sessionName = _sessionName;

        ringan = _ringan;
        normal = _normal;
        sedang = _sedang;
        berat = _berat;
        mati = _mati;
        matiN = _matiN;
        total = _total;
        status = _status;
        return frag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_continue_session, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ResultCensusActivity activity = (ResultCensusActivity) context;
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        Objects.requireNonNull(getDialog()).setCancelable(false);
        // Get field from view
        mSessionId = view.findViewById(R.id.tv_dg_session_id);
        mDate = view.findViewById(R.id.tv_dg_session_date);
        mTime = view.findViewById(R.id.tv_dg_session_time);
        mUser = view.findViewById(R.id.tv_dg_session_user);
        mSessionName = view.findViewById(R.id.tv_dg_session_name);
//        mSpinnerBlock = view.findViewById(R.id.spinner_block);
        mBlock = view.findViewById(R.id.dg_block);
        bt_save = view.findViewById(R.id.bt_save_session);
        bt_cancel = view.findViewById(R.id.bt_cancel_session);
        mNormal = view.findViewById(R.id.dg_normal);
        mRingan = view.findViewById(R.id.dg_ringan);
        mSedang = view.findViewById(R.id.dg_sedang);
        mBerat = view.findViewById(R.id.dg_berat);
        mMati = view.findViewById(R.id.dg_mati);
        mMatiNon = view.findViewById(R.id.dg_mati_non);
        mTotal = view.findViewById(R.id.dg_total);
        // Fetch arguments from bundle and set title
        mSessionId.setText(sessionId);
        mSessionName.setText(sessionName);
        mDate.setText(DateTime.toStringDate(date));
        mTime.setText(time);
        mUser.setText(user);
        mBlock.setText(block);
        mNormal.setText(normal);
        mRingan.setText(ringan);
        mSedang.setText(sedang);
        mBerat.setText(berat);
        mMati.setText(mati);
        mMatiNon.setText(matiN);
        mTotal.setText(total);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width,height);
        if (getDifferenceDays(date, Calendar.getInstance().getTime())>=2 || status.equals("2")){
            bt_save.setVisibility(View.GONE);
        }
        if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
            bt_save.setText("OKE");
            bt_save.setOnClickListener(v -> {
                getDialog().dismiss();
            });
        }
        else {
            bt_save.setOnClickListener(v -> {
                Prefs.setPrefs().putBoolean(Prefs.ACTIVE_SESSION,true)
                        .putString(Prefs.BLOCK,block)
                        .putString(Prefs.SESSION_ID,sessionId)
                        .apply();
                getDialog().dismiss();
                activity.finish();
            });
        }
//        bt_save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                activity.setCensusSessionActive(mSpinnerBlock.getSelectedItem().toString());
//                getDialog().dismiss();
//            }
//        });


        bt_cancel.setOnClickListener(v -> Objects.requireNonNull(getDialog()).cancel());
        // Show soft keyboard automatically and request focus to field
//        mSessionName.requestFocus();
//        getDialog().getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
}
