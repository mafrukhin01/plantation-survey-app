package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;

import com.esri.arcgisruntime.data.Feature;

import java.util.List;
import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;

public class DialogMergeAncak extends DialogFragment {
    private TextView mSessionId, mDate, mTime, mUser, mEstate,mSessionName,  no1,no2,tph1,tph2,luas1,luas2,pokok1,pokok2;
    Button bt_save, bt_cancel;
    private static List<String> arraySpinner;
    private static Context context;
    private static String newSessionId;
    private Spinner mSpinnerBlock;
    CardView base_info;
    private static String sessionId, sessionName, date, time, user, block, estate;
    private static Feature _feature,_feature2;
    public DialogMergeAncak(){
    }

    public static DialogMergeAncak newInstance(Context context1, List<String> array, String _sessionId, String _sessionName, String _date, Feature feature, Feature feature2){
        DialogMergeAncak frag = new DialogMergeAncak();
        context = context1;
        arraySpinner = array;
        sessionId = _sessionId;
        date = _date;
        _feature = feature;
        _feature2 = feature2;
        sessionName = _sessionName;
        newSessionId = _sessionId;

        return frag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_merge_new, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // Get field from view
        mSessionId = view.findViewById(R.id.tv_dg_session_id);
        base_info = view.findViewById(R.id.base_info);
        mDate = view.findViewById(R.id.tv_dg_session_date);
        mTime = view.findViewById(R.id.tv_dg_session_time);
        mUser = view.findViewById(R.id.tv_dg_session_user);
        mSessionName = view.findViewById(R.id.tv_dg_session_name);
//        mSpinnerBlock = view.findViewById(R.id.spinner_block);
        bt_save = view.findViewById(R.id.bt_save_session);
        bt_cancel = view.findViewById(R.id.bt_cancel_session);
        no1 = view.findViewById(R.id.no_ancak_1);
        no2 = view.findViewById(R.id.no_ancak_2);
        tph1 = view.findViewById(R.id.tph1);
        tph2 = view.findViewById(R.id.tph2);
        luas1 = view.findViewById(R.id.luas1);
        luas2 = view.findViewById(R.id.luas2);

        no1.setText("NoAncak : "+ _feature.getAttributes().get("NoAncak"));
        tph1.setText("NoTPH : "+ _feature.getAttributes().get("tphCode"));
        luas1.setText("Luas : "+ _feature.getAttributes().get("LuasHa") +" Ha");
        no2.setText("NoAncak: "+ _feature2.getAttributes().get("NoAncak"));
        tph2.setText("NoTPH: "+ _feature2.getAttributes().get("tphCode"));
        luas2.setText("Luas : "+ _feature2.getAttributes().get("LuasHa")+" Ha");

        // Fetch arguments from bundle and set title
//        Objects.requireNonNull(getDialog()).setCancelable(false);
//        String[] strings = arraySpinner.toArray(new String[0]);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item,arraySpinner);
//        mSpinnerBlock.setAdapter(adapter);
//        mSessionId.setText(sessionId);
//        mSessionName.setText(sessionName);
//        mDate.setText(date);
//        mTime.setText(time);
//        mUser.setText(user);
        getDialog().getWindow().setLayout(width,height);
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activity.setVisibleBase();
                    MapReplantingActivity activity = (MapReplantingActivity) context;
                    activity.mergePolygons2();
                    getDialog().dismiss();
            }
        });

        bt_cancel.setOnClickListener(v -> {
            MapReplantingActivity activity = (MapReplantingActivity) context;
            activity.clearMultiple();
            getDialog().dismiss();
            activity.setNone();
            activity.refreshAllLayers();
        });


    }

    private void covert(){
        String[] part = sessionId.split(";");
        String convert = part[3].replace(part[3],mSpinnerBlock.getSelectedItem().toString());
        StringBuilder sb = new StringBuilder();
        sb.append(part[0]).append(";").append(part[1]).append(";").append(part[2]).append(";")
                .append(convert).append(";").append(part[4]);
        newSessionId = sb.toString();
        mSessionId.setText(sb);
//        String convert = Arrays.toString(part).replaceAll("[\\[\\]]","");
//        String convert2 = convert.replaceAll(", ",";");
//        mSessionId.setText(convert2);
        Log.e("Spinner",newSessionId);
    }
}
