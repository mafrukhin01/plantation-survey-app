package id.co.ams_plantation.plantation_survey_app.ui;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;

import java.util.List;
import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;


/**
 * Created by Mafrukhin on 24/05/2022.
 */

public class DialogNewSession extends DialogFragment {
    private TextView mSessionId, mDate, mTime, mUser, mEstate,mSessionName;
    Button bt_save, bt_cancel;
    private static  List<String> arraySpinner;
    private static Context context;
    private static String newSessionId;
    private Spinner mSpinnerBlock;
    CardView base_info;
    private static String sessionId, sessionName, date, time, user, block, estate;
    public DialogNewSession(){
    }

    public static DialogNewSession newInstance(Context context1,List<String> array, String _sessionId, String _sessionName, String _date, String _time, String _user){
        DialogNewSession frag = new DialogNewSession();
        context = context1;
        arraySpinner = array;
        sessionId = _sessionId;
        date = _date;
        time = _time;
        user = _user;
        sessionName = _sessionName;
        newSessionId = _sessionId;

        return frag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_new_session, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // Get field from view
        mSessionId = view.findViewById(R.id.tv_dg_session_id);
        base_info = view.findViewById(R.id.base_info);
        mDate = view.findViewById(R.id.tv_dg_session_date);
        mTime = view.findViewById(R.id.tv_dg_session_time);
        mUser = view.findViewById(R.id.tv_dg_session_user);
        mSessionName = view.findViewById(R.id.tv_dg_session_name);
//        mSpinnerBlock = view.findViewById(R.id.spinner_block);
        bt_save = view.findViewById(R.id.bt_save_session);
        bt_cancel = view.findViewById(R.id.bt_cancel_session);
        // Fetch arguments from bundle and set title
//        Objects.requireNonNull(getDialog()).setCancelable(false);
//        String[] strings = arraySpinner.toArray(new String[0]);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item,arraySpinner);
//        mSpinnerBlock.setAdapter(adapter);
        mSessionId.setText(sessionId);
        mSessionName.setText(sessionName);
        mDate.setText(date);
        mTime.setText(time);
        mUser.setText(user);
        getDialog().getWindow().setLayout(width,height);
        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activity.setVisibleBase();
                if (context instanceof MapInventoryActivity){
                    MapInventoryActivity activity = (MapInventoryActivity) context;
                    activity.setCensusSessionActive(newSessionId);
                }else {
                    MapHguActivity activity = (MapHguActivity) context;
                    activity.setCensusSessionActive(newSessionId);
                }

                getDialog().dismiss();
            }
        });

        bt_cancel.setOnClickListener(v -> Objects.requireNonNull(getDialog()).cancel());

//        mSpinnerBlock.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                covert();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


//        mSpinnerBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String[] part = sessionId.split(";");
//                part[3].replace(part[3],mSpinnerBlock.getSelectedItem().toString());
//                String convert = Arrays.toString(part).replaceAll("[\\[\\]]","");
//                convert.replaceAll(", ",";");
//                mSessionId.setText(convert);
//            }
//        });
        // Show soft keyboard automatically and request focus to field
//        mSessionName.requestFocus();
//        getDialog().getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void covert(){
        String[] part = sessionId.split(";");
        String convert = part[3].replace(part[3],mSpinnerBlock.getSelectedItem().toString());
        StringBuilder sb = new StringBuilder();
        sb.append(part[0]).append(";").append(part[1]).append(";").append(part[2]).append(";")
                .append(convert).append(";").append(part[4]);
        newSessionId = sb.toString();
        mSessionId.setText(sb);
//        String convert = Arrays.toString(part).replaceAll("[\\[\\]]","");
//        String convert2 = convert.replaceAll(", ",";");
//        mSessionId.setText(convert2);
        Log.e("Spinner",newSessionId);
    }

}
