package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Date;
import java.util.Objects;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.ActivityLoggerHelper;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;

/**
 * Created by Mafrukhin on 08/06/2022.
 */
public class DialogUpload extends DialogFragment {
    private TextView mCountSession, mCountCensus, mLastSync;
    private LinearLayout bt_upload_sync, bt_re_sync_block;
    private Button mCancel;
    private static Context context;
    private static Date date;
    private static String countSession, countCensus;

    public DialogUpload(){}

    public static DialogUpload newInstance(Context context1,  Date _date, String _countSession, String _countCensus){

        DialogUpload frag = new DialogUpload();
        context = context1;
        date = _date;
        countSession = _countSession;
        countCensus = _countCensus;
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_upload_sync, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
//        Objects.requireNonNull(getDialog()).setCancelable(false);
        // Get field from view
        mLastSync = view.findViewById(R.id.tv_last_sync);
        mCountCensus = view.findViewById(R.id.tv_count_census);
        mCountSession = view.findViewById(R.id.tv_count_session);
        bt_upload_sync = view.findViewById(R.id.btn_upload_sinkron);
        bt_re_sync_block = view.findViewById(R.id.btn_sync_block);
        mCancel = view.findViewById(R.id.bt_cancel);
        // Fetch arguments from bundle and set title
        mLastSync.setText(DateTime.getStringDate());
        mCountSession.setText(countSession);
        mCountCensus.setText(countCensus);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width,height);

        bt_re_sync_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MapInventoryActivity){
                    MapInventoryActivity activity = (MapInventoryActivity) context;
                    activity.syncBidiectional();
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
                        getDialog().dismiss();
                    }
                    else {
//                    PDCApp.deleteCache(context);
//                    activity.openSyncBlock();
                        getDialog().dismiss();
                    }
                }else if(context instanceof MapHguActivity){
                    MapHguActivity activity = (MapHguActivity) context;
                    activity.uploadImage();
                    getDialog().dismiss();
                }


            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getDialog()).dismiss();
            }
        });

        bt_upload_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MapInventoryActivity) {
                    MapInventoryActivity activity = (MapInventoryActivity) context;
                    getDialog().dismiss();
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
                        ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btUpload",new Date(),new Date(),activity);
                        activity.endSessionDialog(true, 1);
//                    activity.uploadData();
                    }
                    else {
                        activity.uploadImage();
                    }
                } else if(context instanceof MapHguActivity){
                    MapHguActivity activity = (MapHguActivity) context;
                    getDialog().dismiss();
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_HGU)){
                        ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btUpload",new Date(),new Date(),activity);
                        activity.endSessionDialog(true, 1);
//                    activity.uploadData();
                    }
                    else {
                        activity.uploadImage();
                    }
                }



            }
        });
    }

}
