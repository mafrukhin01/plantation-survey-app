package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Date;
import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.ActivityLoggerHelper;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;

public class DialogUploadAncak extends DialogFragment {
    private TextView mCountSession, mCountCensus, mLastSync, tAncak;
    private LinearLayout bt_upload_sync, bt_re_sync_block;
    private Button mCancel;
    private static Context context;
    private static Date date;
    private static String countSession, countCensus;

    public DialogUploadAncak(){}

    public static DialogUploadAncak newInstance(Context context1,  Date _date, String _countSession, String _countCensus){
        DialogUploadAncak frag = new DialogUploadAncak();
        context = context1;
        date = _date;
        countSession = _countSession;
        countCensus = _countCensus;
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_upload_ancak, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
//        Objects.requireNonNull(getDialog()).setCancelable(false);
        // Get field from view
        mLastSync = view.findViewById(R.id.tv_last_sync);
        mCountCensus = view.findViewById(R.id.tv_count_census);
        mCountSession = view.findViewById(R.id.tv_count_session);
        bt_upload_sync = view.findViewById(R.id.btn_upload_sinkron);
        bt_re_sync_block = view.findViewById(R.id.btn_sync_block);
        tAncak = view.findViewById(R.id.tv_ancak);
        mCancel = view.findViewById(R.id.bt_cancel);
        // Fetch arguments from bundle and set title
        mLastSync.setText(DateTime.getStringDate());
//        tAncak.setText("Total Ancak Perlu upload : 0");
        mCountCensus.setText(countCensus);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width,height);
        bt_re_sync_block.setOnClickListener(v -> {
            MapReplantingActivity activity = (MapReplantingActivity) context;
            Intent intent = new Intent(context, SyncBlockActivity.class);
            startActivity(intent);
            getDialog().dismiss();
            activity.finish();
//                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
//                        getDialog().dismiss();
//                    }
//                    else {
//                        getDialog().dismiss();
//                    }
        });

        mCancel.setOnClickListener(v -> Objects.requireNonNull(getDialog()).dismiss());

        bt_upload_sync.setOnClickListener(v -> {
            MapReplantingActivity activity = (MapReplantingActivity) context;
//                activity.uploadTPH("");
//                activity.syncBidiectional();
            activity.validateUpload();
//                Intent intent = new Intent(context, SyncBlockActivity.class);
//                startActivity(intent);
            getDialog().dismiss();

        });
    }
}
