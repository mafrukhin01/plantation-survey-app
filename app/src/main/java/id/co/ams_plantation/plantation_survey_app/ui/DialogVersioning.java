package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Objects;

import id.co.ams_plantation.plantation_survey_app.R;


public class DialogVersioning extends DialogFragment {

    private static Context context;
    RelativeLayout mVtpk, mFum;
    Button btnGenerate;
    DialogVersioning(){}
    public static DialogVersioning newInstance(Context _context){
        DialogVersioning frag = new DialogVersioning();
        context = _context;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_versioning, container);
    }
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return dialog;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        if(context instanceof MapReplantingActivity){
//            MapReplantingActivity activity = (MapReplantingActivity) context;
            if(context instanceof MapReplantingActivity){
                MapHguActivity activity = (MapHguActivity) context;
            btnGenerate = view.findViewById(R.id.buttonGenerate);
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
            btnGenerate.setOnClickListener(view1 -> {
                getDialog().dismiss();
                activity.generateGeodatabase();
            });
        }
        else {
            MapInventoryActivity activity = (MapInventoryActivity) context;
            btnGenerate = view.findViewById(R.id.buttonGenerate);
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
            btnGenerate.setOnClickListener(view1 -> {
                getDialog().dismiss();
                activity.generateGeodatabase();
            });
        }



    }

}
