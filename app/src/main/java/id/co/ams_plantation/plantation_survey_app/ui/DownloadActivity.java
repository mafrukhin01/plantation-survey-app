package id.co.ams_plantation.plantation_survey_app.ui;

import static id.co.ams_plantation.plantation_survey_app.services.ApiClient.castToJsonObject;
import static id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper.encryptString;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.Status;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.MapRVAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.plantation_survey_app.services.ApiClient;
import id.co.ams_plantation.plantation_survey_app.services.ApiServices;
import id.co.ams_plantation.plantation_survey_app.services.model.AppInfo;
import id.co.ams_plantation.plantation_survey_app.services.model.FileTypeUrlMapping;
import id.co.ams_plantation.plantation_survey_app.services.model.Maps;
import id.co.ams_plantation.plantation_survey_app.services.model.SimpleGetResponse;
import id.co.ams_plantation.plantation_survey_app.utils.DownloadMapHelper;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.WidgetHelper;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadActivity extends AppCompatActivity {

    RecyclerView rv;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    RefreshLayout refreshRv;
    ImageView ivUserEntry;
    ImageView ivCompanyEntry;
    TextView tvUserEntry;
    TextView tvCompanyEntry;
    TextView keterangan_entry;
    GifImageView giv_loader;
    CoordinatorLayout myCoordinatorLayout;

    ArrayList<FileTypeUrlMapping> fileTypeUrlMappings;
    ArrayList<AppInfo> mapFiles;
    boolean disableRefresh;

    final int LongOperation_GetAllMapFilesByEstate = 0;
    final int LongOperation_SetUpAllMapFilesByEstate = 1;

    public static final int ButtonGroup_Lokal = 0;
    public static final int ButtonGroup_Public = 1;
    JSONArray dataRespon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.download_layout);
        if(getSupportActionBar()!=null)
            getSupportActionBar().hide();

        segmentedButtonGroup = findViewById(R.id.segmentedButtonGroup);
        rv = findViewById(R.id.rv);
        lnoData = findViewById(R.id.lnoData);
        refreshRv = findViewById(R.id.refreshRv);
        ivUserEntry =  findViewById(R.id.iv_user_entry);
        ivCompanyEntry = findViewById(R.id.iv_company_entry);
        tvUserEntry = findViewById(R.id.user_entry);
        tvCompanyEntry = findViewById(R.id.company_entry);
        keterangan_entry = findViewById(R.id.keterangan_entry);
        giv_loader = findViewById(R.id.giv_loader);
        myCoordinatorLayout = findViewById(R.id.myCoordinatorLayout);
        fileTypeUrlMappings = new ArrayList<>();
        mapFiles = new ArrayList<>();
        disableRefresh = false;

        ivUserEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_account)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );
        ivCompanyEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_home)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        SharedPreferences preferences = PSAApp.getContext().getSharedPreferences(PSAApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(PSAApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }
        tvUserEntry.setText(GlobalHelper.getUser().getUserFullName());
        tvCompanyEntry.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstCode() + " : "
                + GlobalHelper.getEstate().getEstName());
        keterangan_entry.setText(this.getString(R.string.app_name));

        rv.setLayoutManager(new LinearLayoutManager(this));
        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(this));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();
    }

    private void refresh() {
        if(!disableRefresh && mapFiles.size() == 0) {
            lnoData.setVisibility(View.GONE);
            refreshRv.getLayout().postDelayed(() -> {
                new LongOperation().execute(String.valueOf(LongOperation_GetAllMapFilesByEstate));
            }, 2000);
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"List downloadd peta sudah muncul tidak perlu refresh list");
            refreshRv.finishRefresh();
        }
    }

    private void setupAdapter(){
        MapRVAdapter adapter = new MapRVAdapter(this,mapFiles);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if(mapFiles.size() > 0){
            lnoData.setVisibility(View.GONE);
        }else{
            lnoData.setVisibility(View.VISIBLE);
        }
    }

    public void rowMapItemAction(AppInfo appInfo,int position){
        disableRefresh = true;
        if (Status.RUNNING == PRDownloader.getStatus(appInfo.getId())) {
            PRDownloader.pause(appInfo.getId());
            return;
        }

        if (Status.PAUSED == PRDownloader.getStatus(appInfo.getId())) {
            PRDownloader.resume(appInfo.getId());
            return;
        }
        appInfo.setId(
                PRDownloader.download(appInfo.getUrl(),
                        appInfo.getFilePath(),
                        encryptString(appInfo.getMaps().getFilename())
                )
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.btnDownload.setText("PAUSE");
                                }
                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.btnDownload.setText("RESUME");
                                }
                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.btnDownload.setText("DOWNLOAD");
                                    appInfo.setId(0);
                                }
                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.map_tv_filesize.setText(
                                            DownloadMapHelper.fileSizeShorter(String.valueOf(progress.currentBytes),true)
                                                    +"/"+
                                                    DownloadMapHelper.fileSizeShorter(String.valueOf(progress.totalBytes),true)
                                    );
                                }
                            }
                        })
                        .start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.btnDownload.setText("COMPLATE");
                                    cekFile();
                                }
                            }

                            @Override
                            public void onError(Error error) {
                                if(isCurrentListViewItemVisible(position)){
                                    final MapRVAdapter.Holder holderMap = getViewHolder(position);
                                    holderMap.btnDownload.setText("DOWNLOAD");
                                    appInfo.setId(0);
                                    holderMap.map_tv_filesize.setText(DownloadMapHelper.fileSizeShorter(String.valueOf(0),true));
                                }
                            }
                        })
        );
    }

    private void cekFile(){
        String download = null;
        String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_GEODATABASE);
        File file = new File(path);
//        if(!file.exists()){
//            download = "Mohon Download Geodatabase Dahulu Sampai COMPLETE ";
//        }
        path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        file = new File(path);
        if(!file.exists()){
            download = "Mohon Download KML Block Dahulu Sampai COMPLETE ";
        }
        path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
        file = new File(path);
        if(!file.exists()){
            download = "Mohon Download VTPK Dahulu Sampai COMPLETE ";
        }

        WidgetHelper.showSnackBar(myCoordinatorLayout,download);

        if(download == null){
            nextActivity();
        }
    }

    private void nextActivity(){
        Intent intent = new Intent(this, HomeActivity.class);
//        Intent intent = new Intent(this, SyncBlockActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean isCurrentListViewItemVisible(int position){
        LinearLayoutManager layoutManager = (LinearLayoutManager) rv.getLayoutManager();
        int first = layoutManager.findFirstVisibleItemPosition();
        int last = layoutManager.findLastVisibleItemPosition();
        return first <= position && position <=last;
    }

    private MapRVAdapter.Holder getViewHolder(int position){
        return (MapRVAdapter.Holder) rv.findViewHolderForLayoutPosition(position);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void getAllFileTypeUrlMapping(){
        ApiServices apiService = ApiClient.getClientAdminApps().create(ApiServices.class);
        Call<SimpleGetResponse> call = apiService.GetAllFileTypeUrlMapping();
        call.enqueue(new Callback<SimpleGetResponse>() {
            @Override
            public void onResponse(Call<SimpleGetResponse> call, Response<SimpleGetResponse> response) {
                assert response.body() != null;
                SimpleGetResponse simpleGetResponse = (SimpleGetResponse) response.body();
                JSONObject jsonObject = castToJsonObject(simpleGetResponse);
                Log.e("Response", response.body().getData().toString());
                try {
                    fileTypeUrlMappings = new ArrayList<>();
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i = 0 ; i < jsonArray.length() ; i++){
                        Gson gson = new Gson();
                        FileTypeUrlMapping fileTypeUrlMapping = gson.fromJson(jsonArray.get(i).toString(),FileTypeUrlMapping.class);
                        fileTypeUrlMappings.add(fileTypeUrlMapping);
                    }
                    getAllMapFilesByEstate();
                } catch (JSONException e) {
                    e.printStackTrace();
                    refreshRv.finishRefresh();
                    Toast.makeText(DownloadActivity.this,"FileTypeUrlMapping response tidak benar",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<SimpleGetResponse> call, Throwable t) {
                refreshRv.finishRefresh();
                Toast.makeText(DownloadActivity.this,"FileTypeUrlMapping Failed Sync Data",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getAllMapFilesByEstate(){
        ApiServices apiService = ApiClient.getClientAdminApps().create(ApiServices.class);
        Call<SimpleGetResponse> call = apiService.GetAllMapFilesByEstate(
                GlobalHelper.getUser().getUserID(),
                GlobalHelper.getUser().getUserName(),
                GlobalHelper.getEstate().getEstCode()
        );
        call.enqueue(new Callback<SimpleGetResponse>() {
            @Override
            public void onResponse(Call<SimpleGetResponse> call, Response<SimpleGetResponse> response) {
                assert response.body() != null;
                SimpleGetResponse simpleGetResponse = (SimpleGetResponse) response.body();
                JSONObject jsonObject = castToJsonObject(simpleGetResponse);
                Log.e("Response", response.body().getData().toString());
                try {
                    dataRespon = jsonObject.getJSONArray("data");
                    new LongOperation().execute(String.valueOf(LongOperation_SetUpAllMapFilesByEstate));
                } catch (JSONException e) {
                    e.printStackTrace();
                    refreshRv.finishRefresh();
                    Toast.makeText(DownloadActivity.this,"response tidak benar",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<SimpleGetResponse> call, Throwable t) {
                refreshRv.finishRefresh();
                Toast.makeText(DownloadActivity.this,"Failed Sync Data",Toast.LENGTH_LONG).show();
            }
        });
    }

    public class LongOperation extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_GetAllMapFilesByEstate:
                        getAllFileTypeUrlMapping();
                        break;
                    case LongOperation_SetUpAllMapFilesByEstate:
                        mapFiles = new ArrayList<>();
                        try {
                            JSONArray jsonArray = dataRespon;
                            Gson gson = new Gson();
                            for(int i = 0 ;i < jsonArray.length();i++) {
                                Maps map = gson.fromJson(jsonArray.get(i).toString(),Maps.class);
                                Log.e("Map",map.getFilename());
                                if(map != null) {
                                    if (map.getPrefixName().equalsIgnoreCase("BLOCK") || map.getPrefixName().equalsIgnoreCase("VTPK")&&!map.getPrefixName().equalsIgnoreCase("WM")) {
                                        for (int j = 0; j < fileTypeUrlMappings.size(); j++) {
                                            FileTypeUrlMapping fileTypeUrlMapping = fileTypeUrlMappings.get(j);
                                            if(fileTypeUrlMapping.getFileType().toUpperCase().equals(map.getFileType().toUpperCase())){
                                                AppInfo appInfo = new AppInfo();
                                                appInfo.setMaps(map);
                                                if (appInfo.getMaps() != null) {
                                                    appInfo.setUrl(DownloadMapHelper.getUri(appInfo,fileTypeUrlMapping));
                                                    appInfo.setFilePath(DownloadMapHelper.getFilePath(appInfo.getMaps()));
                                                    appInfo.setFileName(encryptString(appInfo.getMaps().getFilename()));
                                                    mapFiles.add(appInfo);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return null;
                        }
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));
                        WidgetHelper.showSnackBar(myCoordinatorLayout,(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total"))));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("Exception")){
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Prepare Data GetAllMapFilesByEstate");
                refreshRv.finishRefresh();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpAllMapFilesByEstate:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                }
            }
        }
    }
}
