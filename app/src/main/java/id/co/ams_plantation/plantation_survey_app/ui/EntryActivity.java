package id.co.ams_plantation.plantation_survey_app.ui;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.mikepenz.iconics.utils.Utils;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.BuildConfig;
import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ProjectsNMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.services.ApiClient;
import id.co.ams_plantation.plantation_survey_app.services.ApiResponses;
import id.co.ams_plantation.plantation_survey_app.services.ApiServices;
import id.co.ams_plantation.plantation_survey_app.services.model.GetProjects;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.WidgetHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class EntryActivity extends BaseActivity {
    @BindView(R.id.layout_splash)
    LinearLayout splashLayout;
    @BindView(R.id.layoutIntro)
    RelativeLayout introLayout;
    @BindView(R.id.keterangan_entry)
    TextView txtHarapTunggu;
    @BindView(R.id.tvNamaAplikasi)
    TickerView namaAplikasi;
    @BindView(R.id.ic_paslogo)
    ImageView ivPas;
    @BindView(R.id.ic_pas_typo)
    ImageView ivTypographPas;
    boolean isFinishedButPaused = false;
    protected boolean isPermissionOK = true;
    String[] originalPermissionList;
    private char[] alphabetlist;
    private ValueAnimator progressAnimator;
    private Intent intent;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    final int LongOperation_SetUpAllMapFilesByEstate = 1;
    JSONArray dataRespon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        ButterKnife.bind(this);
        initDB();
        main();
    }
    private void main(){
        ArrayList<String> permissionList = new ArrayList<>();
        if(!checkPermission(Manifest.permission.INTERNET)) permissionList.add(Manifest.permission.INTERNET);
        if(!checkPermission(Manifest.permission.ACCESS_NETWORK_STATE))permissionList.add(Manifest.permission.ACCESS_NETWORK_STATE);
        if(!checkPermission(Manifest.permission.ACCESS_WIFI_STATE))permissionList.add(Manifest.permission.ACCESS_WIFI_STATE);
        if(!checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION))permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        if(!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION))permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if(!checkPermission(Manifest.permission.DISABLE_KEYGUARD))permissionList.add(Manifest.permission.DISABLE_KEYGUARD);
        if(!checkPermission(Manifest.permission.VIBRATE))permissionList.add(Manifest.permission.VIBRATE);
        if(!checkPermission(Manifest.permission.CAMERA))permissionList.add(Manifest.permission.CAMERA);
        if(!checkPermission(Manifest.permission.NFC))permissionList.add(Manifest.permission.NFC);
        if(!checkPermission(Manifest.permission.BLUETOOTH_ADMIN))permissionList.add(Manifest.permission.BLUETOOTH_ADMIN);
        if(!checkPermission(Manifest.permission.BLUETOOTH))permissionList.add(Manifest.permission.BLUETOOTH);
        if(!checkPermission(Manifest.permission.MANAGE_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.BLUETOOTH);
        if(!checkPermission(Manifest.permission.MANAGE_DOCUMENTS))permissionList.add(Manifest.permission.BLUETOOTH);
        String[] strPermission = new String[permissionList.size()];
        if(permissionList.size()>0){
            requestPermission(permissionList.toArray(strPermission),1);
        }else{

            screeningApplication();
        }
//        trimCache(this);
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        }
        else {
            return false;
        }
    }

    private boolean checkPermission(String permission){
        int result = ContextCompat.checkSelfPermission(this,permission);
        if(result== PackageManager.PERMISSION_GRANTED){
            isPermissionOK &= true;
            return true;
        }else{
            isPermissionOK &=false;
            return false;
        }
    }

    public boolean checkPlayService(){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS){
            if(googleApiAvailability.isUserResolvableError(resultCode)){
                googleApiAvailability.getErrorDialog(this,resultCode,PLAY_SERVICES_RESOLUTION_REQUEST);
            }else{
                Toast.makeText(PSAApp.getContext(),"Knp Nih Google Play Servicesnya",Toast.LENGTH_LONG);
                finish();
            }
            return false;
        }
        return true;
    }

    private void requestPermission(String[] permission,int reqcode){
        ActivityCompat.requestPermissions(this,permission,reqcode);
    }

    private void screeningApplication(){
//        if(isNetworkConnected()){
//
//        }
        if(!checkPlayService()){
            WidgetHelper.showOKDialog(this,
                    "Warning",
                    "Anda Harus Instal Google Play Service Dahulu ",
                    (dialog, which) -> {
                        dialog.dismiss();
                        finish();
                    }
            );
        }else {
//            startAnimation();
            if (GlobalHelper.getUser() != null && GlobalHelper.getEstate() != null &&
//                    BuildConfig.BUILD_VARIANT.equals("dev")?
//                    GlobalHelper.isPackageAvailable("id.co.ams_plantation.amsadminapps.dev", getPackageManager()):
                    GlobalHelper.isPackageAvailable("id.co.ams_plantation.amsadminapps", getPackageManager()))
            {
                if(isNetworkConnected()){
//                    getLicense();
//                    LicenseInfo licenseInfo = LicenseInfo.fromJson(Prefs.getLicense(Prefs.LICENSE));
//                    ArcGISRuntimeEnvironment.setLicense(licenseInfo);
//                    syncDataNew();
//                    syncData();
//                    syncEstate();
                    startAnimation();
                }else {
                    startAnimation();
                }
            } else {
                WidgetHelper.showOKDialog(this,
                        "Warning",
                        "Anda Tidak Memiliki Akses Ke Aplikasi Ini",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        }
                );
            }
        }
    }

    public void syncEstate(){
        Call<ApiResponses<List<EstateMapping>>> getEstateMapping = apiService.GetEstateMappingByEstCode(GlobalHelper.getEstate().getEstCode());
        getEstateMapping.enqueue(new Callback<ApiResponses<List<EstateMapping>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<EstateMapping>>> call, Response<ApiResponses<List<EstateMapping>>> response) {
                if(response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().data.size()>0) {
                        mAssetViewModel.insertListEstateMapping(response.body().data);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponses<List<EstateMapping>>> call, Throwable t) {

            }
        });
    }


    public void syncData(){
        Call<ApiResponses<List<UserProject>>> call = apiService.GetProjectByUserEstate(GlobalHelper.getUser().getUserID(),GlobalHelper.getEstate().getEstCode());
        call.enqueue(new Callback<ApiResponses<List<UserProject>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<UserProject>>> call, Response<ApiResponses<List<UserProject>>> response) {
                Log.e("responseBody",String.valueOf(response.body()));
                if(response.isSuccessful()){
                    List<UserProject> list = new ArrayList<>();
                    if(response.body().data.size()>0){
//                        mAssetViewModel.truncateUserProjects();
                        for(UserProject userProject: response.body().data){
                            list.add(userProject);
//                            mAssetViewModel.insertUserProject(userProject);
//                            Log.e("LogUserProject",String.valueOf(list.size()));
                        }
                        mAssetViewModel.truncateAndInsertProjects(list);
                    }
                    else {
                        mAssetViewModel.truncateUserProjects();
                    }
//                            mAssetViewModel.truncateUserProjects();
                }
            }

            @Override
            public void onFailure(Call<ApiResponses<List<UserProject>>> call, Throwable t) {

            }
        });

    }

    public void syncDataNew(){
        Call<ApiResponses<ProjectsNMapping>> call = apiService.GetProjectNMapping(GlobalHelper.getUser().getUserID(),GlobalHelper.getEstate().getEstCode());
        call.enqueue(new Callback<ApiResponses<ProjectsNMapping>>() {
            @Override
            public void onResponse(Call<ApiResponses<ProjectsNMapping>> call, Response<ApiResponses<ProjectsNMapping>> response) {
                if(response.isSuccessful()){
                    assert response.body() != null;
//                    Log.e("response body", String.valueOf(response.body().data));
                    List<UserProject> list = new ArrayList<>();
                    if(response.body().data.projects.size()>0){
                        Log.e("project lengt", String.valueOf(response.body().data.projects.size()));

//                        mAssetViewModel.truncateUserProjects();
                        for(UserProject userProject: response.body().data.projects){
                            list.add(userProject);
//                            mAssetViewModel.insertUserProject(userProject);
//                            Log.e("LogUserProject",String.valueOf(list.size()));
                        }
                        mAssetViewModel.truncateAndInsertProjects(list);
                        mAssetViewModel.insertEstateMapping(response.body().data.mapping);

                    }
                    else {
                        mAssetViewModel.truncateUserProjects();
                    }


//                            mAssetViewModel.truncateUserProjects();
                }

            }

            @Override
            public void onFailure(Call<ApiResponses<ProjectsNMapping>> call, Throwable t) {

            }
        });
    }

    private void startAnimation(){
//        determinateRoadRunner1 = (DeterminateRoadRunner) findViewById(R.id.determinate1);
//        determinateRoadRunner2 = (DeterminateRoadRunner) findViewById(R.id.determinate2);
        namaAplikasi = (TickerView) findViewById(R.id.tvNamaAplikasi);
        alphabetlist = new char[53];
        alphabetlist[0] = TickerUtils.EMPTY_CHAR;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 26; j++) {
                // Add all lowercase characters first, then add the uppercase characters.
                alphabetlist[1 + i * 26 + j] = (char) ((i == 0) ? j + 97 : j + 65);
            }
        }
        ivPas = findViewById(R.id.ic_paslogo);
        ivTypographPas = findViewById(R.id.ic_pas_typo);
//        determinateRoadRunner1.setValue(0);
//        determinateRoadRunner2.setValue(0);
        namaAplikasi.setCharacterList(alphabetlist);
        namaAplikasi.setText("-");
        progressAnimator = ValueAnimator.ofInt(0,500).setDuration(500);
        progressAnimator.setStartDelay(500);
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int value = (Integer) valueAnimator.getAnimatedValue();
//                determinateRoadRunner1.setValue(value);
//                determinateRoadRunner2.setValue(value);
            }
        });
        progressAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }
            @Override
            public void onAnimationEnd(Animator animator) {
//                YoYo.with(Techniques.FadeOut).duration(400).playOn(determinateRoadRunner1);
//                YoYo.with(Techniques.FadeOut).duration(400).playOn(determinateRoadRunner2);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ivPas.setVisibility(View.VISIBLE);
                        YoYo.with(Techniques.FadeIn).duration(400).playOn(ivPas);

                    }
                },200);
                finishedAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        progressAnimator.start();
    }

    private void finishedAnimation(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                float tmpWidth = (float) ivPas.getWidth();
                float tmpHeight = (float) ivPas.getHeight();
                float tujuanX = ivPas.getX()-(((ivPas.getWidth()*3)/4) - Utils.convertDpToPx(EntryActivity.this,8));
                float tujuanY = ivTypographPas.getPivotY();
                ivPas.animate()
                        .setDuration(400)
                        .scaleX(0.75f)
                        .scaleY(0.75f)
                        .x(tujuanX);
                ivTypographPas.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.FadeInRight).duration(400).playOn(ivTypographPas);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        namaAplikasi.setVisibility(View.VISIBLE);
                        namaAplikasi.setText(getResources().getString(R.string.app_name));
                        YoYo.with(Techniques.SlideInUp).duration(400).playOn(namaAplikasi);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                YoYo.with(Techniques.FadeOut).duration(500).playOn(introLayout);
                                new Handler().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        switchLayout(true);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
//                                                Intent i = new Intent(getApplicationContext(),ProjectsActivity.class);
//                                                startActivity(i);
                                                cekAdaIsi();
                                            }
                                        },1200);
                                    }
                                });
                            }
                        },1800);
                    }
                },1200);

            }
        },800);
    }

    public void switchLayout(boolean isSplashVisible){

        if(isSplashVisible){
            introLayout.setVisibility(View.GONE);
            splashLayout.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.FadeIn).duration(100).playOn(splashLayout);
        }else{
            splashLayout.setVisibility(View.GONE);
            introLayout.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.FadeInUp).duration(100).playOn(introLayout);

        }
    }



    private void cekAdaIsi(){
        JSONObject jModule = GlobalHelper.getModule();
        if(jModule == null){
            Toast.makeText(PSAApp.getContext(),"Anda tidak memiliki akses Ke PSA Android App ! Mohon buka GIS Admin Apps pada Mode Online",Toast.LENGTH_LONG).show();
            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SpannableString spanPesan = null;
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            Log.e("sisahari", String.valueOf(sisaHari));
            if(sisaHari <= 0 ){
//                Toast.makeText(GenodermaApp.getContext(),getResources().getString(R.string.token_expired),Toast.LENGTH_SHORT).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage(GlobalHelper.GIS_ADMIN_APPS);
                if(intent!=null){
                    WidgetHelper.showOKDialog(this,
                            "Warning!",
                            "Token anda telah expired! harap memperbarui melalui Admin Apps",
                            (dialog, which) -> {
                                startActivity(intent);
                                dialog.dismiss();
                                finish();
//                                    finish();
                            }
                    );
                    return;

                }else{
                    Toast.makeText(getApplicationContext(), "Token anda telah berakhir! Silahkan download GIS Admin Apps di google playstore", Toast.LENGTH_SHORT).show();
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + GlobalHelper.GIS_ADMIN_APPS)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + GlobalHelper.GIS_ADMIN_APPS)));
//                    }
                    return;

                }
            }else if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS) {
                String redWord = sisaHari + " Hari Lagi";
                String pesan = String.format("Token Anda Akan Habis Dalam Waktu %,d Hari Lagi\n" +
                        "Harap Extand Token Di Admin Apps.\n" +
                        "Buka Admin Apps ?", sisaHari);
                spanPesan = new SpannableString(pesan);
                spanPesan.setSpan(new ForegroundColorSpan(Color.RED),
                        spanPesan.toString().indexOf(redWord),
                        spanPesan.toString().indexOf(redWord) + redWord.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                WidgetHelper.showOKDialog2(this,
                        "Warning !",
                        spanPesan,
                        (dialog, which) -> {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                        Log.i(TAG, "run: heres");
                                    Intent intent = new Intent(EntryActivity.this,ProjectsActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            },1200);
                            dialog.dismiss();
//                                    finish();
                        }
                );
                return;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(PSAApp.getContext(),"Get User Gagal",Toast.LENGTH_SHORT);
            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return;
        }

        if(!GlobalHelper.isTimeAutomatic(EntryActivity.this)){
            if(!BuildConfig.BUILD_VARIANT.equals("dev")) {
                AlertDialog alertDialog = WidgetHelper.showOKCancelDialog(EntryActivity.this,
                        "Aktifkan Tanggal & Waktu Otomatis",
                        "Mohon aktifkan konfigurasi tanggal & waktu otomatis untuk melanjutkan menggunakan aplikasi",
                        "Tutup",
                        "Buka Pengaturan",
                        false,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                                finish();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(EntryActivity.this, "Anda harus mengaktifkan tanggal otomatis", Toast.LENGTH_SHORT).show();
                                EntryActivity.this.finish();
                            }
                        });

                alertDialog.show();
                return;
            }
        }

        Intent intent = new Intent(EntryActivity.this,ProjectsActivity.class);

        if(spanPesan != null){
            AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                    .setCancelable(false)
                    .setTitle("Perhatian")
                    .setMessage(spanPesan)
                    .setNegativeButton("Tidak", (dialog, which) -> {
                        dialog.dismiss();
                        startActivity(intent);
                        finish();
                    })
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                                }
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .create();
            alertDialog.show();
        }else {
            startActivity(intent);
            finish();
        }
    }

    public void intentDownload(){
        Intent i = new Intent(this, DownloadActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("Result permission " + requestCode, permissions.toString() +" "+ grantResults.toString());
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        isPermissionOK = true;
        for(int codeResult:grantResults){
            isPermissionOK &= codeResult == PackageManager.PERMISSION_GRANTED;
        }
        if(isPermissionOK){
            screeningApplication();
        }else{
            showOKDialog(this,
                    "Kesalahan",
                    "Mohon berikan izin!",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    }
            );
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(isFinishedButPaused){
            switchLayout(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    cekAdaIsi();
                }
            },1200);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ArrayList<String> permissionList = new ArrayList<>();

        if(!checkPermission(Manifest.permission.INTERNET)) permissionList.add(Manifest.permission.INTERNET);
        if(!checkPermission(Manifest.permission.ACCESS_NETWORK_STATE))permissionList.add(Manifest.permission.ACCESS_NETWORK_STATE);
        if(!checkPermission(Manifest.permission.ACCESS_WIFI_STATE))permissionList.add(Manifest.permission.ACCESS_WIFI_STATE);
        if(!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE))permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(!checkPermission(Manifest.permission.CAMERA))permissionList.add(Manifest.permission.CAMERA);
        if(permissionList.size()==0) isFinishedButPaused = true;
    }

//    public class LongOperation extends AsyncTask<String, String, String> {
//        @Override
//        protected String doInBackground(String... strings) {
//            try {
//                switch (Integer.parseInt(strings[0])) {
//                    case LongOperation_SetUpAllMapFilesByEstate:
//                        JSONArray jsonArray = dataRespon;
//                        Gson gson = new Gson();
//                        Maps mapSelected = null;
//                        for(int i = 0 ;i < jsonArray.length();i++) {
//                            Maps map = gson.fromJson(jsonArray.get(i).toString(),Maps.class);
//                            if(map != null) {
//                                if (map.getPrefixName().toUpperCase().equals("GEODATABASE")) {
//                                    mapSelected = map;
//                                    break;
//                                }
//                            }
//                        }
//
//                        if(mapSelected != null){
//                            String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_GEODATABASE);
//                            File file = new File(path);
//
//                            if(file.exists()) {
//                                if (file.lastModified() < Long.parseLong(mapSelected.getLastModified())) {
//                                    file.delete();
//                                    return strings[0];
//                                }
//                            }
//                        }
//                        break;
//                }
//                return null;
//            } catch (Exception e) {
//                return null;
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            if(result == null){
////                intentMap();
//            } else {
//                switch (Integer.parseInt(result)) {
//                    case LongOperation_SetUpAllMapFilesByEstate:
//                        Toast.makeText(EntryActivity.this,"File Geodatabase anda tidak update",Toast.LENGTH_LONG).show();
////                        intentDownload();
//                        break;
//                }
//            }
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        PSAApp.deleteCache(this);
    }
    public static AlertDialog showOKDialog(Context context,
                                           String title,
                                           String message,
                                           DialogInterface.OnClickListener onClickListener){
//        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_upload_dialog_layout,null);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }


}