package id.co.ams_plantation.plantation_survey_app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.btn)
    Button pas;
    @BindView(R.id.btn2)
    Button rep;
    @BindView(R.id.btn1)
    Button wtw;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footer)
    CardView footer;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    private String userID, estateCode, userFullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();

//        mAssetViewModel.streamHistoryBRD11().observe(this,data ->{
//                Log.e("totalHBRD1",String.valueOf(data.size()));
//
//        });
//        GlobalHelper.getCompanies();
        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName()  + " "
                + GlobalHelper.getEstate().getEstName());

        pas.setOnClickListener(v ->
        {
            Intent i = new Intent(getApplicationContext(),MapInventoryActivity.class);
            startActivity(i);
        });
        rep.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), MapReplantingActivity.class);
            startActivity(i);
        });

        wtw.setOnClickListener(v -> {
            Intent i = new Intent(getApplicationContext(), ProjectsActivity.class);
            startActivity(i);
        });


    }
}