package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.SliderLayout;
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseInfo;
import com.esri.arcgisruntime.concurrent.Job;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.ArcGISFeature;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.data.Geodatabase;
import com.esri.arcgisruntime.data.GeodatabaseFeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.GeometryType;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.io.RequestConfiguration;
import com.esri.arcgisruntime.layers.ArcGISTiledLayer;
import com.esri.arcgisruntime.layers.ArcGISVectorTiledLayer;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.layers.KmlLayer;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.layers.LayerContent;
import com.esri.arcgisruntime.layers.LegendInfo;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.GeoElement;
import com.esri.arcgisruntime.mapping.MobileMapPackage;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.IdentifyLayerResult;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.mapping.view.SketchEditConfiguration;
import com.esri.arcgisruntime.mapping.view.SketchEditor;
import com.esri.arcgisruntime.ogc.kml.KmlDataset;
import com.esri.arcgisruntime.security.UserCredential;
import com.esri.arcgisruntime.symbology.Renderer;
import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleRenderer;
import com.esri.arcgisruntime.symbology.Symbol;
import com.esri.arcgisruntime.symbology.UniqueValueRenderer;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.GeodatabaseSyncTask;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.SyncLayerOption;
import com.esri.arcgisruntime.util.ListenableList;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FilterHGUAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.OptionAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.SelectionAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Optionlayer;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.services.ApiClient;
import id.co.ams_plantation.plantation_survey_app.services.ApiServices;
import id.co.ams_plantation.plantation_survey_app.services.model.PostImage;
import id.co.ams_plantation.plantation_survey_app.utils.ActivityLoggerHelper;
import id.co.ams_plantation.plantation_survey_app.utils.CompassHelper;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.SequenceNumber;
import id.zelory.compressor.Compressor;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapHguActivity extends  BaseActivity implements SelectionAdapter.ItemClickListener{
    private static final String TAG = "Test App";
    private MobileMapPackage mMapPackage;
    ViewGroup mCalloutContent;
    FragmentManager fm = getSupportFragmentManager();
    Dialog sweetAlert;
    public ArrayList<File> ALselectedImage = new ArrayList<>();
    public File selectedImage = null;
    ArcGISVectorTiledLayer vectorTiledLayer;
    ArcGISTiledLayer tiledLayer;
    public LocationDisplay mLocationDisplay;
    //    ViewAttributePoint viewAttributePoint;
    ViewFormHGU viewFormObject;
    ViewFormCheckHGU viewFormCheckHGU;
    ViewFormRealize viewFormRealize;
    String sequenceSession, sessionId, sessionName;
    public String locKmlMap;
    String global_id = "";
    String oldFeatureId;
    public Feature featureTemp;
    KmlLayer kmlLayer;
    List<Optionlayer> optionLayers;
    List<FeatureLayer> featureLayers;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.fabMap)
    FloatingActionButton fab_gps;
    @BindView(R.id.fabLayer)
    FloatingActionButton fab_layer;
    @BindView(R.id.fab)
    FloatingActionButton fab_extend;
//    @BindView(R.id.sliderLayout)
//    SliderLayout sliderLayout;
    @BindView(R.id.fab2)
    FloatingActionButton fab_versioning;
    @BindView(R.id.recycle_layer)
    RecyclerView rv_layer;
    @BindView(R.id.recycle_option)
    RecyclerView rv_option;
    @BindView(R.id.view_setting)
    LinearLayoutCompat view_setting;
    @BindView(R.id.view_option)
    LinearLayoutCompat view_option;
    @BindView(R.id.button2)
    Button mGeodatabaseButton;
    @BindView(R.id.iv_target)
    ImageView mTarget;
    @BindView(R.id.bt_edit)
    Button finish;
    @BindView(R.id.pointButton)
    ImageButton mPointButton;
    @BindView(R.id.pointsButton)
    ImageButton mMultiPointButton;
    @BindView(R.id.polylineButton)
    ImageButton mPolylineButton;
    @BindView(R.id.polygonButton)
    ImageButton mPolygonButton;
    @BindView(R.id.freehandLineButton)
    ImageButton mFreehandLineButton;
    @BindView(R.id.freehandPolygonButton)
    ImageButton mFreehandPolygonButton;
    @BindView(R.id.pointAdd)
    ImageButton mPointAdd;
    @BindView(R.id.pointsAdd)
    ImageButton mPointsAdd;
    @BindView(R.id.done)
    ImageButton bt_stop;
    @BindView(R.id.undo)
    ImageButton bt_undo;
    @BindView(R.id.redo)
    ImageButton bt_redo;
    @BindView(R.id.toolbarInclude)
    ConstraintLayout toolbar;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbarAdd;
    @BindView(R.id.fab3)
    FloatingActionButton fabAdd;
    @BindView(R.id.action)
    LinearLayout l_action;
    @BindView(R.id.tv_coordinate)
    TextView tv_cor;
    @BindView(R.id.viewDistance)
    TableLayout tb_distance;
    @BindView(R.id.tv_dis_manual)
    TextView tv_dis;

    @BindView(R.id.tv_deg_manual)
    TextView tv_deg;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footer)
    CardView footer;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    @BindView(R.id.bt_end_session)
    Button bt_end_session;
    @BindView(R.id.bmb)
    BoomMenuButton bmb;
    ArrayList<LayerMap> layers;
    ArrayList<Bitmap> bitmaps;
    SelectionAdapter adapter;
    FilterHGUAdapter adapter2;
    static boolean isModeAdd = false;
    private String userID, estateCode, userFullName;
    FeatureLayer featureLayer;
    Callout mCallout;
    public List<EditFeature> editFeatures;
    public EditState mCurrentEditState;

    public OpenCam openCamState;
    private List<Feature> mSelectedFeatures;
    public Feature selectedFeature, objPatok, eventPatok;
    private GeodatabaseSyncTask mGeodatabaseSyncTask;
    public FeatureTable mFeatureTable;
    private Geodatabase mGeodatabase;
    private GraphicsOverlay mGraphicsOverlay;
    private FeatureLayer mFeatureLayer;
    private android.graphics.Point mClickPoint;
    String surveyor, name, jenis;
    private ArcGISFeature mSelectedArcGISFeature;
    Iterator<Feature> iteratorFilterLayer;
    private Feature featureResult;
    private boolean mFeatureUpdated;
    private String mSelectedArcGISFeatureAttributeValue;
    private static PointCollection points;
    private static Point pointTemp;
    public CompassHelper ch;
    public RelativeLayout layoutCompass;
    public ImageView ivCompass, ivCompassLight;
    public PulsatorLayout pulsatorLayout;
    @BindView(R.id.sniper)
    AppCompatImageView sniper;
    @BindView(R.id.base_info)
    CardView base_info;
    @BindView(R.id.session_name)
    TextView session_name;
    private SimpleMarkerSymbol mPointSymbol;
    private SimpleLineSymbol mLineSymbol;
    private SimpleFillSymbol mFillSymbol;
    private SketchEditor mSketchEditor;
    SketchEditConfiguration configuration;
    GraphicsOverlay layerDraw;
    public double latManual, longManual;
    String groupCompanyName, companyCode;
    public static com.esri.arcgisruntime.geometry.Point pointManual;
    private static final SpatialReference wgs84 = SpatialReferences.getWgs84();
    enum OpenCam {
        FormObject, // Geodatabase has not yet been generated
        FormRealize, // A feature is in the process of being moved// The geodatabase is ready for synchronization or further edits
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_hgu);
        ButterKnife.bind(this);
        initDB();
        getLicense();
        LicenseInfo licenseInfo = LicenseInfo.fromJson(Prefs.getLicense(Prefs.LICENSE));
        Log.e("License Pref", Prefs.getLicense(Prefs.LICENSE));
        ArcGISRuntimeEnvironment.setLicense(licenseInfo);
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();
        mAssetViewModel.watchSequenceSession().observe(this, integer -> {
            sequenceSession = SequenceNumber.getSequence(integer);
        });
        fabAdd.setVisibility(View.GONE);
//        mAssetViewModel.streamHistoryBRD11().observe(this,data ->{
//                Log.e("totalHBRD1",String.valueOf(data.size()));
//
//        });
//        GlobalHelper.getCompanies();
        if (mAssetViewModel.getEstateMapping() != null) {
            companyCode = mAssetViewModel.getEstateMapping().companyCode;
            groupCompanyName = mAssetViewModel.getEstateMapping().groupCompanyName;
        }

        optionLayers = GlobalHelper.getFilterHGU();
        featureLayers = new ArrayList<>();
        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " "
                + GlobalHelper.getEstate().getEstName());
//         bottomSheetLayout = findViewById(R.id.sample_sheet2);
//         bottomSheetBehavior= BottomSheetBehavior.from(bottomSheetLayout);
        mCurrentEditState = EditState.NotReady;
        UserCredential credential = new UserCredential("app_dev", "appdev123!@#");
//        Portal portal = new Portal("https://app.gis-div.com/portal");
//        portal.setCredential(credential);
//        portal.loadAsync();
        mSelectedFeatures = new ArrayList<>();
        configuration = new SketchEditConfiguration();
        configuration.setVertexEditMode(SketchEditConfiguration.SketchVertexEditMode.INTERACTION_EDIT);
        points = new PointCollection(wgs84);
        layerDraw = new GraphicsOverlay();

        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_HGU);
        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE, Prefs.PASS_SDE));
//        mSketchEditor = new SketchEditor();
//        mapView.setSketchEditor(mSketchEditor);

        mPointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 20);
        mLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.BLUE, 4);
        mFillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.CROSS, Color.BLUE, mLineSymbol);
        ch = new CompassHelper(this);
//        readGeodatabase();
        mGeodatabaseButton.setOnClickListener(v -> {
//            generateGeodatabase();
            if (mCurrentEditState == EditState.NotReady) {
//                generateGeodatabase2();
            } else if (mCurrentEditState == EditState.Ready) {
//                syncGeodatabase();
            }
        });

        fabAdd.setOnClickListener(view -> {
//            sampleAddObject();
//            showDialogNewObject();
////            bufferRadius();

        });

        fab_layer.setOnClickListener(view -> {

            if (view_option.getVisibility() == View.VISIBLE) {
                view_option.setVisibility(View.GONE);
            } else {
                view_option.setVisibility(View.VISIBLE);
            }
        });
        setBaseMap();
        menuSetup();
        mapView.addViewpointChangedListener(viewpointChangedEvent -> {
            com.esri.arcgisruntime.geometry.Point center = mapView.getVisibleArea().getExtent().getCenter();
            com.esri.arcgisruntime.geometry.Point wgs84Point = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(center, wgs84);
            tv_cor.setText("Lat: " + String.format("%.5f", wgs84Point.getY()) + " | Long: " + String.format("%.5f", wgs84Point.getX()));
            com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
            if (mCurrentEditState == EditState.Editing) {
                float centreX = mapView.getX() + mapView.getWidth() / 2;
                float centreY = mapView.getY() + mapView.getHeight() / 2;
                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(centreX), Math.round(centreY));
                Point mapPoint = mapView.screenToLocation(screenPoint);
                Point wgs84Point2 = (Point) GeometryEngine.project(mapPoint, SpatialReferences.getWgs84());
                latManual = wgs84Point2.getX();
                longManual = wgs84Point2.getY();
            }

            if (isModeAdd) {
                tb_distance.setVisibility(View.VISIBLE);
                pointManual = pointTemp;
                double imeter = distance(gps.getY(), longManual, gps.getX(), latManual);
                double bearing = bearing(gps.getY(), longManual, gps.getX(), latManual);
                Log.e("lat1,lat2,long1,long2", "" + gps.getX() + ";" + latManual + ";" + gps.getY() + ";" + longManual);
                tv_dis.setText("Bearing : " + String.format("%.0f m", imeter));
                tv_deg.setText("Deg : " + String.format("%.0f", bearing));
                if (imeter > 30) {
                    tb_distance.setBackgroundColor(getResources().getColor(R.color.Tomato));
                    tv_dis.setTextColor(Color.WHITE);
                    tv_deg.setTextColor(Color.WHITE);
                } else {
                    tb_distance.setBackgroundColor(Color.WHITE);
                    tv_dis.setTextColor(Color.BLACK);
                    tv_deg.setTextColor(Color.BLACK);
                }
            } else {
                tb_distance.setVisibility(View.GONE);
            }

        });

        bt_end_session.setOnClickListener(v -> {
//                checkCensusSession();
            endSessionDialog(true, 0);
        });
        fab_gps.setOnClickListener(view -> {
            mLocationDisplay.setInitialZoomScale(499);
            mapView.setViewpointScaleAsync(499);
            mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
            if (!mLocationDisplay.isStarted()) {
                mLocationDisplay.startAsync();
            }

        });
        fab_extend.setOnClickListener(view -> {
            if (mapView.isViewInsetsValid() && mapView.getMap().getOperationalLayers().size() > 0) {
                mapView.setViewpoint(new Viewpoint(mapView.getMap().getOperationalLayers().get(0).getFullExtent()));
            }
        });
    }

    public static double bearing(double startLat, double endLat, double startLng,  double endLng){
        double longitude1 = startLng;
        double longitude2 = endLng;
        double latitude1 = Math.toRadians(startLat);
        double latitude2 = Math.toRadians(endLat);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }
    private void showDialogNewObject() {
        FragmentManager fm = getSupportFragmentManager();
        DialogChooseObject basemapDialog = DialogChooseObject.newInstance(this);
        basemapDialog.show(fm, "Basemap");
    }
    public void menuSetup() {
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setBackgroundEffect(true);
        bmb.clearBuilders();
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);
        bmb.addBuilder(syncBMB());
        bmb.addBuilder(newCencus());
        bmb.addBuilder(hasilCensus());
        bmb.addBuilder(baseMap());
        bmb.addBuilder(upBMB());
    }

    public static double distance(double lat1, double lat2, double lon1,double lon2) {
        final int R = 6371; // Radius of the earth
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        distance = Math.pow(distance, 2);
        return Math.sqrt(distance);
    }
    protected void setBaseMap() {
        String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_TPK);
        tiledLayer = new ArcGISTiledLayer(path);
        String vtpk = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
        layers = new ArrayList<>();
        bitmaps = new ArrayList<>();
//        String path2 = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
        vectorTiledLayer = new ArcGISVectorTiledLayer(vtpk);
//        String pathS = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
//        Log.e("path location",pathS);
//        mMapPackage = new MobileMapPackage(pathS);
//        mMapPackage.loadAsync();
        ArcGISMap arcGISMap = new ArcGISMap(new Basemap(vectorTiledLayer));
        ArcGISMap mMap2 = new ArcGISMap(new Basemap(tiledLayer));
        mapView.setMap(arcGISMap);
//        mapView.setMap(mMap2);
        mLocationDisplay = mapView.getLocationDisplay();
        if (!mLocationDisplay.isStarted())
            mLocationDisplay.startAsync();
        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        if (locKmlMap != null) {
            KmlDataset kmlDataset = new KmlDataset(locKmlMap);
            kmlLayer = new KmlLayer(kmlDataset);
//            kmlLayer.setOpacity(0.03f);
            mapView.getMap().getOperationalLayers().add(kmlLayer);
            kmlLayer.setVisible(false);
            kmlDataset.addDoneLoadingListener(() -> {
                if (kmlDataset.getLoadStatus() != LoadStatus.LOADED) {
                    String error = "Failed to load kml layer from URL: " + kmlDataset.getLoadError().getMessage();
                    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//                    Log.e(TAG, error);
                }
            });
        }
        readGeodatabase();
        checkCensusSession();
    }

    private HamButton.Builder syncBMB() {
        return new HamButton.Builder()
                .normalText("Sync Data")
                .subNormalText("Synchronize data with server")
                .normalImageRes(R.drawable.refresh)
                .pieceColorRes(R.color.Tomato)
                .normalColorRes(R.color.Tomato)
                .listener(index -> {
                    showUploadSyncDialog();
//                    uploadImage();
//                    syncBidiectional();
                    ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBmbSync",new Date(),new Date(),this);
//                    syncGeodatabase();
//                    backupGDB();
//                    showUploadSyncDialog();
                });
    }

    private HamButton.Builder upBMB() {
        return new HamButton.Builder()
                .normalText("Generate Geodatabase")
                .subNormalText("Redownload Geodatabase")
                .normalImageRes(R.drawable.ic_baseline_sync_24)
                .pieceColorRes(R.color.WhiteTrans)
                .normalColorRes(R.color.WhiteTrans)
                .listener(index -> {
                    if(isNetworkConnected()){
//                        openVersioning();
                        dialogAction("Proses Generate akan menghapus semua data yang ada, pastikan data anda di upload dahulu!","Yakin ingin generate ulang data?");
//                        generateGeodatabase();
                    }else {
                        alertDialog(true);
                    }
                    ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBmbGenerate",new Date(),new Date(),this);

//                        insertSessionSurveys();
//                    syncGeodatabase();
//                    showBottomSheet();
                });
    }

    private HamButton.Builder baseMap() {
        return new HamButton.Builder()
                .normalText("Base Map")
                .subNormalText("Pilih basemap aplikasi")
                .normalImageRes(R.drawable.map)
                .pieceColorRes(R.color.Green)
                .normalColorRes(R.color.Green)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBmbBaseMap",new Date(),new Date(),this);
                    Intent intent = new Intent(this, DownloadActivity.class);
                    startActivity(intent);
                });
    }
    private HamButton.Builder newCencus() {
        return new HamButton.Builder()
                .normalText("Create New Session")
                .subNormalText("Buat sesi untuk memulai Survei")
                .normalImageRes(R.drawable.sketchbook)
                .pieceColorRes(R.color.YellowGreen)
                .normalColorRes(R.color.YellowGreen)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBmbNewSession",new Date(),new Date(),this);
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_HGU)) {
                        endSessionDialog(true, 0);
                    } else {
                        newSession();
                    }
                });
    }

    private void showUploadSyncDialog() {
        long longDate = Prefs.getLong(Prefs.LAST_SYNC_DATA);
        Date theDate = new Date(longDate);
//        DataToUpload data = mSessionViewModel.getCountSessionToUpload();
        DialogUpload dialogUploadSync = DialogUpload.newInstance(this, theDate, "0", "0");
        if (isNetworkConnected()) {
            dialogUploadSync.show(fm, "");
        } else {
            alertDialog(true);
        }
    }

    public void endSessionDialog(boolean param, int from) {
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(MapHguActivity.this);
        builder.setMessage("Apakah anda yakin akhiri sesi ini?");
        builder.setTitle("End Session");
        builder.setCancelable(false);
        builder.setPositiveButton("Oke", (dialog, which) -> {
            ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btEndSessionOk",new Date(),new Date(),this);

//            mAssetViewModel.updateEndSession(Prefs.getString(Prefs.SESSION_ID),addTimeStamp().getTime());
            if (from == 1) {
                Prefs.setPrefs()
                        .putString(Prefs.SESSION_ID_HGU, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION_HGU, false)
                        .putString(Prefs.BLOCK, "")
                        .apply();
                dialog.cancel();
                checkCensusSession();
                uploadImage();
//                loading.show();
//                uploadData();
            } else {
                Prefs.setPrefs()
                        .putString(Prefs.SESSION_ID_HGU, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION_HGU, false)
                        .putString(Prefs.BLOCK, "")
                        .apply();
                dialog.cancel();

                checkCensusSession();
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();
        if (param) {
            alertDialog.show();
        } else {
            alertDialog.dismiss();
        }
    }

    public void uploadImage(){
        showLoadingAlert("Uploading", "Please wait..");
        List<PostImage> toUpload = new ArrayList<>();
        List<ImagesUpload> images = mAssetViewModel.getAllImageUpload();
        Log.e("Count Images", String.valueOf(images.size()));
        for( ImagesUpload image : images){
            Log.e("Count Images", String.valueOf(image.getImageId()));
            Log.e("Count Images", String.valueOf(image.getLocation()));
            File file = new File(image.getLocation());
            if (file.exists()) {
                String nameFile = image.getImageId();
//            String nameImg = nameFile.substring(0, nameFile.lastIndexOf("."));
                PostImage newPost = new PostImage(
                        nameFile,
                        image.getSessionSurvey(),
                        GlobalHelper.getBase64FromFile(image.getLocation()),
                        ""
                );
                toUpload.add(newPost);
            } else {
                Log.d("FileCheck", "File does not exist");
            }

        }
        ApiServices apiServices = ApiClient.getClient().create(ApiServices.class);
        Call<ResponseBody> call = apiServices.postImages(toUpload);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    images.forEach(c->c.setIsUpload(2));
                    mAssetViewModel.updateList(images);
//                    ALselectedImage.clear();
                    syncBidiectional();
                    hideLoadingAlert();
                }
                else {
                    Log.e("errorBody", response.message());
//                    assert response.errorBody() != null;
                    Log.e("errorBody", response.errorBody().toString());
                    try {
                        hideLoadingAlert();
                        showAlert("Failed",response.errorBody().string(),SweetAlertDialog.ERROR_TYPE);

                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideLoadingAlert();
                showAlert("Failed",t.getMessage(),SweetAlertDialog.ERROR_TYPE);

            }
        });
    }

    public void newSession() {
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_HGU.geodatabase");
        if (!fileLoc.exists()) {
            showAlert("Geodatabase Missing!", "Harap Generate Geodatabase sebelum survei", SweetAlertDialog.WARNING_TYPE);
            return;
        }
        sessionId = DateTime.getStringLongDate() + "_" + estateCode + "_" + userID + "_" + "HGU" + "_" + "PSA" + "_" + sequenceSession;
//        String survId = DateTime.getStringLongDate()+"_"+GlobalHelper.getEstate().getEstCode()+"_"+GlobalHelper.getUser().getUserID()+"_AWM_PSA_001";
        sessionName = estateCode + "_" + userID + "_" + "PSA" + "_" + sequenceSession;
        List<String> a = new ArrayList<>();
        FragmentManager fm = getSupportFragmentManager();
        DialogNewSession dialogSessionFragment = DialogNewSession.newInstance(this, a, sessionId, sessionName, DateTime.getStringDate(), DateTime.getStringTime(), userFullName);
        dialogSessionFragment.show(fm, "");
    }

    private void dialogAction(String message, String title) {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this,SweetAlertDialog.WARNING_TYPE);
        exitAlert.setTitle(title);
        exitAlert.setContentText(message);
        exitAlert.setConfirmButton("Generate", sweetAlertDialog -> {
            exitAlert.dismiss();
            generateGeodatabase();
        });
        exitAlert.setCancelButton("Batal",n ->{exitAlert.dismiss();});
        exitAlert.show();
    }


    public void setCensusSessionActive(String _sessionId) {
        saveSession(_sessionId);
//        checkCensusSession();
    }
    private void saveSession(String _sessionId) {
        Prefs.setPrefs()
                .putString(Prefs.SESSION_ID_HGU, _sessionId)
                .putBoolean(Prefs.ACTIVE_SESSION_HGU, true)
                .apply();
        Session toSave = new Session(_sessionId, sessionName,
                userID, Calendar.getInstance().getTime(), userID,
                Calendar.getInstance().getTime(), estateCode, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "", userFullName, "1",GlobalHelper.getAppVersion(this));
        mAssetViewModel.insertSession(toSave);
        checkCensusSession();
    }

    private HamButton.Builder hasilCensus() {
        return new HamButton.Builder()
                .normalText("Statistik")
                .subNormalText("Statistik hasil survei")
                .normalImageRes(R.drawable.stadistics)
                .pieceColorRes(R.color.CornflowerBlue)
                .normalColorRes(R.color.CornflowerBlue)
                .listener(index -> {
//                   FeatureLayer featureL = (FeatureLayer) mapView.getMap().getOperationalLayers().get(2);
//                    getGlobalID(featureL.getFeatureTable());
                            ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBMBStatistic",new Date(),new Date(),this);
                            Intent i = new Intent(getApplicationContext(), ResultCensusActivity.class);
                            startActivity(i);
//                    Intent intent = new Intent(getApplicationContext(), MenuResultActivity.class);
//                    startActivityForResult(intent, 1898);
                        }
                );
    }

    public void generateGeodatabase() {
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
//        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE,Prefs.PASS_SDE));
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_HGU.geodatabase");
        File fileBackup = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/HGU/"+"backup");
        if (!fileBackup.exists()) {
            fileBackup.mkdirs();
        }
        final String path = fileLoc.getAbsolutePath();
        mGeodatabaseSyncTask.loadAsync();
        mGeodatabaseSyncTask.addDoneLoadingListener(() -> {
            Log.e("versionSDE", mGeodatabaseSyncTask.getFeatureServiceInfo().getVersion());
//            final Envelope extent = mapView.getVisibleArea().getExtent();
//            double minX = 94.97; // Minimum X-coordinate (longitude) for the extent
//            double minY = -11.00; // Minimum Y-coordinate (latitude) for the extent
//            double maxX = 141.00; // Maximum X-coordinate (longitude) for the extent
//            double maxY = 6.00;
//            Envelope envelope = new Envelope(minX, minY, maxX, maxY, wgs84);
            final Envelope extentFull = mGeodatabaseSyncTask.getFeatureServiceInfo().getFullExtent();
            final ListenableFuture<GenerateGeodatabaseParameters> defaultParameters = mGeodatabaseSyncTask
                    .createDefaultGenerateGeodatabaseParametersAsync(extentFull);
            defaultParameters.addDoneListener(() -> {
                try {
                    // set parameters and don't include attachments
                    GenerateGeodatabaseParameters parameters = defaultParameters.get();
                    parameters.setReturnAttachments(false);
//                    parameters.setSyncModel(SyncModel.PER_GEODATABASE);
                    // define the local path where the geodatabase will be stored
//                    final String localGeodatabasePath = getCacheDir() + "/file_inventory.geodatabase";
                    Log.e("patchLoc", path);
                    // create and start the job
                    if (fileLoc.exists()) {
                        GlobalHelper.moveFile(fileLoc, fileBackup, DateTime.getStringDate());
                    }
                    final GenerateGeodatabaseJob generateGeodatabaseJob = mGeodatabaseSyncTask
                            .generateGeodatabase(parameters, path);
                    RequestConfiguration requestConfiguration = new RequestConfiguration();
                    requestConfiguration.setConnectionTimeout(300000);
                    requestConfiguration.setSocketTimeout(300000);
                    requestConfiguration.setMaxNumberOfAttempts(5);
                    generateGeodatabaseJob.setRequestConfiguration(requestConfiguration);
//                    String timeout = String.valueOf(generateGeodatabaseJob.getRequestConfiguration().getConnectionTimeout());
//                    Log.e("timeout",timeout);
                    generateGeodatabaseJob.start();
                    createProgressDialog(generateGeodatabaseJob);
                    // get geodatabase when done
                    generateGeodatabaseJob.addJobDoneListener(() -> {
                        if (generateGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                            mGeodatabase = generateGeodatabaseJob.getResult();
                            mGeodatabase.loadAsync();
                            mGeodatabase.addDoneLoadingListener(() -> {
//                                Toast.makeText(this, mGeodatabase.get)
                                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                                    featureLayers.clear();
                                    // get only the first table which, contains points
                                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                                        table.loadAsync();
//                                    GeodatabaseFeatureTable pointsGeodatabaseFeatureTable = mGeodatabase
//                                            .getGeodatabaseFeatureTables().get(0);
//                                    pointsGeodatabaseFeatureTable.loadAsync();
                                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                                        featureLayers.add(geodatabaseFeatureLayer);
                                        Log.e("Layer GDB name", table.getTableName());
                                        // add geodatabase layer to the map as a feature layer and make it selectable
                                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                                    }
//                                    mapView.getMap().getOperationalLayers().addAll(featureLayers);
                                    mGeodatabaseButton.setVisibility(View.GONE);
                                    int layerTouch = mapView.getMap().getOperationalLayers().size() - 1;
                                    Log.e("Feature layer", String.valueOf(layerTouch));
                                    Log.e("mapView size", mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size() - 1).getName());
                                    featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(layerTouch);
//                                    Log.i("689", "Local geodatabase stored at: " + localGeodatabasePath);
//                                    applyDefinition();
//                                    touchListener();
//                                    touchListenerEdit();
                                } else {
                                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                                }
                            });
                            // set edit state to ready
                            mCurrentEditState = EditState.Ready;
                        } else if (generateGeodatabaseJob.getError() != null) {
                            Log.e("err clause", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause());
                            Log.e("err mesage", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getMessage());
                            Log.e("err domain", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getErrorDomain());

                            Toast.makeText(this,
                                    "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("703", "Unknown Error generating geodatabase");
                            Toast.makeText(this, "Unknown Error generating geodatabase", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("708", "Error generating geodatabase parameters : " + e.getMessage());
                    Toast.makeText(this, "Error generating geodatabase parameters: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    public void
    readGeodatabase() {
        copyGDB();
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + GlobalHelper.FILE_HGU);
//        File fileLoc  = new File(getCacheDir() + "/file_inventory.geodatabase");
        Log.e("ReadGDB", fileLoc.getAbsolutePath());
        List<Layer> layerLegend = new ArrayList<>();
        if (fileLoc.exists()) {
            Log.e("File exist", "true");
//            mGeodatabase = new Geodatabase(getCacheDir() + "/file_inventory.geodatabase");
            mGeodatabase = new Geodatabase(fileLoc.getAbsolutePath());
            mGeodatabase.loadAsync();
            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    int counter = 0;
                    // get only the first table which, contains points
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                        table.loadAsync();
                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                        Log.e("Layer GDB name", table.getTableName());
                        // add geodatabase layer to the map as a feature layer and make it selectable
                        layerLegend.add(geodatabaseFeatureLayer);
                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                        featureLayers.add(geodatabaseFeatureLayer);
//                        QueryParameters query = new QueryParameters();
//                        query.setWhereClause("survey_date=null AND QC=3");
                    }
//                    mapView.getMap().getOperationalLayers().addAll(featureLayers);
                    float density = MapHguActivity.this.getResources().getDisplayMetrics().density;
//                    for(Layer layer : mapView.getMap().getOperationalLayers()){
                    for (Layer layer : layerLegend) {

                        counter = counter++;
                        List<LegendInfo> a;
                        LayerMap layerMap = new LayerMap(counter - 1, true, layer.getName());
                        layers.add(layerMap);
//                        FeatureLayer featureL = (FeatureLayer) layer;
                        try {
                            FeatureLayer fl = (FeatureLayer)layer;
                            Renderer renderer = fl.getRenderer();
                            if (renderer != null ) {
                                UniqueValueRenderer uniqueVal = (UniqueValueRenderer) renderer;
                                List<UniqueValueRenderer.UniqueValue> uniqueValues = uniqueVal.getUniqueValues();
                                Log.e("uniquer", String.valueOf("test"));

                                // Gunakan uniqueValues sesuai kebutuhan Anda
                            } else {
                                // Handler jika renderer tidak ditetapkan atau bukan UniqueValueRenderer
                            }
                            if (renderer instanceof UniqueValueRenderer) {
                                UniqueValueRenderer uniqueValueRenderer = (UniqueValueRenderer) renderer;
                                uniqueValueRenderer.getUniqueValues();
//                                List<UniqueValueRenderer.UniqueValue>
//                                ((UniqueValueRenderer) renderer).getDefaultSymbol().toJson();
                                List<UniqueValueRenderer.UniqueValue> uniqueValues = uniqueValueRenderer.getUniqueValues();
                                Log.e("uniquerenderer", String.valueOf(uniqueValues.size()));
                                for (UniqueValueRenderer.UniqueValue uniqueValue : uniqueValues) {
                                    Symbol symbol = uniqueValue.getSymbol();
                                    ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24, density, Color.TRANSPARENT);
                                    Bitmap swatch = symbolSwatch.get();
                                    bitmaps.add(swatch);
                                    // Lakukan sesuatu dengan simbol
                                }
                            } else if (renderer instanceof SimpleRenderer) {
                                SimpleRenderer simpleRenderer = (SimpleRenderer) renderer;
                                Symbol symbol = simpleRenderer.getSymbol();
                                ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24, density, Color.TRANSPARENT);
                                Bitmap swatch = symbolSwatch.get();
                                bitmaps.add(swatch);
                                // Lakukan sesuatu dengan simbol
                            }
//                            ListenableList<LayerContent> layerContentFuture = layer.getSubLayerContents();
//                            for (LayerContent layerContent : layerContentFuture){
//                                a = layerContent.fetchLegendInfosAsync().get();
//                                Symbol symbol = a.get(0).getSymbol();
//                                ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24, density, Color.TRANSPARENT);
//                                Bitmap swatch = symbolSwatch.get();
//                                bitmaps.add(swatch);
//                            }
//                            a = layer.fetchLegendInfosAsync().get();
//                            Symbol symbol = a.get(0).getSymbol();
//                            ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24, density, Color.TRANSPARENT);
//                            Bitmap swatch = symbolSwatch.get();
//                            bitmaps.add(swatch);
                        } catch (ExecutionException | InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        Log.e("", String.valueOf(layer.getName()));
                    }
                    Log.e("size Layer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    Log.e("size Bitmap", String.valueOf(bitmaps.size()));

                    mapView.getMap().getLoadSettings().setPreferredPolylineFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
                    layerDraw = new GraphicsOverlay();
                    mapView.getGraphicsOverlays().add(layerDraw);
//                    adapter = new SelectionAdapter(this, layers, bitmaps);
                    adapter2 = new FilterHGUAdapter(this, optionLayers);
//                    adapter.setClickListener(this);
//                    adapter2.setClickListener(this);
//                adapter.setData(layers);
//                    rv_layer.setAdapter(adapter);
                    rv_option.setAdapter(adapter2);
//                    int layerTouch = mapView.getMap().getOperationalLayers().size() -1;
                    Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    Log.e("Option size", GlobalHelper.getOptionLayers().size() + "");
//                    featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(layerTouch);
                    applyDefinition();
                    touchListener();
                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                }
            });
            // set edit state to ready
            mCurrentEditState = EditState.Ready;
        } else {
            Log.e("fileNotExist", "true");
        }
    }

    public void applyDefinition() {
        String def = "GlobalID NOT NULL AND CompanyCode = '" + companyCode + "'";
        StringBuilder sb = new StringBuilder();
        boolean firstCondition = true;

        for (Optionlayer ol : optionLayers) {
            if (ol.isChecked()) {
                if (firstCondition) {
                    sb.append(" AND (typePatok = ").append(ol.getIndex()).append(" ");
                    firstCondition = false;
                } else {
                    sb.append("OR typePatok = ").append(ol.getIndex()).append(" ");
                }
            }

            // Uncomment this section if you need to handle special cases
        /*
        if (ol.getIndex() == 9 && ol.isChecked()) {
            sb.append("AND status_asset = 2");
            fabAdd.setVisibility(View.GONE);
        }
        */
        }

        // Close the parenthesis for the typePatok condition
        if (!firstCondition) {
            sb.append(")");
        }

        for (FeatureLayer layer : featureLayers) {
            String layerDefinitionExpression = def + sb.toString();
            Log.e("DefinitionExpression", layerDefinitionExpression);
            layer.setDefinitionExpression(layerDefinitionExpression);
        }
    }


    public void checkLayer(){
        FeatureLayer fl = (FeatureLayer) mapView.getMap().getOperationalLayers().get(1);
        fl.getRenderer();
        Log.e("Renderer","ss");
    }
    private void createProgressDialog(Job job) {
        ProgressDialog syncProgressDialog = new ProgressDialog(this);
        syncProgressDialog.setTitle("Sync geodatabase job");
        syncProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        syncProgressDialog.setCanceledOnTouchOutside(false);
        syncProgressDialog.show();
        job.addProgressChangedListener(() -> {
            syncProgressDialog.setProgress(job.getProgress());
            Log.e("Progress", String.valueOf(job.getProgress()));
            Log.e("Progress", String.valueOf(job.getStatus()));
        });
//        job.addJobDoneListener(syncProgressDialog::dismiss);
        job.addJobDoneListener(() -> {
            syncProgressDialog.dismiss();
            if(job.getError()!= null){
                showAlert("Failed", "Sync geodatabase", SweetAlertDialog.ERROR_TYPE);
            }else {
                showAlert("Success", "Sync geodatabase success", SweetAlertDialog.SUCCESS_TYPE);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void touchListener() {
        mapView.setOnTouchListener(
                new DefaultMapViewOnTouchListener(MapHguActivity.this, mapView) {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
//                        featureLayer.clearSelection();
                        if (mCurrentEditState == EditState.Ready) {
//                            Log.e("featureLayer Touch","true"+featureLayer.getName());
//                            selectFeaturesAt(mapPointFrom(motionEvent), 10);
                            selectFeatures(motionEvent, 10);
//                            android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));
//                            selectFeature(screenPoint);
                        } else if (mCurrentEditState == EditState.Editing) {
//                            if (finish.getVisibility() == View.GONE) {
//                                finish.setVisibility(View.VISIBLE);
//                            }
//                            mSketchEditor.start(selectedFeature.getGeometry(),SketchCreationMode.POLYLINE, configuration );
//
                        }
                        return true;
                    }
                });
        checkLayer();
    }

    private void selectFeatures(MotionEvent motionEvent, int tolerance) {
        mCallout = mapView.getCallout();
        mCallout.dismiss();
        // define the tolerance for identifying the feature
        final double mapTolerance = tolerance * mapView.getUnitsPerDensityIndependentPixel();
        // create objects required to do a selection with a query
        android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
                Math.round(motionEvent.getY()));
        ListenableFuture<List<IdentifyLayerResult>> identifyLayerResultFuture = mapView
                .identifyLayersAsync(screenPoint, tolerance, false, -1);
        identifyLayerResultFuture.addDoneListener(() -> {
            try {
                boolean isSTK1Found = false;
                List<IdentifyLayerResult> identifyLayerResult = identifyLayerResultFuture.get();
                for (IdentifyLayerResult identifyResult : identifyLayerResult) {
                    if (Objects.equals(identifyResult.getLayerContent().getName(), "STK1")) {
                        isSTK1Found = true;
                        for (GeoElement geoElement : identifyResult.getElements()) {
                            if (geoElement instanceof Feature) {
                                Feature feature = (Feature) geoElement;
                                mSelectedArcGISFeature = (ArcGISFeature) feature;
                                Map<String, Object> attr = feature.getAttributes();
                                Set<String> keys = attr.keySet();
                                selectedFeature = feature;
                                objPatok = feature;
                                mFeatureTable = feature.getFeatureTable();
                                com.esri.arcgisruntime.geometry.Point wgs84Point2 = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(feature.getGeometry(), SpatialReferences.getWgs84());
                                com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
                                if (feature.getGeometry().getGeometryType() == GeometryType.POINT) {
                                    // Cast the geometry to Point
//                                    Point pointGeometry = (Point) feature.getGeometry();
                                    // Retrieve the x and y coordinates
//                                    double x = pointGeometry.getX();
//                                    showAlert("point", "feature" + "", SweetAlertDialog.ERROR_TYPE);
//                                    double y = pointGeometry.getY();
                                    double distance = GlobalHelper.getDistance(gps.getY(), wgs84Point2.getY(), gps.getX(), wgs84Point2.getX());
//                                    Log.e("Distance Feature", String.valueOf(distance));
                                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_HGU)) {
                                        if (distance > 30) {
                                            // Show alert if the distance is greater than 30 meters and session is active
                                            showAlert("Diluar jangkauan", "Maksimal jarak 30 meter", SweetAlertDialog.ERROR_TYPE);
                                        } else {
                                            // Show callout if the distance is within 30 meters and session is active
                                            Envelope envelope = feature.getGeometry().getExtent();
                                            mCalloutContent = CalloutHGU.setInfoFeature(this, selectedFeature, 1);
                                            mCallout.setLocation(envelope.getCenter());
                                            mCallout.setContent(mCalloutContent);
                                            mCallout.show();
                                        }
                                    } else {
                                        // Show callout directly if no active session
                                        Envelope envelope = feature.getGeometry().getExtent();
                                        mCalloutContent = CalloutHGU.setInfoFeature(this, selectedFeature,0);
                                        mCallout.setLocation(envelope.getCenter());
                                        mCallout.setContent(mCalloutContent);
                                        mCallout.show();
                                    }

                                    // Use the x and y coordinates as needed
                                    // ...
                                }

//                                showBottomSheet2(selectedFeature,1);


                            }
                        }
                    }
                    else {
                        if (!identifyResult.getElements().isEmpty()) {
                            // Ambil semua elemen yang merupakan Feature
                            List<Feature> features = identifyResult.getElements()
                                    .stream()
                                    .filter(element -> element instanceof Feature)
                                    .map(element -> (Feature) element)
                                    .collect(Collectors.toList());
                            Log.e("Size:", String.valueOf(features.size()));

                            if (!features.isEmpty()) {
                                // Urutkan berdasarkan last_edited_date descending (terbaru ke terlama)
                                features.sort(Comparator.comparing(f -> {
                                    Object obj = f.getAttributes().get("last_edited_date");
                                    return (obj instanceof Date) ? (Date) obj : new Date(0);
                                }, Comparator.reverseOrder()));

                                // Ambil fitur dengan tanggal terbaru (indeks 0 setelah sorting)
                                eventPatok = features.get(features.size()-1);
                            }
                        }
                    }
                }
//                if (!isSTK1Found) {
//                    for (IdentifyLayerResult identifyResult : identifyLayerResult) {
//                        if (!identifyResult.getElements().isEmpty()) {
//                            GeoElement topFeature = identifyResult.getElements().get(0);
//                            if (topFeature instanceof Feature) {
//                                eventPatok = (Feature) topFeature;
//                            }
//                            break; // Exit after selecting the first available feature
//                        }
//                    }
//                }


            } catch (Exception e) {
                String error = "Select features failed: " + e.getMessage();
                Toast.makeText(MapHguActivity.this, error, Toast.LENGTH_SHORT).show();
                Log.e(TAG, error);
            }
        });
//        }
    }

    public int checkEventPatok(String patokId) {
        Log.e("debug on",patokId);
        AtomicInteger count = new AtomicInteger(0);
        FeatureLayer fl = (FeatureLayer) mapView.getMap().getOperationalLayers().get(1);
        FeatureTable featureTable = fl.getFeatureTable();
//        for (Layer layer : mapView.getMap().getOperationalLayers()) {
//            if(layer instanceof FeatureLayer){
//                if(Objects.equals(layer.getName(),"STK1E")){
//                    featureTable = ((FeatureLayer) layer).getFeatureTable();
//                }
//            }
//        }
        QueryParameters queryParameters = new QueryParameters();
        // Batasi jumlah fitur yang diambil menjadi 1 (fitur terbaru)
//        queryParameters.setMaxFeatures(1);
        queryParameters.setWhereClause("patokID = '" + patokId + "'");

        ListenableFuture<FeatureQueryResult> future = featureTable.queryFeaturesAsync(queryParameters);
        future.addDoneListener(() -> {
            try {
                // Mengambil hasil query
                Log.e("debug on","true");
                FeatureQueryResult result = future.get();
                int featureCount = 0;
                for (Feature feature : result) {
                    count.incrementAndGet();
                }

            } catch (InterruptedException | ExecutionException e) {
                // Tangani kesalahan jika ada
            }
        });
        return count.get();
    }
    private void openVersioning(){
        FragmentManager fm = getSupportFragmentManager();
        DialogVersioning basemapDialog = DialogVersioning.newInstance(this);
        basemapDialog.show(fm,"Basemap");
    }

    @Override
    public void onItemClick(View view, int position) {

    }
    public void checkCensusSession() {
        if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_HGU)) {
            String sessionActive = Prefs.getString(Prefs.SESSION_ID_HGU);

            String sessionName = mAssetViewModel.getSessionName();
            Log.e("Session Pref",sessionActive);
//            String[] parts = inputString.split("_");
            session_name.setText(sessionName);
            base_info.setVisibility(View.VISIBLE);
            fabAdd.setVisibility(View.GONE);
//              refreshPointToCensus();
        } else {
            base_info.setVisibility(View.GONE);
            fabAdd.setVisibility(View.GONE);
        }
    }

    public List<String> getUserALl(){
       return mAssetViewModel.getListUser();
    }


    public void addNewEvent(Feature _feature){
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if(layer instanceof FeatureLayer){
                if(Objects.equals(layer.getName(),"STK1E")){
                    FeatureLayer featureLayer = (FeatureLayer) layer;
                    UUID uuid = UUID.randomUUID();
                    FeatureTable featureTable = featureLayer.getFeatureTable();
                    Map<String, Object> defAttribute = new HashMap<>();
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
                    String dev_loc = gps.getY() + ";" + gps.getX();
                    defAttribute.put("GlobalID", uuid);
                    defAttribute.put("PatokID", _feature.getAttributes().get("PatokID"));
                    defAttribute.put("PatokName", _feature.getAttributes().get("PatokName"));
                    defAttribute.put("CompanyCode", _feature.getAttributes().get("CompanyCode"));
                    defAttribute.put("GroupCompanyName", _feature.getAttributes().get("GroupCompanyName"));
                    defAttribute.put("block", _feature.getAttributes().get("block"));
                    defAttribute.put("ESTNR", _feature.getAttributes().get("ESTNR"));
                    defAttribute.put("Afdeling", _feature.getAttributes().get("Afdeling"));
                    defAttribute.put("typePatok", _feature.getAttributes().get("typePatok"));
                    defAttribute.put("typeMaterial", _feature.getAttributes().get("typeMaterial"));
                    defAttribute.put("survey_date", DateTime.timeStampCalendar());
                    defAttribute.put("last_edited_user", userFullName);
                    defAttribute.put("survey_by", userFullName);
                    defAttribute.put("last_edited_date", DateTime.timeStampCalendar());
                    defAttribute.put("device_loc", dev_loc);
                    defAttribute.put("created_user", userFullName);
                    defAttribute.put("created_date", DateTime.timeStampCalendar());
                    defAttribute.put("rekomendasi", "0");

//                    defAttribute.put("GroupCompanyName", groupCompanyName);
//                    defAttribute.put("created_user", userFullName);
//                    defAttribute.put("created_date", DateTime.timeStampCalendar());
//
//                    defAttribute.put("image", "");
//                    defAttribute.put("last_edited_user", userFullName);
//                    defAttribute.put("last_edited_date", DateTime.timeStampCalendar());

//                    defAttribute.put("surveyor", userFullName);
                    Feature newFeature = featureTable.createFeature(defAttribute, _feature.getGeometry());

                    selectedFeature = newFeature;
//                    saveNewFeature(newFeature);
//                    Feature newFeature = featureTable.createFeature();
//                    newFeature.setGeometry(_feature.getGeometry());
//                    saveNewFeature(newFeature);
                }
            }
        }
    }
    public void saveImage(String globalId){
        for(File file:ALselectedImage){
            String sess = Prefs.getString(Prefs.SESSION_ID_HGU);
            ImagesUpload imagesUpload = new ImagesUpload(file.getName(),globalId,file.getAbsolutePath(),sess, new Date(),1);
            mAssetViewModel.insertImageUpload(imagesUpload);
        }

    }
    public void saveNewFeature(Feature feature) {
        feature.getFeatureTable().addFeatureAsync(feature).addDoneListener(() -> {
            ALselectedImage.clear();
            Log.e("totalFeature", "" + feature.getFeatureTable().getTotalFeatureCount());
            Log.e("imageFeature", "" + feature.getAttributes().get("image"));
//            ALselectedImage.clear();
        });
        objPatok.getAttributes().put("condition",feature.getAttributes().get("condition"));

        objPatok.getFeatureTable().updateFeatureAsync(objPatok);
        mSelectedFeatures.clear();
        mCallout.dismiss();
//        mCurrentEditState = MapInventoryActivity.EditState.Ready;
//        viewAttributePoint.hideView();
//        viewFormObject.hideView();
    }

    public void syncBidiectional() {
        // create parameters for the sync task
        SyncGeodatabaseParameters syncGeodatabaseParameters = new SyncGeodatabaseParameters();
        syncGeodatabaseParameters.setSyncDirection(SyncGeodatabaseParameters.SyncDirection.BIDIRECTIONAL);
        syncGeodatabaseParameters.setRollbackOnFailure(false);
        // get the layer ID for each feature table in the geodatabase, then add to the sync job
        for (GeodatabaseFeatureTable geodatabaseFeatureTable : mGeodatabase.getGeodatabaseFeatureTables()) {
            long serviceLayerId = geodatabaseFeatureTable.getServiceLayerId();
            SyncLayerOption syncLayerOption = new SyncLayerOption(serviceLayerId);
            syncGeodatabaseParameters.getLayerOptions().add(syncLayerOption);
        }
        final SyncGeodatabaseJob syncGeodatabaseJob = mGeodatabaseSyncTask
                .syncGeodatabase(syncGeodatabaseParameters, mGeodatabase);
        syncGeodatabaseJob.start();
        createProgressDialog(syncGeodatabaseJob);
        syncGeodatabaseJob.addJobDoneListener(() -> {
            if (syncGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                Toast.makeText(this, "Sync complete", Toast.LENGTH_LONG).show();
//                mGeodatabaseButton.setVisibility(View.INVISIBLE);
            } else if (syncGeodatabaseJob.getStatus() == Job.Status.CANCELING || syncGeodatabaseJob.getStatus() == Job.Status.PAUSED || syncGeodatabaseJob.getStatus() == Job.Status.NOT_STARTED) {
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            } else {
                Log.e(TAG, "Database did not sync correctly!");
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void closeCallout(){
        mCallout.dismiss();
    }
    protected void showBottomSheet3(int _action, Feature _feature) {
//        viewAttributePoint = new ViewAttributePoint(this, _action);
//        viewAttributePoint.showView(_action);
        if (_action == 0){
            addNewEvent(_feature);
            viewFormObject = new ViewFormHGU(this, _action);
            viewFormObject.showView(_action);
        }else {
            addNewEvent(_feature);
            viewFormCheckHGU = new ViewFormCheckHGU(this, _action);
            viewFormCheckHGU.showView(_action);
        }

    }
    public void OpenCamera() {
        ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btOpenCam",new Date(),new Date(),this);
        openCamState = MapHguActivity.OpenCam.FormObject;
        if (ALselectedImage.size() < 3) {
//            EasyImage.
            EasyImage.openCamera(this, 1);
        }
    }
    enum EditState {
        NotReady, // Geodatabase has not yet been generated
        Editing, // A feature is in the process of being moved
        Ready // The geodatabase is ready for synchronization or further edits
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if(imageFiles.size() > 0) {
                    File picture = Compressor.getDefault(MapHguActivity.this).compressToFile(imageFiles.get(0));
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
                    picture = GlobalHelper.photoAddStamp(picture,gps.getY(),gps.getX());
                    long time = System.currentTimeMillis();
                    File newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + GlobalHelper.PROJECT_NAME + GlobalHelper.FOLEDRFOTOTEMP);
                    try {
                        selectedImage = GlobalHelper.moveFile(picture,
                                newdir,
                                Long.toString(time));
                        ALselectedImage.add(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    viewFormObject.setupimageslide();

                    if (openCamState == MapHguActivity.OpenCam.FormObject){
                        viewFormObject.setupimageslide();
                        Log.e("OpenCamState","FromObject");
                    }
//                    else {
//                        viewFormRealize.setupimageslide();
//                        Log.e("OpenCamState","FromRealize");
//                    }
//                    showPhoto(selectedImage.getAbsolutePath());
//                    Glide.with(MapInventoryActivity.this)
//                            .load(selectedImage)
//                            .asBitmap()
//                            .error(ResourcesCompat.getDrawable(MapInventoryActivity.this.getResources(), R.drawable.ic_add_photo, null))
//                            .centerCrop();
//                    isImageLoaded = true;
//                    viewAttributePoint.setupimageslide();
//                    viewFormObject.setupimageslide();

//                    iph.setupimageslide();
                }
            }
//            public void showPhoto(String imageFile) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(imageFile);
//                    sliderLayout.setImageBitmap(myBitmap);
//
//
//            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
//                super.onCanceled(source, type);
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MapHguActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }


}
