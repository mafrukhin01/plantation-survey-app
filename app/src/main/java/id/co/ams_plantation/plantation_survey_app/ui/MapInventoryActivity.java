package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
//import androidx.test.platform.app.InstrumentationRegistry;

import com.aspsine.multithreaddownload.util.L;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.SliderLayout;
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseInfo;
import com.esri.arcgisruntime.concurrent.Job;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.ArcGISFeature;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.data.Field;
import com.esri.arcgisruntime.data.Geodatabase;
import com.esri.arcgisruntime.data.GeodatabaseFeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.EnvelopeBuilder;
import com.esri.arcgisruntime.geometry.GeodeticCurveType;
import com.esri.arcgisruntime.geometry.Geometry;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.GeometryType;
import com.esri.arcgisruntime.geometry.LinearUnit;
import com.esri.arcgisruntime.geometry.LinearUnitId;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.Polygon;
import com.esri.arcgisruntime.geometry.PolygonBuilder;
import com.esri.arcgisruntime.geometry.Polyline;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.geometry.Unit;
import com.esri.arcgisruntime.io.RequestConfiguration;
import com.esri.arcgisruntime.layers.ArcGISTiledLayer;
import com.esri.arcgisruntime.layers.ArcGISVectorTiledLayer;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.layers.KmlLayer;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.layers.LegendInfo;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.GeoElement;
import com.esri.arcgisruntime.mapping.MobileMapPackage;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.IdentifyLayerResult;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.mapping.view.SketchEditConfiguration;
import com.esri.arcgisruntime.mapping.view.SketchEditor;
import com.esri.arcgisruntime.ogc.kml.KmlDataset;
import com.esri.arcgisruntime.portal.Portal;
import com.esri.arcgisruntime.security.UserCredential;
import com.esri.arcgisruntime.symbology.CompositeSymbol;
import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleRenderer;
import com.esri.arcgisruntime.symbology.Symbol;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateLayerOption;
import com.esri.arcgisruntime.tasks.geodatabase.GeodatabaseSyncTask;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.SyncLayerOption;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.OptionAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.SelectionAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.PostEndPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AssetWaterManagement.ToUploadPR;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.BudgetLayer;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Optionlayer;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SessionSurveys;
import id.co.ams_plantation.plantation_survey_app.services.ApiClient;
import id.co.ams_plantation.plantation_survey_app.services.ApiServices;
import id.co.ams_plantation.plantation_survey_app.services.model.PostImage;
import id.co.ams_plantation.plantation_survey_app.utils.ActivityLoggerHelper;
import id.co.ams_plantation.plantation_survey_app.utils.CompassHelper;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.SequenceNumber;
import id.co.ams_plantation.plantation_survey_app.utils.ShowBottomSheetPoint;
import id.co.ams_plantation.plantation_survey_app.utils.ShowBottomSheetDialog;
import id.zelory.compressor.Compressor;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mafrukhin on 12/04/2023.
 */
public class MapInventoryActivity extends BaseActivity implements SelectionAdapter.ItemClickListener {
    private static final String TAG = "Test App";
    private MobileMapPackage mMapPackage;
    ViewGroup mCalloutContent;
    FragmentManager fm = getSupportFragmentManager();
    Dialog sweetAlert;
    public ArrayList<File> ALselectedImage = new ArrayList<>();
    public File selectedImage = null;
    ArcGISVectorTiledLayer vectorTiledLayer;
    ArcGISTiledLayer tiledLayer;
    public LocationDisplay mLocationDisplay;
//    ViewAttributePoint viewAttributePoint;
    ViewFormObject viewFormObject;
    ViewFormRealize viewFormRealize;
    String sequenceSession, sessionId, sessionName;
    public String locKmlMap;
    String global_id = "";
    String oldFeatureId;
    public Feature featureTemp;
    KmlLayer kmlLayer;
    List<Optionlayer> optionLayers;
    List<String> budgetLayers;
    List<FeatureLayer> featureLayers;
    @BindView(R.id.spinner_budget)
    AppCompatSpinner spinnerBudget;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.fabMap)
    FloatingActionButton fab_gps;
    @BindView(R.id.fabLayer)
    FloatingActionButton fab_layer;
    @BindView(R.id.fab)
    FloatingActionButton fab_extend;
    @BindView(R.id.sliderLayout)
    SliderLayout sliderLayout;
    @BindView(R.id.fab2)
    FloatingActionButton fab_versioning;
    @BindView(R.id.recycle_layer)
    RecyclerView rv_layer;
    @BindView(R.id.recycle_option)
    RecyclerView rv_option;
    @BindView(R.id.view_setting)
    LinearLayoutCompat view_setting;
    @BindView(R.id.view_option)
    LinearLayoutCompat view_option;
    @BindView(R.id.button2)
    Button mGeodatabaseButton;
    @BindView(R.id.iv_target)
    ImageView mTarget;
    @BindView(R.id.bt_edit)
    Button finish;
    @BindView(R.id.pointButton)
    ImageButton mPointButton;
    @BindView(R.id.pointsButton)
    ImageButton mMultiPointButton;
    @BindView(R.id.polylineButton)
    ImageButton mPolylineButton;
    @BindView(R.id.polygonButton)
    ImageButton mPolygonButton;
    @BindView(R.id.freehandLineButton)
    ImageButton mFreehandLineButton;
    @BindView(R.id.freehandPolygonButton)
    ImageButton mFreehandPolygonButton;
    @BindView(R.id.pointAdd)
    ImageButton mPointAdd;
    @BindView(R.id.pointsAdd)
    ImageButton mPointsAdd;
    @BindView(R.id.done)
    ImageButton bt_stop;
    @BindView(R.id.undo)
    ImageButton bt_undo;
    @BindView(R.id.redo)
    ImageButton bt_redo;
    @BindView(R.id.toolbarInclude)
    ConstraintLayout toolbar;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbarAdd;
    @BindView(R.id.fab3)
    FloatingActionButton fabAdd;
    @BindView(R.id.action)
    LinearLayout l_action;
    @BindView(R.id.tv_coordinate)
    TextView tv_cor;
    @BindView(R.id.viewDistance)
    TableLayout tb_distance;
    @BindView(R.id.tv_dis_manual)
    TextView tv_dis;

    @BindView(R.id.tv_deg_manual)
    TextView tv_deg;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footer)
    CardView footer;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    @BindView(R.id.bt_end_session)
    Button bt_end_session;
    @BindView(R.id.bmb)
    BoomMenuButton bmb;
    ArrayList<LayerMap> layers;
    ArrayList<Bitmap> bitmaps;
    SelectionAdapter adapter;
    OptionAdapter adapter2;
    static boolean isModeAdd = false;
    private String userID, estateCode, userFullName;
    FeatureLayer featureLayer;
    Callout mCallout;
    public List<EditFeature> editFeatures;
    public MapInventoryActivity.EditState mCurrentEditState;

    public OpenCam openCamState;
    private List<Feature> mSelectedFeatures;
    public Feature selectedFeature;
    private GeodatabaseSyncTask mGeodatabaseSyncTask;
    public FeatureTable mFeatureTable;
    private Geodatabase mGeodatabase;
    private GraphicsOverlay mGraphicsOverlay;
    private FeatureLayer mFeatureLayer;
    private android.graphics.Point mClickPoint;
    String surveyor, name, jenis;
    private ArcGISFeature mSelectedArcGISFeature;
    Iterator<Feature> iteratorFilterLayer;
    private Feature featureResult;
    private boolean mFeatureUpdated;
    private String mSelectedArcGISFeatureAttributeValue;
    private static PointCollection points;
    private static Point pointTemp;
    public CompassHelper ch;
    public RelativeLayout layoutCompass;
    public ImageView ivCompass, ivCompassLight;
    public PulsatorLayout pulsatorLayout;
    @BindView(R.id.sniper)
    AppCompatImageView sniper;
    @BindView(R.id.base_info)
    CardView base_info;
    @BindView(R.id.session_name)
    TextView session_name;
    private SimpleMarkerSymbol mPointSymbol;
    private SimpleLineSymbol mLineSymbol;
    private SimpleFillSymbol mFillSymbol;
    private SketchEditor mSketchEditor;
    SketchEditConfiguration configuration;
    GraphicsOverlay layerDraw;
    public double latManual, longManual;
    String groupCompanyName, companyCode;
    public static com.esri.arcgisruntime.geometry.Point pointManual;
    private static final SpatialReference wgs84 = SpatialReferences.getWgs84();
    private static final SpatialReference web = SpatialReferences.getWebMercator();
    BottomSheetBehavior<LinearLayout> bottomSheetBehavior;
    enum OpenCam {
        FormObject, // Geodatabase has not yet been generated
        FormRealize, // A feature is in the process of being moved// The geodatabase is ready for synchronization or further edits
    }

    //    private GraphicsOverlay mGraphicsOverlay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_asset);
        ButterKnife.bind(this);
        initDB();
        getLicense();
        LicenseInfo licenseInfo = LicenseInfo.fromJson(Prefs.getLicense(Prefs.LICENSE));
        Log.e("License Pref", Prefs.getLicense(Prefs.LICENSE));
        ArcGISRuntimeEnvironment.setLicense(licenseInfo);
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();
        mAssetViewModel.watchSequenceSession().observe(this, integer -> {
            sequenceSession = SequenceNumber.getSequence(integer);
        });
//        mAssetViewModel.streamHistoryBRD11().observe(this,data ->{
//                Log.e("totalHBRD1",String.valueOf(data.size()));
//
//        });
//        GlobalHelper.getCompanies();
        if (mAssetViewModel.getEstateMapping() != null) {
            companyCode = mAssetViewModel.getEstateMapping().companyCode;
            groupCompanyName = mAssetViewModel.getEstateMapping().groupCompanyName;
        }

        optionLayers = GlobalHelper.getOptionLayers();
        budgetLayers = GlobalHelper.getBudgetLayers();

        featureLayers = new ArrayList<>();
        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " "
                + GlobalHelper.getEstate().getEstName());
//         bottomSheetLayout = findViewById(R.id.sample_sheet2);
//         bottomSheetBehavior= BottomSheetBehavior.from(bottomSheetLayout);
        mCurrentEditState = MapInventoryActivity.EditState.NotReady;

        UserCredential credential = new UserCredential("app_dev", "appdev123!@#");
//        Portal portal = new Portal("https://app.gis-div.com/portal");
//        portal.setCredential(credential);
//        portal.loadAsync();
        mSelectedFeatures = new ArrayList<>();
        configuration = new SketchEditConfiguration();
        configuration.setVertexEditMode(SketchEditConfiguration.SketchVertexEditMode.INTERACTION_EDIT);
        points = new PointCollection(wgs84);
        layerDraw = new GraphicsOverlay();
//        configuration.setAllowPartSelection(true);
//        configuration.setContextMenuEnabled(true);
//        configuration.setRequireSelectionBeforeDrag(true);

        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE, Prefs.PASS_SDE));
//        mSketchEditor = new SketchEditor();
//        mapView.setSketchEditor(mSketchEditor);

//        mPointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 20);
//        mLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.BLUE, 4);
//        mFillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.CROSS, Color.BLUE, mLineSymbol);
        ch = new CompassHelper(this);
//        readGeodatabase();
        mGeodatabaseButton.setOnClickListener(v -> {
//            generateGeodatabase();
            if (mCurrentEditState == MapInventoryActivity.EditState.NotReady) {
//                generateGeodatabase2();
            } else if (mCurrentEditState == MapInventoryActivity.EditState.Ready) {
                syncGeodatabase();
            }
        });

        setBaseMap();
        menuSetup();
        mapView.addViewpointChangedListener(viewpointChangedEvent -> {
            com.esri.arcgisruntime.geometry.Point center = mapView.getVisibleArea().getExtent().getCenter();
            com.esri.arcgisruntime.geometry.Point wgs84Point = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(center, wgs84);
            tv_cor.setText("Lat: " + String.format("%.5f", wgs84Point.getY()) + " | Long: " + String.format("%.5f", wgs84Point.getX()));
            com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
            if (mCurrentEditState == EditState.Editing) {
                float centreX = mapView.getX() + mapView.getWidth() / 2;
                float centreY = mapView.getY() + mapView.getHeight() / 2;
                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(centreX), Math.round(centreY));
                Point mapPoint = mapView.screenToLocation(screenPoint);
                Point wgs84Point2 = (Point) GeometryEngine.project(mapPoint, SpatialReferences.getWgs84());
                latManual = wgs84Point2.getX();
                longManual = wgs84Point2.getY();
            }

            if (isModeAdd) {
                tb_distance.setVisibility(View.VISIBLE);
                pointManual = pointTemp;
                double imeter = distance(gps.getY(), longManual, gps.getX(), latManual);
                double bearing = bearing(gps.getY(), longManual, gps.getX(), latManual);
                Log.e("lat1,lat2,long1,long2", "" + gps.getX() + ";" + latManual + ";" + gps.getY() + ";" + longManual);
                tv_dis.setText("Bearing : " + String.format("%.0f m", imeter));
                tv_deg.setText("Deg : " + String.format("%.0f", bearing));
                if (imeter > 30) {
                    tb_distance.setBackgroundColor(getResources().getColor(R.color.Tomato));
                    tv_dis.setTextColor(Color.WHITE);
                    tv_deg.setTextColor(Color.WHITE);
                } else {
                    tb_distance.setBackgroundColor(Color.WHITE);
                    tv_dis.setTextColor(Color.BLACK);
                    tv_deg.setTextColor(Color.BLACK);
                }
            } else {
                tb_distance.setVisibility(View.GONE);
            }

        });

        fab_gps.setOnClickListener(view -> {
            mLocationDisplay.setInitialZoomScale(499);
            mapView.setViewpointScaleAsync(499);
            mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
            if (!mLocationDisplay.isStarted()) {
                mLocationDisplay.startAsync();
            }

        });
//        fab_set.setVisibility(View.GONE);
//        fab_versioning.setVisibility(View.GONE);
//        fabAdd.setVisibility(View.GONE);
        fab_layer.setOnClickListener(view -> {
            if (view_setting.getVisibility() == View.VISIBLE) {
                view_setting.setVisibility(View.GONE);
            } else {
                view_setting.setVisibility(View.VISIBLE);
            }
        });
        fab_extend.setOnClickListener(view -> {
            if (mapView.isViewInsetsValid() && mapView.getMap().getOperationalLayers().size() > 0) {
                mapView.setViewpoint(new Viewpoint(mapView.getMap().getOperationalLayers().get(0).getFullExtent()));
            }
        });

        fab_versioning.setOnClickListener(view -> {
            if (view_option.getVisibility() == View.VISIBLE) {
                view_option.setVisibility(View.GONE);
            } else {
                view_option.setVisibility(View.VISIBLE);
            }
//            openVersioning();
        });
        finish.setOnClickListener(view -> {
            mCurrentEditState = MapInventoryActivity.EditState.Ready;
            featureLayer.clearSelection();
            finish.setVisibility(View.GONE);

        });
        fabAdd.setOnClickListener(view -> {
            showDialogNewObject();
//            bufferRadius();

        });

        bt_end_session.setOnClickListener(v -> {
//                checkCensusSession();
            endSessionDialog(true, 0);
        });
    }

    public void showWarning() {
        showAlertUpdate("Check objek sekitar!", "Terdapat jembatan di sekitar anda, apakah anda yakin menambahkan jembatan lagi?", SweetAlertDialog.WARNING_TYPE);
//        showAlert("Check objek sekitar!", "Terdapat jembatan di sekitar anda, apakah anda yakin menambahkan jembatan lagi?",SweetAlertDialog.WARNING_TYPE);
    }

    public void showAlertUpdate(String title, String message, int type) {
        sweetAlert = new SweetAlertDialog(this, type)
//                .setCancelButton("Cancel", sweetAlertDialog -> {
//                    sweetAlert.dismiss();
//                })
//                .setCancelButtonBackgroundColor(Color.RED)
//                .showCancelButton(false)
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlert.dismiss();
                })
                .setTitleText(title).setContentText(message);
        sweetAlert.show();
    }

    public void showAlertDuplicate(String objCode){
        sweetAlert = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setCancelButton("Cancel", sweetAlertDialog -> {
                    sweetAlert.dismiss();
                })
//                .setCancelButtonBackgroundColor(Color.RED)
//                .showCancelButton(false)
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlert.dismiss();
                    addNewObject(objCode);
                })
                .setTitleText("Ditemukan " +GlobalHelper.getNameObject(objCode)+" !").setContentText("Terdapat "+GlobalHelper.getNameObject(objCode)+" disekitar anda" +
                        "apakah anda yakin menambahkan objek "+GlobalHelper.getNameObject(objCode)+"?");
        sweetAlert.show();
    }


    public void menuSetup() {
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setBackgroundEffect(true);
        bmb.clearBuilders();
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);
        bmb.addBuilder(syncBMB());
        bmb.addBuilder(newCencus());
        bmb.addBuilder(hasilCensus());
        bmb.addBuilder(baseMap());
        bmb.addBuilder(upBMB());
    }

    private HamButton.Builder syncBMB() {
        return new HamButton.Builder()
                .normalText("Sync Data")
                .subNormalText("Synchronize data with server")
                .normalImageRes(R.drawable.refresh)
                .pieceColorRes(R.color.Tomato)
                .normalColorRes(R.color.Tomato)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btBmbSync",new Date(),new Date(),this);
//                    syncGeodatabase();
                    backupGDB();
                    showUploadSyncDialog();
                });
    }

    private HamButton.Builder upBMB() {
        return new HamButton.Builder()
                .normalText("Generate Geodatabase")
                .subNormalText("Redownload Geodatabase")
                .normalImageRes(R.drawable.ic_baseline_sync_24)
                .pieceColorRes(R.color.WhiteTrans)
                .normalColorRes(R.color.WhiteTrans)
                .listener(index -> {
                    if(isNetworkConnected()){
                        dialogAction("Proses Generate akan menghapus semua data yang ada, pastikan data anda di upload dahulu!","Yakin ingin generate ulang data?");

//                        generateGeodatabase();
                    }else {
                        alertDialog(true);
                    }
                    ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btBmbGenerate",new Date(),new Date(),this);

//                        insertSessionSurveys();
//                    syncGeodatabase();
//                    showBottomSheet();
                });
    }

    private HamButton.Builder baseMap() {
        return new HamButton.Builder()
                .normalText("Base Map")
                .subNormalText("Pilih basemap aplikasi")
                .normalImageRes(R.drawable.map)
                .pieceColorRes(R.color.Green)
                .normalColorRes(R.color.Green)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btBmbBaseMap",new Date(),new Date(),this);
                    Intent intent = new Intent(this, DownloadActivity.class);
                    startActivity(intent);
                });
    }

    private HamButton.Builder newCencus() {
        return new HamButton.Builder()
                .normalText("Create New Session")
                .subNormalText("Buat sesi untuk memulai Survei")
                .normalImageRes(R.drawable.sketchbook)
                .pieceColorRes(R.color.YellowGreen)
                .normalColorRes(R.color.YellowGreen)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btBmbNewSession",new Date(),new Date(),this);
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)) {
                        endSessionDialog(true, 0);
                    } else {
                        newSession();
                    }
                });
    }


    private HamButton.Builder hasilCensus() {
        return new HamButton.Builder()
                .normalText("Statistik")
                .subNormalText("Statistik hasil survei")
                .normalImageRes(R.drawable.stadistics)
                .pieceColorRes(R.color.CornflowerBlue)
                .normalColorRes(R.color.CornflowerBlue)
                .listener(index -> {
//                   FeatureLayer featureL = (FeatureLayer) mapView.getMap().getOperationalLayers().get(2);
//                    getGlobalID(featureL.getFeatureTable());
                            ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btBMBStatistic",new Date(),new Date(),this);
                            Intent i = new Intent(getApplicationContext(), ResultCensusActivity.class);
                            startActivity(i);
//                    Intent intent = new Intent(getApplicationContext(), MenuResultActivity.class);
//                    startActivityForResult(intent, 1898);
                        }
                );
    }

    private void showDialogNewObject() {
        FragmentManager fm = getSupportFragmentManager();
        DialogChooseObject basemapDialog = DialogChooseObject.newInstance(this);
        basemapDialog.show(fm, "Basemap");
    }

    public void setVisible(int i) {
        featureLayers.get(i).setVisible(true);
//        mapView.getMap().getOperationalLayers().get(i).setVisible(true);
    }

//    public void setVisibleBase() {
//        base_info.setVisibility(View.VISIBLE);
//    }

    public void setInVisible(int i) {
        featureLayers.get(i).setVisible(false);
//        mapView.getMap().getOperationalLayers().get(i).setVisible(false);
    }

    public void setOpacity(int tag, float result) {
        featureLayers.get(tag).setOpacity(result);
    }
    private void dialogAction(String message, String title) {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this,SweetAlertDialog.WARNING_TYPE);
        exitAlert.setTitle(title);
        exitAlert.setContentText(message);
        exitAlert.setConfirmButton("Generate", sweetAlertDialog -> {
            exitAlert.dismiss();
            generateGeodatabase();
        });
        exitAlert.setCancelButton("Batal",n ->{exitAlert.dismiss();});
        exitAlert.show();
    }




    public void addNewObject(String objCode) {
        featureResult = null;
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                if (Objects.equals(layer.getName(), objCode)) {
                    FeatureLayer featureLayer = (FeatureLayer) layer;
                    FeatureTable featureTable = featureLayer.getFeatureTable();
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
                    String dev_loc = gps.getY() + ";" + gps.getX();
                    mFeatureTable = featureTable;
                    Geometry geometry = mLocationDisplay.getMapLocation();
                    UUID uuid = UUID.randomUUID();
                    String survId = Prefs.getString(Prefs.SESSION_ID);
//                    String survId = DateTime.getStringLongDate()+"_"+GlobalHelper.getEstate().getEstCode()+"_"+GlobalHelper.getUser().getUserID()+"_AWM_PSA_001";
                    String groupCompName = GlobalHelper.getEstate().getCompanyShortName();
                    groupCompName = groupCompName.replace("-", " ");
                    Map<String, Object> defAttribute = new HashMap<>();
                    defAttribute.put("GlobalID", uuid);
                    defAttribute.put("CompanyCode", companyCode);
                    defAttribute.put("survey_ID", survId);
                    defAttribute.put("block", getBlock());
                    defAttribute.put("ESTNR", GlobalHelper.getEstate().getEstCode());
                    defAttribute.put("GroupCompanyName", groupCompanyName);
                    defAttribute.put("created_user", userFullName);
                    defAttribute.put("created_date", DateTime.timeStampCalendar());
                    defAttribute.put("source", Short.valueOf("1"));
                    defAttribute.put("QC", Short.valueOf("1"));
                    defAttribute.put("status_asset", Short.valueOf("3"));
                    defAttribute.put("image", "");
                    defAttribute.put("survey_date", DateTime.timeStampCalendar());
                    defAttribute.put("last_edited_user", userFullName);
                    defAttribute.put("last_edited_date", DateTime.timeStampCalendar());
                    defAttribute.put("device_loc", dev_loc);
                    defAttribute.put("surveyor", userFullName);
                    Feature newFeature = mFeatureTable.createFeature(defAttribute, geometry);
                    selectedFeature = newFeature;
                    showBottomSheet3(0);
                }
            }
        }
    }

    public void saveNewFeature(Feature feature) {
        mFeatureTable.addFeatureAsync(feature).addDoneListener(() -> {
            ALselectedImage.clear();
            Log.e("totalFeature", "" + mFeatureTable.getTotalFeatureCount());
            Log.e("imageFeature", "" + feature.getAttributes().get("image"));
//            ALselectedImage.clear();
        });

        mSelectedFeatures.clear();
        mCurrentEditState = MapInventoryActivity.EditState.Ready;
//        viewAttributePoint.hideView();
        viewFormObject.hideView();
    }

    public void saveNewFeatureRealize(Feature feature) {
        mFeatureTable.addFeatureAsync(feature).addDoneListener(() -> {
            ALselectedImage.clear();
            Log.e("totalFeature", "" + mFeatureTable.getTotalFeatureCount());
            Log.e("imageFeature", "" + feature.getAttributes().get("image"));
        });
        mSelectedFeatures.clear();
        mCurrentEditState = MapInventoryActivity.EditState.Ready;
        viewFormRealize.hideView();
    }

    public void showRealizeForm(Feature feature){
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
        String dev_loc = gps.getY() + ";" + gps.getX();
//        Map<String, Object> defAttribute = new HashMap<>();
        Map<String, Object> attributes = feature.getAttributes();
        UUID glbId = (UUID) attributes.get("GlobalID");
        String uuid = glbId.toString();
        oldFeatureId = uuid;
        Log.e("GlobalId",uuid);
        Feature newFeature = feature.getFeatureTable().createFeature(attributes,feature.getGeometry());
        newFeature.getAttributes().put("created_user", userFullName);
        newFeature.getAttributes().put("created_date", DateTime.timeStampCalendar());
        newFeature.getAttributes().put("source", Short.valueOf("1"));
        newFeature.getAttributes().put("QC", Short.valueOf("1"));
        newFeature.getAttributes().put("status_asset", Short.valueOf("3"));
        newFeature.getAttributes().put("image", "");
        newFeature.getAttributes().put("QC_remarks", "");
        newFeature.getAttributes().put("qc_user", "");
        newFeature.getAttributes().put("survey_date", DateTime.timeStampCalendar());
        newFeature.getAttributes().put("last_edited_user", userFullName);
        newFeature.getAttributes().put("last_edited_date", DateTime.timeStampCalendar());
        newFeature.getAttributes().put("device_loc", dev_loc);
        newFeature.getAttributes().put("surveyor", userFullName);
        newFeature.getAttributes().put("status_object","1");
        newFeature.getAttributes().put("budgetref_id",uuid);
        selectedFeature = newFeature;
        viewFormRealize = new ViewFormRealize(this, 0);
        viewFormRealize.showView(0);

    }

    public void savePr(String date){
        PostEndPR pr = new PostEndPR(oldFeatureId,selectedFeature.getFeatureTable().getTableName(),date);
        mAssetViewModel.insertPrDate(pr);
    }

    protected void setBaseMap() {
        String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_TPK);
        tiledLayer = new ArcGISTiledLayer(path);
        String vtpk = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
        layers = new ArrayList<>();
        bitmaps = new ArrayList<>();
//        String path2 = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
        vectorTiledLayer = new ArcGISVectorTiledLayer(vtpk);
//        String pathS = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
//        Log.e("path location",pathS);
//        mMapPackage = new MobileMapPackage(pathS);
//        mMapPackage.loadAsync();
        ArcGISMap arcGISMap = new ArcGISMap(new Basemap(vectorTiledLayer));
        ArcGISMap mMap2 = new ArcGISMap(new Basemap(tiledLayer));
        mapView.setMap(arcGISMap);
//        mapView.setMap(mMap2);
        mLocationDisplay = mapView.getLocationDisplay();
        if (!mLocationDisplay.isStarted())
            mLocationDisplay.startAsync();
        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        if (locKmlMap != null) {
            KmlDataset kmlDataset = new KmlDataset(locKmlMap);
            kmlLayer = new KmlLayer(kmlDataset);
//            kmlLayer.setOpacity(0.03f);
            mapView.getMap().getOperationalLayers().add(kmlLayer);
            kmlLayer.setVisible(false);
            kmlDataset.addDoneLoadingListener(() -> {
                if (kmlDataset.getLoadStatus() != LoadStatus.LOADED) {
                    String error = "Failed to load kml layer from URL: " + kmlDataset.getLoadError().getMessage();
                    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//                    Log.e(TAG, error);
                }
            });
        }
        readGeodatabase();
        checkCensusSession();
    }


    private void showLayer(int[] list) {
        for (int i : list) {
            mapView.getMap().getOperationalLayers().get(i).setVisible(true);
        }
    }

    private void hideLayer(int[] list) {
        for (int i : list) {
            mapView.getMap().getOperationalLayers().get(i).setVisible(false);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }


    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.pause();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @SuppressLint("ClickableViewAccessibility")
    private void touchListener() {
        mapView.setOnTouchListener(
                new DefaultMapViewOnTouchListener(MapInventoryActivity.this, mapView) {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
//                        featureLayer.clearSelection();
                        if (mCurrentEditState == MapInventoryActivity.EditState.Ready) {
//                            Log.e("featureLayer Touch","true"+featureLayer.getName());
//                            selectFeaturesAt(mapPointFrom(motionEvent), 10);
                            selectFeatures(motionEvent, 10);
//                            android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));
//                            selectFeature(screenPoint);
                        } else if (mCurrentEditState == MapInventoryActivity.EditState.Editing) {
//                            if (finish.getVisibility() == View.GONE) {
//                                finish.setVisibility(View.VISIBLE);
//                            }
//                            mSketchEditor.start(selectedFeature.getGeometry(),SketchCreationMode.POLYLINE, configuration );
//
                        }
                        return true;
                    }
                });
    }

    public String getBlock() {
        try {
            com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());

            String _block = "";
            String blok = GlobalHelper.getAndAssignBlock(new File(locKmlMap), gps.getX(), gps.getY());
            String[] split = blok.split(";");
            if (split.length > 1) {
                _block = split[2];
            }
//            String _block = blok.split(";")[2];
            Log.e("blokGet", blok);
            return _block;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void moveFeatureTo(Feature feature) {
        Log.e("update", "true");
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
        String dev_loc = gps.getY() + ";" + gps.getX();
        feature.getAttributes().put("survey_date", DateTime.timeStampCalendar());
        feature.getAttributes().put("QC", Short.valueOf("1"));
        feature.getAttributes().put("status_asset", Short.valueOf("3"));
        feature.getAttributes().put("device_loc", dev_loc);
        feature.getAttributes().put("last_edited_user", userFullName);
        feature.getAttributes().put("last_edited_date", DateTime.timeStampCalendar());
        feature.getAttributes().put("surveyor", userFullName);
        feature.getAttributes().put("qc_user", "");
        feature.getAttributes().put("QC_remarks", "");

//        feature.setGeometry(mLocationDisplay.getMapLocation());
        selectedFeature.getFeatureTable().updateFeatureAsync(feature);
        mCurrentEditState = MapInventoryActivity.EditState.Ready;

//        saveImage((String)selectedFeature.getAttributes().get("GlobalID"));
        viewFormObject.hideView();
//        viewAttributePoint.hideView();

    }

    public void updateGeometry(Feature feature) {
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
        double imeter = distance(gps.getY(), longManual, gps.getX(), latManual);
        if (imeter > 30) {
            showAlert("Diluar jangkauan", "Maksimal jarak 30 meter", SweetAlertDialog.ERROR_TYPE);
//            showToast("Maksimal jarak 30 meter");
        } else {
            layerDraw.getGraphics().clear();
            Point addPoint = new Point(latManual, longManual, wgs84);
            feature.setGeometry(addPoint);
//            showToast("Berhasil Update posisi");
            showAlert("Update geometry", "Berhasil Update posisi", SweetAlertDialog.SUCCESS_TYPE);
            SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
            List<Symbol> symbols = new ArrayList();
            symbols.add(symbol);
            CompositeSymbol cms = new CompositeSymbol(symbols);
            Graphic graph = new Graphic(addPoint, cms);
            layerDraw.getGraphics().add(graph);
        }

//        selectedFeature.getFeatureTable().updateFeatureAsync(feature);
//        mCurrentEditState = MapInventoryActivity.EditState.Ready;
//        viewAttributePoint.hideView();
    }

    public void updateToFinal(Feature f){
        f.getAttributes().put("status_asset",Short.valueOf("5"));
        selectedFeature.getFeatureTable().updateFeatureAsync(f);
    }

    private Point mapPointFrom(MotionEvent motionEvent) {
        // get the screen point
        android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
                Math.round(motionEvent.getY()));
        // return the point that was clicked in map coordinates
        return mapView.screenToLocation(screenPoint);
    }


    private final void selectFeature(android.graphics.Point screenPoint) {
        Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
//        GeometryEngine.nearestVertex()
        mSelectedFeatures.clear();
        featureLayer.clearSelection();
//        final double mapTolerance = 10 * mapView.getUnitsPerDensityIndependentPixel();
        final double mapTolerance = 10;
//        this.clearSelection();
        ListenableFuture<IdentifyLayerResult> identifyLayerResultFuture = mapView.identifyLayerAsync(featureLayer, screenPoint, mapTolerance, false, -1);
//        final ListenableFuture<IdentifyLayerResult> identifyLayerResultFuture = mapView.identifyLayersAsync(featureLayer,screenPoint, 10.0, false,-1);
        identifyLayerResultFuture.addDoneListener(() -> {
            try {
                IdentifyLayerResult identifyLayerResult = identifyLayerResultFuture.get();
                List<Feature> featureList = new ArrayList<>();
                for (GeoElement geoElement : identifyLayerResult.getElements()) {
                    Feature f = (Feature) geoElement;
                    featureList.add(f);
//                    f.getFeatureTable().getGeometryType().toString()
//                    mSelectedFeatures.add(f);
//                    featureLayer.selectFeature(f);
//                    selectedFeature = f;
//                    showBottomSheetDialog(f,"","","");
                    mCurrentEditState = MapInventoryActivity.EditState.Editing;
                    Log.e("element", f.getGeometry().getGeometryType().toString());
                }
                featureLayer.selectFeature(featureList.get(featureList.size() - 1));
                selectedFeature = featureList.get(featureList.size() - 1);
//                showBottomSheetDialog(featureList.get(featureList.size()-1),"","","");
//                showBottomSheet2(featureList.get(featureList.size()-1));

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                Log.e("Error : ", e.getMessage());
            }

        });
    }


    private void selectFeatures(MotionEvent motionEvent, int tolerance) {
        mCallout = mapView.getCallout();
        mCallout.dismiss();
        // define the tolerance for identifying the feature
        final double mapTolerance = tolerance * mapView.getUnitsPerDensityIndependentPixel();
        // create objects required to do a selection with a query
        android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
                Math.round(motionEvent.getY()));
        // select features within the envelope for all features on the map
//        for (Layer layer : mapView.getMap().getOperationalLayers()) {
//            final FeatureLayer featureLayer = (FeatureLayer) layer;

        ListenableFuture<List<IdentifyLayerResult>> identifyLayerResultFuture = mapView
                .identifyLayersAsync(screenPoint, tolerance, false, -1);
        identifyLayerResultFuture.addDoneListener(() -> {
            try {
                List<IdentifyLayerResult> identifyLayerResult = identifyLayerResultFuture.get();
                for (IdentifyLayerResult identifyResult : identifyLayerResult) {
                    // Get the feature table associated with the layer
                    Log.e("Lname", identifyResult.getLayerContent().getName());
                    FeatureLayer featureLayer = (FeatureLayer) identifyResult.getLayerContent();
                    FeatureTable featureTable = featureLayer.getFeatureTable();

                    // Iterate over the geo elements in the result and extract the features
                    if (!Objects.equals(identifyResult.getLayerContent().getName(), "LUS3")) {
                        for (GeoElement geoElement : identifyResult.getElements()) {
                            if (geoElement instanceof Feature) {
                                Feature feature = (Feature) geoElement;
                                mSelectedArcGISFeature = (ArcGISFeature) feature;
                                Map<String, Object> attr = feature.getAttributes();
                                Set<String> keys = attr.keySet();
                                selectedFeature = feature;
                                mFeatureTable = feature.getFeatureTable();
                                com.esri.arcgisruntime.geometry.Point wgs84Point2 = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(feature.getGeometry(), SpatialReferences.getWgs84());
                                com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
                                if (feature.getGeometry().getGeometryType() == GeometryType.POINT) {
                                    // Cast the geometry to Point
//                                    Point pointGeometry = (Point) feature.getGeometry();
                                    // Retrieve the x and y coordinates
//                                    double x = pointGeometry.getX();
//                                    double y = pointGeometry.getY();
                                    double distance = GlobalHelper.getDistance(gps.getY(), wgs84Point2.getY(), gps.getX(), wgs84Point2.getX());
                                    Log.e("Distance Feature", String.valueOf(distance));
                                    if (distance > 30) {
//                                        showToast("maksimal jarak object 30 meter");
                                        showAlert("Diluar jangkauan", "Maksimal jarak 30 meter", SweetAlertDialog.ERROR_TYPE);
                                    } else if (!Prefs.getBoolean(Prefs.ACTIVE_SESSION)) {
                                        showAlert("Attention", "Aktifkan sesi Survey", SweetAlertDialog.ERROR_TYPE);
                                    } else {
//                                        showBottomSheet3(1);
                                        Envelope envelope = feature.getGeometry().getExtent();
                                        mCalloutContent = CalloutInfoFeature.setInfoFeature(MapInventoryActivity.this, selectedFeature,GlobalHelper.getNameObject(selectedFeature.getFeatureTable().getDisplayName()));
                                        mCallout.setLocation(envelope.getCenter());
                                        mCallout.setContent(mCalloutContent);
                                        mCallout.show();
                                    }

                                    // Use the x and y coordinates as needed
                                    // ...
                                }

//                                showBottomSheet2(selectedFeature,1);


                            }
                        }
                    }
                }


            } catch (Exception e) {
                String error = "Select features failed: " + e.getMessage();
                Toast.makeText(MapInventoryActivity.this, error, Toast.LENGTH_SHORT).show();
                Log.e(TAG, error);
            }
        });
//        }
    }

    public void applyDefinition() {
        String def = "ESTNR = '" + estateCode + "' AND status_object != 0 AND (status_asset!=1 OR status_asset IS NULL)";
        StringBuilder sb = new StringBuilder();
        for (Optionlayer ol : optionLayers) {
            if (ol.getIndex()!= 9 && !ol.isChecked()) {
                sb.append(" AND status_object != " + ol.getIndex() + " ");
            }
            if (ol.getIndex()==9 && ol.isChecked){
                sb.append(" AND status_asset = 2"+ " ");
                fabAdd.setVisibility(View.GONE);
            }
            else {
               if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)){
                   fabAdd.setVisibility(View.VISIBLE);
               }
            }
        }

        String selectedItem = (String) spinnerBudget.getSelectedItem();
        if (selectedItem!= null && !selectedItem.equalsIgnoreCase("ALL")){
            sb.append("AND year_budget = " + selectedItem + " ");
        }
        for (FeatureLayer layer : featureLayers) {
//                table.loadAsync();
            String layerDefinitionExpression = def + sb;
            Log.e("DefinitionWxpression", def + sb);
            layer.setDefinitionExpression(layerDefinitionExpression);
        }
    }
    public void
    readGeodatabase() {
        copyGDB();
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_inventory.geodatabase");
//        File fileLoc  = new File(getCacheDir() + "/file_inventory.geodatabase");
        Log.e("ReadGDB", fileLoc.getAbsolutePath());
        List<Layer> layerLegend = new ArrayList<>();
        if (fileLoc.exists()) {
            Log.e("File exist", "true");
//            mGeodatabase = new Geodatabase(getCacheDir() + "/file_inventory.geodatabase");
            mGeodatabase = new Geodatabase(fileLoc.getAbsolutePath());
            mGeodatabase.loadAsync();
            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    int counter = 0;
                    // get only the first table which, contains points
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                        table.loadAsync();
                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                        Log.e("Layer GDB name", table.getTableName());
                        // add geodatabase layer to the map as a feature layer and make it selectable
                        layerLegend.add(geodatabaseFeatureLayer);
                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                        featureLayers.add(geodatabaseFeatureLayer);
//                        QueryParameters query = new QueryParameters();
//                        query.setWhereClause("survey_date=null AND QC=3");
                    }
//                    mapView.getMap().getOperationalLayers().addAll(featureLayers);
                    float density = MapInventoryActivity.this.getResources().getDisplayMetrics().density;
//                    for(Layer layer : mapView.getMap().getOperationalLayers()){
                    for (Layer layer : layerLegend) {
                        counter = counter++;
                        List<LegendInfo> a;
                        LayerMap layerMap = new LayerMap(counter - 1, true, layer.getName());
                        layers.add(layerMap);
//                        FeatureLayer featureL = (FeatureLayer) layer;
                        try {
                            a = layer.fetchLegendInfosAsync().get();
                            Symbol symbol = a.get(0).getSymbol();
                            ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24, density, Color.TRANSPARENT);
                            Bitmap swatch = symbolSwatch.get();
                            bitmaps.add(swatch);
                        } catch (ExecutionException | InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        Log.e("", String.valueOf(layer.getName()));
                    }
                    Log.e("size Layer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    Log.e("size Bitmap", String.valueOf(bitmaps.size()));
                    mapView.getMap().getLoadSettings().setPreferredPolylineFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
                    layerDraw = new GraphicsOverlay();
                    mapView.getGraphicsOverlays().add(layerDraw);
                    adapter = new SelectionAdapter(this, layers, bitmaps);
                    adapter2 = new OptionAdapter(this, optionLayers);
                    ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, budgetLayers);
                    adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerBudget.setAdapter(adapter3);
                    spinnerBudget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            applyDefinition();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    adapter.setClickListener(this);
                    adapter2.setClickListener(this);
//                adapter.setData(layers);
                    rv_layer.setAdapter(adapter);
                    rv_option.setAdapter(adapter2);
//                    int layerTouch = mapView.getMap().getOperationalLayers().size() -1;
                    Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    Log.e("Option size", GlobalHelper.getOptionLayers().size() + "");
//                    featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(layerTouch);
                    applyDefinition();
                    touchListener();
                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                }
            });
            // set edit state to ready
            mCurrentEditState = MapInventoryActivity.EditState.Ready;
        } else {
            Log.e("fileNotExist", "true");
        }
    }


    public void
    generateGeodatabase() {
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
//        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE,Prefs.PASS_SDE));
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_inventory.geodatabase");
        File fileBackup = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "backup");
        if (!fileBackup.exists()) {
            fileBackup.mkdirs();
        }
        final String path = fileLoc.getAbsolutePath();
        mGeodatabaseSyncTask.loadAsync();
        mGeodatabaseSyncTask.addDoneLoadingListener(() -> {
//            Log.e("versionSDE", mGeodatabaseSyncTask.getFeatureServiceInfo().getVersion());
//            final Envelope extent = mapView.getVisibleArea().getExtent();
//            double minX = 94.97; // Minimum X-coordinate (longitude) for the extent
//            double minY = -11.00; // Minimum Y-coordinate (latitude) for the extent
//            double maxX = 141.00; // Maximum X-coordinate (longitude) for the extent
//            double maxY = 6.00;
//            Envelope envelope = new Envelope(minX, minY, maxX, maxY, wgs84);
            final Envelope extentFull = mGeodatabaseSyncTask.getFeatureServiceInfo().getFullExtent();
            final ListenableFuture<GenerateGeodatabaseParameters> defaultParameters = mGeodatabaseSyncTask
                    .createDefaultGenerateGeodatabaseParametersAsync(extentFull);
            defaultParameters.addDoneListener(() -> {
                try {
                    // set parameters and don't include attachments
                    GenerateGeodatabaseParameters parameters = defaultParameters.get();
                    parameters.setReturnAttachments(false);
                    // define the local path where the geodatabase will be stored
//                    final String localGeodatabasePath = getCacheDir() + "/file_inventory.geodatabase";
                    Log.e("patchLoc", path);
                    // create and start the job
                    if (fileLoc.exists()) {
                        GlobalHelper.moveFile(fileLoc, fileBackup, DateTime.getStringDate());
                    }
                    final GenerateGeodatabaseJob generateGeodatabaseJob = mGeodatabaseSyncTask
                            .generateGeodatabase(parameters, path);
                    RequestConfiguration requestConfiguration = new RequestConfiguration();
                    requestConfiguration.setConnectionTimeout(300000);
                    requestConfiguration.setSocketTimeout(300000);
                    requestConfiguration.setMaxNumberOfAttempts(5);
                    generateGeodatabaseJob.setRequestConfiguration(requestConfiguration);
//                    String timeout = String.valueOf(generateGeodatabaseJob.getRequestConfiguration().getConnectionTimeout());
//                    Log.e("timeout",timeout);
                    generateGeodatabaseJob.start();
                    createProgressDialog(generateGeodatabaseJob);
                    // get geodatabase when done
                    generateGeodatabaseJob.addJobDoneListener(() -> {
                        if (generateGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                            mGeodatabase = generateGeodatabaseJob.getResult();
                            mGeodatabase.loadAsync();
                            mGeodatabase.addDoneLoadingListener(() -> {
//                                Toast.makeText(this, mGeodatabase.get)
                                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                                    featureLayers.clear();
                                    // get only the first table which, contains points
                                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                                        table.loadAsync();
//                                    GeodatabaseFeatureTable pointsGeodatabaseFeatureTable = mGeodatabase
//                                            .getGeodatabaseFeatureTables().get(0);
//                                    pointsGeodatabaseFeatureTable.loadAsync();
                                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                                        featureLayers.add(geodatabaseFeatureLayer);
                                        Log.e("Layer GDB name", table.getTableName());
                                        // add geodatabase layer to the map as a feature layer and make it selectable
                                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                                    }
//                                    mapView.getMap().getOperationalLayers().addAll(featureLayers);
                                    mGeodatabaseButton.setVisibility(View.GONE);
                                    int layerTouch = mapView.getMap().getOperationalLayers().size() - 1;
                                    Log.e("Feature layer", String.valueOf(layerTouch));
                                    Log.e("mapView size", mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size() - 1).getName());
                                    featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(layerTouch);
//                                    Log.i("689", "Local geodatabase stored at: " + localGeodatabasePath);
                                    applyDefinition();
                                    touchListener();
//                                    touchListenerEdit();
                                } else {
                                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                                }
                            });
                            // set edit state to ready
                            mCurrentEditState = MapInventoryActivity.EditState.Ready;
                        } else if (generateGeodatabaseJob.getError() != null) {
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause());
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getMessage());
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getErrorCode());
                            Toast.makeText(this,
                                    "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("703", "Unknown Error generating geodatabase");
                            Toast.makeText(this, "Unknown Error generating geodatabase", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("708", "Error generating geodatabase parameters : " + e.getMessage());
                    Toast.makeText(this, "Error generating geodatabase parameters: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    private void createProgressDialog(Job job) {
        ProgressDialog syncProgressDialog = new ProgressDialog(this);
        syncProgressDialog.setTitle("Sync geodatabase job");
        syncProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        syncProgressDialog.setCanceledOnTouchOutside(false);
        syncProgressDialog.show();
        job.addProgressChangedListener(() -> {
            syncProgressDialog.setProgress(job.getProgress());
            Log.e("Progress", String.valueOf(job.getProgress()));
            Log.e("Progress", String.valueOf(job.getStatus()));
        });
//        job.addJobDoneListener(syncProgressDialog::dismiss);
        job.addJobDoneListener(() -> {
            syncProgressDialog.dismiss();
            showAlert(job.getStatus().name(), job.getStatus().toString(), SweetAlertDialog.SUCCESS_TYPE);
        });
    }

    public void syncBidiectional() {
        // create parameters for the sync task
        SyncGeodatabaseParameters syncGeodatabaseParameters = new SyncGeodatabaseParameters();
        syncGeodatabaseParameters.setSyncDirection(SyncGeodatabaseParameters.SyncDirection.BIDIRECTIONAL);
        syncGeodatabaseParameters.setRollbackOnFailure(false);
        // get the layer ID for each feature table in the geodatabase, then add to the sync job
        for (GeodatabaseFeatureTable geodatabaseFeatureTable : mGeodatabase.getGeodatabaseFeatureTables()) {
            long serviceLayerId = geodatabaseFeatureTable.getServiceLayerId();
            SyncLayerOption syncLayerOption = new SyncLayerOption(serviceLayerId);
            syncGeodatabaseParameters.getLayerOptions().add(syncLayerOption);
        }
        final SyncGeodatabaseJob syncGeodatabaseJob = mGeodatabaseSyncTask
                .syncGeodatabase(syncGeodatabaseParameters, mGeodatabase);
        syncGeodatabaseJob.start();
        createProgressDialog(syncGeodatabaseJob);
        syncGeodatabaseJob.addJobDoneListener(() -> {
            if (syncGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                Toast.makeText(this, "Sync complete", Toast.LENGTH_LONG).show();
//                mGeodatabaseButton.setVisibility(View.INVISIBLE);
            } else if (syncGeodatabaseJob.getStatus() == Job.Status.CANCELING || syncGeodatabaseJob.getStatus() == Job.Status.PAUSED || syncGeodatabaseJob.getStatus() == Job.Status.NOT_STARTED) {
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            } else {
                Log.e(TAG, "Database did not sync correctly!");
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void syncGeodatabase() {
        // create parameters for the sync task
        SyncGeodatabaseParameters syncGeodatabaseParameters = new SyncGeodatabaseParameters();
        syncGeodatabaseParameters.setSyncDirection(SyncGeodatabaseParameters.SyncDirection.UPLOAD);
        syncGeodatabaseParameters.setRollbackOnFailure(false);
        // get the layer ID for each feature table in the geodatabase, then add to the sync job
        for (GeodatabaseFeatureTable geodatabaseFeatureTable : mGeodatabase.getGeodatabaseFeatureTables()) {
            long serviceLayerId = geodatabaseFeatureTable.getServiceLayerId();
            SyncLayerOption syncLayerOption = new SyncLayerOption(serviceLayerId);
            syncGeodatabaseParameters.getLayerOptions().add(syncLayerOption);
        }
        final SyncGeodatabaseJob syncGeodatabaseJob = mGeodatabaseSyncTask
                .syncGeodatabase(syncGeodatabaseParameters, mGeodatabase);
        syncGeodatabaseJob.start();
        createProgressDialog(syncGeodatabaseJob);
        syncGeodatabaseJob.addJobDoneListener(() -> {
            if (syncGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                postPR();
//                Toast.makeText(this, "Sync complete", Toast.LENGTH_LONG).show();
                showAlert("Sync Data", "Sync complete", SweetAlertDialog.SUCCESS_TYPE);
//                mGeodatabaseButton.setVisibility(View.INVISIBLE);
            } else if (syncGeodatabaseJob.getStatus() == Job.Status.CANCELING || syncGeodatabaseJob.getStatus() == Job.Status.PAUSED || syncGeodatabaseJob.getStatus() == Job.Status.NOT_STARTED) {
                showAlert("Sync Data", "Database did not sync correctly!", SweetAlertDialog.ERROR_TYPE);

                Log.e("Sync Failed", syncGeodatabaseJob.getError().toString());
//                Log.e("cause", syncGeodatabaseJob.getError().getCause().toString());
                Log.e("domain", syncGeodatabaseJob.getError().getErrorDomain().toString());

//                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();

            } else {
                showAlert("Sync Data", "Database did not sync correctly!", SweetAlertDialog.ERROR_TYPE);
                Log.e("Sync Failed", syncGeodatabaseJob.getError().toString());
                Log.e("err", "Error  getCause: " + syncGeodatabaseJob.getError().getCause());
//                Log.e("cause", Objects.requireNonNull(syncGeodatabaseJob.getError().getCause().getMessage()));
                Log.e("domain", syncGeodatabaseJob.getError().getErrorDomain().toString());

//                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            }
        });
    }


    protected void showBottomSheet3(int _action) {
//        viewAttributePoint = new ViewAttributePoint(this, _action);
//        viewAttributePoint.showView(_action);
        viewFormObject = new ViewFormObject(this, _action);
        viewFormObject.showView(_action);
    }

    private void showUploadSyncDialog() {
        long longDate = Prefs.getLong(Prefs.LAST_SYNC_DATA);
        Date theDate = new Date(longDate);
//        DataToUpload data = mSessionViewModel.getCountSessionToUpload();
        DialogUpload dialogUploadSync = DialogUpload.newInstance(this, theDate, "0", "0");
        if (isNetworkConnected()) {
            dialogUploadSync.show(fm, "");
        } else {
            alertDialog(true);
        }
    }

    public void closeCallout(){
        mCallout.dismiss();
    }
    public void updateFeature(List<EditFeature> editFeature) {
        Log.e("mSelectedArcGISFeature.getAttributes()", "true");
        for (EditFeature item : editFeature) {
            mSelectedArcGISFeature.getAttributes().put(item.getKey(), item.getValue());
            Log.e("mSelectedArcGISFeature.getAttributes()", item.getKey() + item.getValue());
        }
    }


    private void openVersioning() {
        FragmentManager fm = getSupportFragmentManager();
        DialogVersioning basemapDialog = DialogVersioning.newInstance(this);
        basemapDialog.show(fm, "Basemap");

        List<String> a = new ArrayList<>();
        a.add("a");
        DialogNewSession dialogSessionFragment = DialogNewSession.newInstance(this, a, "a", "SEN_8869_PSA_001", DateTime.getStringDate(), DateTime.getStringTime(), "Mafrukhin");
        dialogSessionFragment.show(fm, "");
    }

    public Iterator<Feature> filterLayer(FeatureTable featureTable) {
        QueryParameters queryParameters = new QueryParameters();
        String estate = GlobalHelper.getEstate().getEstCode();
        queryParameters.setWhereClause("ESTNR = " + estate);
        List<Feature> features = new ArrayList<>();
//        FeatureQueryResult queryResult
//        Iterator<Feature> iterator;

        final ListenableFuture<FeatureQueryResult> queryResultFuture = featureTable.queryFeaturesAsync(queryParameters);
        queryResultFuture.addDoneListener(new Runnable() {
            @Override
            public void run() {
                try {
                    FeatureQueryResult queryResult = queryResultFuture.get();
//                    Iterator<Feature> iterator = queryResult.iterator();
                    iteratorFilterLayer = queryResult.iterator();
//                    if (iterator.hasNext()) {
//                        features.add(iterator.next());
//                        Feature lastFeature = iterator.next();
//                        // Process the last feature
//                    }
                } catch (InterruptedException | ExecutionException e) {
                    // Handle any exceptions
                }
            }
        });
        return iteratorFilterLayer;
    }


    public String getGlobalId(Feature feature, int param) {
        // 0 = new ,1 = edit
        if (param == 0) {
            return getGlobalIDNewObject(feature.getFeatureTable());
        } else {
            return String.valueOf(feature.getAttributes().get("GlobalID"));
        }
    }

    public String getGlobalIDNewObject(FeatureTable featureTable) {
        QueryParameters queryParameters = new QueryParameters();
        List<Field> fields = featureTable.getFields();
//        for(Field field : fields){
//            Log.e("field Name", field.getName());
//        }
        queryParameters.setWhereClause("ESTNR = '" + estateCode + "'");
        queryParameters.setMaxFeatures(1);
        QueryParameters.OrderBy orderBy = new QueryParameters.OrderBy("OBJECTID", QueryParameters.SortOrder.DESCENDING);
        List<QueryParameters.OrderBy> orderByList = new ArrayList<>();
        orderByList.add(orderBy);
        queryParameters.getOrderByFields().clear();
        queryParameters.getOrderByFields().addAll(orderByList);
// Set the maximum number of features to 1 to retrieve only the last row
        final ListenableFuture<FeatureQueryResult> queryResultFuture = featureTable.queryFeaturesAsync(queryParameters);
        queryResultFuture.addDoneListener(() -> {
            try {
                FeatureQueryResult queryResult = queryResultFuture.get();
                Iterator<Feature> iterator = queryResult.iterator();
                if (iterator.hasNext()) {
                    Feature lastFeature = iterator.next();
                    global_id = String.valueOf(lastFeature.getAttributes().get("GlobalID"));
                    Log.e("GlobalId", String.valueOf(lastFeature.getAttributes().get("GlobalID")));

                    // Process the last feature
                }
            } catch (InterruptedException | ExecutionException e) {
                // Handle any exceptions
            }
        });
        return global_id;
    }

    public void newSession() {
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_inventory.geodatabase");
        if (!fileLoc.exists()) {
            showAlert("Geodatabase Missing!", "Harap Generate Geodatabase sebelum survei", SweetAlertDialog.WARNING_TYPE);
            return;
        }
        sessionId = DateTime.getStringLongDate() + "_" + estateCode + "_" + userID + "_" + "AWM" + "_" + "PSA" + "_" + sequenceSession;
//        String survId = DateTime.getStringLongDate()+"_"+GlobalHelper.getEstate().getEstCode()+"_"+GlobalHelper.getUser().getUserID()+"_AWM_PSA_001";
        sessionName = estateCode + "_" + userID + "_" + "PSA" + "_" + sequenceSession;
        List<String> a = new ArrayList<>();
        FragmentManager fm = getSupportFragmentManager();
        DialogNewSession dialogSessionFragment = DialogNewSession.newInstance(this, a, sessionId, sessionName, DateTime.getStringDate(), DateTime.getStringTime(), userFullName);
        dialogSessionFragment.show(fm, "");
    }


    public void setCensusSessionActive(String _sessionId) {
        saveSession(_sessionId);
//        checkCensusSession();
    }

    private void saveSession(String _sessionId) {
        Prefs.setPrefs()
                .putString(Prefs.SESSION_ID, _sessionId)
                .putBoolean(Prefs.ACTIVE_SESSION, true)
                .apply();
        Session toSave = new Session(_sessionId, sessionName,
                userID, Calendar.getInstance().getTime(), userID,
                Calendar.getInstance().getTime(), estateCode, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "", userFullName, "1",GlobalHelper.getAppVersion(this));
        mAssetViewModel.insertSession(toSave);
        checkCensusSession();
    }


    public void checkCensusSession() {
        if (Prefs.getBoolean(Prefs.ACTIVE_SESSION)) {
            String sessionActive = Prefs.getString(Prefs.SESSION_ID);

            String sessionName = mAssetViewModel.getSessionName();
            Log.e("Session Pref",sessionActive);
//            String[] parts = inputString.split("_");
            session_name.setText(sessionName);
            base_info.setVisibility(View.VISIBLE);
            fabAdd.setVisibility(View.VISIBLE);
//              refreshPointToCensus();
        } else {
            base_info.setVisibility(View.GONE);
            fabAdd.setVisibility(View.GONE);
        }
    }

    public void endSessionDialog(boolean param, int from) {
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(MapInventoryActivity.this);
        builder.setMessage("Apakah anda yakin akhiri sesi ini?");
        builder.setTitle("End Session");
        builder.setCancelable(false);
        builder.setPositiveButton("Oke", (dialog, which) -> {
            ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btEndSessionOk",new Date(),new Date(),this);

//            mAssetViewModel.updateEndSession(Prefs.getString(Prefs.SESSION_ID),addTimeStamp().getTime());
            if (from == 1) {
                Prefs.setPrefs()
                        .putString(Prefs.SESSION_ID, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION, false)
                        .putString(Prefs.BLOCK, "")
                        .apply();
                dialog.cancel();
                checkCensusSession();
                uploadImage();
//                loading.show();
//                uploadData();
            } else {
                Prefs.setPrefs()
                        .putString(Prefs.SESSION_ID, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION, false)
                        .putString(Prefs.BLOCK, "")
                        .apply();
                dialog.cancel();

                checkCensusSession();
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        // Create the Alert dialog
        AlertDialog alertDialog = builder.create();
        if (param) {
            alertDialog.show();
        } else {
            alertDialog.dismiss();
        }
    }


    public void OpenCamera() {
        ActivityLoggerHelper.recordHitTime(MapInventoryActivity.class.getName(),"btOpenCam",new Date(),new Date(),this);
        openCamState = OpenCam.FormObject;
        if (ALselectedImage.size() < 3) {
//            EasyImage.
            EasyImage.openCamera(this, 1);
        }
    }

    public void OpenCamera2() {
        openCamState = OpenCam.FormRealize;
        if (ALselectedImage.size() < 3) {
//            EasyImage.
            EasyImage.openCamera(this, 1);
        }
    }


    public static void markResurvey(Feature _feature, GraphicsOverlay graphicsOverlay) {
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.YELLOW, 12);
        Point addPoint;
        addPoint = (Point) _feature.getGeometry();
        List<Symbol> symbols = new ArrayList();
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);
        Graphic graph = new Graphic(addPoint, cms);
        graphicsOverlay.getGraphics().add(graph);

    }

    public static void markPoint(double lat, double lon, GraphicsOverlay graphicsOverlay) {
        isModeAdd = true;
//        CompositeSymbol cms2 = new CompositeSymbol();
//        SimpleLineSymbol sls = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,3,3);
        Log.e("AddMethod", "true");
        List<Symbol> symbols = new ArrayList();
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
        Point addPoint;
        addPoint = new Point(lat, lon, wgs84);
        pointTemp = addPoint;
//        points.add(addPoint);
//        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, 3), SimpleMarkerSymbol.STYLE.X);
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);
        Graphic graph = new Graphic(addPoint, cms);
        graphicsOverlay.getGraphics().add(graph);
//        final Graphic polylineGraphic = new Graphic(new android.graphics.Point(pointTemp), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT,
//                0xFFFF0000, 3));
//        graphicsOverlay.getGraphics().add(polylineGraphic);
    }


    public void checkDuplicateObject(String layerName) {
        fab_gps.callOnClick();
        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
        FeatureLayer checkLayer = getLayerFromName(layerName);
        mapView.getGraphicsOverlays().add(graphicsOverlay);
        int[] count = {0};
//        double radius = 10;
        double bufferInMeters = 50;
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
//        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
        Point addPoint;
        addPoint = new Point(gps.getX(), gps.getY(), wgs84);
        LinearUnit bufferUnit = new LinearUnit(LinearUnitId.METERS); // Create a linear unit representing meters
        Polygon bufferGeometry = GeometryEngine.bufferGeodetic(addPoint, bufferInMeters, bufferUnit, Double.NaN, GeodeticCurveType.GEODESIC);
//        addGraphWithRadius(addPoint);
        QueryParameters queryParameters = new QueryParameters();
//        queryParameters.setGeometry(bufferGeometry);
//        queryParameters.setSpatialRelationship(QueryParameters.SpatialRelationship.INTERSECTS);
        queryParameters.setGeometry(bufferGeometry);
        queryParameters.setSpatialRelationship(QueryParameters.SpatialRelationship.INTERSECTS);
        final ListenableFuture<FeatureQueryResult> futures = checkLayer.selectFeaturesAsync(queryParameters, FeatureLayer.SelectionMode.NEW);
        // Execute the query on the feature layer
        futures.addDoneListener(
                () -> {
                    try {
                        FeatureQueryResult result = futures.get();
//                        List<Feature> list = (List<Feature>) result.iterator();
//                        showToast(String.valueOf(list.size()));
                        for (Feature feature : result) {
                            count[0]++;
                        }
                        if(count[0]>0){
                            showAlertDuplicate(layerName);
                        }
                        else {
                            addNewObject(layerName);
                        }
//                        if (resultIterator.hasNext()) {
//                            count[0]++;
//                            // get the extent of the first feature in the result to zoom to
//                            Feature feature = resultIterator.next();
//
////                            Envelope envelope = feature.getGeometry().getExtent();
////                            mapView.setViewpointGeometryAsync(envelope, 10);
//                        }
//                        showToast(String.valueOf(count[0]));

                    } catch (Exception e) {
                        addNewObject(layerName);
                        throw new RuntimeException(e);
                    }
                }
        );
    }



    private void addGraphWithRadius(Point clickedPoint) {
        GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
        mapView.getGraphicsOverlays().add(graphicsOverlay);
        // Add a point graphic at the clicked location
        SimpleMarkerSymbol pointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.RED, 10);
        Graphic pointGraphic = new Graphic(clickedPoint, pointSymbol);
        graphicsOverlay.getGraphics().add(pointGraphic);
        SimpleLineSymbol planarOutlineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.BLACK, 2);
        // Create a buffer around the clicked location to represent the 100 meters radius
        SpatialReference spatialReference = mapView.getSpatialReference(); // Get the spatial reference of the map
        double bufferRadiusMeters = 20.0;
        LinearUnit bufferUnit = new LinearUnit(LinearUnitId.METERS); // Create a linear unit representing meters
        Polygon bufferGeometry = GeometryEngine.bufferGeodetic(clickedPoint, bufferRadiusMeters, bufferUnit, Double.NaN, GeodeticCurveType.GEODESIC);
        SimpleFillSymbol bufferSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, Color.TRANSPARENT, planarOutlineSymbol);
        Graphic bufferGraphic = new Graphic(bufferGeometry, bufferSymbol);
        graphicsOverlay.getGraphics().add(bufferGraphic);

    }



    public void bufferRadius(){
//        android.graphics.Point screenPoint = new android.graphics.Point(Math.round(centreX), Math.round(centreY));
        float pointY=mapView.getY() + mapView.getHeight() / 2;
        double bufferRadiusInMeters = 500;
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
        Geometry bufferGeometry = GeometryEngine.buffer(mLocationDisplay.getMapLocation(), bufferRadiusInMeters);
        Point centerPoint = new Point(mLocationDisplay.getMapLocation().getX(), mLocationDisplay.getMapLocation().getY(), SpatialReferences.getWgs84());
        Viewpoint currentViewpoint = mapView.getCurrentViewpoint(Viewpoint.Type.CENTER_AND_SCALE);
        android.graphics.Point screenPoint = mapView.locationToScreen(gps);
// Create a QueryParameters object for the spatial query
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setGeometry(bufferGeometry);
        queryParameters.setSpatialRelationship(QueryParameters.SpatialRelationship.INTERSECTS);
        ListenableFuture<List<IdentifyLayerResult>> identifyFuture = mapView.identifyLayersAsync(screenPoint, bufferRadiusInMeters, false);
        identifyFuture.addDoneListener(new Runnable() {
            @Override
            public void run() {
                try {
                    List<IdentifyLayerResult> identifyLayerResult = identifyFuture.get();
                    int count = 0;
                    for (IdentifyLayerResult identifyResult : identifyLayerResult) {
                        // Get the feature table associated with the layer
                        Log.e("Lname", identifyResult.getLayerContent().getName());
                        FeatureLayer featureLayer = (FeatureLayer) identifyResult.getLayerContent();
                        FeatureTable featureTable = featureLayer.getFeatureTable();
                        // Iterate over the geo elements in the result and extract the features
                        if (!Objects.equals(identifyResult.getLayerContent().getName(), "LUS3")) {
                            for (GeoElement geoElement : identifyResult.getElements()) {
                                if (geoElement instanceof Feature) {
                                    Feature feature = (Feature) geoElement;
                                    mSelectedArcGISFeature = (ArcGISFeature) feature;
                                    Map<String, Object> attr = feature.getAttributes();
                                    Set<String> keys = attr.keySet();
                                    count++;
                                    selectedFeature = feature;
                                    mFeatureTable = feature.getFeatureTable();
                                    com.esri.arcgisruntime.geometry.Point wgs84Point2 = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(feature.getGeometry(), SpatialReferences.getWgs84());
                                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());

//                                showBottomSheet2(selectedFeature,1);
                                }
                            }
                            Toast.makeText(MapInventoryActivity.this,String.valueOf(count), Toast.LENGTH_SHORT).show();
                        }
                    }


                } catch (Exception e) {
                    String error = "Select features failed: " + e.getMessage();
                    Toast.makeText(MapInventoryActivity.this, error, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, error);
                }
            }
        });
    }


    enum EditState {
        NotReady, // Geodatabase has not yet been generated
        Editing, // A feature is in the process of being moved
        Ready // The geodatabase is ready for synchronization or further edits
    }



    public static double distance(double lat1, double lat2, double lon1,double lon2) {
        final int R = 6371; // Radius of the earth
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        distance = Math.pow(distance, 2);
        return Math.sqrt(distance);
    }

    public static double bearing(double startLat, double endLat, double startLng,  double endLng){
        double longitude1 = startLng;
        double longitude2 = endLng;
        double latitude1 = Math.toRadians(startLat);
        double latitude2 = Math.toRadians(endLat);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }

    public void saveImage(String globalId){
        for(File file:ALselectedImage){
            String sess = Prefs.getString(Prefs.SESSION_ID);
            ImagesUpload imagesUpload = new ImagesUpload(file.getName(),globalId,file.getAbsolutePath(),sess, new Date(),1);
            mAssetViewModel.insertImageUpload(imagesUpload);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if(imageFiles.size() > 0) {
                    File picture = Compressor.getDefault(MapInventoryActivity.this).compressToFile(imageFiles.get(0));
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
                    picture = GlobalHelper.photoAddStamp(picture,gps.getY(),gps.getX());
                    long time = System.currentTimeMillis();
                    File newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + GlobalHelper.PROJECT_NAME + GlobalHelper.FOLEDRFOTOTEMP);
                    try {
                        selectedImage = GlobalHelper.moveFile(picture,
                                newdir,
                                Long.toString(time));
                        ALselectedImage.add(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    viewFormObject.setupimageslide();

                    if (openCamState == OpenCam.FormObject){
                        viewFormObject.setupimageslide();
                        Log.e("OpenCamState","FromObject");
                    }
                    else {
                        viewFormRealize.setupimageslide();
                        Log.e("OpenCamState","FromRealize");
                    }
//                    showPhoto(selectedImage.getAbsolutePath());
//                    Glide.with(MapInventoryActivity.this)
//                            .load(selectedImage)
//                            .asBitmap()
//                            .error(ResourcesCompat.getDrawable(MapInventoryActivity.this.getResources(), R.drawable.ic_add_photo, null))
//                            .centerCrop();
//                    isImageLoaded = true;
//                    viewAttributePoint.setupimageslide();
//                    viewFormObject.setupimageslide();

//                    iph.setupimageslide();
                }
            }
//            public void showPhoto(String imageFile) {
//                Bitmap myBitmap = BitmapFactory.decodeFile(imageFile);
//                    sliderLayout.setImageBitmap(myBitmap);
//
//
//            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
//                super.onCanceled(source, type);
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MapInventoryActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    public FeatureLayer getLayerFromName(String code){
        FeatureLayer featureLayer1 = null;
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                if (Objects.equals(layer.getName(), code)){
                    FeatureLayer featureLayer = (FeatureLayer) layer;
                   featureLayer1 = featureLayer;
                }
            }
        }
        return featureLayer1;
    }

    public void uploadImage(){
        showLoadingAlert("Uploading", "Please wait..");
        List<PostImage> toUpload = new ArrayList<>();
        List<ImagesUpload> images = mAssetViewModel.getAllImageUpload();
        Log.e("Count Images", String.valueOf(images.size()));
        for( ImagesUpload image : images){
            String nameFile = image.getImageId();
//            String nameImg = nameFile.substring(0, nameFile.lastIndexOf("."));
            PostImage newPost = new PostImage(
                    nameFile,
                    image.getSessionSurvey(),
                    GlobalHelper.getBase64FromFile(image.getLocation()),
                    ""
            );
            toUpload.add(newPost);
        }
        ApiServices apiServices = ApiClient.getClient().create(ApiServices.class);
        Call<ResponseBody> call = apiServices.postImages(toUpload);
        call.enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    images.forEach(c->c.setIsUpload(2));
                    mAssetViewModel.updateList(images);
//                    ALselectedImage.clear();
                    syncGeodatabase();
                    hideLoadingAlert();
                }
                else {
                    Log.e("errorBody", response.message());
//                    assert response.errorBody() != null;
                    Log.e("errorBody", response.errorBody().toString());
                    try {
                        hideLoadingAlert();
                        showAlert("Failed",response.errorBody().string(),SweetAlertDialog.ERROR_TYPE);

                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
           }

           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {
               hideLoadingAlert();
               showAlert("Failed",t.getMessage(),SweetAlertDialog.ERROR_TYPE);

           }
       });
    }

    public void postPR(){
       List<ToUploadPR>toUpload = mAssetViewModel.getAllPR();
        ApiServices apiServices = ApiClient.getClient().create(ApiServices.class);
        Call<ResponseBody> call = apiServices.postPr(toUpload);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void onCompassStopped() {
        pulsatorLayout.stop();
        this.layoutCompass.setVisibility(View.GONE);
        ch.stop();
    }
    public void onCompassStarted() {
        ch.start();
        pulsatorLayout.start();
        this.layoutCompass.setVisibility(View.VISIBLE);
//        GlobalHelper.hideKeyboard(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.resume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.dispose();
        mapView.destroyDrawingCache();
    }

}
