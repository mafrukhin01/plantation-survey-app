package id.co.ams_plantation.plantation_survey_app.ui;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.LicenseInfo;
import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.arcgisservices.LabelingPlacement;
import com.esri.arcgisruntime.arcgisservices.ServiceVersionParameters;
import com.esri.arcgisruntime.concurrent.Job;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.ArcGISFeature;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.data.FeatureTableEditResult;
import com.esri.arcgisruntime.data.Geodatabase;
import com.esri.arcgisruntime.data.GeodatabaseFeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.data.ServiceFeatureTable;
import com.esri.arcgisruntime.data.ServiceGeodatabase;
import com.esri.arcgisruntime.data.SyncModel;
import com.esri.arcgisruntime.geometry.AreaUnit;
import com.esri.arcgisruntime.geometry.AreaUnitId;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.GeodeticCurveType;
import com.esri.arcgisruntime.geometry.Geometry;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.GeometryType;
import com.esri.arcgisruntime.geometry.ImmutablePart;
import com.esri.arcgisruntime.geometry.Part;
import com.esri.arcgisruntime.geometry.PartCollection;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.Polygon;
import com.esri.arcgisruntime.geometry.PolygonBuilder;
import com.esri.arcgisruntime.geometry.Polyline;
import com.esri.arcgisruntime.geometry.PolylineBuilder;
import com.esri.arcgisruntime.geometry.ProximityResult;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.io.RequestConfiguration;
import com.esri.arcgisruntime.layers.ArcGISTiledLayer;
import com.esri.arcgisruntime.layers.ArcGISVectorTiledLayer;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.layers.KmlLayer;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.location.LocationDataSource;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.GeoElement;
import com.esri.arcgisruntime.mapping.MobileMapPackage;
import com.esri.arcgisruntime.mapping.Viewpoint;
import com.esri.arcgisruntime.mapping.labeling.ArcadeLabelExpression;
import com.esri.arcgisruntime.mapping.labeling.LabelExpression;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.IdentifyLayerResult;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapRotationChangedEvent;
import com.esri.arcgisruntime.mapping.view.MapRotationChangedListener;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.mapping.view.SketchCreationMode;
import com.esri.arcgisruntime.mapping.view.SketchEditConfiguration;
import com.esri.arcgisruntime.mapping.view.SketchEditor;
import com.esri.arcgisruntime.ogc.kml.KmlDataset;
import com.esri.arcgisruntime.portal.Portal;
import com.esri.arcgisruntime.security.UserCredential;
import com.esri.arcgisruntime.symbology.CompositeSymbol;
import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.Symbol;
import com.esri.arcgisruntime.symbology.TextSymbol;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.GeodatabaseSyncTask;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.SyncGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.SyncLayerOption;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.BuildConfig;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.SelectionAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ImagesUpload;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MapPointTPH;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PointEntity;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PostCheckout;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.services.model.HMSResponse;
import id.co.ams_plantation.plantation_survey_app.utils.ActivityLoggerHelper;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.SequenceNumber;
import id.co.ams_plantation.plantation_survey_app.utils.SheetAddPoint;
import id.co.ams_plantation.plantation_survey_app.utils.ShowBottomSheetDialog;
import id.co.ams_plantation.plantation_survey_app.utils.StyleFeature;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class MapReplantingActivity extends BaseActivity implements SelectionAdapter.ItemClickListener {
    private static final String TAG = "Test App";
    List<MapPointTPH> pointTPHList;
    Feature mFeature1, mFeature2;
    String appVersion = BuildConfig.VERSION_NAME;
    Dialog sweetAlertDialog ;
    ArcGISMap mMap;
    ArcGISMap mMap2;
    private MobileMapPackage mMapPackage;
    List<String> anckList = new ArrayList<>();
    ViewFormTPH viewFormTPH;
    public ArrayList<File> ALselectedImage = new ArrayList<>();

    ViewFormAncak viewFormAncak;
    ViewCreateFeature viewCreateFeature;
    ViewEditAncak viewEditAncak;

    @BindView(R.id.bmb)
    BoomMenuButton bmb;
    private MapRotationChangedListener listener;
    String sequenceSession, sessionId, sessionName;
    ViewGroup mCalloutContent;
    FragmentManager fm = getSupportFragmentManager();
    public FeatureTable mFeatureTable;
    private Point imaginaryPoint;
    boolean isModeFree;
    public String locKmlMap;
    private   Graphic lineGraphicImaginer;
    public OpenCam openCamState;
    public File selectedImage = null;
    enum OpenCam {
        FormObject, // Geodatabase has not yet been generated
        FormRealize, // A feature is in the process of being moved// The geodatabase is ready for synchronization or further edits
    }
//    FeatureLayer featureLayer;
    List<FeatureLayer> featureLayers;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.fabMain)
    FloatingActionButton fab_main;
    @BindView(R.id.fabNav)
    FloatingActionButton fab_navigate;
    @BindView(R.id.fab)
    FloatingActionButton fab_initiate;
    @BindView(R.id.fabMap)
    FloatingActionButton fab_map;
    @BindView(R.id.fabLayer)
    FloatingActionButton fab_layer;
//    @BindView(R.id.fab)
//    FloatingActionButton fab_set;
//    @BindView(R.id.fab2)
//    FloatingActionButton fab_versioning;
    @BindView(R.id.recycle_layer)
    RecyclerView rv_layer;
    @BindView(R.id.view_setting)
    LinearLayoutCompat view_setting;
    @BindView(R.id.button2)
    Button mGeodatabaseButton;
    @BindView(R.id.iv_target)
    ImageView mTarget;
    @BindView(R.id.bt_edit)
    Button finish;
    @BindView(R.id.pointButton)
    ImageButton mPointButton ;
    @BindView(R.id.pointsButton)
    ImageButton mMultiPointButton ;
    @BindView(R.id.polylineButton)
    ImageButton mPolylineButton ;
    GeodatabaseFeatureTable gdb ;
    @BindView(R.id.polygonButton)
    ImageButton mPolygonButton;
    @BindView(R.id.freehandLineButton)
    ImageButton mFreehandLineButton;
    @BindView(R.id.freehandPolygonButton)
    ImageButton mFreehandPolygonButton;
    @BindView(R.id.pointAdd)
    ImageButton mPointAdd;
    @BindView(R.id.makePolygon)
    ImageButton makePolygon;
    @BindView(R.id.pointsAdd)
    ImageButton mPointsAdd;
    @BindView(R.id.t_cancel)
    ImageButton bt_stop;
    @BindView(R.id.undo)
    ImageButton bt_undo;
    @BindView(R.id.redo)
    ImageButton bt_redo;
    @BindView(R.id.lineAdd)
    ImageButton bt_addLine;
    @BindView(R.id.toolbarInclude)
    ConstraintLayout toolbar;
    @BindView(R.id.toolbar)
    ConstraintLayout toolbarAdd;
    @BindView(R.id.fab3)
    FloatingActionButton fabAdd;
    @BindView(R.id.action)
    LinearLayout l_action;
    @BindView(R.id.tv_coordinate)
    TextView tv_cor;
    @BindView(R.id.viewDistance)
    TableLayout tb_distance;
    @BindView(R.id.tv_dis_manual)
    TextView tv_dis;
    @BindView(R.id.tv_deg_manual)
    TextView tv_deg;
    @BindView(R.id.base_info)
    CardView base_info;
    @BindView(R.id.session_name)
    TextView session_name;
    @BindView(R.id.bt_end_session)
    Button bt_end_session;
    ArrayList<LayerMap>layers;
    SelectionAdapter adapter;
    static boolean isModeAdd = false;
    KmlLayer kmlLayer;
    FeatureLayer featureLayer,featureLayer2,featureLayer3,featureLayer4;
    Callout mCallout;
    public List<EditFeature> editFeatures;
    private MapReplantingActivity.EditState mCurrentEditState;

    private TypeSurvey mTypeSurvey;
    private List<Feature> mSelectedFeatures;
    private Feature selectedFeature;
    private GeodatabaseSyncTask mGeodatabaseSyncTask;
    private ServiceFeatureTable mServiceFeatureTable;
    ServiceVersionParameters versionParameters;
    ServiceGeodatabase serviceGeodatabase;
    private Geodatabase mGeodatabase, mGeodatabase2;
    private GraphicsOverlay mGraphicsOverlay;
    private FeatureLayer mFeatureLayer;
    private android.graphics.Point mClickPoint;
    String surveyor, name, jenis;
    private ArcGISFeature mSelectedArcGISFeature;
    private boolean mFeatureUpdated;
    private String mSelectedArcGISFeatureAttributeValue;
    private static PointCollection points;
    @BindView(R.id.sniper)
    AppCompatImageView sniper;
    private SimpleMarkerSymbol mPointSymbol;
    private SimpleLineSymbol mLineSymbol;
    private SimpleFillSymbol mFillSymbol;
    private SketchEditor mSketchEditor;
    SketchEditConfiguration configuration;
    GraphicsOverlay layerDraw, layerImagine, layerTPH, layerBorder;
    public double latManual,longManual;
    public LocationDisplay mLocationDisplay;
    public  static com.esri.arcgisruntime.geometry.Point pointManual;
    private static Point pointTemp;
    private static final SpatialReference wgs84 = SpatialReferences.getWgs84();
    private static final SpatialReference web = SpatialReferences.getWebMercator();
    private String userID, estateCode, userFullName;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    @BindView(R.id.footer)
    CardView footer;
    private int tAncakInBlock=0;
    //    private GraphicsOverlay mGraphicsOverlay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mCurrentEditState = MapReplantingActivity.EditState.NotReady;
        mTypeSurvey = TypeSurvey.None;
        isModeAdd = false;
        pointTPHList = new ArrayList<>();
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();
        getLicense();
        LicenseInfo licenseInfo = LicenseInfo.fromJson(Prefs.getLicense(Prefs.LICENSE));
        Log.e("License Pref", Prefs.getLicense(Prefs.LICENSE));
        ArcGISRuntimeEnvironment.setLicense(licenseInfo);
        mSelectedFeatures = new ArrayList<>();
        configuration = new SketchEditConfiguration();
        configuration.setVertexEditMode(SketchEditConfiguration.SketchVertexEditMode.INTERACTION_EDIT);
        points = new PointCollection(wgs84);
        layerBorder = new GraphicsOverlay();
        layerDraw = new GraphicsOverlay();
        layerImagine = new GraphicsOverlay();
        layerTPH = new GraphicsOverlay();
        featureLayers = new ArrayList<>();
//        List<TPH> tphList = mAssetViewModel.getAllTPH();

        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " "
                + GlobalHelper.getEstate().getEstName());
        menuSetup();
        mAssetViewModel.watchSequenceSession().observe(this, integer -> {
            sequenceSession = SequenceNumber.getSequence(integer);
        });
//        checkCensusSession();

        mSketchEditor = new SketchEditor();
        mapView.setSketchEditor(mSketchEditor);


        mPointSymbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 20);
        mLineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.BLUE, 4);
        mFillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.CROSS, Color.BLUE, mLineSymbol);
        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_ANCAK);
//        mGeodatabaseSyncTask.setCredential(new UserCredential("editor1","editor1123"));
        mGeodatabaseButton.setOnClickListener(v -> {
//            generateGeodatabase();
            if (mCurrentEditState == EditState.NotReady) {
//                generateGeodatabase2();
            } else if (mCurrentEditState == EditState.Ready) {
                syncGeodatabase();
            }
        });
        setBaseMap();
        mAssetViewModel.watchAllTPH2().observe(this, tphList -> {
            StyleFeature.addStyleTPH2(tphList,this,layerTPH, pointTPHList);
        });
//        setMmpkMap();

        mapView.addViewpointChangedListener(viewpointChangedEvent -> {
            com.esri.arcgisruntime.geometry.Point center = mapView.getVisibleArea().getExtent().getCenter();
            com.esri.arcgisruntime.geometry.Point wgs84Point = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(center, wgs84);
            com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
            tv_cor.setText("Lat: "+ String.format("%.5f", wgs84Point.getY()) + " | Long: " + String.format("%.5f", wgs84Point.getX()));
            float centreX=mapView.getX() + mapView.getWidth()  / 2;
            float centreY=mapView.getY() + mapView.getHeight() / 2;
            android.graphics.Point screenPoint = new android.graphics.Point(Math.round(centreX), Math.round(centreY));
            Point mapPoint = mapView.screenToLocation(screenPoint);
            if (isModeAdd){
//                pointManual = pointTemp;
                Point wgs84Point2 = (Point) GeometryEngine.project(mapPoint, SpatialReferences.getWgs84());
                latManual = wgs84Point2.getX();
                longManual = wgs84Point2.getY();
                double imeter = distance(gps.getY(), longManual, gps.getX(), latManual);
                double bearing = bearing(gps.getY(), longManual, gps.getX(), latManual);
                tv_dis.setText(String.format("%.0f m",imeter));
                tv_deg.setText(String.format("%.0f",bearing));
                if(imeter>3){
                    tb_distance.setBackgroundColor(Color.RED);
                }
                else {
                    tb_distance.setBackgroundColor(Color.WHITE);
                }
            }
            else {
                tb_distance.setVisibility(View.GONE);
            }

        });

        fab_map.setOnClickListener(view -> {
            mLocationDisplay.setInitialZoomScale(499);
            mapView.setViewpointScaleAsync(499);
            mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
            if (!mLocationDisplay.isStarted())
                mLocationDisplay.startAsync();
        });

        fab_layer.setOnClickListener(view -> {
            if(mapView.isViewInsetsValid()){
                if(vectorTiledLayer!=null){
                    mapView.setViewpoint(new Viewpoint(vectorTiledLayer.getFullExtent()));
                }
            }
        });

        fab_main.setOnClickListener(v -> openFabMenu());

//        fab_versioning.setOnClickListener(view -> {
//            generateGeodatabase();
////            openVersioning();
//        });
        fab_initiate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGPS();
            }
        });
        finish.setOnClickListener(view -> {
            mCurrentEditState = EditState.Ready;
//            featureLayer.clearSelection();
            finish.setVisibility(View.GONE);

        });
        fabAdd.setOnClickListener(view -> {
//            showAddPointDialig();
//            showAddPointDialig();
          showCustomDialog();
        });

        bt_undo.setOnClickListener(view -> {
            if(!points.isEmpty()){
            points.remove(points.size()-1);
            mAssetViewModel.undoPoint();
            if(layerDraw.getGraphics().size()>=2){
               layerDraw.getGraphics().remove(layerDraw.getGraphics().size()-1);
               layerDraw.getGraphics().remove(layerDraw.getGraphics().size()-1);

            }else {
                layerDraw.getGraphics().remove(layerDraw.getGraphics().size() - 1);
            }
            }
//            undo();
        });
        bt_redo.setOnClickListener(view -> {
            loadBackup();
//            redo();
        });
        bt_stop.setOnClickListener(view -> {
          cancelSurvey();
//            stop();
        });
        mPointButton.setOnClickListener(view -> createModePoint());
        mMultiPointButton.setOnClickListener(view -> createModeMultipoint());
        mPolylineButton.setOnClickListener(view -> createModePolyline());
        mPolygonButton.setOnClickListener(view -> createModePolygon());
        mFreehandLineButton.setOnClickListener(view -> createModeFreehandLine());
        mFreehandPolygonButton.setOnClickListener(view -> createModeFreehandPolygon());
        mPointAdd.setOnClickListener(view -> {
            if(isModeFree){
                com.esri.arcgisruntime.geometry.Point center = mapView.getVisibleArea().getExtent().getCenter();
                com.esri.arcgisruntime.geometry.Point wgs84Point = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(center, wgs84);
                tv_cor.setText("Lat: "+ String.format("%.5f", wgs84Point.getY()) + " | Long: " + String.format("%.5f", wgs84Point.getX()));
                float centreX=mapView.getX() + mapView.getWidth()  / 2;
                float centreY=mapView.getY() + mapView.getHeight() / 2;
                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(centreX), Math.round(centreY));
                Point mapPoint = mapView.screenToLocation(screenPoint);
                Point wgs84Point2 = (Point) GeometryEngine.project(mapPoint, SpatialReferences.getWgs84());
                latManual =wgs84Point2.getX();
                longManual = wgs84Point2.getY();
                double imeter = distance(getGPS().getY(), longManual, getGPS().getX(), latManual);
//                markPointMod(latManual,longManual,layerDraw,selectedFeature);
//                markPoint(latManual,longManual,layerDraw,selectedFeature);
                if (imeter<25){
                    markPoint(latManual,longManual,layerDraw,selectedFeature);
//                    removeMapRotationListener();
                }else {
                    showAlert("Warning!","Jarak titik survey maks 10 M dari GPS",SweetAlertDialog.WARNING_TYPE);
                }

            }else {

                markPointImagine(imaginaryPoint,layerDraw,selectedFeature);
                removeMapRotationListener();
                isModeFree = true;

            }

        }
        );
        mPointsAdd.setOnClickListener(v -> {
            showNumberInputDialog2();

//            if(isModeFree) {
//                cutSplit2(selectedFeature);
//            }else {
//                cutSplitImaginer(selectedFeature,lineGraphicImaginer);
//            }
        });
        bt_end_session.setOnClickListener(v -> {
//            showListDialog();
//            showAlertNew();
//                checkCensusSession();
            endSessionDialog(true, 0);
        });
        fab_navigate.setOnClickListener(view -> {
            mLocationDisplay.setInitialZoomScale(250);
            mapView.setViewpointScaleAsync(250);
            mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.COMPASS_NAVIGATION);
            if (!mLocationDisplay.isStarted()) {
                mLocationDisplay.startAsync();
            }
        });

        makePolygon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    actionCreateAncak();
//                    cutSplit2(selectedFeature);
                } catch (Exception e) {
                    throw new RuntimeException(e);
//                    Toast.makeText(MapReplantingActivity.this,"Garis Pemotong tidak memotong Polygon",Toast.LENGTH_LONG);
                }

            }
        });

        bt_addLine.setOnClickListener(view -> {
            showNumberInputDialog();
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // Logika yang ingin Anda jalankan ketika tombol back ditekan
                // Misalnya: tampilkan dialog konfirmasi atau navigasi ke halaman sebelumnya
                handleCustomBackPressed();
            }
        };

        // Tambahkan callback ke OnBackPressedDispatcher
        getOnBackPressedDispatcher().addCallback(this, callback);

    }
    private void handleCustomBackPressed() {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this);
        exitAlert.setTitle("Keluar Survey?");
        exitAlert.setContentText("Apakah anda ingin keluar dari Survey Ancak?");
        exitAlert.setConfirmButton("Exit", sweetAlertDialog -> {
            finish();
        });
        exitAlert.show();
    }


    private void dialogAction(String message, String title) {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this);
        exitAlert.setTitle(title);
        exitAlert.setContentText(message);
        exitAlert.setConfirmButton("OKE", sweetAlertDialog -> {
            exitAlert.dismiss();
        });
        exitAlert.show();
    }
    private void cancelSurvey() {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this);
        exitAlert.setTitle("Batalkan Survey?");
        exitAlert.setContentText("Anda ingin membatalkan memotong ancak? ");
        exitAlert.setConfirmButton("Batalkan", sweetAlertDialog -> {
            removeMapRotationListener();
            layerImagine.getGraphics().clear();
            if(sniper.getVisibility()==View.VISIBLE){
                isModeFree = false;
                mCurrentEditState = EditState.Ready;
                sniper.setVisibility(View.GONE);
                toolbarAdd.setVisibility(View.GONE);
                l_action.setVisibility(View.GONE);
                fabAdd.setVisibility(View.VISIBLE);
                points.clear();
                mAssetViewModel.clearPointsBackup();
                layerDraw.getGraphics().clear();
            }
            exitAlert.dismiss();
        });
        exitAlert.show();
    }






//    private boolean shouldExitApp() {
//        // Logika untuk menentukan apakah harus keluar dari aplikasi
//        return true; // Ganti dengan kondisi Anda
//    }


    private void openFabMenu() {
        if (fab_map.getVisibility() == View.VISIBLE) {
            fab_main.setImageResource(R.drawable.fab_open);
            fab_map.setVisibility(View.GONE);
            fab_layer.setVisibility(View.GONE);
            fab_initiate.setVisibility(View.GONE);
            fab_navigate.setVisibility(View.GONE);

        } else {
            fab_main.setImageResource(R.drawable.fab_close);
            fab_map.setVisibility(View.VISIBLE);
            fab_layer.setVisibility(View.VISIBLE);
            fab_initiate.setVisibility(View.VISIBLE);
            fab_navigate.setVisibility(View.VISIBLE);
    }
// Change to a close icon
    }

    public void setVisible(int i){
        mapView.getMap().getOperationalLayers().get(i).setVisible(true);
    }

    public void setInVisible(int i){
        mapView.getMap().getOperationalLayers().get(i).setVisible(false);
    }

    public void setOpacity(int tag, float result){
        mapView.getMap().getOperationalLayers().get(tag).setOpacity(result);
    }
    public void actionCreateAncak() {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this);
        exitAlert.setTitle("Buat Ancak?");
        exitAlert.setContentText("Pastikan titik awal dan titik akhir survey memotong batas Ancak!");
        exitAlert.setConfirmButton("Buat", sweetAlertDialog -> {
            exitAlert.dismiss();
            cutSplit2(selectedFeature);
        });
        exitAlert.setCancelButton("Batal", sweetAlertDialog1 -> {
            exitAlert.dismiss();
        });
        exitAlert.show();
    }


    private void startGPS(){
        if(mapView!=null){
            if(mLocationDisplay.isStarted()) {
                mLocationDisplay.stop();
                mLocationDisplay.startAsync();
            }else {
                mLocationDisplay.startAsync();
            }
            double CITY_SCALE = 288895.277144;
            mLocationDisplay = mapView.getLocationDisplay();
            mLocationDisplay.setAutoPanMode(LocationDisplay.AutoPanMode.RECENTER);
            mLocationDisplay.setInitialZoomScale(CITY_SCALE);
            mLocationDisplay.addDataSourceStatusChangedListener(new LocationDisplay.DataSourceStatusChangedListener() {
                @Override
                public void onStatusChanged(LocationDisplay.DataSourceStatusChangedEvent dataSourceStatusChangedEvent) {
                    Log.i(TAG, "onStatusChanged: isStarted: "+dataSourceStatusChangedEvent.isStarted());
//                    Log.i(TAG, "onStatusChanged: isStarted: ");
                }
            });
            mLocationDisplay.addLocationChangedListener(new LocationDisplay.LocationChangedListener() {
                @Override
                public void onLocationChanged(LocationDisplay.LocationChangedEvent locationChangedEvent) {
                    Log.i(TAG, "onLocationChanged: latitude: "+locationChangedEvent.getLocation().getPosition().getY()+" longitude: "+locationChangedEvent.getLocation().getPosition().getX()+" atltitude: "+locationChangedEvent.getLocation().getPosition().getZ());
//                String altitude = String.format("%.2f", locationChangedEvent.getLocation().getPosition().getZ())+" m";
                    String altitude = String.format("%d", (int)locationChangedEvent.getLocation().getPosition().getZ())+"m";
                    String coordinate =String.format("%.5f , %.5f", locationChangedEvent.getLocation().getPosition().getX(),locationChangedEvent.getLocation().getPosition().getY());
//                tvAltitude.setText(""+altitude);
//                llAltitude.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    ArcGISVectorTiledLayer vectorTiledLayer;
    ArcGISTiledLayer tiledLayer;
    protected void setBaseMap() {
        String vtpk = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VTPK);
        layers = new ArrayList<>();
        String path = GlobalHelper.decryptFiles(GlobalHelper.TYPE_TPK);
        tiledLayer = new ArcGISTiledLayer(path);

//        bitmaps = new ArrayList<>();
//        String path2 = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
        vectorTiledLayer = new ArcGISVectorTiledLayer(vtpk);
//        String pathS = Environment.getExternalStorageDirectory() + GlobalHelper.MMPK_LOCATION+"/"+"THIP3_Kebun_2.vtpk";
//        Log.e("path location",pathS);
//        mMapPackage = new MobileMapPackage(pathS);
//        mMapPackage.loadAsync();
//        ArcGISMap arcGISMap = new ArcGISMap(new Basemap(vectorTiledLayer));

        mMap = new ArcGISMap(new Basemap(vectorTiledLayer));
        mMap2 = new ArcGISMap(new Basemap(tiledLayer));
        mapView.setMap(mMap);
//        mapView.setMap(mMap2);
        mLocationDisplay = mapView.getLocationDisplay();
        if (!mLocationDisplay.isStarted())
            mLocationDisplay.startAsync();
        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        if (locKmlMap != null) {
            KmlDataset kmlDataset = new KmlDataset(locKmlMap);
            kmlLayer = new KmlLayer(kmlDataset);
//            kmlLayer.setOpacity(0.03f);
            mapView.getMap().getOperationalLayers().add(kmlLayer);
            kmlLayer.setVisible(false);
            kmlDataset.addDoneLoadingListener(() -> {
                if (kmlDataset.getLoadStatus() != LoadStatus.LOADED) {
                    String error = "Failed to load kml layer from URL: " + kmlDataset.getLoadError().getMessage();
                    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
//                    Log.e(TAG, error);
                }
            });
        }
        readBLK();
//        List<TPH>tphList = mAssetViewModel.getAllTPH();
//        Log.e("TPH size", String.valueOf(tphList.size()));
//        StyleFeature.addStyleTPH(tphList,this,layerTPH);
//        checkCensusSession();
    }

    public void setCensusSessionActive(String _sessionId, String _block, String _sessionName) {
        saveSession(_sessionId, _block, _sessionName);
//        checkCensusSession();
    }
    private void saveSession(String _sessionId, String _block, String _name) {
        Prefs.setPrefs()
                .putString(Prefs.SESSION_ID_ANCAK, _sessionId)
                .putBoolean(Prefs.ACTIVE_SESSION_ANCAK, true)
                .putString(Prefs.BLOCK_ANCAK, _block)
                .apply();
        Session toSave = new Session(_sessionId, _name,
                userID, Calendar.getInstance().getTime(), userID,
                Calendar.getInstance().getTime(), estateCode, Calendar.getInstance().getTime(),
                Calendar.getInstance().getTime(), "", userFullName, "1",GlobalHelper.getAppVersion(this));
        mAssetViewModel.insertSession(toSave);
        checkCensusSession();
    }

    public void checkCensusSession() {
        if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_ANCAK)) {
            String sessionActive = Prefs.getString(Prefs.SESSION_ID_ANCAK);
            String sessionName = mAssetViewModel.getSessionName();
            Log.e("Session Pref",sessionActive);
//            String[] parts = inputString.split("_");
            session_name.setText(sessionName);
            base_info.setVisibility(View.VISIBLE);
            fabAdd.setVisibility(View.VISIBLE);
            queryAndDrawBorder(featureLayer2.getFeatureTable(),Prefs.getString(Prefs.BLOCK_ANCAK));
//              refreshPointToCensus();
        } else {
            base_info.setVisibility(View.GONE);
            fabAdd.setVisibility(View.GONE);
        }
    }

    protected void showBottomSheet3(int _action, Feature _feature, boolean isBlock, Feature baseFeature) {
        viewFormAncak = new ViewFormAncak(this, _feature,isBlock,baseFeature);
        viewFormAncak.showView(_action);
    }
    protected void showAssignAncak(Feature _feature) {
        viewCreateFeature = new ViewCreateFeature(this, _feature);
        viewCreateFeature.showView(1);
    }
    protected void showEditAncak(Feature _feature) {
        String _block = Prefs.getString(Prefs.BLOCK_ANCAK);
        String _block2 = String.valueOf(_feature.getAttributes().get("Flag"));
        if (mAssetViewModel.isOpen(_block)){
            viewEditAncak = new ViewEditAncak(this, _feature,true);
        }else {
            viewEditAncak = new ViewEditAncak(this, _feature,false);
        }
        Log.e("Flag",_block2);

        viewEditAncak.showView(1);
    }

    public void showAlertJoinTPH(){
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setConfirmButton("OKE", sweetAlertDialog -> {
                    mTypeSurvey = TypeSurvey.JoinTPH;
                    sweetAlertDialog.dismiss();
                })
//                .setCancelButtonBackgroundColor(Color.RED)`
                .showCancelButton(false)
                .setTitleText("Ikatkan TPH & Ancak")
                .setContentText("Silahkan pilih ancak untuk mengikatkan TPH dengan Ancak");
        sweetAlertDialog.show();

    }
    protected void showBottomSheet4(int _action) {
//        getAncakList("B11");
        int usrId = Integer.valueOf(userID);
        MasterBlock _masterBlock= mAssetViewModel.getSelectedBlock();
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
        long time = System.currentTimeMillis();
        TPH newTPH = new TPH(gps.getY(),gps.getX(),_masterBlock.getAfd(),_masterBlock.getBlock(),usrId, time,estateCode,1,usrId,time,userID,appVersion);
        viewFormTPH = new ViewFormTPH(this,userID, newTPH);
        viewFormTPH.showView(_action);
    }
    public void saveNewTPH(TPH newTPH, String userID){
        String tphCode= mAssetViewModel.createNewTph(userID,newTPH);
        List<String> _foto = new ArrayList<>();
        layerDraw.getGraphics().clear();
        for(File file:ALselectedImage){
            String sess = Prefs.getString(Prefs.SESSION_ID_ANCAK);
            ImagesUpload imagesUpload = new ImagesUpload(file.getName(),tphCode,file.getAbsolutePath(),sess, new Date(),1);
            mAssetViewModel.insertImageUpload(imagesUpload);
            _foto.add(file.getAbsolutePath());
        }
        mAssetViewModel.updateFoto(_foto, tphCode);
//       Log.e("NewTPH", mAssetViewModel.createNewTph(userID,newTPH));
    }

    public com.esri.arcgisruntime.geometry.Point getGPS(){
        com.esri.arcgisruntime.geometry.Point _gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
        return _gps;
    }
    public String getIdTPH(String block, String noTPH){
        return mAssetViewModel.getIdTPH(block,noTPH);
    }

    private void showNumberInputDialog2() {
        // Membuat objek AlertDialog.Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Menyeting layout kustom untuk dialog
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_input, null);
        builder.setView(dialogView);

        // Menemukan EditText dari layout kustom
        final EditText input = dialogView.findViewById(R.id.editTextLength);
        final EditText input2 = dialogView.findViewById(R.id.editTextAngle);

        // Input filter untuk membatasi nilai maksimal pada input pertama
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    double input = Double.parseDouble(dest.toString() + source.toString());
                    if (input > 400) {
                        return "";
                    }
                } catch (NumberFormatException nfe) { }
                return null;
            }
        };
        input.setFilters(new InputFilter[]{filter});

        // Mengatur judul dan tombol pada dialog
        builder.setTitle("Masukkan Panjang Garis dan Sudut");
        builder.setPositiveButton("OK", (dialog, which) -> {
            if (points.isEmpty()){
                showAlert("Tambahkan Titik Awal", "Belum ada titik sebelumnya", SweetAlertDialog.WARNING_TYPE);
                return;
            }
            isModeFree = false;
            if (sniper.getVisibility() == View.VISIBLE){
                sniper.setVisibility(View.GONE);
            }

            String value = input.getText().toString();
            String value2 = input2.getText().toString();

            if (!value.isEmpty() ) {
                Double number = Double.parseDouble(value);
                int number2 =0;
                if (!value2.isEmpty()){
                    number2= Integer.parseInt(value2);
                }
                int finalNumber = number2;
                Toast.makeText(MapReplantingActivity.this, "Panjang garis: " + number + " Meter\nSudut: " + number2 + " Derajat", Toast.LENGTH_LONG).show();
                drawImaginary4(points.get(points.size()-1), layerImagine, number, number2);
                removeMapRotationListener();
                listener = new MapRotationChangedListener() {
                    @Override
                    public void mapRotationChanged(MapRotationChangedEvent mapRotationChangedEvent) {
                        drawImaginary4(points.get(points.size()-1), layerImagine, number, finalNumber);
                    }
                };
                mapView.addMapRotationChangedListener(listener);
            } else {
                Toast.makeText(MapReplantingActivity.this, "Masukan panjang garis dan sudut (opsional)", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Batal", (dialog, which) -> {
            removeMapRotationListener();
            if (sniper.getVisibility() == View.GONE){
                sniper.setVisibility(View.VISIBLE);
            }
            layerImagine.getGraphics().clear();
            dialog.cancel();
        });

        builder.show();
    }

    public void updateGeometry(TPH tph) {
        com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), wgs84);
        double imeter = distance(gps.getY(), longManual, gps.getX(), latManual);
        if (imeter > 15) {
            showAlert("Diluar jangkauan", "Maksimal jarak 30 meter", SweetAlertDialog.ERROR_TYPE);
//            showToast("Maksimal jarak 30 meter");
        } else {
            layerDraw.getGraphics().clear();
            Point addPoint = new Point(latManual, longManual, wgs84);
            tph.setLatitude(addPoint.getY());
            tph.setLongitude(addPoint.getX());
//            showToast("Berhasil Update posisi");
            showAlert("Update geometry", "Berhasil Update posisi", SweetAlertDialog.SUCCESS_TYPE);
            SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
            List<Symbol> symbols = new ArrayList();
            symbols.add(symbol);
            CompositeSymbol cms = new CompositeSymbol(symbols);
            Graphic graph = new Graphic(addPoint, cms);
            layerDraw.getGraphics().add(graph);
        }

    }

    public void loadBackup(){
        PointCollection backup ;

        List<PointEntity> lBackups = mAssetViewModel.getPointBackups();
        if(lBackups.size()>0){
            points.clear();
            for (PointEntity p :lBackups){
                Point _p= new Point(p.longitude, p.latitude, wgs84);
                Log.e("AddMethod", "true");
                List<Symbol> symbols = new ArrayList();
                SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
                Point addPoint;
                points.add(_p);
//        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, 3), SimpleMarkerSymbol.STYLE.X);
                symbols.add(symbol);
                CompositeSymbol cms = new CompositeSymbol(symbols);
                Graphic graph = new Graphic(_p, cms);
                layerDraw.getGraphics().add(graph);
                final Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT,
                        0xFFFF0000, 3));
                layerDraw.getGraphics().add(polylineGraphic);

            }
        }
    }

    public void menuSetup() {
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setBackgroundEffect(true);
        bmb.clearBuilders();
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_3);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_3);
        bmb.addBuilder(syncBMB());
        bmb.addBuilder(newSessionBMB());
        bmb.addBuilder(baseMap());
//        bmb.addBuilder(feedback());
//        bmb.addBuilder(hasilCensus());
//        bmb.addBuilder(hasilCensus());
//        bmb.addBuilder(baseMap());
//        bmb.addBuilder(upBMB());
    }

    private HamButton.Builder baseMap(){
        return new HamButton.Builder()
                .normalText("Base Map")
                .subNormalText("Pilih basemap aplikasi")
                .normalImageRes(R.drawable.map)
                .pieceColorRes(R.color.Green)
                .normalColorRes(R.color.Green)
                .listener(index ->{switchBasemap();}
                );
    }

    private HamButton.Builder syncBMB() {
        return new HamButton.Builder()
                .normalText("Sync Data")
                .subNormalText("Upload dan Sinkronasi Data")
                .normalImageRes(R.drawable.refresh)
                .pieceColorRes(R.color.Tomato)
                .normalColorRes(R.color.Tomato)
                .listener(index -> {
//                    uploadTPH("");
                    showUploadSyncDialog("1");
//                    countingFeatureInBlock(mAssetViewModel.getSelectedBlock().getBlock());

                });
    }
    private HamButton.Builder newSessionBMB() {
        return new HamButton.Builder()
                .normalText("Create New Session")
                .subNormalText("Buat sesi untuk memulai Survey")
                .normalImageRes(R.drawable.sketchbook)
                .pieceColorRes(R.color.YellowGreen)
                .normalColorRes(R.color.YellowGreen)
                .listener(index -> {
                    ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btBmbNewSession",new Date(),new Date(),this);
                    if (Prefs.getBoolean(Prefs.ACTIVE_SESSION_ANCAK)) {
                        endSessionDialog(true, 0);
                    } else {
                        newSession();
                    }
                });
    }
    private HamButton.Builder feedback(){
        return new HamButton.Builder()
                .normalText("Ticketing")
                .subNormalText("Kirim ticket ke Admin GIS")
                .normalImageRes(R.drawable.feedback)
                .pieceColorRes(R.color.WhiteTrans)
                .normalColorRes(R.color.WhiteTrans)
                .listener(index -> {
//                    PDCApp.deleteCache(MapSlidingup.this);
//                    qcDuplicateCensus();
//                    Intent intent = new Intent(this, FeedbackActivity.class);
//                    startActivity(intent);
//                    testTimeStamp();
//                    dispatchTakePictureIntent();

//                    syncData();
                });
    }

    private HamButton.Builder hasilCensus(){
        return new HamButton.Builder()
                .normalText("Kalkulasi Pokok")
                .subNormalText("Hitung jumlah pokok per Ancak")
                .normalImageRes(R.drawable.stadistics)
                .pieceColorRes(R.color.CornflowerBlue)
                .normalColorRes(R.color.CornflowerBlue)
                .listener(index -> {
                            dialogAction("Upload Data Berhasil","Berhasil Upload!");
//                    Intent intent = new Intent(getApplicationContext(), MenuResultActivity.class);
//                    startActivityForResult(intent, 1898);
                }
                );
    }

    private void switchBasemap(){
        DialogBasemap basemapDialog = DialogBasemap.newInstance(this);
        basemapDialog.show(fm,"Basemap");
    }
    public void setAerialPhoto(){
//         mMap.getBasemap().getBaseLayers().remove(0);
//         mMap.getBasemap().getBaseLayers().add(tiledLayer);
        mapView.getMap().getOperationalLayers().clear();
//        mMap.setBasemap(base_a);
        mapView.setMap(mMap2);
        mMap2.getOperationalLayers().add(featureLayer);
        mMap2.getOperationalLayers().add(featureLayer2);
    }
    public void setVectorTPK(){
//        mMap.setBasemap(base_v);
        mapView.getMap().getOperationalLayers().clear();
        mapView.setMap(mMap);
        mMap.getOperationalLayers().add(featureLayer);
        mMap.getOperationalLayers().add(featureLayer2);

    }
    public void newSession() {
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_ancak.geodatabase");
        if (!fileLoc.exists()) {
            showAlert("Geodatabase Missing!", "Harap Checkout Ulang Block", SweetAlertDialog.WARNING_TYPE);
            return;
        }
        MasterBlock _selectedBlock = mAssetViewModel.getSelectedBlock();
        List<String> _blocks = mAssetViewModel.getSelectedBlocks();
        sessionId = "S" + ";" + estateCode + ";" + _blocks.get(0) +";" + userID + ";" + "ANC" + ";" + "PSA" + "_" + sequenceSession;
//        String survId = DateTime.getStringLongDate()+"_"+GlobalHelper.getEstate().getEstCode()+"_"+GlobalHelper.getUser().getUserID()+"_AWM_PSA_001";
        sessionName = estateCode + "_" + userID + "_" + _selectedBlock.getBlock() + "_" + sequenceSession;
        List<String> a = new ArrayList<>();
        FragmentManager fm = getSupportFragmentManager();
        DialogNewSessionAncak dialogSessionFragment = DialogNewSessionAncak.newInstance(this, _blocks, sessionId, sessionName, DateTime.getStringDate(), DateTime.getStringTime(), userFullName);
        dialogSessionFragment.show(fm, "");
    }

    public void endSessionDialog(boolean param, int from) {
        android.app.AlertDialog.Builder builder
                = new android.app.AlertDialog
                .Builder(MapReplantingActivity.this);
        builder.setMessage("Apakah anda yakin akhiri sesi ini?");
        builder.setTitle("End Session");
        builder.setCancelable(false);
        builder.setPositiveButton("Oke", (dialog, which) -> {
            ActivityLoggerHelper.recordHitTime(MapHguActivity.class.getName(),"btEndSessionOk",new Date(),new Date(),this);
//            mAssetViewModel.updateEndSession(Prefs.getString(Prefs.SESSION_ID),addTimeStamp().getTime());
            if (from == 1) {
                Prefs.setPrefs()
//                        .putString(Prefs.SESSION_ID_ANCAK, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION_ANCAK, false)
                        .putString(Prefs.BLOCK_ANCAK, "")
                        .apply();
                dialog.cancel();

//                uploadImage();
//                loading.show();
//                uploadData();
            } else {
                Prefs.setPrefs()
//                        .putString(Prefs.SESSION_ID_HGU, sessionId)
                        .putBoolean(Prefs.ADD_MODE, false)
                        .putBoolean(Prefs.ACTIVE_SESSION_ANCAK, false)
                        .putString(Prefs.BLOCK_ANCAK, "")
                        .apply();
                dialog.cancel();

                checkCensusSession();
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        // Create the Alert dialog
        android.app.AlertDialog alertDialog = builder.create();
        if (param) {
            alertDialog.show();
        } else {
            alertDialog.dismiss();
        }
    }


    public void postImage()  {
        List<JSONObject>listFinal = new ArrayList<>();
        List<ImagesUpload> imagesList = mAssetViewModel.getAllImageUpload();
//        Log.e("PostImage", String.valueOf(imagesList.size()));
        if(imagesList.size() !=0) {
            for (ImagesUpload image : imagesList) {
//                Log.e("Image ID", image.getImageAlias());
                JSONObject postImage = new JSONObject();
                JSONArray listImages = new JSONArray();
                JSONObject childImage = new JSONObject();
                String path = image.getLocation();
                File file = new File(path);
                try {
                    childImage.put("filename", file.getName());
                    childImage.put("filesize", file.getTotalSpace());
                    childImage.put("content", GlobalHelper.getBase64FromFile(file.getAbsolutePath()));
                    childImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                    listImages.put(childImage);
                    postImage.put("noTph", image.getGlobalId());
                    postImage.put("estCode", GlobalHelper.getEstate().getEstCode());
                    postImage.put("photoBy", GlobalHelper.getUser().getUserID());
                    postImage.put("images", listImages);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listFinal.add(postImage);
            }

//            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), listFinal.toString());
            Call<ResponseBody> call = hmsAppsServices.postImageTPH(bodyRequest,estateCode);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()){
                        imagesList.forEach(c->c.setIsUpload(2));
                        mAssetViewModel.updateList(imagesList);
//                        mAssetViewModel.updateDoneUpload();
                        dialogAction("Upload Data Berhasil","Berhasil Upload!");
//                        showAlert("Uploaded","Images uploades",SweetAlertDialog.SUCCESS_TYPE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                    dialogAction("Upload Data Gagal!","Failed Upload!");
                }
            });

        }

    }


    private void showUploadSyncDialog(String cib) {
        long longDate = Prefs.getLong(Prefs.LAST_SYNC_DATA);
        Date theDate = new Date(longDate);
//        DataToUpload data = mSessionViewModel.getCountSessionToUpload();
        DialogUploadAncak dialogUploadSync = DialogUploadAncak.newInstance(this, theDate, cib, "0");
        if (isNetworkConnected()) {
            dialogUploadSync.show(fm, "");
        } else {
            alertDialog(true);
        }
//        Log.e("Ancak Block ", String.valueOf(countingFeatureInBlock(mAssetViewModel.getSelectedBlock().getBlock())));
    }

    private void showLayer(int[] list){
        for(int i :list){
            mapView.getMap().getOperationalLayers().get(i).setVisible(true);
        }
    }

    private void hideLayer(int[] list){
        for(int i :list){
            mapView.getMap().getOperationalLayers().get(i).setVisible(false);
        }
    }
    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    private static Polyline createBorder() {
        PointCollection points = new PointCollection(SpatialReferences.getWebMercator());
        points.add(new Point(-03, 6111053.281447));
        points.add(new Point(-9946518.044066, 6102350.620682));
        points.add(new Point(-9872545.427566, 6152390.920079));
        points.add(new Point(-9838822.617103, 6157830.083057));
        points.add(new Point(-9446115.050097, 5927209.572793));
        points.add(new Point(-9430885.393759, 5876081.440801));
        points.add(new Point(-9415655.737420, 5860851.784463));
        return new Polyline(points);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if (requestCode == 1998 && resultCode == 1998) {
//            Log.e("result move","Oke");
////            readGeodatabase();
//            refreshAllLayers();
//            // Panggil fungsi di sini
//        }
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if(imageFiles.size() > 0) {
                    File picture = Compressor.getDefault(MapReplantingActivity.this).compressToFile(imageFiles.get(0));
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(),wgs84);
                    String _lat = String.format(Locale.US, "%.5f", gps.getY());
                    String _long = String.format(Locale.US, "%.5f", gps.getX());
                    String desc = DateTime.getTimeStamp()+";"+GlobalHelper.getEstate().getCompanyShortName() + " "
                            + GlobalHelper.getEstate().getEstName()+":"+Prefs.getString(Prefs.BLOCK_ANCAK)+";"+_lat+";"+_long;
                    long time = System.currentTimeMillis();
                    String _name = time +"_"+_lat+"_"+_long;
//                    picture = GlobalHelper.photoAddStamp(picture,gps.getY(),gps.getX());
                    File newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES + GlobalHelper.PROJECT_NAME + GlobalHelper.FOLDER_PHOTO_TPH);
                    try {
                        selectedImage = GlobalHelper.moveFilePhoto(picture,
                                newdir,
                                _name,gps.getY(),gps.getX(),DateTime.getTimeStamp(),desc);
                        ALselectedImage.add(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    viewFormObject.setupimageslide();

                    viewFormTPH.setupimageslide();

                }
            }


            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
//                super.onCanceled(source, type);
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(MapReplantingActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }




   @SuppressLint("ClickableViewAccessibility")
   private void touchListener(){
        mapView.setOnTouchListener(
               new DefaultMapViewOnTouchListener(MapReplantingActivity.this, mapView) {
                   @Override
                   public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                       android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));

                       if(mTypeSurvey == TypeSurvey.MergeAncak){
                           selectMultiple(screenPoint);
                       } else if (mTypeSurvey == TypeSurvey.AddTPH) {
                           setClickListenerTPH();
                       } else if (mTypeSurvey == TypeSurvey.JoinTPH){
                           Log.e("true","listener");
                           setClickListenerTPH();
//                           selectFeature2(screenPoint);
                       }
                       else {
                           selectFeature2(screenPoint);
                       }
                       return true;
                   }
               });
   }


    private void moveSelectedFeatureTo(Point point) {
        for (Feature feature : mSelectedFeatures) {
            feature.setGeometry(point);
            feature.getFeatureTable().updateFeatureAsync(feature);
        }
        mSelectedFeatures.clear();
        mCurrentEditState = MapReplantingActivity.EditState.Ready;
//        mGeodatabaseButton.setText(R.string.sync_geodatabase_button_text);
//        mGeodatabaseButton.setVisibility(View.VISIBLE);
    }

    public void updateFeatureAncak(Feature _feature){
        _feature.getFeatureTable().updateFeatureAsync(_feature);
    }

    private Point mapPoin8tFrom(MotionEvent motionEvent) {
        // get the screen point
        android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
                Math.round(motionEvent.getY()));
        // return the point that was clicked in map coordinates
        return mapView.screenToLocation(screenPoint);
    }


    private void moveLine(Point point){
        Feature feature = selectedFeature;
        Polyline  polyline = (Polyline) selectedFeature.getGeometry();
//        GeometryEngine geometryEngine;
        Geometry geometry = GeometryEngine.project((Geometry)point, polyline.getSpatialReference());
        ProximityResult nearestVertex = GeometryEngine.nearestVertex(polyline, (Point) GeometryEngine.project(point,polyline.getSpatialReference()));
        PolylineBuilder polylineBuilder = new PolylineBuilder(polyline);
        PartCollection parts = polylineBuilder.getParts();
        Part part = parts.get((int) nearestVertex.getPartIndex());
        part.removePoint((int)nearestVertex.getPointIndex());
        part.addPoint((com.esri.arcgisruntime.geometry.Point)geometry);
        feature.setGeometry(polylineBuilder.toGeometry());
        feature.getFeatureTable().updateFeatureAsync(feature);

//        mServiceFeatureTable.updateFeatureAsync(feature);
//        polylineBuilder.addParts(new PartCollection(SpatialReference.create((int) nearestVertex.getPartIndex())));
    }


    public void closeCallout(){
        mCallout.dismiss();

//        layer.clearSelection();
    }

    private void selectFeature2(android.graphics.Point screenPoint) {
        mCallout = mapView.getCallout();
        if (mCallout.isShowing()) {
            mCallout.dismiss();
        }
        Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                ((FeatureLayer) layer).clearSelection();
            }
        }

        final double mapTolerance = 10;
        ListenableFuture<List<IdentifyLayerResult>> identifyLayerResultsFuture = mapView.identifyLayersAsync(screenPoint, mapTolerance, false, -1);
        identifyLayerResultsFuture.addDoneListener(() -> {
            try {
                List<IdentifyLayerResult> identifyLayerResults = identifyLayerResultsFuture.get();
                List<Feature> featureList = new ArrayList<>();
                for (IdentifyLayerResult identifyLayerResult : identifyLayerResults) {
                    for (GeoElement geoElement : identifyLayerResult.getElements()) {
                        if (geoElement instanceof Feature) {
                            Feature feature = (Feature) geoElement;
                            featureList.add(feature);

                        }
                    }
                }
                if (!featureList.isEmpty()) {
                    // Pilih fitur terakhir yang ditemukan
                    selectedFeature = featureList.get(featureList.size() - 1);
                    FeatureLayer layer = (FeatureLayer) selectedFeature.getFeatureTable().getLayer();
                    if (layer != null) {
                        String _block2 = String.valueOf(selectedFeature.getAttributes().get("Block"));
                        layer.selectFeature(selectedFeature);
                        if(!(Prefs.getBoolean(Prefs.ACTIVE_SESSION_ANCAK))) {
                            Envelope envelope = selectedFeature.getGeometry().getExtent();
                            mapView.setViewpointGeometryAsync(envelope, 20);
                            mCalloutContent = CalloutDynamicFeature.setInfoFeature(MapReplantingActivity.this, selectedFeature, GlobalHelper.getNameObject(selectedFeature.getFeatureTable().getDisplayName()));
                            mCallout.setLocation(envelope.getCenter());
                            mCallout.setContent(mCalloutContent);
                            mCallout.show();

//                        showBottomSheetDialog(selectedFeature, "", "", "ALL");
//                        mCurrentEditState = EditState.Editing;
                        Log.e("element", selectedFeature.getGeometry().getGeometryType().toString());
                        } else if(mTypeSurvey != TypeSurvey.JoinTPH){
                            String _block = Prefs.getString(Prefs.BLOCK_ANCAK);
//                            Log.e("element", _block);
                            if (_block.equalsIgnoreCase(_block2)) {
                                if(selectedFeature.getFeatureTable().getTableName().equals("LUS3")){
                                Envelope envelope = selectedFeature.getGeometry().getExtent();
                                mapView.setViewpointGeometryAsync(envelope, 20);
                                mCalloutContent = CallOutAssignAncak.setInfoFeature(MapReplantingActivity.this, selectedFeature, "LUS3");
                                mCallout.setLocation(envelope.getCenter());
                                mCallout.setContent(mCalloutContent);
                                if(sniper.getVisibility()!=View.VISIBLE) {
                                    mCallout.show();
                                }
                                }else {
                                    Log.e("selectedFeature", (String) selectedFeature.getAttributes().get("NoAncak"));
                                    Envelope envelope = selectedFeature.getGeometry().getExtent();
                                    mapView.setViewpointGeometryAsync(envelope, 20);
                                    mCalloutContent = CallOutAssignAncak.setInfoFeature(MapReplantingActivity.this, selectedFeature, "ANC3");
                                    mCallout.setLocation(envelope.getCenter());
                                    mCallout.setContent(mCalloutContent);
                                    if(sniper.getVisibility()!=View.VISIBLE){
                                        mCallout.show();
                                    }

                                }

                            }else {
                                showAlert("Warning", "anda hanya bisa piih area block " + _block, SweetAlertDialog.WARNING_TYPE);
//                                layer.clearSelection();
                                refreshAllLayers();
                            }
                        }
                    }
                } else {
                    Log.e("SelectFeature", "No features found at the given screen point.");
                }
            } catch (ExecutionException | InterruptedException e) {
                Log.e("Error", e.getMessage(), e);
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setClickListenerTPH() {
        mapView.setOnTouchListener(new DefaultMapViewOnTouchListener(this, mapView) {
            @Override
            public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
                mCallout = mMapView.getCallout();

//                hideCallOut();
                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));

                int tolerance = 10;
                final ListenableFuture<IdentifyGraphicsOverlayResult> identifyGraphic = mMapView
                        .identifyGraphicsOverlayAsync(layerTPH, screenPoint, 10, false);
                identifyGraphic.addDoneListener(() -> {
                    try {
                        IdentifyGraphicsOverlayResult grOverlayResult = identifyGraphic.get();
                        grOverlayResult.getGraphicsOverlay().getExtent();
                        List<Graphic> graphic = grOverlayResult.getGraphics();
//                        showToast(String.valueOf(graphic.size()));
                        if (!graphic.isEmpty()) {
//                            showToast("clicked tph");
//                            Log.e("geometry", graphic.get(0).getGeometry().toJson());
                            for (MapPointTPH mapPointTPH :pointTPHList ) {
                                if (mapPointTPH.getGraphic().equals(graphic.get(0))) {
                                    Envelope envelope = mapPointTPH.getGraphic().getGeometry().getExtent();
                                    TPH clickedTPH = mapPointTPH.getTph();
                                    Point tphPointWGS = (Point) GeometryEngine.project(mapPointTPH.getGraphic().getGeometry(), wgs84);
                                    Polygon selectedPolygon = (Polygon) selectedFeature.getGeometry();
                                    Log.e("Sistance", String.valueOf(distanceTPH(tphPointWGS,selectedPolygon)));
                                    if(distanceTPH(tphPointWGS,selectedPolygon)>10){
                                        showAlert("Jarak TPH terlalu Jauh","Jarak TPH dan ancak yang akan di ikatkan lebih dari 10M",SweetAlertDialog.WARNING_TYPE);
                                        return;
                                    }else {
                                        mCalloutContent = CalloutTPH.setInfoFeature(MapReplantingActivity.this, clickedTPH);
                                        mCallout.setLocation(envelope.getCenter());
                                        mCallout.setContent(mCalloutContent);
                                        mCallout.show();
                                    }
                                    // Dapatkan data TPH yang terkait
//                                    showToast(clickedTPH.getNoTph());
                                    // Lakukan sesuatu dengan data TPH yang diambil
//                                showCalloutForTPH(clickedTPH, clickedGraphic.getGeometry());
                                    break;
                                }
                            }
                        }else {
                            selectFeature2(screenPoint);
                        }


//                        Graphic flagAdd= getAddGraph(screenPoint);
//                        if (!graphic.isEmpty()) {
//                            Envelope envelope;
//                            envelope = graphic.get(0).getGeometry().getExtent();
//                            Envelope convert = (Envelope) GeometryEngine.project(envelope, SpatialReferences.getWgs84());
//
////                            DataCensusFromAdd data = mGCensusViewModel.getInfoForCallOut(convert.getYMin(), convert.getXMin());
////                            if (mGCensusViewModel.getCountGlobalId(data.getGlobalId()) != 0) {
////                                DataCallOutDetail callOut = mGCensusViewModel.getCalloutDetail(data.getGlobalId());
////                                String imageUrl;
////                                if (mImageViewModel.getCountByGanoId(data.getGlobalId()) != 0) {
////                                    imageUrl = mImageViewModel.getUrlByGanoId(data.getGlobalId());
////                                } else {
////                                    imageUrl = "noImage";
////                                }
////                                getAddGraph(screenPoint,callOut,imageUrl,graphic.get(0));
////                            }
//                        }
                    } catch (InterruptedException | ExecutionException ie) {
                        ie.printStackTrace();
                    }
                });

                return true;
            }
        });
    }
    private void selectMultiple(android.graphics.Point screenPoint) {
        final double mapTolerance = 10;
        ListenableFuture<List<IdentifyLayerResult>> identifyLayerResultsFuture = mapView.identifyLayersAsync(screenPoint, mapTolerance, false, -1);
        identifyLayerResultsFuture.addDoneListener(() -> {
            try {
                List<IdentifyLayerResult> identifyLayerResults = identifyLayerResultsFuture.get();
                List<Feature> featureList = new ArrayList<>();
                for (IdentifyLayerResult identifyLayerResult : identifyLayerResults) {
                    for (GeoElement geoElement : identifyLayerResult.getElements()) {
                        if (geoElement instanceof Feature) {
                            Feature feature = (Feature) geoElement;
                            if (feature.getFeatureTable().getTableName().equals("ANC3")){
                                FeatureLayer layer = (FeatureLayer) feature.getFeatureTable().getLayer();
                                layer.selectFeature(feature);
                                mSelectedFeatures.add(feature);
                                if (mSelectedFeatures.size()==1) {
                                    dialogAction("Silahkan pilih ancak kedua untuk di gabung", "Pilih Ancak");
                                }

//                                showToast("Silahkan pilih ancak kedua");
                            }
//                            featureList.add(feature);
                        }
                    }
                }

                if (!mSelectedFeatures.isEmpty() && mSelectedFeatures.size()>=2) {
                    // Pilih fitur terakhir yang ditemukan
//                    selectedFeature = featureList.get(featureList.size() - 1);
//                    FeatureLayer layer = (FeatureLayer) selectedFeature.getFeatureTable().getLayer();
                    dialogMerge();


                } else {
                    Log.e("SelectFeature", "No features found at the given screen point.");
                }
            } catch (ExecutionException | InterruptedException e) {
                Log.e("Error", e.getMessage(), e);
            }
        });
    }

    public void clearMultiple(){
        mSelectedFeatures.clear();
    }

    public void readScale(){
        if(mapView.getMapScale()<700){
            layerTPH.setVisible(true);
        }
        else {
            layerTPH.setVisible(false);
        }
    }
    private void getAncakList(String block){
        int[] arr = {0};

        QueryParameters query = new QueryParameters();
        query.setWhereClause("upper(Block) LIKE '%" + "2" + "%'");
        final ListenableFuture<FeatureQueryResult> future = gdb.queryFeaturesAsync(query);
        future.addDoneListener(() -> {
            try {
                // call get on the future to get the result
                FeatureQueryResult result = future.get();
                for (Feature ignored : result) {
                    anckList.add(String.valueOf(ignored.getAttributes().get("NoAncak")));
                    Log.e("No Ancak",String.valueOf(ignored.getAttributes().get("NoAncak")));
                    arr[0]++;
                }
                Iterator<Feature> resultIterator = result.iterator();
                if (resultIterator.hasNext()) {
                    // get the extent of the first feature in the result to zoom to
                    Feature feature = resultIterator.next();
                    Envelope envelope = feature.getGeometry().getExtent();

                }
                Log.e("ErrCounting", String.valueOf(arr[0]));
//                showUploadSyncDialog(String.valueOf(arr[0]));
//                tAncakInBlock = arr[0];

//                Log.e("CountingFeature", String.valueOf(arr[0]));
            } catch (Exception e) {
                Log.e("ErrCounting", e.getMessage());
            }
        });
//        return anckList;
    }


    private void queryAndDrawBorder(FeatureTable featureTable, String _block) {
        int[] arr = {0};
        layerBorder.getGraphics().clear();
        QueryParameters query = new QueryParameters();

        query.setWhereClause("upper(Block) = '"+_block.toUpperCase()+"' and ESTNR = '"+estateCode+"'");
        final ListenableFuture<FeatureQueryResult> future = featureLayer2.getFeatureTable().queryFeaturesAsync(query);
        future.addDoneListener(() -> {
            try {
                // call get on the future to get the result
                FeatureQueryResult result = future.get();
                List<Geometry> geometries = new ArrayList<>();

                Log.e("Geometries "+_block, String.valueOf(geometries.size()));

                for (Feature feature : result) {
                    Geometry geometry = feature.getGeometry();
                    geometries.add(geometry);
                    Log.e("counting", "run");
                }
                Geometry unionGeometry = GeometryEngine.union(geometries);
                SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.GREEN, 3);
                Graphic borderGraphic = new Graphic(unionGeometry, lineSymbol);
                layerBorder.getGraphics().add(borderGraphic);

//                GraphicsOverlay graphicsOverlay = new GraphicsOverlay();
//                graphicsOverlay.getGraphics().add(borderGraphic);
//                mapView.getGraphicsOverlays().add(graphicsOverlay);
//                tAncakInBlock = arr[0];
//                Log.e("CountingFeature", String.valueOf(arr[0]));
            } catch (Exception e) {
                Log.e("ErrCounting", e.getMessage());
            }
        });

    }

    public void validateUpload() {
        List<String> _block = mAssetViewModel.getSelectedBlocks();
        QueryParameters query = new QueryParameters();
        String blockInClause = _block.stream()
                .map(block -> "'" + block.toUpperCase() + "'")
                .collect(Collectors.joining(","));
        query.setWhereClause("upper(Block) IN (" + blockInClause + ") and EstCode = '" + estateCode + "'");
//        query.setWhereClause("upper(Block) = '"+_block+"' and ESTNR = '"+estateCode+"'");
        final ListenableFuture<FeatureQueryResult> future = featureLayer.getFeatureTable().queryFeaturesAsync(query);
        future.addDoneListener(() -> {
            try {
                // call get on the future to get the result
                FeatureQueryResult result = future.get();
                for (Feature feature : result) {
                    String block = (String) feature.getAttributes().get("Block");
                    feature.getAttributes().get("Block");
                    String _ancak = (String) feature.getAttributes().get("NoAncak");
                    Object flagObject = feature.getAttributes().get("Flag");
//                    feature.getAttributes().put("tphCode", "");
//                    feature.getFeatureTable().updateFeatureAsync(feature);
                    if ((feature.getAttributes().get("tphCode") == null ||
                            Objects.equals(feature.getAttributes().get("tphCode"), "")) &&
                            (flagObject == null ||
                                    (!Objects.equals(flagObject.toString(), "0") && !Objects.equals(flagObject.toString(), "2")))) {
                        showAlert("Gagal Upload!", "Ancak " + _ancak + " Block " + block + " Belum terikat TPH", SweetAlertDialog.ERROR_TYPE);
                        Log.e("counting", feature.getAttributes().get("Flag") + feature.getAttributes().get("GlobalID").toString());
                        return;
                    }
                }
//                showAlert("up","done",1);
                syncBidiectional();

            } catch (Exception e) {
                Log.e("ErrCounting", e.getMessage());
            }
        });

    }

    public void createAncak(Feature _feature){
        UUID uuid = UUID.randomUUID();
        double area = GeometryEngine.areaGeodetic(_feature.getGeometry(), new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
        Map<String, Object> defAttribute = new HashMap<>();
        defAttribute.put("GlobalID", uuid);
        defAttribute.put("CompanyCode", _feature.getAttributes().get("CompanyCode"));
        defAttribute.put("Block", _feature.getAttributes().get("Block"));
        defAttribute.put("EstCode", _feature.getAttributes().get("ESTNR"));
        defAttribute.put("Division", _feature.getAttributes().get("Division"));
        defAttribute.put("GroupCompany", _feature.getAttributes().get("CompanyCode"));
        defAttribute.put("created_user", userID);
//        defAttribute.put("last_edited_user", userID);
        defAttribute.put("last_edited_date", DateTime.timeStampCalendar());
        defAttribute.put("LuasHa", limitDouble(area));
        defAttribute.put("NoAncak", "0");
        defAttribute.put("status_qc", Short.valueOf("0"));
//        defAttribute.put("remark_qc", "");
        defAttribute.put("created_date", DateTime.timeStampCalendar());
        defAttribute.put("tphCode", "");
        Feature newFeature = mFeatureTable.createFeature(defAttribute, _feature.getGeometry());
        showAssignAncak(newFeature);

    }

    public void editAncak(Feature _feature){
        showEditAncak(_feature);
    }
    public void readBLK(){
        File folder = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_ancak.geodatabase");
        String file = folder.toString();
        Log.e("ReadGDB",file);
        if(folder.exists()){
            Log.e("File exist", "true");
            mGeodatabase = new Geodatabase(file);
            mGeodatabase.loadAsync();
            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    int counter = 0;
                    // get only the first table which, contains points
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()){
                        table.loadAsync();
                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                        featureLayer = geodatabaseFeatureLayer;
                        Log.e("Layer GDB name", table.getTableName());
                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                        if (table.getTableName().equals("ANC3")){
                            mFeatureTable = table;

                            Log.e("Layer Feature Name",  mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1).getName());
                            LabelDefinition blueLabelDefinition = makeLabelDefinition("Test", Color.BLUE, "0");
                            LabelDefinition blueLabelDefinition2 = makeLabelDefinition("Test", Color.BLUE, "1");
                            LabelDefinition blueLabelDefinition3 = makeLabelDefinition("Test", Color.BLUE, "-1");
//                            LabelDefinition redLabelDefinition = makeLabelDefinition("Test2", Color.RED, "< 4");
                            FeatureLayer ancakLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1);
                            ancakLayer.getLabelDefinitions().addAll(Arrays.asList(blueLabelDefinition,blueLabelDefinition2,blueLabelDefinition3));
                            // enable labels
                            ancakLayer.setLabelsEnabled(true);
                            gdb = table;
                        }
                        else {

                        }
                    }
                    mGeodatabaseButton.setVisibility(View.GONE);
                    Log.e("size Layer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    mapView.getMap().getLoadSettings().setPreferredPolylineFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
                    Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()) );
                    Log.e("Feature layer", mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1).getName());
                    readGeodatabase();
                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                }
            });
            // set edit state to ready
            mCurrentEditState = EditState.Ready;
        }
        else {
            Log.e("filenorExist","true");
        }
    }

    public void readGeodatabase(){
        File folder = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_lus3.geodatabase");
        String file = folder.toString();
        Log.e("ReadGDB",file);
        if(folder.exists()){
            Log.e("File exist", "true");
            mGeodatabase2 = new Geodatabase(file);
            mGeodatabase2.loadAsync();
            mGeodatabase2.addDoneLoadingListener(() -> {
                if (mGeodatabase2.getLoadStatus() == LoadStatus.LOADED) {
                    int counter = 0;
                    // get only the first table which, contains points
                    for (GeodatabaseFeatureTable table : mGeodatabase2.getGeodatabaseFeatureTables()){
                        table.loadAsync();
                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                        featureLayer2 = geodatabaseFeatureLayer;
                        Log.e("Layer GDB name", table.getTableName());
                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                        if (table.getTableName().equals("LUS3")){
//                            Log.e("Layer Feature Name",  mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1).getName());
//                            LabelDefinition blueLabelDefinition = makeLabelDefinition("Test", Color.BLUE, ">= 4");
////                            LabelDefinition redLabelDefinition = makeLabelDefinition("Test2", Color.RED, "< 4");
//                            FeatureLayer ancakLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1);
//                            ancakLayer.getLabelDefinitions().addAll(Arrays.asList(blueLabelDefinition));
//                            // enable labels
//                            ancakLayer.setLabelsEnabled(true);
                            float result = (float) 1/10;
                            geodatabaseFeatureLayer.setOpacity(result);
//                            featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1);
                        }
                        else {
//                            float result = (float) 1/10;
//                            geodatabaseFeatureLayer.setOpacity(result);
                        }
                    }
                    mGeodatabaseButton.setVisibility(View.GONE);
                    Log.e("size Layer", String.valueOf(mapView.getMap().getOperationalLayers().size()));
                    mapView.getMap().getLoadSettings().setPreferredPolylineFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
                    layerDraw = new GraphicsOverlay();

                    mapView.getGraphicsOverlays().add(layerTPH);
                    mapView.getGraphicsOverlays().add(layerDraw);
                    mapView.getGraphicsOverlays().add(layerImagine);
                    mapView.getGraphicsOverlays().add(layerBorder);
//                  readScale();
//                mapView.getMap().getLoadSettings().setPreferredPolygonFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
//                mapView.getMap().getLoadSettings().setPreferredPointFeatureRenderingMode(FeatureLayer.RenderingMode.DYNAMIC);
                    Log.e("MapviewLayer", String.valueOf(mapView.getMap().getOperationalLayers().size()) );
                    Log.e("Feature layer", mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size()-1).getName());
                    touchListener();
                    checkCensusSession();
                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase2.getLoadError().getMessage());
                }
            });
            // set edit state to ready
            mCurrentEditState = EditState.Ready;
        }
        else {
            Log.e("filenorExist","true");
        }
    }


    public void generateGeodatabase() {
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
//        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE,Prefs.PASS_SDE));
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_ancak.geodatabase");
        File fileBackup = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "backup");
        if (!fileBackup.exists()) {
            fileBackup.mkdirs();
        }
        final String path = fileLoc.getAbsolutePath();
        mGeodatabaseSyncTask.loadAsync();
        mGeodatabaseSyncTask.addDoneLoadingListener(() -> {
//            Log.e("versionSDE", mGeodatabaseSyncTask.getFeatureServiceInfo().getVersion());
//            final Envelope extent = mapView.getVisibleArea().getExtent();
//            double minX = 94.97; // Minimum X-coordinate (longitude) for the extent
//            double minY = -11.00; // Minimum Y-coordinate (latitude) for the extent
//            double maxX = 141.00; // Maximum X-coordinate (longitude) for the extent
//            double maxY = 6.00;
//            Envelope envelope = new Envelope(minX, minY, maxX, maxY, wgs84);
            final Envelope extentFull = mGeodatabaseSyncTask.getFeatureServiceInfo().getFullExtent();
            final ListenableFuture<GenerateGeodatabaseParameters> defaultParameters = mGeodatabaseSyncTask
                    .createDefaultGenerateGeodatabaseParametersAsync(extentFull);
            defaultParameters.addDoneListener(() -> {
                try {
                    // set parameters and don't include attachments
                    GenerateGeodatabaseParameters parameters = defaultParameters.get();
                    parameters.setReturnAttachments(false);
                    // define the local path where the geodatabase will be stored
//                    final String localGeodatabasePath = getCacheDir() + "/file_inventory.geodatabase";
                    Log.e("patchLoc", path);
                    // create and start the job
                    if (fileLoc.exists()) {
                        GlobalHelper.moveFile(fileLoc, fileBackup, DateTime.getStringDate());
                    }
                    final GenerateGeodatabaseJob generateGeodatabaseJob = mGeodatabaseSyncTask
                            .generateGeodatabase(parameters, path);
                    RequestConfiguration requestConfiguration = new RequestConfiguration();
                    requestConfiguration.setConnectionTimeout(300000);
                    requestConfiguration.setSocketTimeout(300000);
                    requestConfiguration.setMaxNumberOfAttempts(5);
                    generateGeodatabaseJob.setRequestConfiguration(requestConfiguration);
//                    String timeout = String.valueOf(generateGeodatabaseJob.getRequestConfiguration().getConnectionTimeout());
//                    Log.e("timeout",timeout);
                    generateGeodatabaseJob.start();
                    createProgressDialog(generateGeodatabaseJob);
                    // get geodatabase when done
                    generateGeodatabaseJob.addJobDoneListener(() -> {
                        if (generateGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                            mGeodatabase = generateGeodatabaseJob.getResult();
                            mGeodatabase.loadAsync();
                            mGeodatabase.addDoneLoadingListener(() -> {
//                                Toast.makeText(this, mGeodatabase.get)
                                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                                    featureLayers.clear();
                                    // get only the first table which, contains points
                                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                                        table.loadAsync();
//                                    GeodatabaseFeatureTable pointsGeodatabaseFeatureTable = mGeodatabase
//                                            .getGeodatabaseFeatureTables().get(0);
//                                    pointsGeodatabaseFeatureTable.loadAsync();
                                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
                                        featureLayers.add(geodatabaseFeatureLayer);
                                        Log.e("Layer GDB name", table.getTableName());
                                        // add geodatabase layer to the map as a feature layer and make it selectable
                                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
                                    }
//                                    mapView.getMap().getOperationalLayers().addAll(featureLayers);
                                    mGeodatabaseButton.setVisibility(View.GONE);
                                    int layerTouch = mapView.getMap().getOperationalLayers().size() - 1;
                                    Log.e("Feature layer", String.valueOf(layerTouch));
                                    Log.e("mapView size", mapView.getMap().getOperationalLayers().get(mapView.getMap().getOperationalLayers().size() - 1).getName());
                                    featureLayer = (FeatureLayer) mapView.getMap().getOperationalLayers().get(layerTouch);
//                                    Log.i("689", "Local geodatabase stored at: " + localGeodatabasePath);
//                                    applyDefinition();
                                    touchListener();
//                                    touchListenerEdit();
                                } else {
                                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                                }
                            });
                            // set edit state to ready
                            mCurrentEditState = MapReplantingActivity.EditState.Ready;
                        } else if (generateGeodatabaseJob.getError() != null) {
                            Log.e("699", "Error generating getCause: " + generateGeodatabaseJob.getError().getCause());
                            Log.e("699", "Error generating getMessage: " + generateGeodatabaseJob.getError().getMessage());
                            Log.e("699", "Error generating getCode: " + generateGeodatabaseJob.getError().getErrorCode());
                            Toast.makeText(this,
                                    "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("703", "Unknown Error generating geodatabase");
                            Toast.makeText(this, "Unknown Error generating geodatabase", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("708", "Error generating geodatabase parameters : " + e.getMessage());
                    Toast.makeText(this, "Error generating geodatabase parameters: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }




    private void createProgressDialog(Job job) {

        ProgressDialog syncProgressDialog = new ProgressDialog(this);
        syncProgressDialog.setTitle("Upload data Ancak");
        syncProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        syncProgressDialog.setCanceledOnTouchOutside(false);
        syncProgressDialog.show();
        job.addProgressChangedListener(() -> syncProgressDialog.setProgress(job.getProgress()));

        job.addJobDoneListener(syncProgressDialog::dismiss);
    }

    private void syncGeodatabase() {
        // create parameters for the sync task
        SyncGeodatabaseParameters syncGeodatabaseParameters = new SyncGeodatabaseParameters();

        syncGeodatabaseParameters.setSyncDirection(SyncGeodatabaseParameters.SyncDirection.UPLOAD);
        syncGeodatabaseParameters.setRollbackOnFailure(true);
        // get the layer ID for each feature table in the geodatabase, then add to the sync job
        for (GeodatabaseFeatureTable geodatabaseFeatureTable : mGeodatabase.getGeodatabaseFeatureTables()) {

            long serviceLayerId = geodatabaseFeatureTable.getServiceLayerId();
            SyncLayerOption syncLayerOption = new SyncLayerOption(serviceLayerId);

            syncGeodatabaseParameters.getLayerOptions().add(syncLayerOption);
        }


        final SyncGeodatabaseJob syncGeodatabaseJob = mGeodatabaseSyncTask
                .syncGeodatabase(syncGeodatabaseParameters, mGeodatabase);
        syncGeodatabaseJob.start();
        createProgressDialog(syncGeodatabaseJob);


        syncGeodatabaseJob.addJobDoneListener(() -> {
            if (syncGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                Toast.makeText(this, "Sync complete", Toast.LENGTH_SHORT).show();
                mGeodatabaseButton.setVisibility(View.INVISIBLE);
            } else {
                Log.e(TAG, "Database did not sync correctly!");
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            }
        });
    }


    protected void showAddPointDialig() {
        editFeatures = new ArrayList<>();
        SheetAddPoint bottomSheetDialog= new SheetAddPoint(this);
        bottomSheetDialog.showDialog();
    }


    public void updateFeature(List<EditFeature> editFeature){
        Log.e("mSelectedArcGISFeature.getAttributes()","true");
        for (EditFeature item:editFeature){
            mSelectedArcGISFeature.getAttributes().put(item.getKey(), item.getValue());
            Log.e("putAttributes()",item.getKey()+ item.getValue());
        }
    }

    public void reverseVertex(Feature feature){
        // reverse the vertices of the polyline
        Polyline  polyline = (Polyline) feature.getGeometry();
        Log.e("Polygon",String.valueOf(polyline.getParts().size()));
        int lastVertex = polyline.getParts().size()-1;
//        Geometry geometryEngine = GeometryEngine.project(selectedFeature.getGeometry(), polyline.getSpatialReference());
//        List<Point> points = (List<Point>) polyline.getParts().getPartsAsPoints();
        PolylineBuilder polylineBuilder = new PolylineBuilder(polyline);
        PartCollection parts = polylineBuilder.getParts();

        parts.stream().sorted(Collections.reverseOrder());
        feature.setGeometry(polylineBuilder.toGeometry());
        feature.getFeatureTable().updateFeatureAsync(feature);

    }

    private void countingFeatureInBlock(String _block) {
        int[] arr = {0};
        QueryParameters query = new QueryParameters();
        query.setWhereClause("upper(Block) LIKE '%" + _block + "%'");
        final ListenableFuture<FeatureQueryResult> future = gdb.queryFeaturesAsync(query);
        future.addDoneListener(() -> {
            try {
                // call get on the future to get the result
                FeatureQueryResult result = future.get();
                for (Feature ignored : result) {
                    arr[0]++;
                }
                Iterator<Feature> resultIterator = result.iterator();
                if (resultIterator.hasNext()) {
                    // get the extent of the first feature in the result to zoom to
                    Feature feature = resultIterator.next();
                    Envelope envelope = feature.getGeometry().getExtent();

                }
                Log.e("ErrCounting", String.valueOf(arr[0]));
                showUploadSyncDialog(String.valueOf(arr[0]));
//                tAncakInBlock = arr[0];

//                Log.e("CountingFeature", String.valueOf(arr[0]));
            } catch (Exception e) {
                Log.e("ErrCounting", e.getMessage());
            }
        });

    }

    public void showAlertNew(){
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setConfirmButton("OKE", sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                })
//                .setCancelButtonBackgroundColor(Color.RED)`
                .showCancelButton(false)
                .setTitleText("Masih ada Ancak tanpa TPH!")
                .setContentText("Tidak dapat end session saat progress block belum selesai");
        sweetAlertDialog.show();
    }


    public void showDialogTPH(){
        int greenColor = Color.parseColor("#008000");
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setConfirmButton("Buat TPH", sweetAlertDialog -> {
//                    warningLimitTPH("B11");
                    showBottomSheet4(1);
                    sweetAlertDialog.dismiss();
                })
                .setCancelButtonBackgroundColor(greenColor)
                .showCancelButton(true)
                .setCancelButton("Ikatkan TPH",sweetAlertDialog1 -> {
                    sweetAlertDialog1.dismiss();
                    showIkatkanTPH();
                })
                .setTitleText("Management TPH")
                .setContentText("Silahkan Pilih");
        sweetAlertDialog.show();
    }

    public void warningLimitTPH(String block){
        int greenColor = Color.parseColor("#008000");
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setConfirmButton("OK", sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                })
                .setTitleText("Block "+""+block)
                .setContentText("Single TPH, Tidak Bisa add TPH karena ancak sudah memiliki TPH!");
        sweetAlertDialog.show();
    }

    public void showIkatkanTPH(){
        int greenColor = Color.parseColor("#008000");
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setConfirmButton("Pilih Ancak", sweetAlertDialog2 -> {
                    mTypeSurvey = TypeSurvey.JoinTPH;
//                    showBottomSheet4(1);
                    sweetAlertDialog2.dismiss();
                })
                .setTitleText("Block")
                .setContentText("Multiple TPH, Silahkan pilih Ancak yang akan di ikatkan dengan TPH");
        sweetAlertDialog.show();
    }

    public void joinTPH(String code, String _block){
        Log.e("codeTPH",code);
        String blockAncak = (String) selectedFeature.getAttributes().get("Block");
        if(_block.equalsIgnoreCase(blockAncak)){
            String addTPH = selectedFeature.getAttributes().get("tphCode")+code+";";
            mAssetViewModel.updateFlagJoin(code,"");
            selectedFeature.getAttributes().put("tphCode", addTPH);
            selectedFeature.getFeatureTable().updateFeatureAsync(selectedFeature);
//            refreshAllLayers();
            refreshMap();
        }else {
            showAlert("Gagal ikatkan TPH", "Block TPH dan Block Ancak Harus Sama", SweetAlertDialog.ERROR_TYPE);
        }

    }

    private double distanceTPH(Point mapPoint, Polygon selectedPolygon) {
        // Get the nearest point on the polygon to the mapPoint (TPH)
        Point nearestPointOnPolygon = (Point) GeometryEngine.nearestCoordinate(selectedPolygon, mapPoint).getCoordinate();

        // Calculate the distance between the mapPoint (TPH) and the nearest point on the polygon
            double _distance = distance(nearestPointOnPolygon.getX(),mapPoint.getX(),nearestPointOnPolygon.getY(),mapPoint.getY());

        // Return the calculated distance (in meters if using projected coordinates, like Web Mercator)
        return _distance;
    }
    public void refreshMap(){
        mapView.invalidate();
    }

    public void startCreateAncak(FeatureLayer _feature){
        mCurrentEditState = EditState.Editing;
        mTypeSurvey = TypeSurvey.AddAncak;
        fab_map.callOnClick();
        if(sniper.getVisibility()==View.VISIBLE){
            isModeFree = false;
            sniper.setVisibility(View.GONE);
            toolbarAdd.setVisibility(View.GONE);
            l_action.setVisibility(View.GONE);
        }
        else {
            isModeFree = true;
            fabAdd.setVisibility(View.GONE);
            sniper.setVisibility(View.VISIBLE);
            toolbarAdd.setVisibility(View.VISIBLE);
            l_action.setVisibility(View.VISIBLE);
//                toolbar.setVisibility(View.VISIBLE);
//                l_action.setVisibility(View.VISIBLE);
        }
        closeCallout();

    }

    public void cutSplit2(Feature feature){
        isModeAdd = false;
        com.esri.arcgisruntime.geometry.Polygon wgs84Polygon = (com.esri.arcgisruntime.geometry.Polygon) GeometryEngine.project(feature.getGeometry(), wgs84);
        // Find the nearest coordinate on the polygon from the first point of the polyline
        com.esri.arcgisruntime.geometry.Point firstPoint = points.get(0);
        ProximityResult nearestPointOnPolygon = GeometryEngine.nearestCoordinate(wgs84Polygon, firstPoint);
        // Replace the first point of the polyline with the nearest point on the polygon
        points.set(0, (com.esri.arcgisruntime.geometry.Point) nearestPointOnPolygon.getCoordinate());
        final Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT, 0xFFFF0000, 3));
        Log.e("polylineGraph", polylineGraphic.getGeometry().toJson());
        List<Geometry> parts = GeometryEngine.cut(wgs84Polygon, (Polyline) polylineGraphic.getGeometry());
        Log.e("partsSize", String.valueOf(parts.size()));

        if (parts.size() >1){
            double area = GeometryEngine.areaGeodetic(parts.get(0), new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
            double area2 = GeometryEngine.areaGeodetic(parts.get(1), new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
            if(area <=0.01){
                dialogAction("Hasil potongan ancak anda terlalu kecil = "+limitDouble(area),"Gagal Buat Ancak!");
                return;
            }
            if(area2 <=0.01){
                dialogAction("Sisa potongan ancak anda terlalu kecil = "+limitDouble(area),"Gagal Buat Ancak!");
                return;
            }
            Graphic canadaSide = new Graphic(parts.get(0), new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0xFF00FF00, new SimpleLineSymbol(SimpleLineSymbol.Style.NULL, 0xFFFFFFFF, 0)));
            Graphic usSide = new Graphic(parts.get(1), new SimpleFillSymbol(SimpleFillSymbol.Style.FORWARD_DIAGONAL, 0xFFFFFF00, new SimpleLineSymbol(SimpleLineSymbol.Style.NULL, 0xFFFFFFFF, 0)));
            layerDraw.getGraphics().addAll(Arrays.asList(canadaSide, usSide));
            Log.e("nearestCoor", String.valueOf(GeometryEngine.nearestCoordinate(wgs84Polygon, points.get(points.size() - 1))));
            Log.e("nearestCoor", String.valueOf(wgs84Polygon.toJson()));
            addNewObject(feature,parts);
            points.clear();
            refreshAllLayers();
            if(toolbarAdd.getVisibility()==View.VISIBLE){
                sniper.setVisibility(View.GONE);
                toolbarAdd.setVisibility(View.GONE);
                l_action.setVisibility(View.GONE);
            }
            else {
                sniper.setVisibility(View.VISIBLE);
                toolbarAdd.setVisibility(View.VISIBLE);
                l_action.setVisibility(View.VISIBLE);
//                toolbar.setVisibility(View.VISIBLE);
//                l_action.setVisibility(View.VISIBLE);
            }
            mCurrentEditState = EditState.Ready;

//            refreshMap();
        } else {
            refreshAllLayers();
            showAlert("Gagal Buat ancak!","Titik-titik Survey tidak memotong Ancak", SweetAlertDialog.ERROR_TYPE);
        }

//        PolygonBuilder polygonBuilder = new PolygonBuilder(wgs84);
//        PartCollection parts2 = polygonBuilder.getParts();

        // parts2.add(parts);
//        feature.setGeometry(parts.get(0));
//        feature.getFeatureTable().updateFeatureAsync(feature);
//        feature.getFeatureTable().addFeatureAsync(feature);
//        featureLayer.clearSelection();

//        fabAdd.callOnClick();

    }

    public void refreshAllLayers() {
        selectedFeature = null;
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                FeatureLayer featureLayer = (FeatureLayer) layer;
                // Clear any selected features
                featureLayer.clearSelection();
            }
        }
        // Optionally refresh the map view to ensure all layers are re-rendered
        mapView.invalidate();
    }

    public void syncBidiectional() {

        // create parameters for the sync task
        SyncGeodatabaseParameters syncGeodatabaseParameters = new SyncGeodatabaseParameters();
        syncGeodatabaseParameters.setSyncDirection(SyncGeodatabaseParameters.SyncDirection.BIDIRECTIONAL);
        syncGeodatabaseParameters.setRollbackOnFailure(false);
        // get the layer ID for each feature table in the geodatabase, then add to the sync job
        for (GeodatabaseFeatureTable geodatabaseFeatureTable : mGeodatabase.getGeodatabaseFeatureTables()) {
            long serviceLayerId = geodatabaseFeatureTable.getServiceLayerId();
            SyncLayerOption syncLayerOption = new SyncLayerOption(serviceLayerId);
            syncGeodatabaseParameters.getLayerOptions().add(syncLayerOption);
        }
        final SyncGeodatabaseJob syncGeodatabaseJob = mGeodatabaseSyncTask
                .syncGeodatabase(syncGeodatabaseParameters, mGeodatabase);
        syncGeodatabaseJob.start();
        createProgressDialog(syncGeodatabaseJob);
        syncGeodatabaseJob.addJobDoneListener(() -> {
            if (syncGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
//                Toast.makeText(this, "Sync complete", Toast.LENGTH_LONG).show();
//                uploadTPH("");
//                mGeodatabaseButton.setVisibility(View.INVISIBLE);
                if(mAssetViewModel.getSelectedBlocks().size()>0){
//                    uploadTPH();
                    postCheckout(mAssetViewModel.getSelectedBlocks());
                }

                uploadTPH();
//                dialogAction("Upload Data Berhasil","Berhasil Upload!");
            } else if (syncGeodatabaseJob.getStatus() == Job.Status.CANCELING || syncGeodatabaseJob.getStatus() == Job.Status.PAUSED || syncGeodatabaseJob.getStatus() == Job.Status.NOT_STARTED) {
//                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
                dialogAction("Database did not sync correctly!","Gagal Upload!");

                Log.e(TAG, "Database did not sync correctly!");
            } else {
                Log.e("Replica",mGeodatabase.getSyncId().toString());
                Log.e(TAG, "Database did not sync correctly!");
                Log.e("error message", syncGeodatabaseJob.getError().getMessage());
                Log.e("err cause", "cause : " + syncGeodatabaseJob.getError().getCause());
                Log.e("err admessage", "message: " + syncGeodatabaseJob.getError().getMessage());
//                Log.e(TAG, syncGeodatabaseJob.getError().getCause());
                Log.e("error code", String.valueOf(syncGeodatabaseJob.getError().getErrorCode()));
                Log.e("error domain", syncGeodatabaseJob.getError().getErrorDomain().toString());
                Toast.makeText(this, "Database did not sync correctly!", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void uploadTPH(){
//        List<JSONObject>listFinal = new ArrayList<>();
        List<TPH> list= mAssetViewModel.getAllTPHToUpload();
        if (list.size()>0){
            showLoadingAlert("Upload!","upload data");
            Call<ResponseBody> postTPHTPH = hmsAppsServices.postTPH(list,estateCode);
            postTPHTPH.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        mAssetViewModel.updateDoneUpload();
                        postImage();
                    }else {
                        dialogAction("Upload Data Gagal","Failed Upload!");

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialogAction("Upload Data Gagal","Failed Upload!");
                }
            });
        }
        else {
            dialogAction("Upload Data Berhasil","Berhasil Upload!");

        }

    }

    private void drawImaginary5(Point currentLocation, GraphicsOverlay graphicsOverlay, double meters, int degree) {
        // Mendapatkan skala peta saat ini
        double scale = mapView.getMapScale();

        // Mendapatkan DPI layar
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        double dpi = displayMetrics.xdpi;

        // Mendapatkan faktor konversi dari inci ke meter
        double metersPerInch = 0.0254;

        // Menghitung jumlah meter yang diproyeksikan dalam satu piksel pada skala peta saat ini
        double metersPerPixel = metersPerInch / dpi * scale;

        // Menentukan jarak dalam piksel
        double distanceInPixels = meters / metersPerPixel;

        // Konversi sudut dari derajat ke radian
        double angleRad = Math.toRadians(degree);

        // Hitung perubahan koordinat menggunakan trigonometri
        double dx = distanceInPixels * Math.cos(angleRad);
        double dy = distanceInPixels * Math.sin(angleRad);

        // Mendapatkan koordinat layar titik awal (titik GPS)
        android.graphics.Point screenPoint = mapView.locationToScreen(currentLocation);

        // Tentukan titik akhir dengan menambahkan dx dan dy ke koordinat awal
        android.graphics.Point endPoint = new android.graphics.Point(screenPoint.x + (int) dx, screenPoint.y - (int) dy);

        // Mendapatkan koordinat dunia (koordinat geografis) titik akhir dari koordinat layar
        Point endLocation = mapView.screenToLocation(endPoint);
        imaginaryPoint = endLocation;

        // Pastikan referensi spasial sama
        if (!endLocation.getSpatialReference().equals(currentLocation.getSpatialReference())) {
            endLocation = (Point) GeometryEngine.project(endLocation, currentLocation.getSpatialReference());
        }

        // Membuat garis dari titik awal ke titik akhir
        PointCollection pointCollection = new PointCollection(currentLocation.getSpatialReference());
        pointCollection.add(currentLocation);
        pointCollection.add(endLocation);
        Polyline line = new Polyline(pointCollection);

        // Mengatur simbol dan properti garis
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.RED, 3);
        Graphic newLineGraphic = new Graphic(line, lineSymbol);

        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(newLineGraphic);

        // Update the reference to the current line graphic
        lineGraphicImaginer = newLineGraphic;
    }


    private void drawImaginary4(Point currentLocation, GraphicsOverlay graphicsOverlay, double meters, Integer degree) {
        // Mendapatkan skala peta saat ini
        double scale = mapView.getMapScale();

        // Mendapatkan DPI layar
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        double dpi = displayMetrics.xdpi;

        // Mendapatkan faktor konversi dari inci ke meter
        double metersPerInch = 0.0254;

        // Menghitung jumlah meter yang diproyeksikan dalam satu piksel pada skala peta saat ini
        double metersPerPixel = metersPerInch / dpi * scale;

        // Menentukan jarak dalam piksel
        double distanceInPixels = meters / metersPerPixel;

        // Jika degree adalah null atau 0, tidak menentukan rotasi
        double angleRad = 0;
        if (degree != null && degree != 0) {
            // Konversi sudut dari derajat ke radian dan sesuaikan agar sudut 0 berada di atas (jam 12)
            angleRad = Math.toRadians(degree);
        }

        // Hitung perubahan koordinat menggunakan trigonometri
        double dx = distanceInPixels * Math.sin(angleRad); // X dipengaruhi oleh sin
        double dy = -distanceInPixels * Math.cos(angleRad); // Y dipengaruhi oleh cos, dikali -1 agar sudut 0 adalah ke atas (jam 12)

        // Mendapatkan koordinat layar titik awal (titik GPS)
        android.graphics.Point screenPoint = mapView.locationToScreen(currentLocation);

        // Tentukan titik akhir dengan menambahkan dx dan dy ke koordinat awal
        android.graphics.Point endPoint = new android.graphics.Point(screenPoint.x + (int) dx, screenPoint.y + (int) dy);

        // Mendapatkan koordinat dunia (koordinat geografis) titik akhir dari koordinat layar
        Point endLocation = mapView.screenToLocation(endPoint);
        imaginaryPoint = endLocation;

        // Pastikan referensi spasial sama
        if (!endLocation.getSpatialReference().equals(currentLocation.getSpatialReference())) {
            endLocation = (Point) GeometryEngine.project(endLocation, currentLocation.getSpatialReference());
        }

        // Membuat garis dari titik awal ke titik akhir
        PointCollection pointCollection = new PointCollection(currentLocation.getSpatialReference());
        pointCollection.add(currentLocation);
        pointCollection.add(endLocation);
        Polyline line = new Polyline(pointCollection);

        // Mengatur simbol dan properti garis
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.RED, 3);
        Graphic newLineGraphic = new Graphic(line, lineSymbol);

        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(newLineGraphic);

        // Update the reference to the current line graphic
        lineGraphicImaginer = newLineGraphic;
    }



    private void drawImaginary3(Point currentLocation, GraphicsOverlay graphicsOverlay, double meters) {

        double scale = mapView.getMapScale();
        // Mendapatkan DPI layar
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        double dpi = displayMetrics.xdpi;
        // Mendapatkan faktor konversi dari inci ke meter
        double metersPerInch = 0.0254;
        // Menghitung jumlah meter yang diproyeksikan dalam satu piksel pada skala peta saat ini
        double metersPerPixel = metersPerInch / dpi * scale;
        // Menentukan jarak dalam piksel (asumsi 100 meter ke atas)
        double distanceInPixels = meters / metersPerPixel;

        // Mendapatkan koordinat layar titik awal (titik GPS)
        android.graphics.Point screenPoint = mapView.locationToScreen(currentLocation);
        // Menentukan koordinat layar titik akhir (vertikal 100 meter dari titik awal)
        android.graphics.Point endPoint = new android.graphics.Point(screenPoint.x, screenPoint.y - (int) distanceInPixels);

        // Mendapatkan koordinat dunia (koordinat geografis) titik akhir dari koordinat layar
        Point endLocation = mapView.screenToLocation(endPoint);
        imaginaryPoint = endLocation;

        // Pastikan referensi spasial sama
        if (!endLocation.getSpatialReference().equals(currentLocation.getSpatialReference())) {
            endLocation = (Point) GeometryEngine.project(endLocation, currentLocation.getSpatialReference());
        }

        // Membuat garis dari titik awal ke titik akhir
        PointCollection pointCollection = new PointCollection(currentLocation.getSpatialReference());
        pointCollection.add(currentLocation);
        pointCollection.add(endLocation);
        Polyline line = new Polyline(pointCollection);

        // Mengatur simbol dan properti garis
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.RED, 3);
        Graphic newLineGraphic = new Graphic(line, lineSymbol);

        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(newLineGraphic);

        // Update the reference to the current line graphic
        lineGraphicImaginer = newLineGraphic;
        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(newLineGraphic);

    }

    public void cutSplit(Feature feature){
        isModeAdd=false;
        com.esri.arcgisruntime.geometry.Polygon wgs84Polygon = (com.esri.arcgisruntime.geometry.Polygon) GeometryEngine.project(feature.getGeometry(), wgs84);
        final Graphic polylineGraphic = new Graphic(new Polyline(points) , new SimpleLineSymbol(SimpleLineSymbol.Style.DOT, 0xFFFF0000, 3));
        Log.e("polylineGraph",polylineGraphic.getGeometry().toJson());
        List<Geometry> parts = GeometryEngine
                .cut(wgs84Polygon, (Polyline) polylineGraphic.getGeometry());
        Log.e("partsSize",String.valueOf(parts.size()));
        Graphic canadaSide = new Graphic(parts.get(0), new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, 0xFF00FF00, new SimpleLineSymbol(SimpleLineSymbol.Style.NULL, 0xFFFFFFFF, 0)));
        Graphic usSide = new Graphic(parts.get(1), new SimpleFillSymbol(SimpleFillSymbol.Style.FORWARD_DIAGONAL, 0xFFFFFF00, new SimpleLineSymbol(SimpleLineSymbol.Style.NULL, 0xFFFFFFFF, 0)));
        layerDraw.getGraphics().addAll(Arrays.asList(canadaSide, usSide));
        Log.e("nearestCoor",String.valueOf(GeometryEngine.nearestCoordinate(wgs84Polygon,points.get(points.size()-1))));
        Log.e("nearestCoor",String.valueOf(wgs84Polygon.toJson()));
        PolygonBuilder polygonBuilder =   new PolygonBuilder(SpatialReferences.getWebMercator());
        PartCollection parts2 = polygonBuilder.getParts();
//        parts2.add(parts);
        feature.setGeometry(parts.get(0));
//        feature.getFeatureTable().updateFeatureAsync(feature);
        feature.getFeatureTable().addFeatureAsync(feature);
        featureLayer.clearSelection();
        points.clear();
        fabAdd.callOnClick();
    }
    private void openVersioning(){
        FragmentManager fm = getSupportFragmentManager();
        DialogVersioning basemapDialog = DialogVersioning.newInstance(this);
        basemapDialog.show(fm,"Basemap");
    }


    private void removeMapRotationListener() {
//        mapView.removeMapRotationChangedListener(listener);
//        listener = null;
        // Hapus listener dari MapView
        if (listener != null) {
            mapView.removeMapRotationChangedListener(listener);
            listener = null;
        }
    }

    private void createModePoint() {
        resetButtons();
        mPointButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.POINT,configuration);
    }

    /**
     * When the multipoint button is clicked, reset other buttons, show the multipoint button as selected, and start
     * multipoint drawing mode.
     */
    private void createModeMultipoint() {
        resetButtons();
        mMultiPointButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.MULTIPOINT);
    }

    /**
     * When the polyline button is clicked, reset other buttons, show the polyline button as selected, and start
     * polyline drawing mode.
     */
    private void createModePolyline() {
        resetButtons();
        mPolylineButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.POLYLINE,configuration);

    }

    /**
     * When the polygon button is clicked, reset other buttons, show the polygon button as selected, and start polygon
     * drawing mode.
     */
    private void createModePolygon() {
        resetButtons();
        mPolygonButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.POLYGON);
    }

    /**
     * When the freehand line button is clicked, reset other buttons, show the freehand line button as selected, and
     * start freehand line drawing mode.
     */
    private void createModeFreehandLine() {
        resetButtons();
        mFreehandLineButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.FREEHAND_LINE);
    }

    /**
     * When the freehand polygon button is clicked, reset other buttons, show the freehand polygon button as selected,
     * and enable freehand polygon drawing mode.
     */
    private void createModeFreehandPolygon() {
        resetButtons();
        mFreehandPolygonButton.setSelected(true);
        mSketchEditor.start(SketchCreationMode.FREEHAND_POLYGON);
    }
    private void resetButtons() {
        mPointButton.setSelected(false);
        mMultiPointButton.setSelected(false);
        mPolylineButton.setSelected(false);
        mPolygonButton.setSelected(false);
        mFreehandLineButton.setSelected(false);
        mFreehandPolygonButton.setSelected(false);
    }

    private void undo() {
        if (mSketchEditor.canUndo()) {
            mSketchEditor.undo();
        }
    }

    /**
     * When the redo button is clicked, redo the last undone event on the SketchEditor.
     */
    private void redo() {
        if (mSketchEditor.canRedo()) {
            mSketchEditor.redo();
        }
    }

    /**
     * When the stop button is clicked, check that sketch is valid. If so, get the geometry from the sketch, set its
     * symbol and add it to the graphics overlay.
     */
    private void stop() {
        if (!mSketchEditor.isSketchValid()) {
            reportNotValid();
            mSketchEditor.stop();
            resetButtons();
            return;
        }

        // get the geometry from sketch editor
        Geometry sketchGeometry = mSketchEditor.getGeometry();

        mSketchEditor.stop();
        resetButtons();

        if (sketchGeometry != null) {

            // create a graphic from the sketch editor geometry
            Graphic graphic = new Graphic(sketchGeometry);

            // assign a symbol based on geometry type
            if (graphic.getGeometry().getGeometryType() == GeometryType.POLYGON) {
                graphic.setSymbol(mFillSymbol);
            } else if (graphic.getGeometry().getGeometryType() == GeometryType.POLYLINE) {
                graphic.setSymbol(mLineSymbol);
            } else if (graphic.getGeometry().getGeometryType() == GeometryType.POINT ||
                    graphic.getGeometry().getGeometryType() == GeometryType.MULTIPOINT) {
                graphic.setSymbol(mPointSymbol);
            }
            // add the graphic to the graphics overlay
            mGraphicsOverlay.getGraphics().add(graphic);
        }
    }

    public void dialogMerge() {

        String b1 = (String) mSelectedFeatures.get(0).getAttributes().get("Block");
        String b2 = (String) mSelectedFeatures.get(1).getAttributes().get("Block");
        if (!b1.equalsIgnoreCase(b2)){
            dialogAction("Tidak bisa gabungkan ancak beda block", "Gagal Gabungkan ancak");
            refreshAllLayers();
            return;
        }
        List<String> a = new ArrayList<>();
        FragmentManager fm = getSupportFragmentManager();
        DialogMergeAncak dialogSessionFragment = DialogMergeAncak.newInstance(this, a, "sessionId", "sessionName", DateTime.getStringDate(), mSelectedFeatures.get(0), mSelectedFeatures.get(1));
        dialogSessionFragment.show(fm, "");
    }

    /**
     * Called if sketch is invalid. Reports to user why the sketch was invalid.
     */
    private void reportNotValid() {
        String validIf;
        if (mSketchEditor.getSketchCreationMode() == SketchCreationMode.POINT) {
            validIf = "Point only valid if it contains an x & y coordinate.";
        } else if (mSketchEditor.getSketchCreationMode() == SketchCreationMode.MULTIPOINT) {
            validIf = "Multipoint only valid if it contains at least one vertex.";
        } else if (mSketchEditor.getSketchCreationMode() == SketchCreationMode.POLYLINE
                || mSketchEditor.getSketchCreationMode() == SketchCreationMode.FREEHAND_LINE) {
            validIf = "Polyline only valid if it contains at least one part of 2 or more vertices.";
        } else if (mSketchEditor.getSketchCreationMode() == SketchCreationMode.POLYGON
                || mSketchEditor.getSketchCreationMode() == SketchCreationMode.FREEHAND_POLYGON) {
            validIf = "Polygon only valid if it contains at least one part of 3 or more vertices which form a closed ring.";
        } else {
            validIf = "No sketch creation mode selected.";
        }
        String report = "Sketch geometry invalid:\n" + validIf;
        Snackbar reportSnackbar = Snackbar.make(findViewById(R.id.toolbarInclude), report, Snackbar.LENGTH_INDEFINITE);
        reportSnackbar.setAction("Dismiss", view -> reportSnackbar.dismiss());
        TextView snackbarTextView = reportSnackbar.getView().findViewById(com.google.android.material.R.id.snackbar_text);
        snackbarTextView.setSingleLine(false);
        reportSnackbar.show();
        Log.e(TAG, report);
    }

    private void showCustomDialog() {
        // Inflate the custom layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.dialog_method, null);

        // Create the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogLayout);
        builder.setCancelable(true);

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();

        // Set up the buttons
        Button buttonOption1 = dialogLayout.findViewById(R.id.buttonOption1);
        Button buttonOption2 = dialogLayout.findViewById(R.id.buttonOption2);
        Button buttonOption3 = dialogLayout.findViewById(R.id.buttonOption3);

        buttonOption1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentEditState = EditState.Editing;
                mTypeSurvey = TypeSurvey.AddAncak;
                fab_map.callOnClick();
                if(sniper.getVisibility()==View.VISIBLE){
                    isModeFree = false;
                    sniper.setVisibility(View.GONE);
                    toolbarAdd.setVisibility(View.GONE);
                    l_action.setVisibility(View.GONE);
                }
                else {
                    isModeFree = true;
                    fabAdd.setVisibility(View.GONE);
                    sniper.setVisibility(View.VISIBLE);
                    toolbarAdd.setVisibility(View.VISIBLE);
                    l_action.setVisibility(View.VISIBLE);
//                toolbar.setVisibility(View.VISIBLE);
//                l_action.setVisibility(View.VISIBLE);
                }
                // Handle Option 1 click
                dialog.dismiss();
            }
        });

        buttonOption2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTypeSurvey = TypeSurvey.MergeAncak;
                mSelectedFeatures.clear();
                refreshAllLayers();
//                toolbarAdd.setVisibility(View.VISIBLE);
                dialogAction("Silahkan pilih ancak pertama untuk di gabung","Pilih Ancak");
//                showToast("Silahkan Pilih Ancak Pertama");
              // Handle Option 2 click
                dialog.dismiss();
            }
        });
        buttonOption3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                cutSplit2(mSelectedArcGISFeature);
//                showBottomSheet4(1);
                showDialogTPH();
                dialog.dismiss();
            }
        });
    }


    public void OpenCamera() {
//        ActivityLoggerHelper.recordHitTime(ResultCensusActiv.class.getName(),"btOpenCam",new Date(),new Date(),this);
//        openCamState = MapHguActivity.OpenCam.FormObject;
        if (ALselectedImage.size() < 3) {
//            EasyImage.
            EasyImage.openCamera(this, 1);
        }
    }


    private void drawImaginaryLine(Point currentLocation, GraphicsOverlay graphicsOverlay) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        double dpi = displayMetrics.xdpi;
        // Mendapatkan faktor konversi dari inci ke meter
        double metersPerInch = 0.0254;
        // Mendapatkan skala peta saat ini
        double scale = mapView.getMapScale();
        // Menghitung jumlah meter yang diproyeksikan dalam satu piksel pada skala peta saat ini
        double metersPerPixel = metersPerInch / dpi * scale;
        // Menentukan jarak dalam piksel (asumsi 100 meter)
        double distanceInPixels = 500 / metersPerPixel;
        // Mendapatkan koordinat layar titik awal (titik GPS)
        android.graphics.Point screenPoint = mapView.locationToScreen(currentLocation);
        // Menentukan koordinat layar titik akhir (vertikal 100 meter dari titik awal)
        android.graphics.Point endPoint = new android.graphics.Point(screenPoint.x, screenPoint.y - (int) distanceInPixels);
        // Mendapatkan koordinat dunia (koordinat geografis) titik akhir dari koordinat layar
        Point endLocation = mapView.screenToLocation(endPoint);
        // Periksa apakah endLocation memiliki referensi spasial yang sama dengan currentLocation
        if (!endLocation.getSpatialReference().equals(currentLocation.getSpatialReference())) {
            endLocation = (Point) GeometryEngine.project(endLocation, currentLocation.getSpatialReference());
        }
        // Membuat garis dari titik awal ke titik akhir
        PointCollection pointCollection = new PointCollection(currentLocation.getSpatialReference());
        pointCollection.add(currentLocation);
        pointCollection.add(endLocation);
        Polyline line = new Polyline(pointCollection);
        // Mengatur simbol dan properti garis
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.RED, 3);
        Graphic lineGraphic = new Graphic(line, lineSymbol);
        lineGraphicImaginer = lineGraphic;
        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(lineGraphic);
    }

    private void drawVerticalLine(Point currentLocation, MapView mapView, GraphicsOverlay graphicsOverlay) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        double dpi = displayMetrics.xdpi;

        // Mendapatkan faktor konversi dari inci ke meter
        double metersPerInch = 0.0254;

        // Mendapatkan skala peta saat ini
        double scale = mapView.getMapScale();

        // Menghitung jumlah meter yang diproyeksikan dalam satu piksel pada skala peta saat ini
        double metersPerPixel = metersPerInch / dpi * scale;

        // Menentukan jarak dalam piksel (asumsi 100 meter)
        double distanceInPixels = 100 / metersPerPixel;

        // Mendapatkan koordinat layar titik awal (titik GPS)
        android.graphics.Point screenPoint = mapView.locationToScreen(currentLocation);

        // Menentukan koordinat layar titik akhir (vertikal 100 meter dari titik awal)
        android.graphics.Point endPoint = new android.graphics.Point(screenPoint.x, screenPoint.y - (int) distanceInPixels);

        // Mendapatkan koordinat dunia (koordinat geografis) titik akhir dari koordinat layar
        Point endLocation = mapView.screenToLocation(endPoint);

        // Periksa apakah endLocation memiliki referensi spasial yang sama dengan currentLocation
        if (!endLocation.getSpatialReference().equals(currentLocation.getSpatialReference())) {
            endLocation = (Point) GeometryEngine.project(endLocation, currentLocation.getSpatialReference());
        }

        // Membuat garis dari titik awal ke titik akhir
        PointCollection pointCollection = new PointCollection(currentLocation.getSpatialReference());
        pointCollection.add(currentLocation);
        pointCollection.add(endLocation);
        Polyline line = new Polyline(pointCollection);

        // Mengatur simbol dan properti garis
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.RED, 3);
        Graphic lineGraphic = new Graphic(line, lineSymbol);

        // Menambahkan garis ke layer grafis
        graphicsOverlay.getGraphics().clear(); // Menghapus grafik sebelumnya
        graphicsOverlay.getGraphics().add(lineGraphic);

    }


    private void drawCuttingLine(Point currentLocation, double heading, GraphicsOverlay graphicsOverlay) {
        // Clear previous line
        mGraphicsOverlay.getGraphics().clear();

        // Define the length of the line (e.g., 1000 meters)
        double lineLength = 1000.0;

        // Calculate the end point of the line
        double headingRadians = Math.toRadians(heading);
        double endX = currentLocation.getX() + lineLength * Math.cos(headingRadians);
        double endY = currentLocation.getY() + lineLength * Math.sin(headingRadians);
        Point endPoint = new Point(endX, endY, currentLocation.getSpatialReference());

        // Create the polyline
        PolylineBuilder polylineBuilder = new PolylineBuilder(currentLocation.getSpatialReference());
        polylineBuilder.addPoint(currentLocation);
        polylineBuilder.addPoint(endPoint);
        Polyline polyline = polylineBuilder.toGeometry();

        // Create a graphic for the polyline
        SimpleLineSymbol lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.DASH, Color.RED, 2);
        Graphic polylineGraphic = new Graphic(polyline, lineSymbol);

        // Add the graphic to the graphics overlay
        graphicsOverlay.getGraphics().add(polylineGraphic);
    }


    public  void markPoint(double lat, double lon, GraphicsOverlay graphicsOverlay, Feature f){
        isModeAdd = true;
        if(f == null){
            dialogAction("Harap pilih dahulu ancak yang akan di potong","Pilih Ancak!");
            return;
        }
        com.esri.arcgisruntime.geometry.Polygon wgs84Polygon = (com.esri.arcgisruntime.geometry.Polygon) GeometryEngine.project(f.getGeometry(), wgs84);
//        CompositeSymbol cms2 = new CompositeSymbol();
//        SimpleLineSymbol sls = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,3,3);
        Log.e("AddMethod", "true");
        List<Symbol> symbols = new ArrayList();
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
        Point addPoint;
        addPoint = new Point(lat, lon, wgs84);
//        Point projectedPoint = (Point) GeometryEngine.project(addPoint, wgs84Polygon.getSpatialReference());
//        ProximityResult nearestVertexOnPolygon = GeometryEngine.nearestVertex(wgs84Polygon, projectedPoint);
//        addPoint = (Point) nearestVertexOnPolygon.getCoordinate();
        ProximityResult nearestPointOnPolygon = GeometryEngine.nearestCoordinate(wgs84Polygon, addPoint);
        addPoint = (com.esri.arcgisruntime.geometry.Point) nearestPointOnPolygon.getCoordinate();
        points.add(addPoint);
        mAssetViewModel.insertPointBackup(addPoint);
//        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, 3), SimpleMarkerSymbol.STYLE.X);
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);
        Graphic graph = new Graphic(addPoint, cms);
        graphicsOverlay.getGraphics().add(graph);
        final Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT,
                0xFFFF0000, 3));
        graphicsOverlay.getGraphics().add(polylineGraphic);
    }

    public  void markPointMod(double lat, double lon, GraphicsOverlay graphicsOverlay, Feature f) {
        isModeAdd = true;

        // Proyeksikan geometri poligon ke WGS84
        com.esri.arcgisruntime.geometry.Polygon wgs84Polygon = (com.esri.arcgisruntime.geometry.Polygon) GeometryEngine.project(f.getGeometry(), wgs84);

        // Simbol untuk titik yang akan ditambahkan
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
        Point addPoint = new Point(lat, lon, wgs84);

        // Jika points.size() == 0, cari titik terdekat dari poligon

            ProximityResult nearestPointOnPolygon = GeometryEngine.nearestCoordinate(wgs84Polygon, addPoint);
        Point addPoint2 = (Point) nearestPointOnPolygon.getCoordinate();  // Gunakan titik terdekat sebagai addPoint


        // Tambahkan titik ke daftar points
        points.add(addPoint2);

        // Buat simbol gabungan untuk menampilkan grafik pada peta
        List<Symbol> symbols = new ArrayList<>();
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);

        // Buat grafik titik dan tambahkan ke GraphicsOverlay
        Graphic pointGraphic = new Graphic(addPoint2, cms);
//        layerDraw.getGraphics().add(pointGraphic);
        graphicsOverlay.getGraphics().add(pointGraphic);
        final Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT,
                0xFFFF0000, 3));
        graphicsOverlay.getGraphics().add(polylineGraphic);

        // Buat grafik polyline yang menghubungkan semua titik dalam points
//        if (points.size() > 1) {
//            Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT, 0xFFFF0000, 3));
//            graphicsOverlay.getGraphics().add(polylineGraphic);
//        }
    }

    public static void markPointImagine(Point pointImagine, GraphicsOverlay graphicsOverlay, Feature f){
        isModeAdd = true;
        com.esri.arcgisruntime.geometry.Polygon wgs84Polygon = (com.esri.arcgisruntime.geometry.Polygon) GeometryEngine.project(f.getGeometry(), wgs84);
//        CompositeSymbol cms2 = new CompositeSymbol();
//        SimpleLineSymbol sls = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID,3,3);
        Log.e("AddMethod", "true");
        List<Symbol> symbols = new ArrayList();
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.BLUE, 12);
        Point addPoint;
//        addPoint = new Point(pointImagine, wgs84);
        Point projectedPoint = (Point) GeometryEngine.project(pointImagine, wgs84Polygon.getSpatialReference());
//        ProximityResult nearestVertexOnPolygon = GeometryEngine.nearestVertex(wgs84Polygon, projectedPoint);
//        addPoint = (Point) nearestVertexOnPolygon.getCoordinate();

//        ProximityResult nearestPointOnPolygon = GeometryEngine.nearestCoordinate(wgs84Polygon, projectedPoint);
//        addPoint = (com.esri.arcgisruntime.geometry.Point) nearestPointOnPolygon.getCoordinate();

        points.add(projectedPoint);
//        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, 3), SimpleMarkerSymbol.STYLE.X);
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);
        Graphic graph = new Graphic(projectedPoint, cms);
        graphicsOverlay.getGraphics().add(graph);
        final Graphic polylineGraphic = new Graphic(new Polyline(points), new SimpleLineSymbol(SimpleLineSymbol.Style.DOT,
                0xFFFF0000, 3));
        graphicsOverlay.getGraphics().add(polylineGraphic);


    }


    enum EditState {
        NotReady, // Geodatabase has not yet been generated
        Editing, // A feature is in the process of being moved
        Ready // The geodatabase is ready for synchronization or further edits
    }

    enum  TypeSurvey{
        None,
        AddAncak,
        MergeAncak,
        JoinTPH,
        AddTPH,
        LockTap
    }


    public static double distance(double lat1, double lat2, double lon1,double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }

    public static double bearing(double startLat, double endLat, double startLng,  double endLng){
        double longitude1 = startLng;
        double longitude2 = endLng;
        double latitude1 = Math.toRadians(startLat);
        double latitude2 = Math.toRadians(endLat);
        double longDiff= Math.toRadians(longitude2-longitude1);
        double y= Math.sin(longDiff)*Math.cos(latitude2);
        double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);
        return (Math.toDegrees(Math.atan2(y, x))+360)%360;
    }

    private void showNumberInputDialog() {
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);

        // Input filter to restrict max value to 200
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    int input = Integer.parseInt(dest.toString() + source.toString());
                    if (input > 15) {
                        showToast("Maksimal jarak GPS 15 M");
                        return "";
                    }
                } catch (NumberFormatException nfe) { }
                return null;
            }
        };

        input.setFilters(new InputFilter[]{filter});

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Masukan jarak titik tuju dari GPS (M)");
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (points.isEmpty()){
                    showAlert("Tambahkan titik Awal","Belum ada titik sebelumnya",SweetAlertDialog.WARNING_TYPE);
                    return;
                }
                isModeFree = false;
                if (sniper.getVisibility()==View.VISIBLE){
                    sniper.setVisibility(View.GONE);
                }
                String value = input.getText().toString();
                if (!value.isEmpty()) {
                    Double number = Double.parseDouble(value);
                    Toast.makeText(MapReplantingActivity.this, "Panjang line: " + number + "Meter", Toast.LENGTH_LONG).show();
                    drawImaginary3(getGPS(),layerImagine,number);
                    removeMapRotationListener();
//                    drawImaginary2(points.get(points.size()-1),layerImagine,number);
                    listener = new MapRotationChangedListener() {
                        @Override
                        public void mapRotationChanged(MapRotationChangedEvent mapRotationChangedEvent) {
                            drawImaginary3(getGPS(),layerImagine,number);
//                            drawImaginary2(points.get(points.size()-1), layerImagine,number);

                        }
                    };

                        mapView.addMapRotationChangedListener(listener);



//                    mapView.addMapRotationChangedListener(new MapRotationChangedListener() {
//                        @Override
//                        public void mapRotationChanged(MapRotationChangedEvent mapRotationChangedEvent) {
//                            drawImaginary2(points.get(points.size()-1), layerImagine,number);
////                            rotationChangedListener = mapRotationChangedEvent;
//                        }
//                    });
                } else {
                    Toast.makeText(MapReplantingActivity.this, "Harap set panjang line", Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeMapRotationListener();
                if(sniper.getVisibility()==View.GONE){
                    sniper.setVisibility(View.VISIBLE);
                }
                layerImagine.getGraphics().clear();
                dialog.cancel();
            }
        });

        builder.show();
    }



    public void addNewObject(Feature _feature, List<Geometry> parts) {
        boolean isBlock = false;
        String _estCode = "EstCode";
        String _groupCompany = "GroupCompany";
        if (_feature.getFeatureTable().getTableName().equals("ANC3")){
            isBlock = false;
        }else {
            isBlock = true;
            _estCode = "ESTNR";
            _groupCompany = "GroupCompanyName";
        }
//        featureLayer.clearSelection();
//        featureResult = null;
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                if (Objects.equals(layer.getName(), "ANC3")) {
                    FeatureLayer _featureLayer = (FeatureLayer) layer;
                    FeatureTable featureTable = _featureLayer.getFeatureTable();
                    mFeatureTable = featureTable;
                    com.esri.arcgisruntime.geometry.Point gps = (com.esri.arcgisruntime.geometry.Point) GeometryEngine.project(mLocationDisplay.getMapLocation(), SpatialReferences.getWgs84());
                    String dev_loc = gps.getY() + ";" + gps.getX();
//                    mFeatureTable = featureTable;
                    double area = GeometryEngine.areaGeodetic(parts.get(0), new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
                    double area2 = GeometryEngine.areaGeodetic(parts.get(1), new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
                    DecimalFormat df = new DecimalFormat("#.##");
                    Geometry geometry = parts.get(0);
                    Geometry geometry2 = parts.get(1);
                    UUID uuid = UUID.randomUUID();
//                    String survId = Prefs.getString(Prefs.SESSION_ID);
//                    String survId = DateTime.getStringLongDate()+"_"+GlobalHelper.getEstate().getEstCode()+"_"+GlobalHelper.getUser().getUserID()+"_AWM_PSA_001";
                    String groupCompName = GlobalHelper.getEstate().getCompanyShortName();
                    groupCompName = groupCompName.replace("-", " ");
                    Map<String, Object> defAttribute = new HashMap<>();
                    defAttribute.put("GlobalID", uuid);
                    defAttribute.put("CompanyCode", _feature.getAttributes().get("CompanyCode"));
                    defAttribute.put("Block", _feature.getAttributes().get("Block"));
                    defAttribute.put("EstCode", _feature.getAttributes().get(_estCode));
                    defAttribute.put("Division", _feature.getAttributes().get("Division"));
                    defAttribute.put("GroupCompany", _feature.getAttributes().get(_groupCompany));
                    defAttribute.put("created_user",userID);
//                    defAttribute.put("remark_qc", "");
                    defAttribute.put("LuasHa", limitDouble(area));
                    defAttribute.put("NoAncak", "0");
                    defAttribute.put("status_qc", Short.valueOf("0"));
                    defAttribute.put("created_date", DateTime.timeStampCalendar());
                    defAttribute.put("tphCode", "");

                    Map<String, Object> defAttribute2 = new HashMap<>();
                    defAttribute2.put("GlobalID", uuid);
                    defAttribute2.put("CompanyCode", _feature.getAttributes().get("CompanyCode"));
                    defAttribute2.put("Block", _feature.getAttributes().get("Block"));
                    defAttribute2.put("EstCode", _feature.getAttributes().get(_estCode));
                    defAttribute2.put("created_user", userID);
//                    defAttribute.put("remark_qc", "");
                    defAttribute2.put("Division", _feature.getAttributes().get("Division"));
                    defAttribute2.put("created_date", DateTime.timeStampCalendar());
                    defAttribute2.put("GroupCompany", _feature.getAttributes().get(_groupCompany));
                    defAttribute2.put("LuasHa", limitDouble(area2));
                    defAttribute2.put("NoAncak", "0");
                    defAttribute2.put("status_qc", Short.valueOf("0"));
//                  defAttribute.put("latitude", gps.getY());
//                  defAttribute.put("longitude", gps.getX());
                    Feature newFeature = featureTable.createFeature(defAttribute, geometry);
                    Feature newFeature2 = featureTable.createFeature(defAttribute2, geometry2);
                    newFeature.setGeometry(geometry);
                    selectedFeature = newFeature;
                    if(!isBlock){
                        mFeature1 = newFeature;
                        _feature.setGeometry(geometry2);
                        _feature.getAttributes().put("LuasHa", limitDouble(area2));
                        mFeature2 = _feature;
                    } else {
                        mFeature1 = newFeature;
                        mFeature2 = newFeature2;
                    }
//                    if (isBlock){
//                    featureTable.addFeatureAsync(newFeature2);
//                    }
//                    else {
//                        _feature.setGeometry(geometry2);
//                        _feature.getAttributes().put("LuasHa", limitDouble(area2));
//                        featureTable.updateFeatureAsync(_feature);
//                    }


                    showBottomSheet3(0,selectedFeature, isBlock,_feature);
//                    showBottomSheetDialog(newFeature,"","","");
//                    featureTable.addFeatureAsync(newFeature);
                    Log.e("CanAdd",String.valueOf(featureTable.canAdd()));
//                    featureTable.addFeatureAsync(newFeature2);
//                    featureLayer.selectFeature(newFeature);
//                    showBottomSheet3(0);
                }
            }
        }


    }


    private double limitDouble(double param){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("#.##",symbols);
        String formattedNumber = df.format(param);
        Log.e("Result Ha", formattedNumber);
        double result = Double.valueOf(formattedNumber);
        return result;
    }

    public void saveNewFeature(Feature feature, boolean isBlock, Feature baseFeature) {
//        featureLayer.clearSelection();
//        featureLayer.selectFeature(feature);
//        feature.getAttributes().put("LuasHa", "");

        if (isBlock){
            mFeature2.getFeatureTable().addFeatureAsync(mFeature2);
        }
        else {
//            baseFeature.setGeometry(mFeature2.getGeometry());
////            mFeature2.getAttributes().put("LuasHa", limitDouble(area2));
            baseFeature.getFeatureTable().updateFeatureAsync(mFeature2);
        }

        mFeatureTable.addFeatureAsync(feature).addDoneListener(() -> {
            layerDraw.getGraphics().clear();
            layerImagine.getGraphics().clear();
            mSelectedFeatures.clear();
            mCurrentEditState = EditState.Ready;
            mAssetViewModel.clearPointsBackup();
            Log.e("size Ancak", String.valueOf(mFeatureTable.getTotalFeatureCount()));
//            for (Layer layer : mapView.getMap().getOperationalLayers()) {
//                layer.;
//            }

        });

//        mSelectedFeatures.clear();
    }

    public void postCheckout(List<String> blocks){
        for (String block : blocks){
            post(block);
        }
    }

    public void post(String _block){
        PostCheckout _post= new PostCheckout(estateCode,_block,userID,"2");
        Call<ResponseBody> postCheckoutBlock = pdcServices.postCheckoutBlock(_post);
        postCheckoutBlock.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    public void saveFeature(Feature feature) {
//        featureLayer.clearSelection();
//        featureLayer.selectFeature(feature);
//        feature.getAttributes().put("LuasHa", "");

        mFeatureTable.addFeatureAsync(feature).addDoneListener(() -> {
            layerDraw.getGraphics().clear();
            layerImagine.getGraphics().clear();
            mSelectedFeatures.clear();
            mCurrentEditState = EditState.Ready;
            Log.e("size Ancak", String.valueOf(mFeatureTable.getTotalFeatureCount()));
        });

//        mSelectedFeatures.clear();
    }



    private void showListDialog() {
        // Prepare the data for the ListView
        List<String> items = mAssetViewModel.getAllNameTPH("A01");
//        String[] items = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};


        // Create ListView and set its adapter
        ListView listView = new ListView(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);

        // Create the dialog and set ListView as its content
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("List TPH unassign di block A01");
        builder.setView(listView);

        // Set item click listener
        listView.setOnItemClickListener((parent, view, position, id) -> {
            String selectedItem = items.get(position);
            Toast.makeText(MapReplantingActivity.this, "Selected: " + selectedItem, Toast.LENGTH_SHORT).show();
        });

        // Add a dismiss button
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        // Show the dialog
        builder.create().show();
    }


    public List<String> getUserALl(){
        return mAssetViewModel.getAllNameTPH(mAssetViewModel.getSelectedBlock().getBlock());
    }

    private LabelDefinition makeLabelDefinition(String party, int textColor, String whereClause) {// Create the where clause for the label
        String where = "status_qc = '" + whereClause + "'";
        // Create the Arcade expression based on the status
        ArcadeLabelExpression arcadeLabelExpression;
        if (whereClause.equals("0")) {
            arcadeLabelExpression = new ArcadeLabelExpression("\"No : \" + $feature.NoAncak + \"\\nLuas: \" + $feature.LuasHa + \" Ha\"+ \"\\nPalm : 0 \"+\"\\nBelum QC\"");
        } else if (whereClause.equals("1")) {
            arcadeLabelExpression = new ArcadeLabelExpression("\"No : \" + $feature.NoAncak + \"\\nLuas: \" + $feature.LuasHa + \" Ha\"+ \"\\nPalm : 0 \"+\"\\nApprove\"");
        } else {
            arcadeLabelExpression = new ArcadeLabelExpression("\"No : \" + $feature.NoAncak + \"\\nLuas: \" + $feature.LuasHa + \" Ha\"+ \"\\nPalm : 0 \"+\"\\nReject\"");
        }
        // Create text symbol for styling the label
        TextSymbol textSymbol = new TextSymbol();
        textSymbol.setSize(12);
        textSymbol.setColor(textColor);
        textSymbol.setHaloColor(Color.WHITE);
        textSymbol.setHaloWidth(2);

        // Create the label definition with the Arcade expression
        LabelDefinition labelDefinition = new LabelDefinition(arcadeLabelExpression, textSymbol);
        labelDefinition.setPlacement(LabelingPlacement.POLYGON_ALWAYS_HORIZONTAL);

        // Apply the where clause to filter the features
        labelDefinition.setWhereClause(where);
//        labelDefinition.setWhereClause(String.format("PARTY = '%s'", party));

        return labelDefinition;
    }



    public void mergePolygons() {

        for (Layer layer :mapView.getMap().getOperationalLayers()){
            if (layer.getName().equals("ANC3")){
                featureLayer = (FeatureLayer) layer;
            }
        }
        // Ambil geometri dari kedua fitur
        Polygon polygon1 = (Polygon) mSelectedFeatures.get(0).getGeometry();
        Polygon polygon2 = (Polygon) mSelectedFeatures.get(1).getGeometry();
        Feature feature1 = mSelectedFeatures.get(0);
        Feature feature2 = mSelectedFeatures.get(1);
        // Gabungkan kedua poligon menggunakan GeometryEngine
        Polygon mergedPolygon = (Polygon) GeometryEngine.union(polygon1, polygon2);
        // Buat fitur baru dengan geometri gabungan dan atribut dari fitur pertama
        FeatureTable featureTable = feature1.getFeatureTable();
        feature1.setGeometry(mergedPolygon);
        double area = GeometryEngine.areaGeodetic(mergedPolygon, new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
        DecimalFormat df = new DecimalFormat("#.##");
        // Memformat angka ke string dengan dua digit desimal
        String formattedNumber = df.format(area);
        feature1.getAttributes().put("LuasHa", limitDouble(area));
        featureTable.updateFeatureAsync(feature1);
        featureTable.deleteFeatureAsync(mSelectedFeatures.get(1));
        mTypeSurvey = TypeSurvey.None;
        mTypeSurvey = TypeSurvey.AddAncak;
    }
    public void mergePolygons2() {
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer.getName().equals("ANC3")) {
                featureLayer = (FeatureLayer) layer;
            }
        }
        // Ambil geometri dari kedua fitur
        Polygon polygon1 = (Polygon) mSelectedFeatures.get(0).getGeometry();
        Polygon polygon2 = (Polygon) mSelectedFeatures.get(1).getGeometry();
        Feature feature1 = mSelectedFeatures.get(0);
        Feature feature2 = mSelectedFeatures.get(1);

        if (GeometryEngine.equals(polygon1, polygon2)) {
            // Jika kedua poligon sama, tampilkan pesan dan kembalikan
//            Log.e("MergePolygons", "Polygons are the same, cannot merge.");
            showAlert("Gagal Gabung Ancak", "Tidak bisa menggabungkan ancak yang sama!", SweetAlertDialog.ERROR_TYPE);
            refreshAllLayers();
            mSelectedFeatures.clear();
            return;
        }

        // Cek apakah kedua poligon bersinggungan atau tidak
        if (GeometryEngine.intersects(polygon1, polygon2) || GeometryEngine.touches(polygon1, polygon2)) {
            // Gabungkan kedua poligon menggunakan GeometryEngine jika bersinggungan
            Polygon mergedPolygon = (Polygon) GeometryEngine.union(polygon1, polygon2);

            // Buat fitur baru dengan geometri gabungan dan atribut dari fitur pertama
            FeatureTable featureTable = feature1.getFeatureTable();
            feature1.setGeometry(mergedPolygon);

            double area = GeometryEngine.areaGeodetic(mergedPolygon, new AreaUnit(AreaUnitId.HECTARES), GeodeticCurveType.GEODESIC);
            DecimalFormat df = new DecimalFormat("#.##");
            String formattedNumber = df.format(area);

            feature1.getAttributes().put("LuasHa", limitDouble(area));

            // Update fitur yang digabungkan dan hapus fitur kedua
            featureTable.updateFeatureAsync(feature1);
            featureTable.deleteFeatureAsync(feature2);
            refreshAllLayers();

            mTypeSurvey = TypeSurvey.None;
            mTypeSurvey = TypeSurvey.AddAncak;

        } else {
            // Jika poligon tidak bersinggungan, beri pesan bahwa tidak bisa digabung
            Log.e("MergePolygons", "Polygons are not adjacent, cannot merge.");
            showAlert("Gagal Gabung Ancak","Hanya ancak yang terdekat yang bisa di gabungkan!",SweetAlertDialog.ERROR_TYPE);
            mTypeSurvey = TypeSurvey.AddAncak;
            refreshAllLayers();
            mSelectedFeatures.clear();
//            Toast.makeText(context, "Polygons are not adjacent and cannot be merged", Toast.LENGTH_LONG).show();
        }
    }


    public boolean checkUpdate(){
        FeatureTable featureTable = featureLayer.getFeatureTable();

        // Check if the feature table is an instance of GeodatabaseFeatureTable
        if (featureTable instanceof GeodatabaseFeatureTable) {
            // Cast the feature table to GeodatabaseFeatureTable
            GeodatabaseFeatureTable geodatabaseFeatureTable = (GeodatabaseFeatureTable) featureTable;

            // Now you can check for updates, edits, or other operations
            if (geodatabaseFeatureTable.hasLocalEdits()) {
                // If there are local edits, return true
                return true;
            }
        }

        // Return false if no updates or if it's not a GeodatabaseFeatureTable
        return false;
    }



    public void setNone(){
        mTypeSurvey = TypeSurvey.None;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
    public void deleteFeature(Feature param){
        param.getFeatureTable().deleteFeatureAsync(param);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        readGeodatabase();
//        refreshAllLayers();
        mapView.resume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mapView != null){
            mapView.getMap().getOperationalLayers().clear();
            mapView.destroyDrawingCache();
            mapView.dispose();
            mapView = null;
        }

    }

}