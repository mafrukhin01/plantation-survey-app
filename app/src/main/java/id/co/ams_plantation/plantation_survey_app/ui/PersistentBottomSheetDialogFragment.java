package id.co.ams_plantation.plantation_survey_app.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import id.co.ams_plantation.plantation_survey_app.R;

/**
 * Created by Mafrukhin on 24/05/2023.
 */
public class PersistentBottomSheetDialogFragment extends BottomSheetDialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for the bottom sheet
        View view = inflater.inflate(R.layout.layout_sampel, container, false);

        // Set up your bottom sheet layout and its contents

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set the peek height to make the bottom sheet partially visible initially
//        getDialog().getWindow().setPeekDecorViewHeight(200);

        // Set the expanded state for the bottom sheet
        BottomSheetBehavior<View> behavior = BottomSheetBehavior.from((View) getView().getParent());
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}