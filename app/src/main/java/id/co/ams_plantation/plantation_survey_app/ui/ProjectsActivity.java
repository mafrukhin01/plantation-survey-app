package id.co.ams_plantation.plantation_survey_app.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.ProjectAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AppUpdate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.AppVersion;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ProjectsNMapping;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.User;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserProject;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.GetMasterblock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.services.ApiResponses;
import id.co.ams_plantation.plantation_survey_app.services.model.AdminAppsResponse;
import id.co.ams_plantation.plantation_survey_app.services.model.HMSResponse;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.SequenceNumber;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectsActivity extends BaseActivity {
    @BindView(R.id.listview_list)
    RecyclerView listView;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footer)
    CardView footer;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    @BindView(R.id.bt_refresh)
    CardView refresh;
    ProjectAdapter adapter;
    Dialog sweetAlert;
    LinearLayoutManager llm;
    private String userID, estateCode, userFullName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        ButterKnife.bind(this);
        initDB();
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();
        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName() +  " "
                + GlobalHelper.getEstate().getEstName());
        llm = new LinearLayoutManager(this);
        setUI();
        Log.e("VersionName",getVersionName(this));
        if(isNetworkConnected()){
            syncEmployee();
            syncAppVersion2();
            getLicense();
//            getTPHMaster();
            getExtendEstate();
            getBlocksOpen();
        }

        refresh.setOnClickListener(v -> {
           updateList();
        });
    }

    public void setUI(){
        List<UserProject>list1 = mAssetViewModel.getUserProjectByEstate(estateCode);
//        Log.e("project act",String.valueOf(list.size()));
        adapter = new ProjectAdapter(this, list1);
        listView.setLayoutManager(llm);
        listView.setAdapter(adapter);
    }
    public void updateList(){
       showLoadingAlert("Loading...","Please Wait");
       syncDataNew();
       syncEstate();
//       syncData();

    }

    public void getExtendEstate(){
        Call<List<ExtendEstate>> getExtendEstate = apiService.getExtend(GlobalHelper.getEstate().getEstCode());
        getExtendEstate.enqueue(new Callback<List<ExtendEstate>>() {
            @Override
            public void onResponse(Call<List<ExtendEstate>> call, Response<List<ExtendEstate>> response) {
                if(response.isSuccessful()){
                    List<ExtendEstate> list = response.body();
                    Log.e("Extend",String.valueOf(list.size()));
                    mAssetViewModel.inputExtend(list);
                }
            }

            @Override
            public void onFailure(Call<List<ExtendEstate>> call, Throwable t) {

            }
        });
    }
    public void syncEstate(){
        Call<ApiResponses<List<EstateMapping>>> getEstateMapping = apiService.GetEstateMappingByEstCode(GlobalHelper.getEstate().getEstCode());

        getEstateMapping.enqueue(new Callback<ApiResponses<List<EstateMapping>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<EstateMapping>>> call, Response<ApiResponses<List<EstateMapping>>> response) {
                if(response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().data.size()>0) {
                        mAssetViewModel.insertListEstateMapping(response.body().data);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponses<List<EstateMapping>>> call, Throwable t) {
            hideLoadingAlert();
            }
        });
    }


    public void syncEmployee(){
        Call<AdminAppsResponse> getEmployee = adminAppsService.getAllEmployee(GlobalHelper.getEstate().getEstCode());
        getEmployee.enqueue(new Callback<AdminAppsResponse>() {
            @Override
            public void onResponse(Call<AdminAppsResponse> call, Response<AdminAppsResponse> response) {
//                assert response.body() != null;
                mAssetViewModel.insertListEmployee(response.body().getData());
                Log.e("Response employee",response.message());
                if(response.isSuccessful()){
                    Log.e("Log Success","true");
                    mAssetViewModel.insertListEmployee(response.body().getData());
                }
            }
            @Override
            public void onFailure(Call<AdminAppsResponse> call, Throwable t) {

                Log.e("Log Success",t.getMessage());

            }
        });

    }
    public void syncData(){
        Call<ApiResponses<List<UserProject>>> call = apiService.GetProjectByUserEstate(GlobalHelper.getUser().getUserID(),GlobalHelper.getEstate().getEstCode());
        call.enqueue(new Callback<ApiResponses<List<UserProject>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<UserProject>>> call, Response<ApiResponses<List<UserProject>>> response) {
                Log.e("responseBody",String.valueOf(response.body()));
                if(response.isSuccessful()){
                    List<UserProject> list = new ArrayList<>();
                    if(response.body().data.size()>0){
//                        mAssetViewModel.truncateUserProjects();
                        for(UserProject userProject: response.body().data){
                            list.add(userProject);
//                            mAssetViewModel.insertUserProject(userProject);
//                            Log.e("LogUserProject",String.valueOf(list.size()));
                        }
                        UserProject userP = response.body().data.get(0);
                        userP.setDescription("Survey Patok Batas");
                        list.add(userP);
                        mAssetViewModel.truncateAndInsertProjects(list);
                        setUI();
                        hideLoadingAlert();
                    }else {
                        hideLoadingAlert();
                        mAssetViewModel.truncateUserProjects();
                        setUI();
                    }
//                            mAssetViewModel.truncateUserProjects();
                }else {
                    hideLoadingAlert();
                    setUI();
                }
            }
            @Override
            public void onFailure(Call<ApiResponses<List<UserProject>>> call, Throwable t) {
                hideLoadingAlert();
            }
        });

    }

    public void syncDataNew(){
        Log.e("data body", "syncdatanew");
        Call<ApiResponses<ProjectsNMapping>> call = apiService.GetProjectNMapping(GlobalHelper.getUser().getUserID(),GlobalHelper.getEstate().getEstCode());
        call.enqueue(new Callback<ApiResponses<ProjectsNMapping>>() {
            @Override
            public void onResponse(Call<ApiResponses<ProjectsNMapping>> call, Response<ApiResponses<ProjectsNMapping>> response) {
//                Log.e("data body", String.valueOf(response.body().data.projects.get(0).toString()));
                if(response.isSuccessful()){
                    List<UserProject> list = new ArrayList<>();
                    if(response.body().data.projects.size()>0){
//                        mAssetViewModel.truncateUserProjects();
                        for(UserProject userProject: response.body().data.projects){
                            list.add(userProject);
//                            mAssetViewModel.insertUserProject(userProject);
//                            Log.e("LogUserProject",String.valueOf(list.size()));
                        }
//                        UserProject project1 = response.body().data.projects.get(0);
//                        UserProject userP = new UserProject(project1.estCode,project1.companyCode,"Survey Patok Batas",project1.userID,project1.statusEdit,project1.startDate,project1.endDate);
//                        UserProject userA = new UserProject(project1.estCode,project1.companyCode,"Survey Ancak",project1.userID,project1.statusEdit,project1.startDate,project1.endDate);
//                        list.add(userP);
//                        list.add(userA);
                        mAssetViewModel.truncateAndInsertProjects(list);
                        mAssetViewModel.insertEstateMapping(response.body().data.mapping);
                        setUI();
                        hideLoadingAlert();
                    }
                    else {
                        mAssetViewModel.truncateUserProjects();
                        setUI();
                        hideLoadingAlert();
                    }
//                            mAssetViewModel.truncateUserProjects();
                }else {
                    mAssetViewModel.truncateUserProjects();
                    setUI();
                    hideLoadingAlert();
                }
            }

            @Override
            public void onFailure(Call<ApiResponses<ProjectsNMapping>> call, Throwable t) {
                Log.e("failure body", String.valueOf(t.toString()));
                setUI();
                hideLoadingAlert();
            }
        });
    }

    public void backup(){
        backupGDB();
    }
    @NonNull
    public static String getVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            return "Unknown";
        }
    }
    public void showAlertUpdate(String title, String message, int type) {
        sweetAlert = new SweetAlertDialog(this, type)
//                .setCancelButton("Cancel", sweetAlertDialog -> {
//                    sweetAlert.dismiss();
//                })
//                .setCancelButtonBackgroundColor(Color.RED)
//                .showCancelButton(false)
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlert.dismiss();
                })
                .setTitleText(title).setContentText(message);
        sweetAlert.show();
    }

    public void syncAppVersion(){
        Call<List<AppVersion>> getAppVersion = apiService.GetVersionApp(getPackageName());
       getAppVersion.enqueue(new Callback<List<AppVersion>>() {
           @Override
           public void onResponse(Call<List<AppVersion>> call, Response<List<AppVersion>> response) {
                if(response.isSuccessful()){
                    if (response.body().size()>0){
                        AppVersion appVersion= response.body().get(0);
                        try {
                            PackageManager packageManager = getPackageManager();
                            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
                            int versionCode = packageInfo.versionCode;
                            Log.e("diffVersion",versionCode+">"+appVersion.getVersion());
                            if (Integer.parseInt(appVersion.getVersion())>versionCode){
                                showAlertUpdate("Update App!","Pastikan Upload Semua Data dan silahkan Update PSA App melalui Admin Apps.",SweetAlertDialog.WARNING_TYPE);
                            }
                            // Use the versionCode as needed
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
           }

           @Override
           public void onFailure(Call<List<AppVersion>> call, Throwable t) {

           }
       });
    }
    public void syncAppVersion2(){
        Call<List<AppUpdate>> getAppVersion = apiService.CheckUpdateApp();
        getAppVersion.enqueue(new Callback<List<AppUpdate>>() {
            @Override
            public void onResponse(Call<List<AppUpdate>> call, Response<List<AppUpdate>> response) {
                if(response.isSuccessful()){
                    if (response.body().size()>0){
                        AppUpdate appVersion= response.body().get(0);
                        try {
                            PackageManager packageManager = getPackageManager();
                            PackageInfo packageInfo = packageManager.getPackageInfo(getPackageName(), 0);
                            int versionCode = packageInfo.versionCode;
                            Log.e("diffVersion",versionCode+">"+appVersion.versionCode);
                            if (versionCode<Integer.parseInt(appVersion.versionCode)){
                                Intent intent = new Intent(ProjectsActivity.this,UpdateActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            // Use the versionCode as needed
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<List<AppUpdate>> call, Throwable t) {

            }
        });
    }

    public void showAlertExpired(){
        sweetAlert = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
//                .setCancelButton("Cancel", sweetAlertDialog -> {
//                    sweetAlert.dismiss();
//                })
//                .setCancelButtonBackgroundColor(Color.RED)
//                .showCancelButton(false)
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlert.dismiss();
//                    addNewObject(objCode);
                })
                .setTitleText("Projek Kedaluarsa! ")
                .setContentText("Periode Projek anda telah selesai, jika memerlukan akses project ke AWM silahkan hubungi Admin atau pada email berikut : \ngis.support@kpnplantation.com");
        sweetAlert.show();
    }

    public void getBlocksOpen(){
        Call<ApiResponses<List<GetMasterblock>>> getBlock = apiService.getMasterBLockAncak(estateCode);

        getBlock.enqueue(new Callback<ApiResponses<List<GetMasterblock>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<GetMasterblock>>> call, Response<ApiResponses<List<GetMasterblock>>> response) {
//                Log.e("ListBlock","true");
                if(response.body().success){
                    List<String> listInput = new ArrayList<>();
                    for (GetMasterblock m: response.body().data){
//                        DecimalFormat df = new DecimalFormat("#.##");
//                        String haGross = df.format(m.ha_Gross) + " Ha";
                        if(m.isOpen == 1){
                            listInput.add(m.block);
                        }
//                        MasterBlock masterBlock = new MasterBlock(m.getBlock(), m.getDivision(), haGross, 0, 0, 0, "", 1);
//                        listInput.add(masterBlock);
                    }
//                    Log.e("ListBlock",String.valueOf(listInput.size()));
                    mAssetViewModel.setBlockOpen(listInput);
//                    hideLoadingAlert();
//                    setUI();
                }
//                else {
//                    Log.e("List Block",String.valueOf("true"));
//
//                }

            }

            @Override
            public void onFailure(Call<ApiResponses<List<GetMasterblock>>> call, Throwable throwable) {
                Log.e("ListBlock",throwable.getMessage());
            }
        });

    }


}