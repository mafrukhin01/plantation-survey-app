package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.data.Geodatabase;
import com.esri.arcgisruntime.data.GeodatabaseFeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.layers.LegendInfo;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.symbology.Renderer;
import com.esri.arcgisruntime.symbology.SimpleRenderer;
import com.esri.arcgisruntime.symbology.Symbol;
import com.esri.arcgisruntime.symbology.UniqueValueRenderer;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.mikepenz.iconics.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FilterHGUAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.SessionListAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.SummaryHguAdapter;
import id.co.ams_plantation.plantation_survey_app.data.AppDatabase;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.LayerMap;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Session;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.SummaryHGU;
import id.co.ams_plantation.plantation_survey_app.data.view_model.SummaryViewModel;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;


@SuppressLint("NonConstantResourceId")
public class ResultCensusActivity extends BaseActivity {
    @BindView(R.id.list_s)
    ListView listSession;
    @BindView(R.id.tv_ringan)
    TextView ringan;
    @BindView(R.id.tv_normal)
    TextView normal;
    @BindView(R.id.tv_sedang)
    TextView sedang;
    @BindView(R.id.tv_berat)
    TextView berat;
    @BindView(R.id.tv_total)
    TextView total;

    private Context mContext;
    private TableView mTableView;
    private FeatureTable mFeatureTable, mFeatureTable2;
    private AppDatabase appDb;
    private String companyCode;

    private int baikR = 0;
    private int rusakR = 0;
    private int hilangR = 0;
    private int bpR = 0;
    private int tbiR = 0;



    private int baikH = 0;
    private int rusakH = 0;
    private int hilangH = 0;
    private int totalR = 0; // Total for regular counts
    private int totalH = 0;
    private int bpH = 0;
    private int tbiH = 0;

    private SummaryHGU hgu;
    private SummaryHGU riparian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_census);
        ButterKnife.bind(this);
        mContext = this;
        companyCode = mAssetViewModel.getAllEstateMappingByEstCode(GlobalHelper.getEstate().getEstCode()).get(0).companyCode;
        appDb = AppDatabase.getDatabase(this);
        mTableView = findViewById(R.id.tableview);
        readGeodatabase();


    }

    private void setTableViewListener(SummaryHguAdapter censusesAdapter, SummaryViewModel mTableViewModel) {
        mTableView.setTableViewListener(new ITableViewListener() {
            @Override
            public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {}
            @Override
            public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {}
            @Override
            public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {}
            @Override
            public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {}
            @Override
            public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {}
            @Override
            public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {}
            @Override
            public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {}
            @Override
            public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {}
            @Override
            public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {}
        });
        censusesAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel.getRowHeaderList(), mTableViewModel.getCellList());
    }

    public void calculateCounts() {
        final int countTasks = 4; // Adjust the total tasks if needed
        final CountDownLatch latch = new CountDownLatch(countTasks);

        int[] conditions = {1, 2, 3, 4, 5};

        // Use countDistinctLatestCondition for HGU and Riparian counts
        countDistinctLatestCondition(mFeatureTable, 1, conditions, latch::countDown);  // HGU
        countDistinctLatestCondition(mFeatureTable, 2, conditions, latch::countDown);  // Riparian

        // Calculate total objects for HGU and Riparian
        countTotalObject(mFeatureTable2, 1, count -> {
            totalH = count;
            latch.countDown();
        });
        countTotalObject(mFeatureTable2, 2, count -> {
            totalR = count;
            latch.countDown();
        });

        // Wait for latch and update UI
        new Thread(() -> {
            try {
                latch.await(); // Wait for all counting tasks to complete
                runOnUiThread(() -> {
                    hgu = new SummaryHGU("HGU", baikH, rusakH, hilangH, bpH, tbiH, totalH - (baikH + rusakH + hilangH + bpH + tbiH), totalH);
                    riparian = new SummaryHGU("Riparian", baikR, rusakR, hilangR, bpR, tbiR, totalR - (baikR + rusakR + hilangR + bpR + tbiR), totalR);
                    SummaryViewModel mTableViewModel = new SummaryViewModel(this, hgu, riparian);
                    SummaryHguAdapter censusesAdapter = new SummaryHguAdapter(this, mTableViewModel);
                    mTableView.setAdapter(censusesAdapter);
                    setTableViewListener(censusesAdapter, mTableViewModel);
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void countFeaturesRiparian(FeatureTable featureTable, int conditionValue, CountCallback callback) {
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setWhereClause("typePatok = 2 AND CompanyCode = '" + companyCode + "' AND condition = " + conditionValue);

        // Query fitur yang sesuai dengan kondisi
        featureTable.queryFeaturesAsync(queryParameters).addDoneListener(() -> {
            try {
                ListenableFuture<FeatureQueryResult> future = featureTable.queryFeaturesAsync(queryParameters);
                FeatureQueryResult result = future.get();

                // Gunakan Set untuk menghitung distinct `PatokId`
                Set<Object> distinctPatokIDs = new HashSet<>();
                for (Feature feature : result) {
                    Object patokID = feature.getAttributes().get("PatokId");
                    if (patokID != null) {
                        distinctPatokIDs.add(patokID);
                    }
                }

                int distinctCount = distinctPatokIDs.size();
                Log.d("Distinct Count", "Number of distinct PatokId for condition " + conditionValue + ": " + distinctCount);
                callback.onCountCompleted(distinctCount);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Feature Count Error", "Error while counting distinct PatokId: " + e.getMessage());
            }
        });
    }
    public void countDistinctLatestCondition(FeatureTable featureTable, int typePatok, int[] conditions, Runnable callback) {
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setWhereClause("typePatok = " + typePatok + " AND CompanyCode = '" + companyCode + "'");

        featureTable.queryFeaturesAsync(queryParameters).addDoneListener(() -> {
            try {
                FeatureQueryResult result = featureTable.queryFeaturesAsync(queryParameters).get();

                // Map to store the latest condition for each PatokId based on survey_date
                Map<Object, Short> latestConditionByPatokID = new HashMap<>();
                Map<Object, Calendar> latestSurveyDateByPatokID = new HashMap<>();

                for (Feature feature : result) {
                    Object patokID = feature.getAttributes().get("PatokID");
                    Short condition = (Short) feature.getAttributes().get("condition");
                    Calendar surveyDate = (Calendar) feature.getAttributes().get("survey_date");

                    if (patokID != null && condition != null && surveyDate != null) {
                        Calendar existingSurveyDate = latestSurveyDateByPatokID.get(patokID);
                        if (existingSurveyDate == null || surveyDate.after(existingSurveyDate)) {
                            latestConditionByPatokID.put(patokID, condition);
                            latestSurveyDateByPatokID.put(patokID, surveyDate);
                        }
                    }
                }

                // Count occurrences of each condition
                Map<Integer, Integer> conditionCounts = new HashMap<>();
                for (int condition : conditions) {
                    conditionCounts.put(condition, 0);
                }
                for (Short condition : latestConditionByPatokID.values()) {
                    conditionCounts.put((int) condition, conditionCounts.getOrDefault((int) condition, 0) + 1);
                }

                // Assign to global variables based on typePatok
                if (typePatok == 1) {  // HGU
                    baikH = conditionCounts.getOrDefault(1, 0);
                    rusakH = conditionCounts.getOrDefault(2, 0);
                    hilangH = conditionCounts.getOrDefault(3, 0);
                    bpH = conditionCounts.getOrDefault(4, 0);
                    tbiH = conditionCounts.getOrDefault(5, 0);
                } else if (typePatok == 2) {  // Riparian
                    baikR = conditionCounts.getOrDefault(1, 0);
                    rusakR = conditionCounts.getOrDefault(2, 0);
                    hilangR = conditionCounts.getOrDefault(3, 0);
                    bpR = conditionCounts.getOrDefault(4, 0);
                    tbiR = conditionCounts.getOrDefault(5, 0);
                }

                // Trigger callback after counts are set
                callback.run();

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Feature Count Error", "Error while counting distinct conditions: " + e.getMessage());
            }
        });
    }



    public void countFeaturesHGU(FeatureTable featureTable, int conditionValue, CountCallback callback) {
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setWhereClause("typePatok = 1 AND CompanyCode = '" + companyCode + "' AND condition = " + conditionValue);
        Log.e("querystring", "typePatok = 1 AND CompanyCode = '" + companyCode + "' AND condition = " + conditionValue);
        featureTable.queryFeaturesAsync(queryParameters).addDoneListener(() -> {
            try {
                ListenableFuture<FeatureQueryResult> future = featureTable.queryFeaturesAsync(queryParameters);
                FeatureQueryResult result = future.get();
                // Map to store the latest feature for each PatokId based on survey_date
                Map<Object, Feature> latestFeaturesByPatokID = new HashMap<>();
                for (Feature feature : result) {
                    Object patokID = feature.getAttributes().get("PatokID");
                    Calendar surveyDateCalendar = (Calendar) feature.getAttributes().get("survey_date");
                    if (patokID != null && surveyDateCalendar != null) {
                        Date surveyDate = surveyDateCalendar.getTime(); // Convert to Date for comparison
                        Feature existingFeature = latestFeaturesByPatokID.get(patokID);
                        // Check if we need to update the entry based on survey_date
                        if (existingFeature == null || surveyDate.after(((Calendar) existingFeature.getAttributes().get("survey_date")).getTime())) {
                            latestFeaturesByPatokID.put(patokID, feature);
                        }
                    }
                }

                int distinctCount = latestFeaturesByPatokID.size();
                Log.d("Distinct Count", "Number of distinct PatokId with latest survey_date for condition " + conditionValue + ": " + distinctCount);
                callback.onCountCompleted(distinctCount);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Feature Count Error", "Error while counting distinct latest PatokId: " + e.getMessage());
            }
        });
    }




    public void countTotalObject(FeatureTable featureTable, int conditionValue, CountCallback callback) {
        QueryParameters queryParameters = new QueryParameters();
        queryParameters.setWhereClause("typePatok = '"+conditionValue+"'"+"AND CompanyCode = '" + companyCode + "'");
        featureTable.queryFeatureCountAsync(queryParameters).addDoneListener(() -> {
            try {
                long featureCount = featureTable.queryFeatureCountAsync(queryParameters).get();
                Log.d("Feature Count", "Number of features riparian " + conditionValue + ": " + featureCount);
                callback.onCountCompleted((int) featureCount);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Feature Count Error", "Error while counting features: " + e.getMessage());
            }
        });
    }

    public void readGeodatabase() {
        copyGDB(); // Ensure this function copies the geodatabase to the specified location.
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + GlobalHelper.FILE_HGU);

        if (fileLoc.exists()) {
            Geodatabase mGeodatabase = new Geodatabase(fileLoc.getAbsolutePath());
            mGeodatabase.loadAsync();

            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                        if ("STK1E".equals(table.getTableName())) {
                            mFeatureTable = table; // Load table "STK1E"
                            calculateCounts();
                        } else if ("STK1".equals(table.getTableName())) {
                            mFeatureTable2 = table;
                        }
                    }
                } else {
                    Log.e("Error", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                }
            });
        } else {
            Log.e("File Not Exist", "Geodatabase file does not exist.");
        }
    }

    public interface CountCallback {
        void onCountCompleted(int count);
    }

}