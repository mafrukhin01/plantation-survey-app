package id.co.ams_plantation.plantation_survey_app.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.esri.arcgisruntime.arcgisservices.LabelDefinition;
import com.esri.arcgisruntime.concurrent.Job;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.Geodatabase;
import com.esri.arcgisruntime.data.GeodatabaseFeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.geometry.Envelope;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.io.RequestConfiguration;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseJob;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateGeodatabaseParameters;
import com.esri.arcgisruntime.tasks.geodatabase.GenerateLayerOption;
import com.esri.arcgisruntime.tasks.geodatabase.GeodatabaseSyncTask;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.MasterBlockAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.ExtendEstate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.GetMasterBlockSDE;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.GetMasterblock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MasterBlock;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.PostCheckout;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.services.ApiResponses;
import id.co.ams_plantation.plantation_survey_app.services.model.HMSResponse;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncBlockActivity extends BaseActivity {
    ListView mlistView;
    Context context;
    private boolean hasUpload = false;
//    DialogProgress progressDialogFragment;
    MasterBlockAdapter masterBlockAdapter;
    private GeodatabaseSyncTask mGeodatabaseSyncTask;
    private GeodatabaseSyncTask mGeodatabaseSyncTask2;
    private String userID, estateCode, userFullName;
    private Geodatabase mGeodatabase;
    FeatureLayer featureLayer;
    private List<String> listBlockGet;
    private ProgressDialog loading;
    ArrayList<MasterBlock> masterBlocks;
    double minX, minY, maxX, maxY;
    @BindView(R.id.bt_sync_block)
    Button bt_sync_block;
    @BindView(R.id.seach_block)
    EditText search_block;
    @BindView(R.id.footerUser)
    TextView fUser;
    @BindView(R.id.footerEstate)
    TextView fEstate;
    @BindView(R.id.bt_cancel_sync)
    Button bt_cancel;
    @BindView(R.id.period_census)
    TextView tv_period;
    @BindView(R.id.bt_refresh)
    CardView bt_refresh;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_block);
        ButterKnife.bind(this);
        context = this;
        userID = Objects.requireNonNull(GlobalHelper.getUser()).getUserID();
        estateCode = Objects.requireNonNull(GlobalHelper.getEstate()).getEstCode();
        userFullName = GlobalHelper.getUser().getUserFullName();
        mlistView = findViewById(R.id.listview_list);
        ExtendEstate extendEstate = mAssetViewModel.getExtend();
        minX = Double.parseDouble(extendEstate.MinX);
        minY = Double.parseDouble(extendEstate.MinY);
        maxX = Double.parseDouble(extendEstate.MaxX);
        maxY = Double.parseDouble(extendEstate.MaxY);

        masterBlocks = new ArrayList<>();
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask("https://app.gis-div.com/arcgis/rest/services/pub_psa/Block_Ancak/FeatureServer");
        mGeodatabaseSyncTask = new GeodatabaseSyncTask("https://app.gis-div.com/arcgis/rest/services/pub_psa/LUS3_Ancak/FeatureServer");
        mGeodatabaseSyncTask2 = new GeodatabaseSyncTask(Prefs.SERVICE_ANCAK);
//        readGeodatabase();
        setUI();
        fUser.setText(userFullName);
        fEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " "
                + GlobalHelper.getEstate().getEstName());
        readGeodatabase(hasUpload -> {
            this.hasUpload = hasUpload;  // Simpan hasil pengecekan ke variabel global
            if (hasUpload) {
                Log.d("Geodatabase", "Ada perubahan lokal yang belum diunggah.");
            } else {
                Log.d("Geodatabase", "Tidak ada perubahan lokal.");
            }
        });
        bt_refresh.setOnClickListener(v ->{
            if (hasUpload) {
                getBlocksOpen();
                dialogAction("Anda tidak bisa pilih block survey lain karena terdapat data survey yang belum terupload!", "Gagal Sync Block!");
            }
            else {
                showLoadingAlert("load data","please wait");
                getExtendEstate();
                getMasterBlockSDE2();
            }

//            generateGeodatabase();
        });
        bt_sync_block.setOnClickListener(v -> {
            if (hasUpload) {
                dialogAction("Anda tidak bisa pilih block survey lain karena terdapat data survey yang belum terupload!", "Gagal Sync Block!");
                Log.d("Geodatabase", "Ada perubahan lokal yang belum diunggah.");
            } else {
                checkSelected();  // Lanjutkan tindakan untuk sync
                Log.d("Geodatabase", "Tidak ada perubahan lokal, lanjutkan sync.");
            }
        });


        bt_cancel.setOnClickListener(v -> {
            Intent i = new Intent(context, MapReplantingActivity.class);
            i.putExtra("doneSync", "doneSync");
            startActivity(i);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
//            finish();
        });
        search_block.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                blockSyncAdapter.getFilter().filter(s.toString());
                masterBlockAdapter.getFilter().filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }


    public void generateGeodatabase() {
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
//        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE,Prefs.PASS_SDE));
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_lus3.geodatabase");
        File fileBackup = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "backup_LUS3");
        if (!fileBackup.exists()) {
            fileBackup.mkdirs();
        }
        final String path = fileLoc.getAbsolutePath();
        mGeodatabaseSyncTask.loadAsync();
        mGeodatabaseSyncTask.addDoneLoadingListener(() -> {
            final Envelope extentFull = mGeodatabaseSyncTask.getFeatureServiceInfo().getFullExtent();
            Envelope envelope = new Envelope(minX, minY, maxX, maxY, SpatialReferences.getWgs84());

            final ListenableFuture<GenerateGeodatabaseParameters> defaultParameters = mGeodatabaseSyncTask
                    .createDefaultGenerateGeodatabaseParametersAsync(envelope);
            defaultParameters.addDoneListener(() -> {
                try {

                    GenerateLayerOption option = new GenerateLayerOption(0,"");
                    // set parameters and don't include attachments
                    GenerateGeodatabaseParameters parameters = defaultParameters.get();
//                    parameters.getLayerOptions().add(option);
                    parameters.setReturnAttachments(false);
                    // define the local path where the geodatabase will be stored
//                    final String localGeodatabasePath = getCacheDir() + "/file_inventory.geodatabase";
                    Log.e("patchLoc", path);
                    // create and start the job
                    if (fileLoc.exists()) {
                        GlobalHelper.moveFile(fileLoc, fileBackup, DateTime.getStringDate());
                    }
                    final GenerateGeodatabaseJob generateGeodatabaseJob = mGeodatabaseSyncTask
                            .generateGeodatabase(parameters, path);

                    RequestConfiguration requestConfiguration = new RequestConfiguration();
                    requestConfiguration.setConnectionTimeout(300000);
                    requestConfiguration.setSocketTimeout(300000);
                    requestConfiguration.setMaxNumberOfAttempts(5);
                    generateGeodatabaseJob.setRequestConfiguration(requestConfiguration);
//                    String timeout = String.valueOf(generateGeodatabaseJob.getRequestConfiguration().getConnectionTimeout());
//                    Log.e("timeout",timeout);
                    generateGeodatabaseJob.start();
                    createProgressDialog(generateGeodatabaseJob);
                    // get geodatabase when done
                    generateGeodatabaseJob.addJobDoneListener(() -> {
                        if (generateGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                            mGeodatabase = generateGeodatabaseJob.getResult();
                            mGeodatabase.loadAsync();
                            mGeodatabase.addDoneLoadingListener(() -> {
//                                Toast.makeText(this, mGeodatabase.get)
                                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                                    generateAncak();
//                                    featureLayers.clear();
                                    // get only the first table which, contains points
//                                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
////                                        table.loadAsync();
//////                                    GeodatabaseFeatureTable pointsGeodatabaseFeatureTable = mGeodatabase
//////                                            .getGeodatabaseFeatureTables().get(0);
//////                                    pointsGeodatabaseFeatureTable.loadAsync();
////                                        FeatureLayer geodatabaseFeatureLayer = new FeatureLayer(table);
//////                                        featureLayers.add(geodatabaseFeatureLayer);
//                                        Log.e("Layer GDB name", table.getTableName());
////                                        // add geodatabase layer to the map as a feature layer and make it selectable
//////                                        mapView.getMap().getOperationalLayers().add(geodatabaseFeatureLayer);
////
//                                    }
                                    Log.e("SUkses", "Generate BLK3: " );
//                                    generateAncak();
                                } else {
//                                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                                }
                            });
                            // set edit state to ready
//                            mCurrentEditState = MapReplantingActivity.EditState.Ready;
                        } else if (generateGeodatabaseJob.getError() != null) {
                            Log.e("699", "Error generating Lus3 geodatabase: " + generateGeodatabaseJob.getError().getCause());
                            Log.e("699", "Error generating Lus3 geodatabase: " + generateGeodatabaseJob.getError().getMessage());
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getErrorCode());
                            Toast.makeText(this,
                                    "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("703", "Unknown Error generating geodatabase");
                            Toast.makeText(this, "Unknown Error generating geodatabase", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("708", "Error generating geodatabase parameters : " + e.getMessage());
                    Toast.makeText(this, "Error generating geodatabase parameters: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }


    public void getExtendEstate(){
        Call<List<ExtendEstate>> getExtendEstate = apiService.getExtend(GlobalHelper.getEstate().getEstCode());
        getExtendEstate.enqueue(new Callback<List<ExtendEstate>>() {
            @Override
            public void onResponse(Call<List<ExtendEstate>> call, Response<List<ExtendEstate>> response) {
                if(response.isSuccessful()){
                    List<ExtendEstate> list = response.body();
                    Log.e("Extend",String.valueOf(list.size()));
                    mAssetViewModel.inputExtend(list);
                }
            }

            @Override
            public void onFailure(Call<List<ExtendEstate>> call, Throwable t) {

            }
        });
    }


    public void generateAncak() {
//        mGeodatabaseSyncTask = new GeodatabaseSyncTask(Prefs.SERVICE_SDE_INVENTORY);
//        mGeodatabaseSyncTask.setCredential(new UserCredential(Prefs.USER_SDE,Prefs.PASS_SDE));
        File fileLoc = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_ancak.geodatabase");
        File fileBackup = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "backup_anc3");
        if (!fileBackup.exists()) {
            fileBackup.mkdirs();
        }
        final String path = fileLoc.getAbsolutePath();
        mGeodatabaseSyncTask2.loadAsync();
        mGeodatabaseSyncTask2.addDoneLoadingListener(() -> {
            final Envelope extentFull = mGeodatabaseSyncTask.getFeatureServiceInfo().getFullExtent();
            Envelope envelope = new Envelope(minX, minY, maxX, maxY, SpatialReferences.getWgs84());
            final ListenableFuture<GenerateGeodatabaseParameters> defaultParameters = mGeodatabaseSyncTask2
                    .createDefaultGenerateGeodatabaseParametersAsync(envelope);
            defaultParameters.addDoneListener(() -> {
                try {
                    // set parameters and don't include attachments
                    GenerateGeodatabaseParameters parameters = defaultParameters.get();
                    parameters.setReturnAttachments(false);
                    // define the local path where the geodatabase will be stored
//                    final String localGeodatabasePath = getCacheDir() + "/file_inventory.geodatabase";
                    Log.e("patchLoc", path);
                    // create and start the job
                    if (fileLoc.exists()) {
                        GlobalHelper.moveFile(fileLoc, fileBackup, DateTime.getStringDate());
                    }
                    final GenerateGeodatabaseJob generateGeodatabaseJob = mGeodatabaseSyncTask2
                            .generateGeodatabase(parameters, path);
                    RequestConfiguration requestConfiguration = new RequestConfiguration();
                    requestConfiguration.setConnectionTimeout(300000);
                    requestConfiguration.setSocketTimeout(300000);
                    requestConfiguration.setMaxNumberOfAttempts(5);
                    generateGeodatabaseJob.setRequestConfiguration(requestConfiguration);
//                    String timeout = String.valueOf(generateGeodatabaseJob.getRequestConfiguration().getConnectionTimeout());
//                    Log.e("timeout",timeout);
                    generateGeodatabaseJob.start();
                    createProgressDialog(generateGeodatabaseJob);
                    // get geodatabase when done
                    generateGeodatabaseJob.addJobDoneListener(() -> {
                        if (generateGeodatabaseJob.getStatus() == Job.Status.SUCCEEDED) {
                            mGeodatabase = generateGeodatabaseJob.getResult();
                            mGeodatabase.loadAsync();
                            mGeodatabase.addDoneLoadingListener(() -> {
//                                Toast.makeText(this, mGeodatabase.get)
                                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
//                                    for()

//                                    featureLayers.clear();
                                    // get only the first table which, contains points
                                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                                        table.loadAsync();
                                        Log.e("suskses generate name", table.getTableName());
                                        // add geodatabase layer to the map as a feature layer and make it selectable
                                    }
                                    getTPHMaster(mAssetViewModel.getSelectedBlock().getAfd());
                                } else {
                                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                                }
                            });
                            // set edit state to ready
//                            mCurrentEditState = MapReplantingActivity.EditState.Ready;
                        } else if (generateGeodatabaseJob.getError() != null) {
                            Log.e("699", "Error generating ANC 3 geodatabase: " + generateGeodatabaseJob.getError().getCause());
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getMessage());
                            Log.e("699", "Error generating geodatabase: " + generateGeodatabaseJob.getError().getErrorCode());
                            Toast.makeText(this,
                                    "Error generating geodatabase: " + generateGeodatabaseJob.getError().getCause(),
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Log.e("703", "Unknown Error generating geodatabase");
                            Toast.makeText(this, "Unknown Error generating geodatabase", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("708", "Error generating geodatabase parameters : " + e.getMessage());
                    Toast.makeText(this, "Error generating geodatabase parameters: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }


    private void setMasterBlockAdapter(ArrayList<MasterBlock> params){
        masterBlockAdapter = new MasterBlockAdapter(context,params,this);
        mlistView.setAdapter(null);
        mlistView.setAdapter(masterBlockAdapter);
//        Log.e("FillAdapter","true");
        mlistView.setDivider(null);
        masterBlockAdapter.notifyDataSetChanged();
//        loading.dismiss();
    }
    public void getTPHMaster(String afd){
        showLoadingAlert("Sync Data","Mengunduh data block & TPH");
        Call<HMSResponse<List<TPH>>> getTPH = hmsAppsServices.getAllTPHByAfdeling(GlobalHelper.getEstate().getEstCode(),afd);
        getTPH.enqueue(new Callback<HMSResponse<List<TPH>>>() {
            @Override
            public void onResponse(Call<HMSResponse<List<TPH>>> call, Response<HMSResponse<List<TPH>>> response) {
                mAssetViewModel.insertAllTPH(response.body().data);
                Log.e("Data TPH ", String.valueOf(response.body().data.size()));
//                for(TPH tph: response.body().data){
////                    Log.e("Lat Lon ", String.valueOf(tph.getLongitude()+tph.getLatitude()));
//                }
                hideLoadingAlert();
                showToast("Berhasil Checkout block ");
//                finish();
                Intent i = new Intent(context, MapReplantingActivity.class);
                startActivity(i);
//                i.putExtra("doneSync", "doneSync");
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
//                setResult(1998, i);
//                finish();
            }

            @Override
            public void onFailure(Call<HMSResponse<List<TPH>>> call, Throwable t) {
                hideLoadingAlert();
            }
        });

    }

    private void dialogAction(String message, String title) {
        SweetAlertDialog exitAlert = new SweetAlertDialog(this);
        exitAlert.setTitle(title);
        exitAlert.setContentText(message);
        exitAlert.setConfirmButton("OKE", sweetAlertDialog -> {
            exitAlert.dismiss(); // Make sure to dismiss the dialog
        });
        exitAlert.show();
    }

//    public void getMasterBlockSDE(){
//        Call<List<GetMasterBlockSDE>> getBlock = apiService.getMasterBlockSDE(estateCode);
//        getBlock.enqueue(new Callback<List<GetMasterBlockSDE>>() {
//            @Override
//            public void onResponse(Call<List<GetMasterBlockSDE>> call, Response<List<GetMasterBlockSDE>> response) {
//                if(response.isSuccessful() & response.body().size()>1){
//                    List<GetMasterBlockSDE> listBlock = response.body();
//                    List<MasterBlock> listInput = new ArrayList<>();
//                    for (GetMasterBlockSDE m: listBlock){
//                        DecimalFormat df = new DecimalFormat("#.##");
//                        String haGross = df.format(m.ha_Gross) + " Ha";
//                        MasterBlock masterBlock = new MasterBlock(m.getBlock(), m.getDivision(), haGross, 0, 0, 0, "", 1);
//                        listInput.add(masterBlock);
//                    }
//                    mAssetViewModel.insertAllMasterBlock(listInput);
//                    setUI();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<GetMasterBlockSDE>> call, Throwable t) {
//
//            }
//        });
//    }

    public void getMasterBlockSDE2(){
        Call<ApiResponses<List<GetMasterblock>>> getBlock = apiService.getMasterBLockAncak(estateCode);

        getBlock.enqueue(new Callback<ApiResponses<List<GetMasterblock>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<GetMasterblock>>> call, Response<ApiResponses<List<GetMasterblock>>> response) {
//                Log.e("ListBlock","true");
                if(response.body().success){
                    List<MasterBlock> listInput = new ArrayList<>();
                    mAssetViewModel.truncateMasteBlock();
                for (GetMasterblock m: response.body().data){
                        DecimalFormat df = new DecimalFormat("#.##");
                        String haGross = df.format(m.ha_Gross) + " Ha";
                        if (m.isOpen==1){
                            MasterBlock masterBlock = new MasterBlock(m.getBlock(), m.getDivision(), haGross, 0, 0, 0, "", 1, true);
                            listInput.add(masterBlock);
                        }else {
                            MasterBlock masterBlock = new MasterBlock(m.getBlock(), m.getDivision(), haGross, 0, 0, 0, "", 1, false);
                            listInput.add(masterBlock);
                        }

                    }
//                    Log.e("ListBlock",String.valueOf(listInput.size()));
                    mAssetViewModel.insertAllMasterBlock(listInput);
                    hideLoadingAlert();
                    setUI();
                }
//                else {
//                    Log.e("List Block",String.valueOf("true"));
//
//                }

            }

            @Override
            public void onFailure(Call<ApiResponses<List<GetMasterblock>>> call, Throwable throwable) {
                Log.e("ListBlock",throwable.getMessage());
            }
        });

    }


    public void getBlocksOpen(){
        Call<ApiResponses<List<GetMasterblock>>> getBlock = apiService.getMasterBLockAncak(estateCode);

        getBlock.enqueue(new Callback<ApiResponses<List<GetMasterblock>>>() {
            @Override
            public void onResponse(Call<ApiResponses<List<GetMasterblock>>> call, Response<ApiResponses<List<GetMasterblock>>> response) {
//                Log.e("ListBlock","true");
                if(response.body().success){
                    List<String> listInput = new ArrayList<>();
                    for (GetMasterblock m: response.body().data){
//                        DecimalFormat df = new DecimalFormat("#.##");
//                        String haGross = df.format(m.ha_Gross) + " Ha";
                        if(m.isOpen == 1){
                          listInput.add(m.block);
                        }
//                        MasterBlock masterBlock = new MasterBlock(m.getBlock(), m.getDivision(), haGross, 0, 0, 0, "", 1);
//                        listInput.add(masterBlock);
                    }
//                    Log.e("ListBlock",String.valueOf(listInput.size()));
                    mAssetViewModel.setBlockOpen(listInput);
//                    hideLoadingAlert();
//                    setUI();
                }
//                else {
//                    Log.e("List Block",String.valueOf("true"));
//
//                }

            }

            @Override
            public void onFailure(Call<ApiResponses<List<GetMasterblock>>> call, Throwable throwable) {
                Log.e("ListBlock",throwable.getMessage());
            }
        });

    }

    public void setUI(){
        if(mAssetViewModel.getAllMasterBlock().size()>0){
            List<MasterBlock> list = mAssetViewModel.getAllMasterBlock();
            masterBlocks.clear();
            masterBlocks.addAll(list);
//            masterBlocks = mAssetViewModel.getAllMasterBlock();
            setMasterBlockAdapter(masterBlocks);
        }
        else {
            showLoadingAlert("load data","please wait");
            getMasterBlockSDE2();
//            readGeodatabase();
        }
    }
    private void checkSelected(){
//        masterBlocks.stream().filter(MasterBlock::isSelected).forEach(b ->{
////            mAssetViewModel.setFalseBlock();
//        });
        mAssetViewModel.setFalseBlock();
        masterBlocks.stream().filter(MasterBlock::isSelected).forEach(block ->
        {
//            readBlockFeature(block.getBlock());
            Log.e("Afd Terpilih",readBlockFeature(block.getBlock()));
            mAssetViewModel.setSelectedBlock(block.getBlock());
//            getTPHMaster(mAssetViewModel.getSelectedBlock().getAfd());
        });
        generateGeodatabase();
        if(mAssetViewModel.getSelectedBlocks().size()>0){
            postCheckout(mAssetViewModel.getSelectedBlocks());
        }

    }

    public void postCheckout(List<String> blocks){
        for (String block : blocks){
            post(block);
        }
    }
    public void post(String _block){
        PostCheckout _post= new PostCheckout(estateCode,_block,userID,"1");
        Call<ResponseBody> postCheckoutBlock = pdcServices.postCheckoutBlock(_post);
        postCheckoutBlock.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }
    // Create a callback interface to handle the result
    public interface GeodatabaseCheckCallback {
        void onCheckCompleted(boolean hasUpload);
    }

    public void readGeodatabase(GeodatabaseCheckCallback callback) {
        File folder = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_ancak.geodatabase");
        String file = folder.toString();
        Log.e("ReadGDB", file);

        if (folder.exists()) {
            Log.e("File exist", "true");

            if (mGeodatabase != null) {
                mGeodatabase.close();  // Tutup geodatabase jika sudah terbuka
            }

            // Muat ulang geodatabase dari file
            mGeodatabase = new Geodatabase(file);
            mGeodatabase.loadAsync();

            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    boolean hasLocalEdits = false;

                    // Periksa apakah ada perubahan lokal di tabel
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()) {
                        table.loadAsync();
                        if (table.getTableName().equals("ANC3")) {
                            if (table.hasLocalEdits()) {
                                hasLocalEdits = true;
                            }
                        }
                    }

                    // Panggil callback setelah memeriksa tabel
                    callback.onCheckCompleted(hasLocalEdits);

                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                    callback.onCheckCompleted(false);
                }
            });

        } else {
            Log.e("filenorExist", "true");
            callback.onCheckCompleted(false);
        }
    }





    public String readBlockFeature(String _block){
        final String[] afd = {""};
        ArrayList<Feature> _features = new ArrayList<>();
        File folder = new File(Environment.getExternalStorageDirectory() + GlobalHelper.GEODATABASE_LOCATION + "/file_blk3.geodatabase");
        String file = folder.toString();
        Log.e("ReadGDB",file);
        if(folder.exists()){
            Log.e("File exist", "true");
            mGeodatabase = new Geodatabase(file);
            mGeodatabase.loadAsync();
            mGeodatabase.addDoneLoadingListener(() -> {
                if (mGeodatabase.getLoadStatus() == LoadStatus.LOADED) {
                    int counter = 0;
                    // get only the first table which, contains points
                    for (GeodatabaseFeatureTable table : mGeodatabase.getGeodatabaseFeatureTables()){
                        table.loadAsync();
                        Log.e("Layer GDB name", table.getTableName());
                        if (table.getTableName().equals("BLK3")){
                            // Membuat query untuk mendapatkan semua fitur
                            QueryParameters queryParameters = new QueryParameters();
                            queryParameters.setWhereClause("Block='"+_block+"'"); // Mendapatkan semua fitur
                            // Melakukan query
                            final ListenableFuture<FeatureQueryResult> future = table.queryFeaturesAsync(queryParameters);
                            future.addDoneListener(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        FeatureQueryResult result = future.get();

                                        // Iterasi hasil query dan memetakan ke model MasterBlock
                                        for (Feature feature : result) {
                                            _features.add(feature);
                                            afd[0] = (String) feature.getAttributes().get("Division");

                                        }


                                    } catch (Exception e) {
                                        Log.e("Query Error", "Error querying features: " + e.getMessage());
                                    }
                                }
                            });

                        }

                    }

                } else {
                    Log.e("692", "Error loading geodatabase: " + mGeodatabase.getLoadError().getMessage());
                }
            });

        }
        else {
            Log.e("filenorExist","true");
        }
        return afd[0];
    }


    private void createProgressDialog(Job job) {

        ProgressDialog syncProgressDialog = new ProgressDialog(this);
        syncProgressDialog.setTitle("Mengunduh Block SDE");
        syncProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        syncProgressDialog.setCanceledOnTouchOutside(false);
        syncProgressDialog.show();
        job.addProgressChangedListener(() -> syncProgressDialog.setProgress(job.getProgress()));

        job.addJobDoneListener(syncProgressDialog::dismiss);
    }

    public void showIsCheckout(String block){
        showAlert("Gagal Checkout","Block "+block+" Sedang proses survey oleh petugas, pilih block yang belum ter checkout!", SweetAlertDialog.ERROR_TYPE);
    }
    public void showWarning(String block){
        showAlert("Gagal Checkout","Block "+block+" Sedang proses survey oleh petugas, pilih block yang belum ter checkout!", SweetAlertDialog.ERROR_TYPE);
    }

}
