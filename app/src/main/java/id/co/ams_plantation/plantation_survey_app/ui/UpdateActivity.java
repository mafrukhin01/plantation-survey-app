package id.co.ams_plantation.plantation_survey_app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;

/**
 * Created by Mafrukhin on 20/07/2023.
 */
public class UpdateActivity extends BaseActivity {
    @BindView(R.id.btnUpdate)
    AppCompatButton bt_update;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        ButterKnife.bind(this);
        bt_update.setOnClickListener(v -> {
//            Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Intent intent = getPackageManager().getLaunchIntentForPackage(GlobalHelper.GIS_ADMIN_APPS);
            startActivity(intent);
        });
    }


}
