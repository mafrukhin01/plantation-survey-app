package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.ImageViewPagerPinchToZoomAdapter;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.InputFilterMinMaxNumber;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.WidgetHelper;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

/**
 * Created by Mafrukhin on 02/06/2023.
 */
public class ViewAttributePoint {
    private static Context context;
    TextInputLayout spin_title;
    AutoCompleteTextView spin_value;
    public ImageView ivPhoto;
    String urlString;
    LayoutInflater inflater;
    TextView header, qc_stat;
    ImageView iv_marker,iv_compas_lock, imagesview, iv_back;
    AlertDialog alertDialogCompas,ad;
    Feature mData;
    ScrollView sv_object;
    BottomSheetDialog mBottomSheetDialog;
    SliderLayout sliderView;
    MapInventoryActivity activity;
    public SliderLayout sliderLayout;
    public boolean iscompas = false;
    private static LinearLayout viewGroup, bottomSheet, llManual, llAction;
    private action mAction;
    RelativeLayout takePicture;
    Button b_save, b_cancel, btnUpdateLocation;
    enum action{
        Add,
        show,
        edit,
    }

    public ViewAttributePoint(Context context, int _action){

        this.activity = (MapInventoryActivity) context;
            bottomSheet = (LinearLayout) activity.findViewById(R.id.bottomSheetView);
//            spin_title = (TextInputLayout)  activity.findViewById(R.id.spin_title);
//            spin_value = (AutoCompleteTextView) activity.findViewById(R.id.spin_value);
            viewGroup = (LinearLayout) activity.findViewById(R.id.view_group);
            b_cancel = (Button) activity.findViewById(R.id.bt_cancel);
            b_save = (Button) activity.findViewById(R.id.bt_save);
            header = (TextView) activity.findViewById(R.id.tv_header);
            qc_stat = (TextView) activity.findViewById(R.id.tv_qc);
            iv_marker =(ImageView) activity.findViewById(R.id.iv_marker);
            sliderLayout =(SliderLayout) activity.findViewById(R.id.sliderLayout);
            llManual = (LinearLayout) activity.findViewById(R.id.ll_manual);
            sv_object =(ScrollView) activity.findViewById(R.id.scroll_object);
            btnUpdateLocation = (Button) activity.findViewById(R.id.btnUpdateLocation);
            ivPhoto = (ImageView) activity.findViewById(R.id.ivPhoto);
            imagesview = (ImageView) activity.findViewById(R.id.imagesview);
            iv_back = (ImageView) activity.findViewById(R.id.iv_back);
            llAction = (LinearLayout) activity.findViewById(R.id.ll_action_view) ;


        this.mData = activity.selectedFeature;
        this.inflater = LayoutInflater.from(activity);

        header.setText(GlobalHelper.getNameObject(activity.selectedFeature.getFeatureTable().getTableName()));
        try {
            if(mData.getAttributes().get("survey_date")=="" && String.valueOf(mData.getAttributes().get("QC")).equals("3")){
                qc_stat.setText("Resurvey");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        b_cancel.setOnClickListener(v -> {
            activity.ALselectedImage.clear();
            hideView();
        });
        b_save.setOnClickListener(v -> {
//            activity.ALselectedImage > 0?
//            activity.getGlobalID(mData.getFeatureTable());

            String images = "Uploads/Images/"+ Prefs.getString(Prefs.SESSION_ID)+"/";
            StringBuilder sb = new StringBuilder();
            if(activity.ALselectedImage.size()> 0){
                for(int i = activity.ALselectedImage.size() -1 ; i >= 0 ; i --){
                    File img = activity.ALselectedImage.get(i);
                    activity.saveImage(String.valueOf(mData.getAttributes().get("GlobalID")));
                    sb.append(images).append(img.getName()).append(";");
//                    mData.getAttributes().put("image",images+sb.toString());
                }

//                mData.getAttributes().put("image","/Uploads/Images/"+ Prefs.getString(Prefs.SESSION_ID)+"/"+activity.ALselectedImage.get(0).getName()+";");
                mData.getAttributes().put("image",sb.toString());
                activity.ALselectedImage.clear();
            }
            else {
                activity.showAlert("Warning","Silahkan Ambil Gambar", SweetAlertDialog.WARNING_TYPE);
                return;
            }
            if (mAction == action.Add){
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childView = viewGroup.getChildAt(i);
                    LinearLayout ll = (LinearLayout) childView;
                    for (int l = 0; l < ll.getChildCount(); l++) {
                        View child = ll.getChildAt(l);
                        if (child instanceof TextInputLayout){
                            TextInputLayout textInputLayout = (TextInputLayout) child;
                            if(textInputLayout.getEditText() instanceof TextInputEditText) {
                                TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = editText.getText().toString();
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
//                                    if (mData.getFeatureTable().getField(key).isEditable()&& key.equals("block") || key.equals("status_object")
//                                            || key.equals("flow_dir")|| key.equals("condition")|| key.equals("remark")
//                                            || key.equals("length")||key.equals("width")||key.equals("type")||key.equals("month")||key.equals("duration")||key.equals("year")
//                                            ||key.equals("tot_door")||key.equals("hp")||key.equals("brand")||key.equals("model")||key.equals("diameter")||key.equals("throughput")||key.equals("spillway_level")) {
                                        if(fieldType == Field.Type.TEXT){
                                            mData.getAttributes().put(key, value);
                                        }
                                        if(fieldType == Field.Type.INTEGER){
                                            mData.getAttributes().put(key, Integer.valueOf(value));
                                        }
                                        if(fieldType == Field.Type.SHORT ){
                                            mData.getAttributes().put(key, Short.valueOf(value));
                                        }
                                        if(fieldType == Field.Type.DOUBLE){
                                            mData.getAttributes().put(key, Double.valueOf(value));
                                        }
//                                    }
//                                    mData.getAttributes().put(key,value);
                                    Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                }
                            }
                            else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                                MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = String.valueOf(editText.getText());
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                    List<CodedValue> values =  codedValue.getCodedValues();
                                    for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                        if(fieldValue.getName().equals(value)){
                                            Log.e("codedName equals ",  fieldValue.getName());
                                            Log.e("coded equals ", fieldValue.getCode().toString());
                                            if(fieldType == Field.Type.TEXT){
                                                mData.getAttributes().put(key, fieldValue.getCode().toString());
                                            }
                                            if(fieldType == Field.Type.INTEGER){
                                                mData.getAttributes().put(key, Integer.valueOf(fieldValue.getCode().toString()));
                                            }
                                            if(fieldType == Field.Type.SHORT ){
                                                mData.getAttributes().put(key, Short.valueOf(fieldValue.getCode().toString()));
                                            }
//                                            mData.getAttributes().put(key,fieldValue.getCode().toString());
                                            Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                activity.saveNewFeature(mData);
//                hideView();
            } else if (mAction == action.show) {
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childView = viewGroup.getChildAt(i);
                    LinearLayout ll = (LinearLayout) childView;
                    for (int l = 0; l < ll.getChildCount(); l++) {
                        View child = ll.getChildAt(l);
                        if (child instanceof TextInputLayout){
                            TextInputLayout textInputLayout = (TextInputLayout) child;
                            if(textInputLayout.getEditText() instanceof TextInputEditText) {
                                TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = editText.getText().toString();
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    if (mData.getFeatureTable().getField(key).isEditable()&& key.equals("block") || key.equals("status_object")
                                            || key.equals("flow_dir")|| key.equals("condition")|| key.equals("remark")
                                            || key.equals("length")||key.equals("width")||key.equals("type")||key.equals("month")||key.equals("duration")||key.equals("year")
                                            ||key.equals("tot_door")||key.equals("hp")||key.equals("brand")||key.equals("model")||key.equals("diameter")||key.equals("throughput")||key.equals("spillway_level")) {
                                        if(fieldType == Field.Type.TEXT){
                                            mData.getAttributes().put(key, value);
                                        }
                                        if(fieldType == Field.Type.INTEGER){
                                           mData.getAttributes().put(key, Integer.valueOf(value));
                                        }
                                        if(fieldType == Field.Type.SHORT ){
                                            mData.getAttributes().put(key, Short.valueOf(value));
                                        }
                                        if(fieldType == Field.Type.DOUBLE){
                                            mData.getAttributes().put(key, Double.valueOf(value));
                                        }
                                    }
//                                    mData.getAttributes().put(key,value);
                                    Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                }
                            }
                            else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                                MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = String.valueOf(editText.getText());
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                    List<CodedValue> values =  codedValue.getCodedValues();
                                    for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                        if(fieldValue.getName().equals(value)){
                                            Log.e("codedName equals ",  fieldValue.getName());
                                            Log.e("coded equals ", fieldValue.getCode().toString());
                                            if(fieldType == Field.Type.TEXT){
                                                mData.getAttributes().put(key, fieldValue.getCode().toString());
                                            }
                                            if(fieldType == Field.Type.INTEGER){
                                                mData.getAttributes().put(key, Integer.valueOf(fieldValue.getCode().toString()));
                                            }
                                            if(fieldType == Field.Type.SHORT ){
                                                mData.getAttributes().put(key, Short.valueOf(fieldValue.getCode().toString()));
                                            }
//                                            mData.getAttributes().put(key,fieldValue.getCode().toString());
                                            Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                activity.moveFeatureTo(mData);
//                hideView();
            }
        });

        iv_marker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
                activity.fab_gps.callOnClick();
                showManual();
//                activity.updateGeometry(mData);
//                hideView();
            }
        });

        btnUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.updateGeometry(mData);

            }
        });
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.OpenCamera();
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagesZoom();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showManual();
            }
        });

    }

    public void showManual(){

        if (sv_object.getVisibility()==View.VISIBLE){
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = true;
            sv_object.setVisibility(View.GONE);
            llManual.setVisibility(View.VISIBLE);
            activity.sniper.setVisibility(View.VISIBLE);
            llAction.setVisibility(View.GONE);

        }
        else {
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = false;
            sv_object.setVisibility(View.VISIBLE);
            llManual.setVisibility(View.GONE);
            activity.sniper.setVisibility(View.GONE);
            llAction.setVisibility(View.VISIBLE);
            }
    }

    public void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(activity.ALselectedImage);
        activity.ALselectedImage.clear();
        activity.ALselectedImage.addAll(sf);
        if(activity.ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(activity.ALselectedImage.size() != 0){
//            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : activity.ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }else if(file.toString().startsWith("https://")){

                }else if(file.toString().startsWith("https:/")){
                    surl =surl.replace("https:/","https://");
                }
                textSliderView
                        .image(surl)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
//        imagecacnel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
//                try {
//                    if (sliderLayout.getCurrentPosition() == 0 && activity.ALselectedImage.size() == 1) {
//                        activity.ALselectedImage.clear();
//                        sliderLayout.removeAllSliders();
//                        sliderLayout.setVisibility(View.GONE);
//                        imagecacnel.setVisibility(View.GONE);
//                        imagesview.setVisibility(View.GONE);
//                    } else {
//                        deleteditemALselectedImage(sliderLayout.getCurrentPosition());
//                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
//                    }
//                }catch (Exception e){
//                    Log.e("sliderLayout", e.toString());
//                }
//            }
//        });

    }

    public void showImagesZoom(){
        View view = LayoutInflater.from(activity).inflate(R.layout.imageszoom, null);
        ViewPager pinchtozoom_pager = (ViewPager) view.findViewById(R.id.pinchtozoom_pager);
        ImageView iv_iz_close = (ImageView) view.findViewById(R.id.iv_iz_close);
        ImageViewPagerPinchToZoomAdapter imageViewPagerPinchToZoomAdapter = new ImageViewPagerPinchToZoomAdapter(activity.ALselectedImage,urlString);
        pinchtozoom_pager.setOffscreenPageLimit(3);
        pinchtozoom_pager.setAdapter(imageViewPagerPinchToZoomAdapter);
        iv_iz_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ad.isShowing()){
                    ad.dismiss();}
            }
        });
        ad = WidgetHelper.showListReference(ad,view,activity);
    }

    public void showView(int _action){
        viewGroup.removeAllViews();
        sliderLayout.removeAllSliders();
        if (_action ==0 ){
            mAction = action.Add;
        } else if (_action==1) {
            mAction = action.show;
        } else {
            mAction = action.edit;
        }
        if(mAction==action.show){
            Log.e("tagShow","true");
            if(mData.getAttributes().get("image")!=""&&mData.getAttributes().get("image")!=null){
                String urlFeature = (String) mData.getAttributes().get("image");
                String[] strings = urlFeature.split(";");
                if (strings.length>0){
                    TextSliderView textSliderView = new TextSliderView(activity);
                    String imgUrl = "https://app.gis-div.com/PSAService/"+strings[0];
                    urlString = imgUrl;
                    textSliderView
                            .image(imgUrl)
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    sliderLayout.addSlider(textSliderView);
                    sliderLayout.stopAutoCycle();
                    sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
                    sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    sliderLayout.setCustomAnimation(new DescriptionAnimation());
                    imagesview.setVisibility(View.VISIBLE);
//                    Glide.with(activity).load(imgUrl).into(sliderLayout);
//                    sliderLayout.setMinimumHeight(100);
//                    sliderLayout.setMinimumWidth(70);
                }
            }
        }
        else {
//            sliderLayout.setBackground(R.drawable.ic_add_photo);

        }
//        activity.sliderLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                activity.openCam();
//            }
//        });


        setContent(_action);
//        if(activity.fabAdd.getVisibility()==View.VISIBLE){
            activity.fabAdd.setVisibility(View.GONE);
            activity.bmb.setVisibility(View.GONE);
            activity.fab_gps.setVisibility(View.GONE);
            activity.fab_layer.setVisibility(View.GONE);
            activity.base_info.setVisibility(View.GONE);
            activity.fab_extend.setVisibility(View.GONE);
            activity.footer.setVisibility(View.GONE);
            activity.fab_versioning.setVisibility(View.GONE);
//        }
//        if(activity.footer.getVisibility()==View.VISIBLE){
//            activity.footer.setVisibility(View.GONE);
//        }

//        startAnimation();
    }

    public void hideView(){
        viewGroup.removeAllViews();
        bottomSheet.setVisibility(View.GONE);
        if(activity.fabAdd.getVisibility()==View.GONE){
            activity.sniper.setVisibility(View.GONE);
            activity.fabAdd.setVisibility(View.VISIBLE);
            activity.bmb.setVisibility(View.VISIBLE);
            activity.fab_gps.setVisibility(View.VISIBLE);
            activity.fab_layer.setVisibility(View.VISIBLE);
            activity.base_info.setVisibility(View.VISIBLE);
            activity.fab_extend.setVisibility(View.VISIBLE);
            activity.fab_versioning.setVisibility(View.VISIBLE);
        }
        if(activity.footer.getVisibility()==View.GONE){
            activity.footer.setVisibility(View.VISIBLE);
        }
    }

    public void setContent(int _action){
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        Map<String, Object> attr = mData.getAttributes();
//        Map<String, Object> sortedMap = sortByOrder(mData.getAttributes(),GlobalHelper.order());
        Set<String> keys = attr.keySet();
//        Collections.sort(keys,);
//        Map<String, Object> sortedMap = sortByOrder(map, order);
        for (String key : keys) {
            Log.e("key",key );
            Field.Type typeObject = mData.getFeatureTable().getField(key).getFieldType();
            List<String> listAdapter = new ArrayList<>();
            if (mData.getFeatureTable().getField(key).isEditable()&&!key.equals("survey_ID")&&!key.equals("WTR_ID")&&!key.equals("BUN_ID")&&!key.equals("BRD_ID")&&
                    !key.equals("ZIP_ID")&&!key.equals("PUM_ID")&&!key.equals("CVT_ID")&&!key.equals("DAM_ID")&&!key.equals("image")&& !key.equals("survey_date")&&
                    !key.equals("WMA_code")&&!key.equals("source")&&!key.equals("event_type")&&!key.equals("status")&&!key.equals("device_loc")&&!key.equals("built")
                    &&!key.equals("last_edited_user")&&!key.equals("last_edited_date")&&!key.equals("surveyor")&&!key.equals("created_user")&&!key.equals("created_date")
                    &&!key.equals("status_asset")&&!key.equals("year_budget")&&!key.equals("qc_user")){
                Object value = attr.get(key);
                if (value == null){
                    value = "";
                }
//                Log.e("key",key + " | FieldType " + typeObject+ "\n");
                if(mData.getFeatureTable().getField(key).getDomain() != null){
                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                    List<CodedValue> values =  codedValue.getCodedValues();
                    for (CodedValue fieldValue : values) {
                        listAdapter.add(fieldValue.getName());
                    }
                    if (key.equals("QC")){
                        viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key), value, listAdapter, values, linearLayout, false));
                    }
                    else {
                        viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key), value, listAdapter, values, linearLayout, true));

                    }
                }
                else {
//                    if (){
                    if(key.equals("remark")){
                        viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key),value,linearLayout,2,true));
                    }
                    else {
                        if(typeObject== Field.Type.TEXT){
                            if (key.equals("GroupCompanyName")||key.equals("CompanyCode")||key.equals("ESTNR")||key.equals("QC_remarks")){
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
                            }
                            else {
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,true));
                            }
                        }
                        else if (typeObject == Field.Type.DOUBLE){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true, (float) 0,100, true));
                        }
                        else if (typeObject == Field.Type.INTEGER ||typeObject == Field.Type.SHORT){
                            if (key.equals("flow_dir")){
                                viewGroup.addView(viewEditCompass(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true));
                            }else if (key.equals("month")||key.equals("duration")){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,24,false));
                            }
                            else if (key.equals("year")){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,2024,false));
                            }
                            else {
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,100,false));
                            }
                        }
                        else if (typeObject == Field.Type.DATE) {
                            viewGroup.addView(viewEditDate(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                        }
                    }
//                    }
                }
            }
        }
       bottomSheet.setVisibility(View.VISIBLE);
    }

    public void startAnimation(){
        bottomSheet.setVisibility(View.VISIBLE);
        int parentHeight = ((View) bottomSheet.getParent()).getHeight();
        bottomSheet.setTranslationY(parentHeight);
        TranslateAnimation animation = new TranslateAnimation(0, 0, parentHeight, 0);        animation.setDuration(500);
        bottomSheet.startAnimation(animation);

    }
    private View viewEditText(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }

    private View viewEditNumber(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable, float min, float max, boolean isDouble){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        if (isDouble){
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
        else {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        InputFilter[] filters = {new InputFilterMinMaxNumber(min, max)};
//        if (min > 0 ){
            textInputEditText.setFilters(filters);

        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString().trim();
                if(isDouble){
                    if (!input.isEmpty()) {
                    float value = Float.parseFloat(s.toString());
                    if (value < 0.1) {
                       textInputEditText.setError("Minimum value is 0.1");
                    } else {
                        // Value is valid, proceed with further processing
                    }}
                }
            }
        });
        return customView;
    }

    private View viewAutoComplete(String title, Object value, List<String> listSpin, List<CodedValue> codedValues, LinearLayout linearLayout, boolean editable){
        Log.e("title", title);
        View customView = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        int selected = 0;

        for(int loop = 0; loop<codedValues.size(); loop++){
            Log.e(codedValues.get(loop).getCode().toString(), value.toString());
            if(value.toString().equals(codedValues.get(loop).getCode().toString())){
                selected = loop;
                Log.e("selected", listSpin.get(selected));
            }
        }
//        View view = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.spin_title);
        textInputLayout.setHint(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,listSpin);
        MaterialAutoCompleteTextView textInputEditText = customView.findViewById(R.id.spin_value);
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputEditText.setEnabled(false);
        }
        textInputEditText.setAdapter(adapter);
        if (value != null){
            if (title.equals("status_object")&&mAction==action.Add){
                textInputEditText.setText("Pilih Status",false);
//                textInputEditText.setText(adapter.getItem(selected),false);
            }
            else {
                textInputEditText.setText(adapter.getItem(selected),false);
            }

        }
        return customView;
    }

    private View viewEditDate(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_NULL);
        textInputEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            // Create DatePickerDialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                    (view1, year1, month1, dayOfMonth1) -> {
                        // Update TextInputEditText with selected date
                        String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                        textInputEditText.setText(selectedDate);
                    }, year, month, dayOfMonth);
            // Show DatePickerDialog
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
//            textInputEditText.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME);
//            textInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
//                Calendar calendar = Calendar.getInstance();
//                int year = calendar.get(Calendar.YEAR);
//                int month = calendar.get(Calendar.MONTH);
//                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//                // Create DatePickerDialog
//                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
//                        (view1, year1, month1, dayOfMonth1) -> {
//                            // Update TextInputEditText with selected date
//                            String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
//                            textInputEditText.setText(selectedDate);
//                        }, year, month, dayOfMonth);
//                // Show DatePickerDialog
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            });
        return customView;
    }
    private View viewEditCompass(String title, Object value, LinearLayout linearLayout, int maxLine,boolean editable){
        View v = LayoutInflater.from(activity).inflate(R.layout.compas, null);
        View customView = inflater.inflate(R.layout.item_edit_compass, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        final ImageView iv_compas_lock = customView.findViewById(R.id.iv_compas_lock);
//        iv_compas_lock = (ImageView) activity.findViewById(R.id.iv_compas_lock);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        activity.ch.tv_degere2 = textInputEditText;
        InputFilter[] filters = {new InputFilterMinMaxNumber(0, 360)};
        activity.ch.tv_degere2.setFilters(filters);
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }
        setup_iv_compas_lock(iv_compas_lock,iscompas);
        iv_compas_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("trueCompas","true");
                if(iscompas){
                    activity.onCompassStopped();
                    iscompas = false;
                }else{
                    showCompas(iv_compas_lock);
                    activity.onCompassStarted();
                    iscompas = true;
                }
                setup_iv_compas_lock(iv_compas_lock,iscompas);
//                showCompas(iv_compas_lock);
            }
        });

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if(iscompas){
//                    activity.onCompassStopped();
//                    iscompas = false;
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }
    public void setup_iv_compas_lock(ImageView iv_compas_lock, Boolean status){
        if(status){
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_stop).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }else{
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_compass).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }
    }


    public void showCompas(final ImageView iv_compas_lock){
        View v = LayoutInflater.from(activity).inflate(R.layout.compas, null);
        activity.layoutCompass = (RelativeLayout) v.findViewById(R.id.layoutCompass);
        activity.ivCompass = (ImageView) v.findViewById(R.id.imageViewCompass);
        activity.pulsatorLayout = (PulsatorLayout) v.findViewById(R.id.pulsator);
        activity.ch.arrowView = (ImageView) v.findViewById(R.id.imageViewHeading);
//        activity.ch.tvArahCompass = (TextView) v.findViewById(R.id.tvArahCompass);
        activity.ch.tvArahCompassCardinal = (TextView) v.findViewById(R.id.tvArahCompassCardinal);
        v.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        activity.layoutCompass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onCompassStopped();
                iscompas = false;
                setup_iv_compas_lock(iv_compas_lock,iscompas);
                alertDialogCompas.dismiss();
            }
        });

        alertDialogCompas = WidgetHelper.showFormDialogTransparant(alertDialogCompas,v,activity);
    }

    public static Map<String, Object> sortByOrder(Map<String, Object> map, List<String> order) {
        Map<String, Object> sortedMap = new LinkedHashMap<>();
        for (String key : order) {
            if (map.containsKey(key)) {
                sortedMap.put(key, map.get(key));
            }
        }
        return sortedMap;
    }
}
