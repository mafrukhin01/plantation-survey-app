package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.InputFilterMinMaxNumber;

public class ViewCreateFeature {
    private static Context context;
    private Feature baseFeature;
    TextInputLayout spin_title;
    AutoCompleteTextView spin_value;
    public ImageView ivPhoto;
    String urlString;
    LayoutInflater inflater;
    TextView header, qc_stat;
    ImageView iv_marker,iv_compas_lock, imagesview, iv_back;
    AlertDialog alertDialogCompas,ad;
    Feature mData;
    ScrollView sv_object;
    BottomSheetDialog mBottomSheetDialog;
    private boolean isBlock;
    SliderLayout sliderView;
    MapReplantingActivity activity;

    public SliderLayout sliderLayout;
    public boolean iscompas = false;
    private static LinearLayout viewGroup, bottomSheet, llManual, llAction;
    private ViewAttributePoint.action mAction;
    RelativeLayout takePicture;
    Button b_save, b_cancel, btnUpdateLocation;
    enum action{
        Add,
        show,
        edit,
    }

    public ViewCreateFeature(Context context, Feature _action){
        this.activity = (MapReplantingActivity) context;
        bottomSheet = (LinearLayout) activity.findViewById(R.id.bottomSheetView);
//            spin_title = (TextInputLayout)  activity.findViewById(R.id.spin_title);
//            spin_value = (AutoCompleteTextView) activity.findViewById(R.id.spin_value);
        viewGroup = (LinearLayout) activity.findViewById(R.id.view_group);
        b_cancel = (Button) activity.findViewById(R.id.bt_cancel);
        b_save = (Button) activity.findViewById(R.id.bt_save);
        header = (TextView) activity.findViewById(R.id.tv_headerAncak);
        qc_stat = (TextView) activity.findViewById(R.id.tv_qc);
        iv_marker =(ImageView) activity.findViewById(R.id.iv_marker);
        sliderLayout =(SliderLayout) activity.findViewById(R.id.sliderLayout);
        llManual = (LinearLayout) activity.findViewById(R.id.ll_manual);
        sv_object =(ScrollView) activity.findViewById(R.id.scroll_object);
        btnUpdateLocation = (Button) activity.findViewById(R.id.btnUpdateLocation);
        ivPhoto = (ImageView) activity.findViewById(R.id.ivPhoto);
        imagesview = (ImageView) activity.findViewById(R.id.imagesview);
        iv_back = (ImageView) activity.findViewById(R.id.iv_back);
        llAction = (LinearLayout) activity.findViewById(R.id.ll_action_view) ;
        this.mData = _action;
        this.inflater = LayoutInflater.from(activity);
        header.setText("Buat Ancak");
        b_cancel.setOnClickListener(v -> {
            hideView();
        });
        b_save.setOnClickListener(v -> {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childView = viewGroup.getChildAt(i);
                LinearLayout ll = (LinearLayout) childView;
                for (int l = 0; l < ll.getChildCount(); l++) {
                    View child = ll.getChildAt(l);
                    if (child instanceof TextInputLayout){
                        TextInputLayout textInputLayout = (TextInputLayout) child;
                        if(textInputLayout.getEditText() instanceof TextInputEditText) {
                            TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
                            if (editText != null&& !editText.getText().toString().equals("")) {
                                String value = editText.getText().toString();
                                String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                if(fieldType == Field.Type.TEXT ){
                                    mData.getAttributes().put(key, value);
                                }
                                if(fieldType == Field.Type.INTEGER){
                                    mData.getAttributes().put(key, Integer.valueOf(value));
                                }
                                if(fieldType == Field.Type.SHORT ){
                                    mData.getAttributes().put(key, Short.valueOf(value));
                                }
                                if(fieldType == Field.Type.DOUBLE){
                                    mData.getAttributes().put(key, Double.valueOf(value));
                                }
//                                    }
//                                    mData.getAttributes().put(key,value);
                                Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                            }
                        }
                        else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                            MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                            if (editText != null&& !editText.getText().toString().equals("")) {
                                String value = String.valueOf(editText.getText());
                                String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                List<CodedValue> values =  codedValue.getCodedValues();
                                for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                    if(fieldValue.getName().equals(value)){
                                        Log.e("codedName equals ",  fieldValue.getName());
                                        Log.e("coded equals ", fieldValue.getCode().toString());
                                        if(fieldType == Field.Type.TEXT){
                                            mData.getAttributes().put(key, fieldValue.getCode().toString());
                                        }
                                        if(fieldType == Field.Type.INTEGER){
                                            mData.getAttributes().put(key, Integer.valueOf(fieldValue.getCode().toString()));
                                        }
                                        if(fieldType == Field.Type.SHORT ){
                                            mData.getAttributes().put(key, Short.valueOf(fieldValue.getCode().toString()));
                                        }
//                                            mData.getAttributes().put(key,fieldValue.getCode().toString());
                                        Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                    }
                                }
                            }
                        }
                    }
                }
            }
//            String id = activity.getIdTPH(String.valueOf(mData.getAttributes().get("Block")),getTextInputLayout("NoAncak").getEditText().getText().toString());
//            mData.getAttributes().put("tphCode", id);
            activity.saveFeature(mData);
            hideView();

        });

        iv_marker.setOnClickListener(v -> {
            showManual();
        });

        btnUpdateLocation.setOnClickListener(v -> {
//                activity.updateGeometry(mData);

        });
        ivPhoto.setOnClickListener(view -> {
//                activity.OpenCamera();
        });
        imagesview.setOnClickListener(v -> {

//                showImagesZoom();
        });
        iv_back.setOnClickListener(v -> showManual());

    }

    public void showManual(){

        if (sv_object.getVisibility()==View.VISIBLE){
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = true;
            sv_object.setVisibility(View.GONE);
            llManual.setVisibility(View.VISIBLE);
            activity.sniper.setVisibility(View.VISIBLE);
            llAction.setVisibility(View.GONE);

        }
        else {
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = false;
            sv_object.setVisibility(View.VISIBLE);
            llManual.setVisibility(View.GONE);
            activity.sniper.setVisibility(View.GONE);
            llAction.setVisibility(View.VISIBLE);
        }
    }


    public void showView(int _action){
        viewGroup.removeAllViews();
        sliderLayout.removeAllSliders();
        if (_action ==0 ){
            mAction = ViewAttributePoint.action.Add;
        } else if (_action==1) {
            mAction = ViewAttributePoint.action.show;
        } else {
            mAction = ViewAttributePoint.action.edit;
        }
        if(mAction== ViewAttributePoint.action.show){
            Log.e("tagShow","true");
            if(mData.getAttributes().get("image")!=""&&mData.getAttributes().get("image")!=null){
                String urlFeature = (String) mData.getAttributes().get("image");
                String[] strings = urlFeature.split(";");
                if (strings.length>0){
                    TextSliderView textSliderView = new TextSliderView(activity);
                    String imgUrl = "https://app.gis-div.com/PSAService/"+strings[0];
                    urlString = imgUrl;
                    textSliderView
                            .image(imgUrl)
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    sliderLayout.addSlider(textSliderView);
                    sliderLayout.stopAutoCycle();
                    sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
                    sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    sliderLayout.setCustomAnimation(new DescriptionAnimation());
                    imagesview.setVisibility(View.VISIBLE);

                }
            }
        }
        else {

        }

        setContent(_action);
        activity.fabAdd.setVisibility(View.GONE);
        activity.bmb.setVisibility(View.GONE);
        activity.fab_map.setVisibility(View.GONE);
        activity.fab_layer.setVisibility(View.GONE);
        activity.base_info.setVisibility(View.GONE);

        activity.footer.setVisibility(View.GONE);
//        activity.fab_versioning.setVisibility(View.GONE);
    }

    public void hideView(){
        viewGroup.removeAllViews();
        bottomSheet.setVisibility(View.GONE);
        if(activity.fabAdd.getVisibility()==View.GONE){
            activity.sniper.setVisibility(View.GONE);
            activity.fabAdd.setVisibility(View.VISIBLE);
            activity.bmb.setVisibility(View.VISIBLE);
            activity.fab_map.setVisibility(View.VISIBLE);
//            activity.fab_gps.setVisibility(View.VISIBLE);
            activity.fab_layer.setVisibility(View.VISIBLE);
            activity.base_info.setVisibility(View.VISIBLE);
//            activity.fab_extend.setVisibility(View.VISIBLE);
//            activity.fab_versioning.setVisibility(View.VISIBLE);
        }
        if(activity.footer.getVisibility()==View.GONE){
            activity.footer.setVisibility(View.VISIBLE);
        }
    }

    public void setContent(int _action){
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        List<String> setAttr = new ArrayList<>();
        setAttr = GlobalHelper.attributesAncak();
        Map<String, Object> attr = mData.getAttributes();
//        Map<String, Object> sortedMap = sortByOrder(mData.getAttributes(),GlobalHelper.order());
        Set<String> keys = attr.keySet();
//        Collections.sort(keys,);
//        Map<String, Object> sortedMap = sortByOrder(map, order);
        for (String key : keys) {
            if (mData.getFeatureTable().getField(key).isEditable()) {

            }
        }
        for (String _attr : setAttr){
            for (String key : keys) {
                Log.e("debug key true",key );
                Field.Type typeObject = mData.getFeatureTable().getField(key).getFieldType();
                List<String> listAdapter = new ArrayList<>();
                if (_attr.equals(key)){
                    Object value = attr.get(key);
                    if (value == null){
                        value = "";
                    }
                    if(mData.getFeatureTable().getField(key).getDomain() != null){
                        CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                        List<CodedValue> values =  codedValue.getCodedValues();
                        for (CodedValue fieldValue : values) {
                            listAdapter.add(fieldValue.getName());
                        }
                        viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key), value, listAdapter, values, linearLayout, true));

                    }
                    else {
//                    if (){
                        if(typeObject== Field.Type.TEXT){
                            if (key.equals("GroupCompanyName")||key.equals("CompanyCode")||key.equals("EstCode")||key.equals("Block")||key.equals("Division")||key.equals("LuasHa")){
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
                            }else if(key.equals("QC_remarks")&&value!=null){
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
                            } else if (key.equals("tphCode")) {
                                List<String> list= activity.getUserALl();
                                viewGroup.addView(viewEditSurveyor(GlobalHelper.getNameAttribute(key),"0",list,linearLayout,true));

                            } else {
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,true));
                            }
                        }
                        else if (typeObject == Field.Type.DOUBLE){
                            viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true, (float) 0,100, true));
                        }
                        else if (typeObject == Field.Type.INTEGER ||typeObject == Field.Type.SHORT){
                            if (key.equals("month")||key.equals("duration")){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,24,false));
                            }
                            else if (key.equals("year")||key.equals("year_budget")){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,2024,false));
                            }
                            else {
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,200,false));
                            }
                        }
                        else if (typeObject == Field.Type.DATE) {
                            viewGroup.addView(viewEditDate(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                        }

//                    }
                    }
                }
            }
        }
        bottomSheet.setVisibility(View.VISIBLE);
    }

    public void startAnimation(){
        bottomSheet.setVisibility(View.VISIBLE);
        int parentHeight = ((View) bottomSheet.getParent()).getHeight();
        bottomSheet.setTranslationY(parentHeight);
        TranslateAnimation animation = new TranslateAnimation(0, 0, parentHeight, 0);        animation.setDuration(500);
        bottomSheet.startAnimation(animation);

    }
    private View viewEditText(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }

    private View viewEditNumber(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable, float min, float max, boolean isDouble){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        if (isDouble){
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
        else {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        InputFilter[] filters = {new InputFilterMinMaxNumber(min, max)};
//        if (min > 0 ){
        textInputEditText.setFilters(filters);

        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString().trim();
                if(isDouble){
                    if (!input.isEmpty()) {
                        float value = Float.parseFloat(s.toString());
                        if (value < 0.1) {
                            textInputEditText.setError("Minimum value is 0.1");
                        } else {
                            // Value is valid, proceed with further processing
                        }}
                }
            }
        });
        return customView;
    }

    private View viewAutoComplete(String title, Object value, List<String> listSpin, List<CodedValue> codedValues, LinearLayout linearLayout, boolean editable){
        Log.e("title", title);
        View customView = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        int selected = 0;

        for(int loop = 0; loop<codedValues.size(); loop++){
            Log.e(codedValues.get(loop).getCode().toString(), value.toString());
            if(value.toString().equals(codedValues.get(loop).getCode().toString())){
                selected = loop;
                Log.e("selected", listSpin.get(selected));
            }
        }
//        View view = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.spin_title);
        textInputLayout.setHint(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,listSpin);
        MaterialAutoCompleteTextView textInputEditText = customView.findViewById(R.id.spin_value);
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputEditText.setEnabled(false);
        }
        textInputEditText.setAdapter(adapter);
        if (value != null){
            if (title.equals("status_object")&&mAction== ViewAttributePoint.action.Add){
                textInputEditText.setText("Pilih Status",false);
//                textInputEditText.setText(adapter.getItem(selected),false);
            }
            else {
                textInputEditText.setText(adapter.getItem(selected),false);
            }

        }
        return customView;
    }

    private View viewEditDate(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_NULL);
        textInputEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            // Create DatePickerDialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                    (view1, year1, month1, dayOfMonth1) -> {
                        // Update TextInputEditText with selected date
                        String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                        textInputEditText.setText(selectedDate);
                    }, year, month, dayOfMonth);
            // Show DatePickerDialog
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        });
        return customView;
    }

    private View viewEditSurveyor(String title, String value, List<String> list, LinearLayout linearLayout, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setText(value);
        textInputEditText.setFocusable(false);
        textInputEditText.setClickable(true); // Enable clickability
//        if (title.equals("Tipe Perawatan")){
//            textInputLayout.setVisibility(View.GONE);
//
//        }
        if (!editable) {
            textInputLayout.setEnabled(false);
        }
        textInputEditText.setOnClickListener(v -> {
            showAlertDialogButtonClicked(list);
        });
        return customView;
    }

    public void showAlertDialogButtonClicked(List<String> list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("List TPH Block Ini");

        // Inflate the custom layout
        View customLayout = activity.getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);


        // Get references to views
        EditText editText = customLayout.findViewById(R.id.editText);
        ListView listView = customLayout.findViewById(R.id.listView);

        // Initialize the adapter with your data
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                editText.setText(selectedItem);
            }
        });
        // Add a text change listener to filter the list based on input text
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value;
                String[] itemsArray = s.toString().split(",");
                if (itemsArray.length == 1) {
                    // Kembalikan string tersebut sebagai string terakhir
                    value = s.toString();
                } else {
                    // Kembalikan elemen terakhir dari array
                    value = itemsArray[itemsArray.length - 1];
                }
                adapter.getFilter().filter(value); // Apply filter to adapter
                listView.setVisibility(View.VISIBLE); // Show the ListView
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Set positive button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getTextInputLayout("tphCode").getEditText().setText(editText.getText().toString());

                String id = activity.getIdTPH(String.valueOf(mData.getAttributes().get("Block")),editText.getText().toString());

                mData.getAttributes().put("tphCode", id);

//                Prefs.setPrefs().putString(Prefs.PELAKSANA,editText.getText().toString()).apply();
                // Handle OK button click
                // You can get selected item from the adapter if needed
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public TextInputLayout getTextInputLayout(String param){
        TextInputLayout tip = null;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childView = viewGroup.getChildAt(i);
            LinearLayout ll = (LinearLayout) childView;
            for (int l = 0; l < ll.getChildCount(); l++) {
                View child = ll.getChildAt(l);
                if (child instanceof TextInputLayout){
                    TextInputLayout textInputLayout = (TextInputLayout) child;
                    if (textInputLayout.getHint().equals(param)){
                        tip =textInputLayout;
                    }
                }
            }
        }
        return tip;
    }




    public static Map<String, Object> sortByOrder(Map<String, Object> map, List<String> order) {
        Map<String, Object> sortedMap = new LinkedHashMap<>();
        for (String key : order) {
            if (map.containsKey(key)) {
                sortedMap.put(key, map.get(key));
            }
        }
        return sortedMap;
    }
}
