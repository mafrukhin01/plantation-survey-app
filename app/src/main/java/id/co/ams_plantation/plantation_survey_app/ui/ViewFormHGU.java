package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.InfinitePagerAdapter;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.CheckboxAdapter;
import id.co.ams_plantation.plantation_survey_app.adapter.ImageViewPagerPinchToZoomAdapter;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.InputFilterMinMaxNumber;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.WidgetHelper;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class ViewFormHGU {
    private static Context context;
    TextInputLayout spin_stat, textDateRealize;
    AutoCompleteTextView spin_value;
    public ImageView ivPhoto;
    String urlString;
    LayoutInflater inflater;
    TextView header, qc_stat;
    ImageView iv_marker,iv_compas_lock, imagesview, iv_back;
    AlertDialog alertDialogCompas,ad;
    Feature mData;
    ScrollView sv_object;
    BottomSheetDialog mBottomSheetDialog;
    SliderLayout sliderView;
    MapHguActivity activity;
    private SliderLayout sliderLayout;
    public boolean iscompas = false;
    private static LinearLayout viewGroup, bottomSheet, llManual, llAction;
    private ViewFormHGU.action mAction;
    RelativeLayout takePicture;
    Button b_save, b_cancel, btnUpdateLocation;
    enum action{
        Add,
        show,
        edit,
    }

    public ViewFormHGU(Context context, int _action){
        Log.e("ViewFormHGU","inisiate");
        if (_action ==0 ){
            mAction = ViewFormHGU.action.Add;
        } else if (_action==1) {
            mAction = ViewFormHGU.action.show;
        } else {
            mAction = ViewFormHGU.action.edit;
        }
        this.activity = (MapHguActivity) context;
        this.mData = activity.selectedFeature;
        this.inflater = LayoutInflater.from(activity);
        bottomSheet = activity.findViewById(R.id.bottomSheetViewHGU);
        spin_stat= activity.findViewById(R.id.spin_title);
        spin_value = activity.findViewById(R.id.spin_value2);
        textDateRealize = activity.findViewById(R.id.text_pr);
        viewGroup = activity.findViewById(R.id.view_group);
        b_cancel = activity.findViewById(R.id.bt_cancel);
        b_save = activity.findViewById(R.id.bt_save);
        header = activity.findViewById(R.id.tv_header);
        qc_stat = activity.findViewById(R.id.tv_qc);
        iv_marker = activity.findViewById(R.id.iv_marker);
        sliderLayout = activity.findViewById(R.id.sliderLayout2);

        llManual = activity.findViewById(R.id.ll_manual);
        sv_object = activity.findViewById(R.id.scroll_object);
        btnUpdateLocation = activity.findViewById(R.id.btnUpdateLocation);
        ivPhoto = activity.findViewById(R.id.ivPhoto);
        imagesview = activity.findViewById(R.id.imagesview);
        iv_back = activity.findViewById(R.id.iv_back);
        llAction = activity.findViewById(R.id.ll_action_view);
        header.setText(GlobalHelper.getNameObject(activity.selectedFeature.getFeatureTable().getTableName()));
        textDateRealize.setVisibility(View.GONE);
        if (sliderLayout!=null) {
           sliderLayout.removeSliderAt(0);
        }
//        sliderLayout.removeAllViews();

        if(mAction == ViewFormHGU.action.show){
            int stat = (int) mData.getAttributes().get("condition");
            String name = nameStatus(stat);
            spin_stat.setHint("Status Objek");
            b_save.setText("Survey");
            spin_value.setText(name);
            List<String> list = new ArrayList<>();
            list.add(name);
            list.add("Delete");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                    android.R.layout.simple_dropdown_item_1line,list);
            spin_value.setAdapter(adapter);
            setContent(name);
        }else {
            setContent("selectedValue");
        }

        try {
            if(mData.getAttributes().get("survey_date")=="" && String.valueOf(mData.getAttributes().get("QC")).equals("3")){
                qc_stat.setText("Resurvey");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        b_cancel.setOnClickListener(v -> {
            activity.ALselectedImage.clear();
            sliderLayout.removeAllSliders();
            hideView();
        });
        b_save.setOnClickListener(v -> {
            if ((isFieldVisibleAndEmpty("Rekomendasi") || isFieldVisibleAndEmpty("Tipe Perawatan"))) {
                activity.showAlert("Warning", "Silahkan lengkapi data survey", SweetAlertDialog.WARNING_TYPE);
                return;
            }
//            activity.ALselectedImage > 0?
//            activity.getGlobalID(mData.getFeatureTable());
            String images = "Uploads/Images/"+ Prefs.getString(Prefs.SESSION_ID_HGU)+"/";
            StringBuilder sb = new StringBuilder();
            if(activity.ALselectedImage.size()> 0){
                for(int i = activity.ALselectedImage.size() -1 ; i >= 0 ; i --){
                    File img = activity.ALselectedImage.get(i);
                    activity.saveImage(String.valueOf(mData.getAttributes().get("GlobalID")));
                    sb.append(images).append(img.getName()).append(";");
//                    mData.getAttributes().put("image",images+sb.toString());
                }
//                mData.getAttributes().put("image","/Uploads/Images/"+ Prefs.getString(Prefs.SESSION_ID)+"/"+activity.ALselectedImage.get(0).getName()+";");
                mData.getAttributes().put("image",sb.toString());
                activity.ALselectedImage.clear();
            }
            else {
                activity.showAlert("Warning","Silahkan Ambil Gambar", SweetAlertDialog.WARNING_TYPE);
                return;
            }


            if (mAction == ViewFormHGU.action.Add){
                Log.e("value Status",spin_value.getText().toString());
//                setStatusObject(mData,spin_value.getText().toString());
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childView = viewGroup.getChildAt(i);
                    LinearLayout ll = (LinearLayout) childView;
                    for (int l = 0; l < ll.getChildCount(); l++) {
                        View child = ll.getChildAt(l);
                        if (child instanceof TextInputLayout & child.getVisibility()==View.VISIBLE){
                            TextInputLayout textInputLayout = (TextInputLayout) child;
                            if(textInputLayout.getEditText() instanceof TextInputEditText) {
                                TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = editText.getText().toString();
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    if(fieldType == Field.Type.TEXT){
                                        mData.getAttributes().put(key, value);
                                    }
                                    if(fieldType == Field.Type.INTEGER){
                                        mData.getAttributes().put(key, Integer.valueOf(value));
                                    }
                                    if(fieldType == Field.Type.SHORT ){
                                        mData.getAttributes().put(key, Short.valueOf(value));
                                    }
                                    if(fieldType == Field.Type.DOUBLE){
                                        mData.getAttributes().put(key, Double.valueOf(value));
                                    }
//                                    }
//                                    mData.getAttributes().put(key,value);
                                    Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                }
                            }
                            else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                                MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = String.valueOf(editText.getText());
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                    List<CodedValue> values =  codedValue.getCodedValues();
                                    for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                        if(fieldValue.getName().equals(value)){
                                            Log.e("codedName equals ",  fieldValue.getName());
                                            Log.e("coded equals ", fieldValue.getCode().toString());
                                            if(fieldType == Field.Type.TEXT){
                                                mData.getAttributes().put(key, fieldValue.getCode().toString());
                                            }
                                            if(fieldType == Field.Type.INTEGER){
                                                mData.getAttributes().put(key, Integer.valueOf(fieldValue.getCode().toString()));
                                            }
                                            if(fieldType == Field.Type.SHORT ){
                                                mData.getAttributes().put(key, Short.valueOf(fieldValue.getCode().toString()));
                                            }
//                                            mData.getAttributes().put(key,fieldValue.getCode().toString());
                                            Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                activity.saveNewFeature(mData);
                hideView();
            } else if (mAction == ViewFormHGU.action.show) {
//                setStatusEditObject(mData,spin_value.getText().toString());
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childView = viewGroup.getChildAt(i);
                    LinearLayout ll = (LinearLayout) childView;
                    for (int l = 0; l < ll.getChildCount(); l++) {
                        View child = ll.getChildAt(l);
                        if (child instanceof TextInputLayout){
                            TextInputLayout textInputLayout = (TextInputLayout) child;
                            if(textInputLayout.getEditText() instanceof TextInputEditText) {
                                TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = editText.getText().toString();
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
//                                    if (mData.getFeatureTable().getField(key).isEditable()&& key.equals("block") || key.equals("status_object")
//                                            || key.equals("flow_dir")|| key.equals("condition")|| key.equals("remark")
//                                            || key.equals("length")||key.equals("width")||key.equals("type")||key.equals("month")||key.equals("duration")||key.equals("year")
//                                            ||key.equals("tot_door")||key.equals("hp")||key.equals("brand")||key.equals("model")||key.equals("diameter")||key.equals("throughput")||key.equals("spillway_level")) {
                                    if(fieldType == Field.Type.TEXT){
                                        mData.getAttributes().put(key, value);
                                    }
                                    if(fieldType == Field.Type.INTEGER){
                                        mData.getAttributes().put(key, Integer.valueOf(value));
                                    }
                                    if(fieldType == Field.Type.SHORT ){
                                        mData.getAttributes().put(key, Short.valueOf(value));
                                    }
                                    if(fieldType == Field.Type.DOUBLE){
                                        mData.getAttributes().put(key, Double.valueOf(value));
                                    }
//                                    }
//                                    mData.getAttributes().put(key,value);
                                    Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                }
                            }
                            else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                                MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                                if (editText != null&& !editText.getText().toString().equals("")) {
                                    String value = String.valueOf(editText.getText());
                                    String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                                    Field.Type fieldType = mData.getFeatureTable().getField(key).getFieldType();
                                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                    List<CodedValue> values =  codedValue.getCodedValues();
                                    for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                        if(fieldValue.getName().equals(value)){
                                            Log.e("codedName equals ",  fieldValue.getName());
                                            Log.e("coded equals ", fieldValue.getCode().toString());
                                            if(fieldType == Field.Type.TEXT){
                                                mData.getAttributes().put(key, fieldValue.getCode().toString());
                                            }
                                            if(fieldType == Field.Type.INTEGER){
                                                mData.getAttributes().put(key, Integer.valueOf(fieldValue.getCode().toString()));
                                            }
                                            if(fieldType == Field.Type.SHORT ){
                                                mData.getAttributes().put(key, Short.valueOf(fieldValue.getCode().toString()));
                                            }
//                                            mData.getAttributes().put(key,fieldValue.getCode().toString());
                                            Log.e("result =" + key, "value = " + mData.getAttributes().get(key));
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
//                activity.moveFeatureTo(mData);
//                hideView();
            }
        });

        iv_marker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.mCurrentEditState = MapHguActivity.EditState.Editing;
                activity.fab_gps.callOnClick();
                showManual();
//                activity.updateGeometry(mData);
//                hideView();
            }
        });

        btnUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activity.updateGeometry(mData);

            }
        });
        ivPhoto.setOnClickListener(view -> {
            activity.OpenCamera();

        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagesZoom();
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showManual();
            }
        });

    }

    public void showView(int _action){
//        clearSliderLayoutCache();
//        sliderLayout.setVisibility(View.VISIBLE);


        ivPhoto.setVisibility(View.VISIBLE);
        b_save.setVisibility(View.VISIBLE);
        iv_marker.setVisibility(View.VISIBLE);

        bottomSheet.setVisibility(View.VISIBLE);
//        if(activity.fabAdd.getVisibility()==View.VISIBLE){
        activity.fabAdd.setVisibility(View.GONE);
        activity.bmb.setVisibility(View.GONE);
        activity.fab_gps.setVisibility(View.GONE);
        activity.fab_layer.setVisibility(View.GONE);
        activity.base_info.setVisibility(View.GONE);
        activity.fab_extend.setVisibility(View.GONE);
        activity.footer.setVisibility(View.GONE);
        activity.fab_versioning.setVisibility(View.GONE);

    }

    public void clearSliderLayoutCache() {
        // Stop auto-cycling to release resources associated with timers
        sliderLayout.stopAutoCycle();

        // Remove all sliders to clear out any cached images or views
        sliderLayout.removeAllSliders();

        // Clear the adapter to ensure it releases any references to cached data
        try {
            Method method = sliderLayout.getClass().getDeclaredMethod("getWrapperAdapter");
            method.setAccessible(true);
            InfinitePagerAdapter wrapperAdapter = (InfinitePagerAdapter) method.invoke(sliderLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Stop auto cycle to ensure no further actions are taken
        sliderLayout.stopAutoCycle();

// Remove all sliders to clear the current images and views
        sliderLayout.removeAllSliders();

// Reset the transformer to a default or no transformation
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);

// Reset the indicator to default or hide it if necessary
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

// Clear any custom animations
        sliderLayout.setCustomAnimation(null);

// Optionally, set auto-cycle to false if you don’t want it to restart automatically

        // Clear any custom animations if they are holding onto memory


        // Clear ViewPager transformer to free up resources

//        sliderLayout.setPagerTransformer(false, null);

        // Optionally, request garbage collection (not always necessary but can help in some cases)
        System.gc();
    }


    public void hideView(){
        viewGroup.removeAllViews();
        bottomSheet.setVisibility(View.GONE);
        if(activity.fabAdd.getVisibility()==View.GONE){
            activity.sniper.setVisibility(View.GONE);
//            activity.fabAdd.setVisibility(View.VISIBLE);
            activity.bmb.setVisibility(View.VISIBLE);
            activity.fab_gps.setVisibility(View.VISIBLE);
//            activity.fab_layer.setVisibility(View.VISIBLE);
            activity.base_info.setVisibility(View.VISIBLE);
            activity.fab_extend.setVisibility(View.VISIBLE);
//            activity.fab_versioning.setVisibility(View.VISIBLE);
        }
        if(activity.footer.getVisibility()==View.GONE){
            activity.footer.setVisibility(View.VISIBLE);
        }
        imagesview.setVisibility(View.GONE);
//        sliderLayout.removeAllViews();
//        sliderLayout.clearAnimation();
        sliderLayout.removeAllSliders();

//        sliderLayout.removeAllViewsInLayout();
//        sliderLayout.removeAllViews();
    }

    public String nameStatus(int status){
        String stat = "";
        if(status == 2){
            stat = "Plan";
        } else if (status==1) {
            stat ="Existing";
        }else if (status==3){
            stat="Budget";
        }else if(status==4){
            stat="PTA";
        }
        return stat;
    }

    public void showManual(){

        if (sv_object.getVisibility()==View.VISIBLE){
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = true;
            sv_object.setVisibility(View.GONE);
            llManual.setVisibility(View.VISIBLE);
            activity.sniper.setVisibility(View.VISIBLE);
            llAction.setVisibility(View.GONE);

        }
        else {
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = false;
            sv_object.setVisibility(View.VISIBLE);
            llManual.setVisibility(View.GONE);
            activity.sniper.setVisibility(View.GONE);
            llAction.setVisibility(View.VISIBLE);
        }
    }

    public void setupimageslide(){
        sliderLayout.removeAllSliders();
        Set<File> sf = new HashSet<>();
        sf.addAll(activity.ALselectedImage);
        activity.ALselectedImage.clear();
        activity.ALselectedImage.addAll(sf);
        if(activity.ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(activity.ALselectedImage.size() != 0){
//            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
            setDP(25);
        }else{
//            imagecacnel.setVisibility(View.GONE);
            // Convert 40dp to pixels based on the device's density
            setDP(70);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }


        for(File file : activity.ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            textSliderView
                    .image(file)
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            sliderLayout.addSlider(textSliderView);
            // initialize a SliderLayout
//            if(file.toString().startsWith("http")){
//                String surl = file.toString();
//                if(file.toString().startsWith("http://")){
//
//                }else if(file.toString().startsWith("http:/")){
//                    surl =surl.replace("http:/","http://");
//                }else if(file.toString().startsWith("https://")){
//
//                }else if(file.toString().startsWith("https:/")){
//                    surl =surl.replace("https:/","https://");
//                }
//                textSliderView
//                        .image(surl)
//                        .setScaleType(BaseSliderView.ScaleType.Fit);
//            }else {

//            }

        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());

    }

    public void showImagesZoom(){
        View view = LayoutInflater.from(activity).inflate(R.layout.imageszoom, null);
        ViewPager pinchtozoom_pager = (ViewPager) view.findViewById(R.id.pinchtozoom_pager);
        ImageView iv_iz_close = (ImageView) view.findViewById(R.id.iv_iz_close);
        ImageViewPagerPinchToZoomAdapter imageViewPagerPinchToZoomAdapter = new ImageViewPagerPinchToZoomAdapter(activity.ALselectedImage,urlString);
        pinchtozoom_pager.setOffscreenPageLimit(3);
        pinchtozoom_pager.setAdapter(imageViewPagerPinchToZoomAdapter);
        iv_iz_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ad.isShowing()){
                    ad.dismiss();}
            }
        });
        ad = WidgetHelper.showListReference(ad,view,activity);
    }

    public void setContent(String  _status){
        setupimageslide();
//        sliderLayout.removeAllSliders();
        List<String> setAttr = new ArrayList<>();
        Log.e("status Content",_status);
        setAttr = GlobalHelper.attributesHGU();
//        if(_status.equals("Baik")){
//            setAttr = GlobalHelper.attributesHGUB();
//        } else if (_status.equals("Rusak")) {
//            setAttr = GlobalHelper.attributesHGU();
//        }else if (_status.equals("Hilang")) {
//            setAttr = GlobalHelper.attributesHGU();
//        }
        viewGroup.removeAllViews();
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        Map<String, Object> attr = mData.getAttributes();
        Set<String> keys = attr.keySet();
//        List<String> statusList = GlobalHelper.getStatusObject();
        for (String _attr : setAttr){
            for (String key : keys) {
                Log.e("debug key true",key );
                Field.Type typeObject = mData.getFeatureTable().getField(key).getFieldType();
                List<String> listAdapter = new ArrayList<>();
                if (_attr.equals(key)){
                    Object value = attr.get(key);
                    if (value == null){
                        value = "";
                    }
//                Log.e("key",key + " | FieldType " + typeObject+ "\n");
                    if(mData.getFeatureTable().getField(key).getDomain() != null){
                        CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                        List<CodedValue> values =  codedValue.getCodedValues();
                        for (CodedValue fieldValue : values) {
                            listAdapter.add(fieldValue.getName());
                        }
                        if (key.equals("typePerawatan")){
                            viewGroup.addView(viewEditCheck(GlobalHelper.getNameAttribute(key), listAdapter,  linearLayout, true));
                        }
                        else {
                            viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key), value, listAdapter, values, linearLayout, true));
                        }
                    }
                    else {
//                    if (){
                        if(key.equals("surveyor")){
                               List<String> list= activity.getUserALl();
                               if(Prefs.getString(Prefs.PELAKSANA)!= null){
                                   viewGroup.addView(viewEditSurveyor(GlobalHelper.getNameAttribute(key),Prefs.getString(Prefs.PELAKSANA),list,linearLayout,true));
                               }else {
                                   viewGroup.addView(viewEditSurveyor(GlobalHelper.getNameAttribute(key),"",list,linearLayout,true));
                               }
                        }

                        else {
                            if(typeObject== Field.Type.TEXT){
                                if (key.equals("GroupCompanyName")||key.equals("CompanyCode")||key.equals("ESTNR")||key.equals("Block")){
                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
                                }else if(key.equals("QC_remarks")&&value!=null){
                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
                                }
                                else {
                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,true));
                                }
                            }
                            else if (typeObject == Field.Type.DOUBLE){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true, (float) 0,100, true));
                            }
                            else if (typeObject == Field.Type.INTEGER ||typeObject == Field.Type.SHORT){
                                if (key.equals("flow_dir")){
                                    viewGroup.addView(viewEditCompass(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true));
                                }else if (key.equals("month")||key.equals("duration")){
                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,24,false));
                                }
                                else if (key.equals("year")||key.equals("year_budget")){
                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,2024,false));
                                }
                                else {
                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,100,false));
                                }
                            }
                            else if (typeObject == Field.Type.DATE) {
                                viewGroup.addView(viewEditDate(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                            }
                        }
//                    }
                    }
                }
            }
        }

    }





//    public void startAnimation(){
//        bottomSheet.setVisibility(View.VISIBLE);
//        int parentHeight = ((View) bottomSheet.getParent()).getHeight();
//        bottomSheet.setTranslationY(parentHeight);
//        TranslateAnimation animation = new TranslateAnimation(0, 0, parentHeight, 0);
//        animation.setDuration(500);
//        bottomSheet.startAnimation(animation);
//
//    }
    public void setDP(int param){
        int sizeInDp = param;
        float scale = activity.getResources().getDisplayMetrics().density;
        int sizeInPx = (int) (sizeInDp * scale + 0.5f);

        ViewGroup.LayoutParams layoutParams = ivPhoto.getLayoutParams();
        layoutParams.width = sizeInPx;
        layoutParams.height = sizeInPx;
        ivPhoto.setLayoutParams(layoutParams);
    }
    private View viewEditText(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }



    private View viewEditNumber(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable, float min, float max, boolean isDouble){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        if (isDouble){
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
        else {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        InputFilter[] filters = {new InputFilterMinMaxNumber(min, max)};
//        if (min > 0 ){
        textInputEditText.setFilters(filters);

        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString().trim();
                if(isDouble){
                    if (!input.isEmpty()) {
                        float value = Float.parseFloat(s.toString());
                        if (value < 0.1) {
                            textInputEditText.setError("Minimum value is 0.1");
                        } else {
//                            textInputEditText.setHint("test Hint");
                            // Value is valid, proceed with further processing
                        }}
                }
            }
        });
        return customView;
    }

    private View viewAutoComplete(String title, Object value, List<String> listSpin, List<CodedValue> codedValues, LinearLayout linearLayout, boolean editable){
        Log.e("title", title);
        View customView = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        int selected = 0;

        for(int loop = 0; loop<codedValues.size(); loop++){
            Log.e(codedValues.get(loop).getCode().toString(), value.toString());
            if(value.toString().equals(codedValues.get(loop).getCode().toString())){
                selected = loop;
                Log.e("selected", listSpin.get(selected));
            }
        }
//        View view = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.spin_title);
        textInputLayout.setHint(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,listSpin);
        MaterialAutoCompleteTextView textInputEditText = customView.findViewById(R.id.spin_value);
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputEditText.setEnabled(false);
        }
        textInputEditText.setAdapter(adapter);

        if (title.equals("Kondisi")){
            textInputEditText.setOnItemClickListener((adapterView, view, i, l) -> {
//                Log.e("Perbaikan",getTextInputLayout("Rekomendasi").getEditText().getText().toString());
                if (textInputEditText.getText().toString().equals("Baik")){
//                    getTextInputLayout("Rekomendasi").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                }
                else {
                    getTextInputLayout("Rekomendasi").setVisibility(View.VISIBLE);
                }
            });
        }

        if (title.equals("Rekomendasi")){
//            textInputLayout.setVisibility(View.GONE);
            textInputEditText.setOnItemClickListener((adapterView, view, i, l) -> {
                if (textInputEditText.getText().toString().equals("Perbaikan")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.VISIBLE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                } else if (textInputEditText.getText().toString().equals("Perawatan")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.VISIBLE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                } else if (textInputEditText.getText().toString().equals("Tidak Dipasang")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.VISIBLE);
                } else {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                }
            });
        }
        if (title.equals("Tipe Perbaikan")||title.equals("Tipe Perawatan")||title.equals("Alasan")){
            textInputLayout.setVisibility(View.GONE);

        }
        if (value != null){
            if (title.equals("condition")){
//                if(value.toString())
//                textInputEditText.setText("Pilih Status",false);
//                textInputEditText.setText(adapter.getItem(selected),false);
            }
            else {
                textInputEditText.setText(adapter.getItem(selected),false);
            }

        }
        return customView;
    }


    private View viewEditDate(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_NULL);
        textInputEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            // Create DatePickerDialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                    (view1, year1, month1, dayOfMonth1) -> {
                        // Update TextInputEditText with selected date
                        String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                        textInputEditText.setText(selectedDate);
                    }, year, month, dayOfMonth);
            // Show DatePickerDialog
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        });
        return customView;
    }
    private View viewEditCompass(String title, Object value, LinearLayout linearLayout, int maxLine,boolean editable){
        View v = LayoutInflater.from(activity).inflate(R.layout.compas, null);
        View customView = inflater.inflate(R.layout.item_edit_compass, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        final ImageView iv_compas_lock = customView.findViewById(R.id.iv_compas_lock);
//        iv_compas_lock = (ImageView) activity.findViewById(R.id.iv_compas_lock);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        activity.ch.tv_degere2 = textInputEditText;
        InputFilter[] filters = {new InputFilterMinMaxNumber(0, 360)};
        activity.ch.tv_degere2.setFilters(filters);
        if (!editable){
            textInputLayout.setEnabled(false);
        }
        setup_iv_compas_lock(iv_compas_lock,iscompas);
        iv_compas_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("trueCompas","true");
                if(iscompas){
//                    activity.onCompassStopped();
                    iscompas = false;
                }else{
                    showCompas(iv_compas_lock);
//                    activity.onCompassStarted();
                    iscompas = true;
                }
                setup_iv_compas_lock(iv_compas_lock,iscompas);
//                showCompas(iv_compas_lock);
            }
        });


        return customView;
    }
    public void setup_iv_compas_lock(ImageView iv_compas_lock, Boolean status){
        if(status){
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_stop).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }else{
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_compass).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }
    }

    public void showCompas(final ImageView iv_compas_lock){
        View v = LayoutInflater.from(activity).inflate(R.layout.compas, null);
        activity.layoutCompass = (RelativeLayout) v.findViewById(R.id.layoutCompass);
        activity.ivCompass = (ImageView) v.findViewById(R.id.imageViewCompass);
        activity.pulsatorLayout = (PulsatorLayout) v.findViewById(R.id.pulsator);
        activity.ch.arrowView = (ImageView) v.findViewById(R.id.imageViewHeading);
//        activity.ch.tvArahCompass = (TextView) v.findViewById(R.id.tvArahCompass);
        activity.ch.tvArahCompassCardinal = (TextView) v.findViewById(R.id.tvArahCompassCardinal);
        v.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        activity.layoutCompass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                activity.onCompassStopped();
                iscompas = false;
                setup_iv_compas_lock(iv_compas_lock,iscompas);
                alertDialogCompas.dismiss();
            }
        });

        alertDialogCompas = WidgetHelper.showFormDialogTransparant(alertDialogCompas,v,activity);
    }

    public TextInputLayout getTextInputLayout(String param){
        TextInputLayout tip = null;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childView = viewGroup.getChildAt(i);
            LinearLayout ll = (LinearLayout) childView;
            for (int l = 0; l < ll.getChildCount(); l++) {
                View child = ll.getChildAt(l);
                if (child instanceof TextInputLayout){
                    TextInputLayout textInputLayout = (TextInputLayout) child;
                    if (textInputLayout.getHint().equals(param)){
                        tip =textInputLayout;
                    }
                }
            }
        }
        return tip;
    }

    private boolean isFieldVisibleAndEmpty(String param) {
        TextInputLayout textInputLayout = getTextInputLayout(param);
        if (textInputLayout != null && textInputLayout.getVisibility() == View.VISIBLE) {
            // Memeriksa apakah teks di dalam TextInputLayout kosong
            EditText editText = textInputLayout.getEditText();
            if (editText != null && editText.getText().toString().trim().isEmpty()) {
                return true;
            }
        }
        return false;
    }
    private void showSearchDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Search");

        // Inflate layout for dialog
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_search, null);
        builder.setView(dialogView);

        // Set up SearchView
        SearchView searchView = dialogView.findViewById(R.id.search_view);
        searchView.setIconifiedByDefault(false); // Show SearchView expanded by default
        searchView.requestFocus(); // Set focus to SearchView
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Handle search query submission
//                performSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Handle search query text change
//                performSearch(newText);
                return true;
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDialogCheckbox(String title, List<String>list, TextInputEditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        String[] itemsArray = list.toArray(new String[0]);
        boolean[] checkedItems = new boolean[itemsArray.length];
        builder.setMultiChoiceItems(itemsArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                StringBuilder selectedItems = new StringBuilder();
                for (int i = 0; i < itemsArray.length; i++) {
                    if (checkedItems[i]) {
                        if (selectedItems.length() > 0) {
                            selectedItems.append(", ");
                        }
                        selectedItems.append(itemsArray[i]);
                    }
                }
                editText.setText(selectedItems.toString());
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle OK button click
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDialogCheckSearch(String title, List<String>list, TextInputEditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
//        EditText searchInput = new EditText(activity);
//        searchInput.setHint("Search...");
//        builder.setView(searchInput);
        String[] itemsArray = list.toArray(new String[0]);
        boolean[] checkedItems = new boolean[itemsArray.length];
        builder.setMultiChoiceItems(itemsArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                StringBuilder selectedItems = new StringBuilder();
                for (int i = 0; i < itemsArray.length; i++) {
                    if (checkedItems[i]) {
                        if (selectedItems.length() > 0) {
                            selectedItems.append(", ");
                        }
                        selectedItems.append(itemsArray[i]);
                    }
                }
                editText.setText(selectedItems.toString());
            }
        });


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle OK button click
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private View viewEditCheck(String title, List<String> list, LinearLayout linearLayout, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setFocusable(false);
        textInputEditText.setClickable(true); // Enable clickability
        if (title.equals("Tipe Perawatan")){
            textInputLayout.setVisibility(View.GONE);

        }
        if (!editable) {
            textInputLayout.setEnabled(false);
        }

        textInputEditText.setOnClickListener(v -> {
            if (title.equals("Tipe Perawatan")){
                showDialogCheckbox(title, list, textInputEditText);
            }
            else
            {
                showDialogCheckSearch(title, list, textInputEditText);

            }
        });
        return customView;
    }

    private View viewEditSurveyor(String title, String value, List<String> list, LinearLayout linearLayout, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setText(value);
        textInputEditText.setFocusable(false);
        textInputEditText.setClickable(true); // Enable clickability
//        if (title.equals("Tipe Perawatan")){
//            textInputLayout.setVisibility(View.GONE);
//
//        }
        if (!editable) {
            textInputLayout.setEnabled(false);
        }
        textInputEditText.setOnClickListener(v -> {
            showAlertDialogButtonClicked(list);
        });
        return customView;
    }

    public void showAlertDialogButtonClicked(List<String> list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Tim Pelaksana");

        // Inflate the custom layout
        View customLayout = activity.getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);


        // Get references to views
        EditText editText = customLayout.findViewById(R.id.editText);
        ListView listView = customLayout.findViewById(R.id.listView);

        // Initialize the adapter with your data
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                String[] parts = selectedItem.split("-");

                // Split text into an array based on comma (,)
                String[] itemsArray = editText.getText().toString().split(",");

                // Create a StringBuilder to manipulate the text
                StringBuilder newText = new StringBuilder();

                // Iterate through the itemsArray and append each item to the newText
                for (int i = 0; i < itemsArray.length - 1; i++) {
                    newText.append(itemsArray[i]).append(",");
                }

                // Append the new item from the selected list view item
                newText.append(parts[0]).append(",");

                // Set the edited text to the EditText
                editText.setText(newText.toString());
            }
        });
        // Add a text change listener to filter the list based on input text
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value;
                String[] itemsArray = s.toString().split(",");
                if (itemsArray.length == 1) {
                    // Kembalikan string tersebut sebagai string terakhir
                    value = s.toString();
                } else {
                    // Kembalikan elemen terakhir dari array
                    value = itemsArray[itemsArray.length - 1];
                }
                // ArrayAdapter<String> filteredAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, filteredArray);
//                listView.setAdapter(filteredAdapter);
                adapter.getFilter().filter(value); // Apply filter to adapter
                listView.setVisibility(View.VISIBLE); // Show the ListView
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Set positive button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getTextInputLayout("Tim Pelaksana").getEditText().setText(editText.getText().toString());
                Prefs.setPrefs().putString(Prefs.PELAKSANA,editText.getText().toString()).apply();
                // Handle OK button click
                // You can get selected item from the adapter if needed
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
