package id.co.ams_plantation.plantation_survey_app.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.ImageViewPagerPinchToZoomAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;
import id.co.ams_plantation.plantation_survey_app.utils.DateTime;
import id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper;
import id.co.ams_plantation.plantation_survey_app.utils.InputFilterMinMaxNumber;
import id.co.ams_plantation.plantation_survey_app.utils.Prefs;
import id.co.ams_plantation.plantation_survey_app.utils.WidgetHelper;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class ViewFormTPH {
    Context context;
    TextInputLayout spin_stat, textDateRealize, no_tph;
    AutoCompleteTextView spin_value;
    public ImageView ivPhoto;
    String urlString;
    LayoutInflater inflater;
    TextView header, qc_stat;
    ImageView iv_marker,iv_compas_lock, imagesview, iv_back;
    AlertDialog alertDialogCompas,ad;
    String userId;
    TPH newTPH;
    Feature mData;
    ScrollView sv_object;
    TextInputEditText nama_tph;
    BottomSheetDialog mBottomSheetDialog;
    SliderLayout sliderView;
    MapReplantingActivity activity;
    public SliderLayout sliderLayout;
    public boolean iscompas = false;
    private static LinearLayout viewGroup, bottomSheet, llManual, llAction;

    RelativeLayout takePicture;
    Button b_save, b_cancel, btnUpdateLocation;


    public ViewFormTPH(android.content.Context context, String userId, TPH tph){
        Log.e("ViewFormHGU","inisiate");
//        if (_action ==0 ){
//            mAction = action.Add;
//        } else if (_action==1) {
//            mAction = action.show;
//        } else {
//            mAction = action.edit;
//        }
        this.activity = (MapReplantingActivity) context;
        this.userId = userId;
//        this.mData = activity.selectedFeature;
        bottomSheet = activity.findViewById(R.id.bottomSheetView2);
        spin_stat= activity.findViewById(R.id.spin_title_tph);
        spin_value = activity.findViewById(R.id.spin_value_tph);
        viewGroup = activity.findViewById(R.id.view_group2);
        b_cancel = activity.findViewById(R.id.bt_cancel2);
        b_save = activity.findViewById(R.id.bt_save2);
        header = activity.findViewById(R.id.tv_header2);
        iv_marker = activity.findViewById(R.id.iv_marker2);
        sliderLayout = activity.findViewById(R.id.sliderLayout2);
        llManual = activity.findViewById(R.id.ll_manual2);
        sv_object = activity.findViewById(R.id.scroll_object2);
        btnUpdateLocation = activity.findViewById(R.id.btnUpdateLocation2);
        ivPhoto = activity.findViewById(R.id.ivPhoto2);
        imagesview = activity.findViewById(R.id.imagesview);
        iv_back = activity.findViewById(R.id.iv_back2);
        llAction = activity.findViewById(R.id.ll_action_view2);
        no_tph = activity.findViewById(R.id.title_notph);
        nama_tph = activity.findViewById(R.id.text_notph);
        this.inflater = LayoutInflater.from(activity);
        newTPH = tph;
        List<String> strings = new ArrayList<>();
//        header.setText(GlobalHelper.getNameObject(activity.selectedFeature.getFeatureTable().getTableName()));
//        textDateRealize.setVisibility(View.GONE);

//        if(mAction == action.show){
//            int stat = (int) mData.getAttributes().get("condition");
//            String name = nameStatus(stat);
//            spin_stat.setHint("Status Objek");
//            b_save.setText("Survey");
//            spin_value.setText(name);
//            List<String> list = new ArrayList<>();
//            list.add(name);
//            list.add("Delete");
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
//                    android.R.layout.simple_dropdown_item_1line,list);
//            spin_value.setAdapter(adapter);
            setContent("name");
//        }else {
//            setContent("selectedValue");
////            spin_value.setText("Pilih Status Object");
////            List<String> listStatus = new ArrayList<>();
////            listStatus = GlobalHelper.getStatusObject();
////            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
////                    android.R.layout.simple_dropdown_item_1line,listStatus);
////            spin_stat.setHint("Pilih Status Objek");
////            spin_value.setAdapter(adapter);
////            spin_value.setOnItemClickListener((adapterView, view, i, l) -> {
////                        String selectedValue = (String)adapterView.getItemAtPosition(i);
//////                Toast.makeText(activity,selectedValue,Toast.LENGTH_LONG).show();
////                        setContent(selectedValue);}
////            );
//        }
//
//        try {
//            if(mData.getAttributes().get("survey_date")=="" && String.valueOf(mData.getAttributes().get("QC")).equals("3")){
//                qc_stat.setText("Resurvey");
//            }
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
        b_cancel.setOnClickListener(v -> {
//            activity.ALselectedImage.clear();
            hideView();
        });
        b_save.setOnClickListener(v -> {
            if(activity.ALselectedImage.size() == 0){
                activity.showAlert("Gagal Save TPH", "Wajib ambil Foto TPH",SweetAlertDialog.ERROR_TYPE);
                return;
            }
            newTPH.setNamaTph(nama_tph.getText().toString());
            newTPH.setStatus(1);
            newTPH.setAncakFlag(9);
            newTPH.setAncak(nama_tph.getText().toString());

            activity.saveNewTPH(newTPH,userId);
            hideView();
        });

        iv_marker.setOnClickListener(v -> {
//                activity.mCurrentEditState = MapHguActivity.EditState.Editing;
//                activity.fab_gps.callOnClick();
            showManual();
//                activity.updateGeometry(mData);
//                hideView();
        });

        btnUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.updateGeometry(newTPH);
            }
        });
        ivPhoto.setOnClickListener(view -> {
            Log.e("true click","done");
//            mData.getFeatureTable().getTableName();
            activity.OpenCamera();

        });
        imagesview.setOnClickListener(v -> showImagesZoom());
        iv_back.setOnClickListener(v -> showManual());

    }

    public void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(activity.ALselectedImage);
        activity.ALselectedImage.clear();
        activity.ALselectedImage.addAll(sf);

        if(activity.ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(activity.ALselectedImage.size() != 0){
//            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : activity.ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }else if(file.toString().startsWith("https://")){

                }else if(file.toString().startsWith("https:/")){
                    surl =surl.replace("https:/","https://");
                }
                textSliderView
                        .image(surl)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
//        imagecacnel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
//                try {
//                    if (sliderLayout.getCurrentPosition() == 0 && activity.ALselectedImage.size() == 1) {
//                        activity.ALselectedImage.clear();
//                        sliderLayout.removeAllSliders();
//                        sliderLayout.setVisibility(View.GONE);
//                        imagecacnel.setVisibility(View.GONE);
//                        imagesview.setVisibility(View.GONE);
//                    } else {
//                        deleteditemALselectedImage(sliderLayout.getCurrentPosition());
//                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
//                    }
//                }catch (Exception e){
//                    Log.e("sliderLayout", e.toString());
//                }
//            }
//        });

    }



    public void showManual(){

        if (sv_object.getVisibility()==View.VISIBLE){
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = true;
            sv_object.setVisibility(View.GONE);
            llManual.setVisibility(View.VISIBLE);
            activity.sniper.setVisibility(View.VISIBLE);
            llAction.setVisibility(View.GONE);

        }
        else {
//            activity.mCurrentEditState = MapInventoryActivity.EditState.Editing;
            activity.isModeAdd = false;
            sv_object.setVisibility(View.VISIBLE);
            llManual.setVisibility(View.GONE);
            activity.sniper.setVisibility(View.GONE);
            llAction.setVisibility(View.VISIBLE);
        }
    }

//    public void setupimageslide(){
//        Set<File> sf = new HashSet<>();
//        sf.addAll(activity.ALselectedImage);
//        activity.ALselectedImage.clear();
//        activity.ALselectedImage.addAll(sf);
//
//        if(activity.ALselectedImage.size() > 3){
//            ivPhoto.setVisibility(View.GONE);
//        }else{
//            ivPhoto.setVisibility(View.VISIBLE);
//        }
//        if(activity.ALselectedImage.size() != 0){
////            imagecacnel.setVisibility(View.VISIBLE);
//            imagesview.setVisibility(View.VISIBLE);
//            sliderLayout.setVisibility(View.VISIBLE);
//        }else{
////            imagecacnel.setVisibility(View.GONE);
//            imagesview.setVisibility(View.GONE);
//            sliderLayout.setVisibility(View.GONE);
//        }
//        sliderLayout.removeAllSliders();
////        for(File file : activity.ALselectedImage){
////            TextSliderView textSliderView = new TextSliderView(activity);
////            // initialize a SliderLayout
////            if(file.toString().startsWith("http")){
////                String surl = file.toString();
////                if(file.toString().startsWith("http://")){
////
////                }else if(file.toString().startsWith("http:/")){
////                    surl =surl.replace("http:/","http://");
////                }else if(file.toString().startsWith("https://")){
////
////                }else if(file.toString().startsWith("https:/")){
////                    surl =surl.replace("https:/","https://");
////                }
////                textSliderView
////                        .image(surl)
////                        .setScaleType(BaseSliderView.ScaleType.Fit);
////            }else {
////                textSliderView
////                        .image(file)
////                        .setScaleType(BaseSliderView.ScaleType.Fit);
////            }
////            sliderLayout.addSlider(textSliderView);
////        }
//
//        sliderLayout.stopAutoCycle();
//        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
//        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//        sliderLayout.setCustomAnimation(new DescriptionAnimation());
////        imagecacnel.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
////                try {
////                    if (sliderLayout.getCurrentPosition() == 0 && activity.ALselectedImage.size() == 1) {
////                        activity.ALselectedImage.clear();
////                        sliderLayout.removeAllSliders();
////                        sliderLayout.setVisibility(View.GONE);
////                        imagecacnel.setVisibility(View.GONE);
////                        imagesview.setVisibility(View.GONE);
////                    } else {
////                        deleteditemALselectedImage(sliderLayout.getCurrentPosition());
////                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
////                    }
////                }catch (Exception e){
////                    Log.e("sliderLayout", e.toString());
////                }
////            }
////        });
//
//    }

    public void showImagesZoom(){
        View view = LayoutInflater.from(activity).inflate(R.layout.imageszoom, null);
        ViewPager pinchtozoom_pager = (ViewPager) view.findViewById(R.id.pinchtozoom_pager);
        ImageView iv_iz_close = (ImageView) view.findViewById(R.id.iv_iz_close);
//        ImageViewPagerPinchToZoomAdapter imageViewPagerPinchToZoomAdapter = new ImageViewPagerPinchToZoomAdapter(activity.ALselectedImage,urlString);
        pinchtozoom_pager.setOffscreenPageLimit(3);
//        pinchtozoom_pager.setAdapter(imageViewPagerPinchToZoomAdapter);
        iv_iz_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ad.isShowing()){
                    ad.dismiss();}
            }
        });
        ad = WidgetHelper.showListReference(ad,view,activity);
    }

    public void setContent(String  _status){
//        setupimageslide();
        List<String> setAttr = new ArrayList<>();
        Log.e("status Content",_status);
        setAttr = GlobalHelper.attributesHGU();
//        if(_status.equals("Baik")){
//            setAttr = GlobalHelper.attributesHGUB();
//        } else if (_status.equals("Rusak")) {
//            setAttr = GlobalHelper.attributesHGU();
//        }else if (_status.equals("Hilang")) {
//            setAttr = GlobalHelper.attributesHGU();
//        }
        viewGroup.removeAllViews();
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        Map<String, Object> attr = mData.getAttributes();
//        Set<String> keys = attr.keySet();
//        List<String> statusList = GlobalHelper.getStatusObject();
//        for (String _attr : setAttr){
//            for (String key : keys) {
//                Log.e("debug key true",key );
//                Field.Type typeObject = mData.getFeatureTable().getField(key).getFieldType();
//                List<String> listAdapter = new ArrayList<>();
//                if (_attr.equals(key)){
//                    Object value = attr.get(key);
//                    if (value == null){
//                        value = "";
//                    }
////                Log.e("key",key + " | FieldType " + typeObject+ "\n");
//                    if(mData.getFeatureTable().getField(key).getDomain() != null){
//                        CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
//                        List<CodedValue> values =  codedValue.getCodedValues();
//                        for (CodedValue fieldValue : values) {
//                            listAdapter.add(fieldValue.getName());
//                        }
//                        if (key.equals("typePerawatan")){
//                            viewGroup.addView(viewEditCheck(GlobalHelper.getNameAttribute(key), listAdapter,  linearLayout, true));
//                        }
//                        else {
//                            viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key), value, listAdapter, values, linearLayout, true));
//                        }
//                    }
//                    else {
////                    if (){
//                        if(key.equals("surveyor")){
//                            List<String> list= activity.getUserALl();
//                            if(Prefs.getString(Prefs.PELAKSANA)!= null){
//                                viewGroup.addView(viewEditSurveyor(GlobalHelper.getNameAttribute(key),Prefs.getString(Prefs.PELAKSANA),list,linearLayout,true));
//                            }else {
//                                viewGroup.addView(viewEditSurveyor(GlobalHelper.getNameAttribute(key),"",list,linearLayout,true));
//                            }
//                        }
//
//                        else {
//                            if(typeObject== Field.Type.TEXT){
//                                if (key.equals("GroupCompanyName")||key.equals("CompanyCode")||key.equals("ESTNR")||key.equals("Block")){
//                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
//                                }else if(key.equals("QC_remarks")&&value!=null){
//                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,false));
//                                }
//                                else {
//                                    viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key), value, linearLayout, 1,true));
//                                }
//                            }
//                            else if (typeObject == Field.Type.DOUBLE){
//                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true, (float) 0,100, true));
//                            }
//                            else if (typeObject == Field.Type.INTEGER ||typeObject == Field.Type.SHORT){
//                                if (key.equals("flow_dir")){
//                                    viewGroup.addView(viewEditCompass(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true));
//                                }else if (key.equals("month")||key.equals("duration")){
//                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,24,false));
//                                }
//                                else if (key.equals("year")||key.equals("year_budget")){
//                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,2024,false));
//                                }
//                                else {
//                                    viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1,true,1,100,false));
//                                }
//                            }
//                            else if (typeObject == Field.Type.DATE) {
//                                viewGroup.addView(viewEditDate(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
//                            }
//                        }
////                    }
//                    }
//                }
//            }
//        }



    }


    public void showView(int _action){
//        viewGroup.removeAllViews();
        sliderLayout.removeAllSliders();
//        if(mAction== action.show){
//            Log.e("tagShow","true");
//            if(mData.getAttributes().get("image")!=""&&mData.getAttributes().get("image")!=null){
//                String urlFeature = (String) mData.getAttributes().get("image");
//                String[] strings = urlFeature.split(";");
//                if (strings.length>0){
//                    TextSliderView textSliderView = new TextSliderView(activity);
//                    String imgUrl = "https://app.gis-div.com/PSAService/"+strings[0];
//                    urlString = imgUrl;
//                    textSliderView
//                            .image(imgUrl)
//                            .setScaleType(BaseSliderView.ScaleType.Fit);
//                    sliderLayout.addSlider(textSliderView);
//                    sliderLayout.stopAutoCycle();
//                    sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
//                    sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//                    sliderLayout.setCustomAnimation(new DescriptionAnimation());
//                    imagesview.setVisibility(View.VISIBLE);
////                    Glide.with(activity).load(imgUrl).into(sliderLayout);
////                    sliderLayout.setMinimumHeight(100);
////                    sliderLayout.setMinimumWidth(70);
//                }
//            }
//        }
//        else {
////            sliderLayout.setBackground(R.drawable.ic_add_photo);
//
//        }
//        activity.sliderLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                activity.openCam();
//                Log.e("tagShowOpenCam",String.valueOf(_action));
//            }
//        });


//        setContent(_action);
        bottomSheet.setVisibility(View.VISIBLE);
//        if(activity.fabAdd.getVisibility()==View.VISIBLE){
        activity.fabAdd.setVisibility(View.GONE);
        activity.bmb.setVisibility(View.GONE);
//        activity.fab_map.setVisibility(View.GONE);
//        activity.fab_gps.setVisibility(View.GONE);
//        activity.fab_layer.setVisibility(View.GONE);
        activity.base_info.setVisibility(View.GONE);
//        activity.fab_extend.setVisibility(View.GONE);
        activity.footer.setVisibility(View.GONE);
//        activity.fab_versioning.setVisibility(View.GONE);
//        }
//        if(activity.footer.getVisibility()==View.VISIBLE){
//            activity.footer.setVisibility(View.GONE);
//        }

//        startAnimation();
    }

    public void hideView(){
        viewGroup.removeAllViews();
        bottomSheet.setVisibility(View.GONE);
        if(activity.fabAdd.getVisibility()==View.GONE){
            activity.sniper.setVisibility(View.GONE);
            activity.fabAdd.setVisibility(View.VISIBLE);
            activity.bmb.setVisibility(View.VISIBLE);
            nama_tph.setText("");
//            activity.fab_map.setVisibility(View.VISIBLE);
//            activity.fab_gps.setVisibility(View.VISIBLE);
//            activity.fab_layer.setVisibility(View.VISIBLE);
            activity.base_info.setVisibility(View.VISIBLE);
//            activity.fab_extend.setVisibility(View.VISIBLE);
//            activity.fab_versioning.setVisibility(View.VISIBLE);
        }
        if(activity.footer.getVisibility()==View.GONE){
            activity.footer.setVisibility(View.VISIBLE);
        }
    }


    public void startAnimation(){
        bottomSheet.setVisibility(View.VISIBLE);
        int parentHeight = ((View) bottomSheet.getParent()).getHeight();
        bottomSheet.setTranslationY(parentHeight);
        TranslateAnimation animation = new TranslateAnimation(0, 0, parentHeight, 0);
        animation.setDuration(500);
        bottomSheet.startAnimation(animation);

    }
    private View viewEditText(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }



    private View viewEditNumber(String title, Object value, LinearLayout linearLayout, int maxLine, boolean editable, float min, float max, boolean isDouble){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        if (isDouble){
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
        else {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        InputFilter[] filters = {new InputFilterMinMaxNumber(min, max)};
//        if (min > 0 ){
        textInputEditText.setFilters(filters);

        if (!editable){
            textInputLayout.setEnabled(false);
            textInputLayout.setEnabled(false);
        }

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString().trim();
                if(isDouble){
                    if (!input.isEmpty()) {
                        float value = Float.parseFloat(s.toString());
                        if (value < 0.1) {
                            textInputEditText.setError("Minimum value is 0.1");
                        } else {
//                            textInputEditText.setHint("test Hint");
                            // Value is valid, proceed with further processing
                        }}
                }
            }
        });
        return customView;
    }

    private View viewAutoComplete(String title, Object value, List<String> listSpin, List<CodedValue> codedValues, LinearLayout linearLayout, boolean editable){
        Log.e("title", title);
        View customView = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        int selected = 0;

        for(int loop = 0; loop<codedValues.size(); loop++){
            Log.e(codedValues.get(loop).getCode().toString(), value.toString());
            if(value.toString().equals(codedValues.get(loop).getCode().toString())){
                selected = loop;
                Log.e("selected", listSpin.get(selected));
            }
        }
//        View view = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.spin_title);
        textInputLayout.setHint(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,listSpin);
        MaterialAutoCompleteTextView textInputEditText = customView.findViewById(R.id.spin_value);
        if (!editable){
            textInputLayout.setEnabled(false);
            textInputEditText.setEnabled(false);
        }
        textInputEditText.setAdapter(adapter);

        if (title.equals("Kondisi")){
            textInputEditText.setOnItemClickListener((adapterView, view, i, l) -> {
//                Log.e("Perbaikan",getTextInputLayout("Rekomendasi").getEditText().getText().toString());
                if (textInputEditText.getText().toString().equals("Baik")){
//                    getTextInputLayout("Rekomendasi").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                }
                else {
                    getTextInputLayout("Rekomendasi").setVisibility(View.VISIBLE);
                }
            });
        }

        if (title.equals("Rekomendasi")){
//            textInputLayout.setVisibility(View.GONE);
            textInputEditText.setOnItemClickListener((adapterView, view, i, l) -> {
                if (textInputEditText.getText().toString().equals("Perbaikan")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.VISIBLE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                } else if (textInputEditText.getText().toString().equals("Perawatan")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.VISIBLE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                } else if (textInputEditText.getText().toString().equals("Tidak Dipasang")) {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.VISIBLE);
                } else {
                    getTextInputLayout("Tipe Perbaikan").setVisibility(View.GONE);
                    getTextInputLayout("Tipe Perawatan").setVisibility(View.GONE);
                    getTextInputLayout("Alasan").setVisibility(View.GONE);
                }
            });
        }
        if (title.equals("Tipe Perbaikan")||title.equals("Tipe Perawatan")||title.equals("Alasan")){
            textInputLayout.setVisibility(View.GONE);

        }
        if (value != null){
            if (title.equals("condition")){
//                if(value.toString())
//                textInputEditText.setText("Pilih Status",false);
//                textInputEditText.setText(adapter.getItem(selected),false);
            }
            else {
                textInputEditText.setText(adapter.getItem(selected),false);
            }

        }
        return customView;
    }


    private View viewEditDate(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_NULL);
        textInputEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            // Create DatePickerDialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                    (view1, year1, month1, dayOfMonth1) -> {
                        // Update TextInputEditText with selected date
                        String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                        textInputEditText.setText(selectedDate);
                    }, year, month, dayOfMonth);
            // Show DatePickerDialog
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        });
//            textInputEditText.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME);
//            textInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
//                Calendar calendar = Calendar.getInstance();
//                int year = calendar.get(Calendar.YEAR);
//                int month = calendar.get(Calendar.MONTH);
//                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//                // Create DatePickerDialog
//                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
//                        (view1, year1, month1, dayOfMonth1) -> {
//                            // Update TextInputEditText with selected date
//                            String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
//                            textInputEditText.setText(selectedDate);
//                        }, year, month, dayOfMonth);
//                // Show DatePickerDialog
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            });
        return customView;
    }
    public void setup_iv_compas_lock(ImageView iv_compas_lock, Boolean status){
        if(status){
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_stop).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }else{
            iv_compas_lock.setImageDrawable(new
                    IconicsDrawable(activity)
                    .icon(MaterialDesignIconic.Icon.gmi_compass).sizeDp(64)
                    .paddingDp(8)
                    .colorRes(R.color.Gray));
        }
    }


    public TextInputLayout getTextInputLayout(String param){
        TextInputLayout tip = null;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childView = viewGroup.getChildAt(i);
            LinearLayout ll = (LinearLayout) childView;
            for (int l = 0; l < ll.getChildCount(); l++) {
                View child = ll.getChildAt(l);
                if (child instanceof TextInputLayout){
                    TextInputLayout textInputLayout = (TextInputLayout) child;
                    if (textInputLayout.getHint().equals(param)){
                        tip =textInputLayout;
                    }
                }
            }
        }
        return tip;
    }

    private void showSearchDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Search");

        // Inflate layout for dialog
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_search, null);
        builder.setView(dialogView);

        // Set up SearchView
        SearchView searchView = dialogView.findViewById(R.id.search_view);
        searchView.setIconifiedByDefault(false); // Show SearchView expanded by default
        searchView.requestFocus(); // Set focus to SearchView
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Handle search query submission
//                performSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Handle search query text change
//                performSearch(newText);
                return true;
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDialogCheckbox(String title, List<String>list, TextInputEditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        String[] itemsArray = list.toArray(new String[0]);
        boolean[] checkedItems = new boolean[itemsArray.length];
        builder.setMultiChoiceItems(itemsArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                StringBuilder selectedItems = new StringBuilder();
                for (int i = 0; i < itemsArray.length; i++) {
                    if (checkedItems[i]) {
                        if (selectedItems.length() > 0) {
                            selectedItems.append(", ");
                        }
                        selectedItems.append(itemsArray[i]);
                    }
                }
                editText.setText(selectedItems.toString());
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle OK button click
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showDialogCheckSearch(String title, List<String>list, TextInputEditText editText){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
//        EditText searchInput = new EditText(activity);
//        searchInput.setHint("Search...");
//        builder.setView(searchInput);
        String[] itemsArray = list.toArray(new String[0]);
        boolean[] checkedItems = new boolean[itemsArray.length];
        builder.setMultiChoiceItems(itemsArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                StringBuilder selectedItems = new StringBuilder();
                for (int i = 0; i < itemsArray.length; i++) {
                    if (checkedItems[i]) {
                        if (selectedItems.length() > 0) {
                            selectedItems.append(", ");
                        }
                        selectedItems.append(itemsArray[i]);
                    }
                }
                editText.setText(selectedItems.toString());
            }
        });

//        searchInput.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // Filter items based on search input
//                List<String> filteredList = new ArrayList<>();
//                for (String item : list) {
//                    if (item.toLowerCase().contains(s.toString().toLowerCase())) {
//                        filteredList.add(item);
//                    }
//                }
//                // Update the dialog with filtered items
//                String[] filteredItems = filteredList.toArray(new String[0]);
//                builder.setMultiChoiceItems(filteredItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//                        // Handle checkbox click
//                    }
//                });
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {}
//        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Handle OK button click
            }
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private View viewEditCheck(String title, List<String> list, LinearLayout linearLayout, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setFocusable(false);
        textInputEditText.setClickable(true); // Enable clickability
        if (title.equals("Tipe Perawatan")){
            textInputLayout.setVisibility(View.GONE);

        }
        if (!editable) {
            textInputLayout.setEnabled(false);
        }

        textInputEditText.setOnClickListener(v -> {
            if (title.equals("Tipe Perawatan")){
                showDialogCheckbox(title, list, textInputEditText);
            }
            else
            {
                showDialogCheckSearch(title, list, textInputEditText);

            }
        });
        return customView;
    }

    private View viewEditSurveyor(String title, String value, List<String> list, LinearLayout linearLayout, boolean editable){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setText(value);
        textInputEditText.setFocusable(false);
        textInputEditText.setClickable(true); // Enable clickability
//        if (title.equals("Tipe Perawatan")){
//            textInputLayout.setVisibility(View.GONE);
//
//        }
        if (!editable) {
            textInputLayout.setEnabled(false);
        }
        textInputEditText.setOnClickListener(v -> {
            showAlertDialogButtonClicked(list);
        });
        return customView;
    }

    public void showAlertDialogButtonClicked(List<String> list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Tim Pelaksana");

        // Inflate the custom layout
        View customLayout = activity.getLayoutInflater().inflate(R.layout.custom_layout, null);
        builder.setView(customLayout);


        // Get references to views
        EditText editText = customLayout.findViewById(R.id.editText);
        ListView listView = customLayout.findViewById(R.id.listView);

        // Initialize the adapter with your data
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                String[] parts = selectedItem.split("-");

                // Split text into an array based on comma (,)
                String[] itemsArray = editText.getText().toString().split(",");

                // Create a StringBuilder to manipulate the text
                StringBuilder newText = new StringBuilder();

                // Iterate through the itemsArray and append each item to the newText
                for (int i = 0; i < itemsArray.length - 1; i++) {
                    newText.append(itemsArray[i]).append(",");
                }

                // Append the new item from the selected list view item
                newText.append(parts[0]).append(",");

                // Set the edited text to the EditText
                editText.setText(newText.toString());
//                String temp;
//                String selectedItem = (String) parent.getItemAtPosition(position);
//                String[] parts = selectedItem.split("-");
//                String[] itemsArray = editText.getText().toString().split(",");
//                if (itemsArray.length == 1) {
//                    // Kembalikan string tersebut sebagai string terakhir
//                    editText.setText(editText.getText()+parts[0]+",");
////                    temp = editText.toString();
//                } else {
//                    temp = itemsArray[itemsArray.length - 1];
//
//                    // Kembalikan elemen terakhir dari array
//                    editText.setText(editText.getText()+parts[0]+",");
////                    value = itemsArray[itemsArray.length - 1];
//                }

                // Set selected item to EditText
            }
        });
        // Add a text change listener to filter the list based on input text
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value;
                String[] itemsArray = s.toString().split(",");
                if (itemsArray.length == 1) {
                    // Kembalikan string tersebut sebagai string terakhir
                    value = s.toString();
                } else {
                    // Kembalikan elemen terakhir dari array
                    value = itemsArray[itemsArray.length - 1];
                }
                // ArrayAdapter<String> filteredAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, filteredArray);
//                listView.setAdapter(filteredAdapter);
                adapter.getFilter().filter(value); // Apply filter to adapter
                listView.setVisibility(View.VISIBLE); // Show the ListView
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Set positive button click listener
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getTextInputLayout("Tim Pelaksana").getEditText().setText(editText.getText().toString());
                Prefs.setPrefs().putString(Prefs.PELAKSANA,editText.getText().toString()).apply();
                // Handle OK button click
                // You can get selected item from the adapter if needed
            }
        });

        // Create and show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
