package id.co.ams_plantation.plantation_survey_app.utils;

import static id.co.ams_plantation.plantation_survey_app.PSAApp.CONNECTION_PREF;
import static id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper.getDatabasePath;
//import static id.co.ams_plantation.plantation_survey_app.utils.KmlHelper.convertStreamToString;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;


import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;


import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.UUID;

import cat.ereza.customactivityoncrash.model.DeviceInfo;
import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.data.AppDatabase;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.UserAppUsage;
import id.co.ams_plantation.plantation_survey_app.data.view_model.AssetViewModel;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;


public class ActivityLoggerHelper {
    private static final String TAG = ActivityLoggerHelper.class.getSimpleName();

    private String className="";

    private String androidId;
    private String idAndroid;
    private String elementId = "";
    private static AssetViewModel mAssetViewModel;
    protected static AppDatabase appDb;


//    static Nitrite db = null;

    public static void newInstanceDB(){
//        db = GlobalVars.getTableNitrit(GlobalVars.CONTAINER_LOG_ACTIVITY);
    }

    public  static void recordScreenTime(String className, Date startTime, Date endTime, ViewModelStoreOwner owner){
        try{
            String androidId = null;
            String idAndroid = null;
            if (getAdminAppsAndroidID()!= null){
                String[] fileInfoApp = getAdminAppsAndroidID().split(";");

                if(fileInfoApp.length>1){
                    androidId = fileInfoApp[0];
                    idAndroid = fileInfoApp[1];
                }else{
                    androidId = fileInfoApp[0];
                    idAndroid=null;
                }
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";
            if(isPublic()){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }
            Date currentDate = new Date();
            if(GlobalHelper.getUser()!=null){
                PackageManager manager = PSAApp.getContext().getPackageManager();
                PackageInfo info = manager.getPackageInfo(
                        PSAApp.getContext().getPackageName(), 0);
                String version = info.versionName;
//                DeviceInfo deviceInfo = AppUtils.getDeviceInfo(PSAApp.getContext());
                UserAppUsage userAppUsage = new UserAppUsage(
                        GlobalHelper.getUser().getUserID(),
                        "PSA",
                        info.packageName,
                        UserAppUsage.REQUEST_DATA,
                        "Android",
                        MapInventoryActivity.class.getSimpleName(),
                        className,
                        connectionInfo,
                        getIPAddress(),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        androidId,
                        idAndroid,
                        version,
                        sdf.format(new Date()));
                insertToLocal(userAppUsage,owner);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean isPublic(){
        SharedPreferences preferences = PSAApp.getContext().getSharedPreferences(CONNECTION_PREF,
                Context.MODE_PRIVATE);
        if(preferences.contains(CONNECTION_PREF)){
            if(preferences.getBoolean(CONNECTION_PREF,false))
                return true;
        }
        return false;
    }
    public static void recordHitTime(String className, String elementId, Date startTime, Date endTime,ViewModelStoreOwner owner){
        try{

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";
            if(!isPublic()){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }
            if(GlobalHelper.getUser()!=null) {
                PackageManager manager = PSAApp.getContext().getPackageManager();
                PackageInfo info = manager.getPackageInfo(
                        PSAApp.getContext().getPackageName(), 0);
                String version = info.versionName;
                String androidId = null;
                String idAndroid = null;
                if (getAdminAppsAndroidID()!= null){
                    String[] fileInfoApp = getAdminAppsAndroidID().split(";");

                    if(fileInfoApp.length>1){
                        androidId = fileInfoApp[0];
                        idAndroid = fileInfoApp[1];
                    }else{
                        androidId = fileInfoApp[0];
                        idAndroid=null;
                    }
                }

//                DeviceInfo deviceInfo = GlobalHelper.getDeviceInfo(PSAApp.getContext());
                UserAppUsage userAppUsage = new UserAppUsage(
                        GlobalHelper.getUser().getUserID(),
                        "PSA",
                        info.packageName,
                        UserAppUsage.HIT,
                        "Android",
                        MapInventoryActivity.class.getSimpleName(),
                        elementId,
                        connectionInfo,
                        getIPAddress(),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        androidId,
                        idAndroid,
                        version,
                        sdf.format(new Date()));
                insertToLocal(userAppUsage, owner);
            }
        }catch (Exception e){
            Log.e("errUAL",e.toString());
            e.printStackTrace();
        }

    }

    public static void recordRequestData(String className, String elementId, Date startTime, Date endTime, ViewModelStoreOwner owner){
        try{
            Date currentDate = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";
            if(isPublic()){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }
            String androidId = null;
            String idAndroid = null;
            if (getAdminAppsAndroidID()!= null){
                String[] fileInfoApp = getAdminAppsAndroidID().split(";");

                if(fileInfoApp.length>1){
                    androidId = fileInfoApp[0];
                    idAndroid = fileInfoApp[1];
                }else{
                    androidId = fileInfoApp[0];
                    idAndroid=null;
                }
            }
            if(GlobalHelper.getUser()!=null) {
//                DeviceInfo deviceInfo = PSAApp.getContext().getPackageManager();
                PackageManager manager = PSAApp.getContext().getPackageManager();
                PackageInfo info = manager.getPackageInfo(
                        PSAApp.getContext().getPackageName(), 0);
                String version = info.versionName;
                UserAppUsage userAppUsage = new UserAppUsage(
                        GlobalHelper.getUser().getUserID(),
                        "PSA",
                        info.packageName,
                        UserAppUsage.HIT,
                        "Android",
                        MapInventoryActivity.class.getSimpleName(),
                        elementId,
                        connectionInfo,
                        getIPAddress(),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        androidId,
                        idAndroid,
                        version,
                        sdf.format(new Date()));
                insertToLocal(userAppUsage,owner);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getCurrentDateString() {
        long currentTimeMillis = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy h:mm:ss a", Locale.getDefault());
        return sdf.format(new Date(currentTimeMillis));
    }



    public static boolean insertToLocal(UserAppUsage userAppUsage, ViewModelStoreOwner owner){
        mAssetViewModel = new ViewModelProvider(owner).get(AssetViewModel.class);
        appDb = AppDatabase.getDatabase(PSAApp.getContext());
        mAssetViewModel.insertUal(userAppUsage);
        return true;
    }



    public static String getAdminAppsAndroidID(){
        String androidId = null;
        String fileName ="deviceName";
        try{
            File root = new File(getDatabasePath(), Encrypts.encrypt(fileName));
            FileInputStream fin = new FileInputStream(root);
            androidId = convertStreamToString(fin);
            //Make sure you close all streams.
            fin.close();
        }catch (Exception e){
            e.printStackTrace();
        }
//        Log.i(TAG, "getAdminAppsAndroidID: androidID: "+androidId);
        return androidId;
    }

    public static String getIPAddress() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress.getHostAddress() != null) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Unknown";
    }
    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }
}
