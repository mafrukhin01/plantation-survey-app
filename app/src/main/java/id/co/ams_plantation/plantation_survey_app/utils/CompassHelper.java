package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

public class CompassHelper implements SensorEventListener {
	private static final String TAG = "CompassHelper";

	private SensorManager sensorManager;
	private Sensor gsensor;
	private Sensor msensor;
	private float[] mGravity = new float[3];
	private float[] mGeomagnetic = new float[3];
	private float azimuth = 0f;
	private float currectAzimuth = 0;

	// compass arrow to rotate
	public ImageView arrowView = null;
	public EditText tv_degere = null;
	public TextInputEditText tv_degere2 = null;
	public TextView tvArahCompassCardinal = null;
	public TextView tvArahCompass = null;
	public CompassHelper(Context context) {
		sensorManager = (SensorManager) context
				.getSystemService(Context.SENSOR_SERVICE);
		gsensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		msensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	}

	public void start() {
		sensorManager.registerListener(this, gsensor,
				SensorManager.SENSOR_DELAY_GAME);
		sensorManager.registerListener(this, msensor,
				SensorManager.SENSOR_DELAY_GAME);
	}

	public void stop() {
		sensorManager.unregisterListener(this);
	}

	private void adjustArrow() {
		if (arrowView == null) {
			Log.i(TAG, "arrow view is not set");
			return;
		}

		Log.i(TAG, "will set rotation from " + currectAzimuth + " to "
				+ azimuth);

		Animation an = new RotateAnimation(-currectAzimuth, -azimuth,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		currectAzimuth = azimuth;

		an.setDuration(500);
		an.setRepeatCount(0);
		an.setFillAfter(true);
//		tv_degere.setText(String.format("%.0f",currectAzimuth));
		tv_degere2.setText(String.format("%.0f",currectAzimuth));
		tvArahCompassCardinal.setText(String.format("%.0f°",currectAzimuth));
//		tvArahCompassCardinal.setText(bearingClassification(currectAzimuth));
//		tvArahCompass.setText(String.format("%.0f°",currectAzimuth));
		arrowView.startAnimation(an);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		final float alpha = 0.97f;

		synchronized (this) {
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

				mGravity[0] = alpha * mGravity[0] + (1 - alpha)
						* event.values[0];
				mGravity[1] = alpha * mGravity[1] + (1 - alpha)
						* event.values[1];
				mGravity[2] = alpha * mGravity[2] + (1 - alpha)
						* event.values[2];

				// mGravity = event.values;

				// Log.e(TAG, Float.toString(mGravity[0]));
			}

			if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				// mGeomagnetic = event.values;

				mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha)
						* event.values[0];
				mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha)
						* event.values[1];
				mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha)
						* event.values[2];
				// Log.e(TAG, Float.toString(event.values[0]));

			}

			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
					mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);
				// Log.d(TAG, "azimuth (rad): " + azimuth);
				azimuth = (float) Math.toDegrees(orientation[0]); // orientation
				azimuth = (azimuth + 360) % 360;
				// Log.d(TAG, "azimuth (deg): " + azimuth);
				adjustArrow();
			}
		}
	}
	public String bearingClassification(double bearing){
		double n = bearing;
		if( n==360 || n>=0 && n<22.5){
			return "N";
		}else if( n>=22.5 && n<45){
			return "NNE";
		}else if( n>=45 && n<67.5){
			return "NE";
		}else if( n>=67.5 && n<90){
			return "ENE";
		}else if( n>=90 && n<112.5){
			return "E";
		}else if( n>=112.5 && n<135){
			return "ESE";
		}else if( n>=135 && n<157.5){
			return "SE";
		}else if( n>=157.5 && n<180){
			return "SSE";
		}else if( n>=180 && n<202.5){
			return "S";
		}else if( n>=202.5 && n<225){
			return "SSW";
		}else if( n>=225 && n<247.5){
			return "SW";
		}else if( n>=247.5 && n<270){
			return "WSW";
		}else if( n>=270 && n<292.5){
			return "W";
		}else if( n>=292.5 && n<315){
			return "WNW";
		}else if( n>=315 && n<337.5){
			return "NW";
		}else if(n >=337.5 && n<360){
			return "NNW";
		}
		return "N";
	}
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}
}
