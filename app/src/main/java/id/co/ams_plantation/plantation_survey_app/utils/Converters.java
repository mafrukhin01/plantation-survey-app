package id.co.ams_plantation.plantation_survey_app.utils;

import androidx.room.TypeConverter;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Mafrukhin on 22/04/2022.
 */
public class Converters {
    @TypeConverter
    public static Date toDate(long date) {
        return new Date(date);
    }

    @TypeConverter
    public static long fromDate(Date date) {
        return date == null ? Calendar.getInstance().getTimeInMillis() : date.getTime();
    }

    @TypeConverter
    public String fromList(List<String> list) {
        if (list == null) {
            return null;
        }
        return String.join(";", list);
    }

    @TypeConverter
    public List<String> fromString(String value) {
        if (value == null) {
            return null;
        }
        return Arrays.asList(value.split(";"));
    }
}
