package id.co.ams_plantation.plantation_survey_app.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTime {
    public static String getStringDate(){
        return new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }
    public static String getStringDate2(){
        return new SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }
    public static String getStringTime(){
        return new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }

    public static String getStringLongDate(){
        return  String.valueOf(Calendar.getInstance().getTimeInMillis());
    }
    public static String getTimeStamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }

    public static String getDateForId(){
        return new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }

    public static String getDate(){
        return new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Calendar.getInstance().getTimeInMillis());
    }

    public static String toStringDate(Date date){
        return new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(date);
    }

    public static String toStringTime(Date date){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(date);
    }

    public static String toStringTimeTemplate(Date date){
        return new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(date);
    }

    public static String toStringTemplate(Date date){
        return new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(date);
    }

    public static String toStringTemplate(long date){
        Date a = new Date(date);
        return new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(a);
    }

    public static Date longToDate(long input){
        return new Date(input);
    }

    public static Calendar timeStampCalendar(){
        Calendar c = Calendar.getInstance();
//        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        TimeZone t = TimeZone.getDefault();
        c.setTimeZone(t);
        return c;
    }
}
