package id.co.ams_plantation.plantation_survey_app.utils;

import static id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper.EXTERNAL_DIR_FILES;
import static id.co.ams_plantation.plantation_survey_app.utils.GlobalHelper.encryptString;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.services.model.AppInfo;
import id.co.ams_plantation.plantation_survey_app.services.model.FileTypeUrlMapping;
import id.co.ams_plantation.plantation_survey_app.services.model.Maps;


public class DownloadMapHelper {
    Context context;
    String TAG =  this.getClass().getName();
    public static final String CONNECTION_PREF = "CONNECTION_PREF";


    public static boolean isPublic(){
        SharedPreferences preferences = PSAApp.getContext().getSharedPreferences(CONNECTION_PREF,
                Context.MODE_PRIVATE);
        if(preferences.contains(CONNECTION_PREF)){
            if(preferences.getBoolean(CONNECTION_PREF,false))
                return true;
        }
        return false;
    }

    public static String getUri(AppInfo appInfo, FileTypeUrlMapping fileTypeUrlMapping){
        String tmpEst = appInfo.getMaps().getEstCode().length()>0 ? appInfo.getMaps().getEstCode() : "";
        String tmpDiv = appInfo.getMaps().getDivisiId().length()>0 ? appInfo.getMaps().getDivisiId() : "";
        String tmpBlock = appInfo.getMaps().getBlockId().length()>0 ? appInfo.getMaps().getBlockId() : "";

        return fileTypeUrlMapping.getUrl()
                + "/"+  appInfo.getMaps().getCompanyCode()
                + (tmpEst.length() > 0 ? "/" + tmpEst : "")
                + (tmpDiv.length() > 0 ? "/" + tmpDiv : "")
                + (tmpBlock.length() > 0 ? "/" +tmpBlock : "")
                + "/" + appInfo.getMaps().getFilename();
    }

    public File getFile(Maps maps){
        String tmpEst = maps.getEstCode().length()>0 ? maps.getEstCode() : "";
        String tmpDiv = maps.getDivisiId().length()>0 ? maps.getDivisiId() : "";
        String tmpBlock = maps.getBlockId().length()>0 ? maps.getBlockId() : "";

//        String fileURI =  maps.getFileType()
//                + "/"+  maps.getCompanyCode()
//                + tmpEst
//                + tmpDiv
//                + tmpBlock
//                + "/" + maps.getFilename();
//        File file = new File(Environment.getExternalStorageDirectory()+GlobalVars.EXTERNAL_DIR_FILES,fileURI);
        File file = new File(getFilePath(
                maps.getFileType(),
                tmpEst,
                maps.getCompanyCode(),
                tmpDiv,
                tmpBlock),
                encryptString(maps.getFilename()));
        return file;
    }

    public static String getFilePath(Maps maps){
        String tmpEst = maps.getEstCode().length()>0 ? maps.getEstCode() : "";
        String tmpDiv = maps.getDivisiId().length()>0 ? maps.getDivisiId() : "";
        String tmpBlock = maps.getBlockId().length()>0 ? maps.getBlockId() : "";

        return getFilePath(
                maps.getFileType(),
                tmpEst,
                maps.getCompanyCode(),
                tmpDiv,
                tmpBlock);
    }

    public static String getFilePath(String type,String estCode,
                                     String companyCode,
                                     String divCode,
                                     String BlockId){
        String initialPath = Environment.getExternalStorageDirectory() +
                EXTERNAL_DIR_FILES + "/"+encryptString(type) +"/"+
                encryptString(companyCode) +
                (estCode.length()>0 ? "/" + encryptString(estCode) : "") +
                (divCode.length()>0 ? "/" + encryptString(divCode) : "") +
                (BlockId.length()>0 ? "/" + encryptString(BlockId) : "") ;
        Log.d("Dir",initialPath);
        return initialPath;
    }

    public static String allCapsConverter(String string){
        String allString="";
        for (String word:string.split(" ")) {
            StringBuilder sb = new StringBuilder(word);
            for(int index=1; index<sb.length();index++){
                char c = sb.charAt(index);
                if(!Character.isLowerCase(c)){
                    sb.setCharAt(index,Character.toLowerCase(c));
                }
            }
            allString += sb.toString() + " ";
        }
        return allString;
    }

    public static SpannableString fileSizeShorter(String fileSize, boolean withSuffix){
        DecimalFormat decimalFormat = new DecimalFormat("#,###.#");
        Double fileSizeLong = Double.parseDouble(fileSize);
        Double finalSize=0.0;
        String suffix = "B";
        if(fileSizeLong >= 1024){
            if(fileSizeLong>= 1024*1024){
                if(fileSizeLong >=1024*1024*1024){
                    finalSize = fileSizeLong / (1024*1024*1024);
                    suffix = "GB";
                }else{
                    finalSize = fileSizeLong / (1024*1024);
                    suffix = "MB";
                }
            }else{
                finalSize = fileSizeLong / 1024;
                suffix = "KB";
            }
        }else{
            finalSize = fileSizeLong;
            suffix = "B";
        }
        if(withSuffix){
            SpannableString string = new SpannableString(decimalFormat.format(finalSize) + suffix);
            switch (suffix){
                case "GB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#00B0FF")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "MB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#FFA500")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "KB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#7C4DFF")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "B":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#FF0000")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;

            }
            return string;
        }else{
            SpannableString string = new SpannableString(decimalFormat.format(finalSize));
            return string;
        }

    }

    public static String versionRead(long epochTime){
        Date date = new Date(epochTime);
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
        return format.format(date);
    }
}
