package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.FeatureQueryResult;
import com.esri.arcgisruntime.data.FeatureTable;
import com.esri.arcgisruntime.data.QueryParameters;
import com.esri.arcgisruntime.geometry.GeometryEngine;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.Polygon;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.layers.Layer;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.mobapphome.mahencryptorlib.MAHEncryptor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.zip.GZIPInputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.BudgetAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.BudgetLayer;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Estate;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EstateFullName;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Images;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.Optionlayer;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.User;


public class GlobalHelper {
    public static final String SELECTED_USER = "User";
    public static final String SELECTED_ESTATE = "SelEstate";
    public static final String PASSWORD_ENCRYPT = "KopiNikmatGakBikinKembung";
    public static final String ESTATE_FULL_NAME = "EstateFullName";
    public static final String MODULE_NAME_AWM = "Asset Water Management";
    public static final String EXTERNAL_DIR_FILES = "/AMSApp/files";
//    public static final String EXTERNAL_DIR_FILES = "/AMSApp/files";
    public static final String PROJECT_NAME = "/PSA";
    public static final String FOLEDRFOTOTEMP = "/Images/Temp";
    public static final String FOLDER_PHOTO_TPH ="/Images/TPH";

    public static final String SQLITE_LOCATION = "/AMSApp/files/PSA/db/";
    public static final String GEODATABASE_LOCATION = "/AMSApp/files/PSA/geodatabase/";
    public static final String FILE_INVENTORY = "/file_inventory.geodatabase";
    public static final String FILE_HGU = "/file_hgu.geodatabase";
    public static final String MMPK_LOCATION = "/AMSApp/files/PSA/vtpk/";
    public static final String THUMBNAIL_LOCATION = "/AMSApp/files/PSA/db2/";
    public static final String GIS_ADMIN_APPS = "id.co.ams_plantation.amsadminapps";
    public static final String REPORT_LOCATION = "/GAMAFiles/report/PSA/";
    public static final String PATH_SELMOD_FILE = Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES +"/db/ModFile";
    public static final int NOTIF_TOKEN_DAYS = 7;
    public static final String FONT_CORBERT = "CorbertCondensed-Regular.otf";
    public static final int TYPE_2DMAP = 1;
    public static final int TYPE_KMLBLOCK = 2;
    public static final int TYPE_TPK = 3;
    public static final int TYPE_VKM = 4;
    public static final int TYPE_GEODATABASE = 5;
    public static final int TYPE_VTPK = 6;
    public static final int STANDARD_DISTANCE = 10;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    public static String getDatabasePath(){
//        Log.e("Storage", Environment.getDataDirectory() + EXTERNAL_DIR_FILES +"/");
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/";
    }
    public static String getFileContent(String fileName){
        File file = new File(getDatabasePath(),fileName);
        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine()) !=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFileContent2(String fileName){
        Log.i(GlobalHelper.class.getSimpleName(), "getFileContent2: fileName:"+fileName);
        File file = new File(fileName);
        try {
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine()) !=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isFileContentAvailable(String fileName){
        File file = new File(getDatabasePath(),fileName);
        return file.exists();
    }
    public static User getUser(){
        Gson gson = new Gson();
//        Log.e("Encryp", Encrypts.encrypt(SELECTED_USER));
        if(isFileContentAvailable(Encrypts.encrypt(SELECTED_USER))){
            User user = gson.fromJson(
                    Encrypts.decrypt(getFileContent(Encrypts.encrypt(SELECTED_USER))),
                    User.class);
//            Log.e("User",gson.toJson(user));
            return user;
        }
        return null;
    }
    public static Estate getEstate(){
        Gson gson = new Gson();
        if(isFileContentAvailable(Encrypts.encrypt(SELECTED_ESTATE))){
            Estate estate = gson.fromJson(
                    Encrypts.decrypt(getFileContent(Encrypts.encrypt(SELECTED_ESTATE))),
                    Estate.class);
//            Log.e("Estate",gson.toJson(estate));
            return estate;
        }
        return null;
    }
    public static ArrayList<EstateFullName> getCompanies(){
        ArrayList<EstateFullName> estateFullNames = new ArrayList<>();
        Gson gson = new Gson();
        if(isFileContentAvailable(Encrypts.encrypt(ESTATE_FULL_NAME))){
            try {
                JSONArray array = new JSONArray(Encrypts.decrypt(getFileContent(Encrypts.encrypt(ESTATE_FULL_NAME))));
                for(int i=0;i<array.length();i++){
                    EstateFullName estateFullName = gson.fromJson(array.getJSONObject(i).toString(),
                            EstateFullName.class);
                    estateFullNames.add(estateFullName);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return estateFullNames;
    }
    public static boolean isPackageAvailable(String packagename, PackageManager packageManager){
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String readFileContent(String path){
        try{
            File root = new File(path);
            File file = new File(root.getParentFile(),Encrypts.encrypt(root.getName()));

            FileInputStream fis = new FileInputStream(file);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line=bufferedReader.readLine())!=null){
                sb.append(line);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
//            Toast.makeText(MainActivity.getContext(),
//                    e.toString(), Toast.LENGTH_SHORT);
            Log.i("Toast",e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Toast",e.toString());
//            Toast.makeText(PZOApp.getContext(),
//                    e.toString(), Toast.LENGTH_SHORT);
        }
        return null;
    }

    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            Log.e("font", customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            Log.e("Can not set custom font", customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
        }
    }

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return Settings.System.getInt(c.getContentResolver(), Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static String decryptFiles(int type){
        String encryptedFilePath ="";
        switch (type){
            case TYPE_TPK:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_TPK,GlobalHelper.getEstate());
                break;
            case TYPE_VKM:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_VKM,GlobalHelper.getEstate());
                break;
            case TYPE_KMLBLOCK:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_KMLBLOCK,GlobalHelper.getEstate());
                break;
            case TYPE_GEODATABASE:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_GEODATABASE,GlobalHelper.getEstate());
                break;
            case TYPE_VTPK:
                encryptedFilePath = GlobalHelper.getFilePath(TYPE_VTPK,GlobalHelper.getEstate());
                break;
        }
        File initialPath = new File(encryptedFilePath);
        Log.e("Initial Path",initialPath.getAbsolutePath());
        if(initialPath.exists()){
            File returnedPath = new File(initialPath.getParentFile().getAbsolutePath(),decryptString(initialPath.getName()));
            initialPath.renameTo(returnedPath);
            return returnedPath.getAbsolutePath();
        }else{
            File returnedPath = new File(initialPath.getParentFile().getAbsolutePath(),decryptString(initialPath.getName()));
            return returnedPath.getAbsolutePath();
        }
    }

    public static String getFilePath(int type, Estate estate){
        String typePath="";
        String typePrefix="";
        String typeExtension="";
        String companyStr="";
        switch (type){
            case TYPE_VKM:
                typePath = "VECTOR_KEBUN_MAP";
                typePrefix = "VKM";
                typeExtension = ".tpk";
                break;
            case TYPE_2DMAP:
                typePath = "2D_MAP";
                typePrefix = "2DM";
                typeExtension = ".kmz";
                break;
            case TYPE_KMLBLOCK:
                typePath = "KML_BLOCK";
                typePrefix = "BLOCK";
                typeExtension = ".kmz";
                break;
            case TYPE_TPK:
                typePath = "FOTO_UDARA_MAP";
                typePrefix = "FUM";
                typeExtension = ".tpk";
                break;
            case TYPE_GEODATABASE:
                typePath = "GEODATABASE";
                typePrefix = "OPL1";
                typeExtension = ".geodatabase";
                break;
            case TYPE_VTPK:
                typePath = "VTPK";
                typePrefix = "VTPK";
                typeExtension = ".vtpk";
                break;
        }
        if (estate.getCompanyShortName().toLowerCase().contains("thip")){
            companyStr = "PT.THIP";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panpl") || estate.getCompanyShortName().toLowerCase().contains("panp-l")){
            companyStr = "PT.PANPL";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panps") || estate.getCompanyShortName().toLowerCase().contains("panp-s")){
            companyStr = "PT.PANPS";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panpp") || estate.getCompanyShortName().toLowerCase().contains("panp-p")){
            companyStr = "PT.PANPP";
        }else if (estate.getCompanyShortName().toLowerCase().contains("panp")){
            companyStr = "PT.PNP";
        }else{
            companyStr = "PT."+estate.getCompanyShortName();
        }
        String filePath = null;
        if(type==TYPE_TPK){
            File folderTPK = new File(Environment.getExternalStorageDirectory()
                    + EXTERNAL_DIR_FILES
                    + "/" + encryptString(typePath) + "/"
                    + encryptString(companyStr) + "/"
                    + encryptString(estate.getEstCode()) + "/");
            ArrayList<File> arrayListFile = new ArrayList<>();
            if(folderTPK.exists()){
                for(File file:folderTPK.listFiles()){
                    arrayListFile.add(file);
                }
            }

            Collections.sort(arrayListFile, new Comparator<File>() {
                @Override
                public int compare(File o1, File o2) {
                    return o2.getName().compareToIgnoreCase(o1.getName());
                }
            });

            if(arrayListFile.size()>0){
                filePath = arrayListFile.get(0).getAbsolutePath();
                if(arrayListFile.get(0).getName().contains("FUM_"+GlobalHelper.getEstate().getEstCode())){
                    File file = new File(arrayListFile.get(0).getParentFile().getAbsolutePath(),
                            encryptString(arrayListFile.get(0).getName()));
                    arrayListFile.get(0).renameTo(file);
                    filePath = file.getAbsolutePath();
                }
            }else{
                filePath = Environment.getExternalStorageDirectory()
                        + EXTERNAL_DIR_FILES
                        + "/" + encryptString(typePath) + "/"
                        + encryptString(companyStr) + "/"
                        + encryptString(estate.getEstCode()) + "/"
                        + encryptString(typePrefix + "_" +estate.getEstCode() + typeExtension);
            }
        }else{
            filePath = Environment.getExternalStorageDirectory()
                    + EXTERNAL_DIR_FILES
                    + "/" + encryptString(typePath) + "/"
                    + encryptString(companyStr) + "/"
                    + encryptString(estate.getEstCode()) + "/"
                    + encryptString(typePrefix + "_" +estate.getEstCode() + typeExtension);
        }

        return filePath;
    }


    public static String encryptString(String encString){
        try {

            MAHEncryptor encryptor = MAHEncryptor.newInstance(PASSWORD_ENCRYPT);
            String encrypted = encryptor.encode(encString);
            String urlSafe = URLEncoder.encode(encrypted,"UTF-8");
            Log.e("URL ENCODE",urlSafe);
            return urlSafe;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decryptString(String decString){
        try {
            String urlSafe = URLDecoder.decode(decString,"UTF-8");
            MAHEncryptor encryptor = MAHEncryptor.newInstance(PASSWORD_ENCRYPT);
            String decrypt = encryptor.decode(urlSafe);
            Log.e("urlSafe",urlSafe);
            return decrypt;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return decString;
    }

    public static String getAndAssignBlock(File kmlLoc, double lat, double lon){
        String block = "";
        if(kmlLoc.exists()) {
            HashMap<String, ArrayList<Polygon>> polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
            Point point = new Point(lat, lon, SpatialReferences.getWgs84());
            for (String str : polygonCollection.keySet()) {
                ArrayList<Polygon> polCollection = polygonCollection.get(str);
                for (Polygon pol : polCollection) {
                    if (GeometryEngine.within(point, pol)) {
                        block = str;
                        break;
                    }
                }
            }
        }else{
            Toast.makeText(PSAApp.getContext(),"KML Tidak Berhasil di baca",Toast.LENGTH_LONG).show();
        }
        return block;
    }

    public static ArrayList<String> getAllBlock(File kmlLoc){
        String block = "";
        ArrayList<String> strKeySet = new ArrayList<>();
        if(kmlLoc.exists()) {
            HashMap<String, ArrayList<Polygon>> polygonCollection = KmlHelper.getLineOfPlacemarks(kmlLoc);
//            Point point = new Point(lat, lon, SpatialReferences.getWgs84());
            for (String str : polygonCollection.keySet()) {
                strKeySet.add(str);
                Log.e("KML",str.toString());
//                ArrayList<Polygon> polCollection = polygonCollection.get(str);
//                for (Polygon pol : polCollection) {
//                    Log.e("Polygon",str);
//                    if (GeometryEngine.within(pol,pol)) {
//                        block = str;
//                        break;
//                    }
//                }
            }
        }else{
            Toast.makeText(PSAApp.getContext(),"KML Tidak Berhasil di baca",Toast.LENGTH_LONG).show();
        }
        return strKeySet;
    }

    public static File  moveFilePhoto(File file, File dir, String name, double lat, double lon, String date, String desc) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."));
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        SimpleDateFormat sdfExif = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        ExifInterface exif = new ExifInterface(newFile.getAbsolutePath());
        exif.setAttribute(ExifInterface.TAG_DATETIME, sdfExif.format(System.currentTimeMillis()));
        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GpsCamera.latitudeRef(lat));
        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GpsCamera.latitudeRef(lat));
        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GpsCamera.convert(lon));
        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GpsCamera.longitudeRef(lon));
        exif.setAttribute(ExifInterface.TAG_DATETIME_DIGITIZED, sdfExif.format(System.currentTimeMillis()));
        exif.setAttribute(ExifInterface.TAG_DATETIME_ORIGINAL, sdfExif.format(System.currentTimeMillis()));
        exif.setAttribute(ExifInterface.TAG_USER_COMMENT,date);
        exif.setAttribute(ExifInterface.TAG_IMAGE_DESCRIPTION,desc);
        exif.saveAttributes();

//        Images image = new Images(newFile.getName(),alias,newFile.getAbsolutePath(),"1",Calendar.getInstance().getTime());
        Log.e("Saving image ", "true");
//        mImagesViewModel = new ViewModelProvider(owner).get(ImagesViewModel.class);
//        mImagesViewModel.insert(image);
        GlobalHelper.getMetaDataFromImages(newFile.getAbsolutePath());
        return newFile;
    }

    public static File  moveFile2(File file, File dir, String name, double lat, double lon, ViewModelStoreOwner owner, String alias) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."));
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        SimpleDateFormat sdfExif = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        ExifInterface exif = new ExifInterface(newFile.getAbsolutePath());
        exif.setAttribute(ExifInterface.TAG_DATETIME, sdfExif.format(System.currentTimeMillis()));
        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, String.valueOf(lat));
        exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, String.valueOf(lat));
        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, String.valueOf(lon));
        exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, String.valueOf(lon));
        exif.setAttribute(ExifInterface.TAG_DATETIME_DIGITIZED, sdfExif.format(System.currentTimeMillis()));
        exif.setAttribute(ExifInterface.TAG_DATETIME_ORIGINAL, sdfExif.format(System.currentTimeMillis()));
        exif.saveAttributes();
        Images image = new Images(newFile.getName(),alias,newFile.getAbsolutePath(),"1",Calendar.getInstance().getTime());
        Log.e("Saving image ", "true");
        GlobalHelper.getMetaDataFromImages(newFile.getAbsolutePath());
        return newFile;
    }

    public static File moveFile(File file, File dir, String name) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."), file.getName().length());
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
//
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        return newFile;
    }

    public static File copyFile(File file, File dir,
                                String name) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."), file.getName().length());
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
//
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
//            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        return newFile;
    }

    public static File movePhoto(File file, File dir, String name) throws IOException {
        String ext = "";
        try {
            ext = file.getName().substring(
                    file.getName().lastIndexOf("."));
        }catch (Exception e){
            Log.e("extension tidak ada " , e.toString());
        }
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        File newFile = new File(dir, name + ext);
        if(newFile.exists())newFile.delete();
        newFile.createNewFile();
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        Images image = new Images(newFile.getName(),file.getName(),newFile.getAbsolutePath(),"1",Calendar.getInstance().getTime());
        Log.e("Saving image ", "true");
//        mImagesViewModel = new ViewModelProvider(owner).get(ImagesViewModel.class);
//        mImagesViewModel.insert(image);
        GlobalHelper.getMetaDataFromImages(newFile.getAbsolutePath());
        return newFile;
    }

    public static String getEncoded64ImageStringFromBitmap(String fullpathimages) {
        File imagefile = new File(fullpathimages );
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);
        return imgString;
    }

    public static String getBase64FromFile(String fullpathimages) {
        try {
            File file = new File(fullpathimages);
            byte[] fileBytes = new byte[(int) file.length()];
            FileInputStream fis = new FileInputStream(file);
            fis.read(fileBytes);
            fis.close();
            return Base64.encodeToString(fileBytes, Base64.NO_WRAP);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static double getDistance(double lat1, double lat2, double lon1, double lon2){
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }

    public static JSONArray getMetaDataFromImages(String fullPathImages){

        JSONArray jsonArray = new JSONArray();
        try {
            String [] allTagExif = ExifAllTag.getAllExsifTag();

            ExifInterface exif = new ExifInterface(fullPathImages);
            for(String s:allTagExif){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(s,exif.getAttribute(s));
                if(jsonObject.length()> 0) {
                    jsonArray.put(jsonObject);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonArray;
    }

    public static String readFromIntent(Intent intent) {
        String action = intent.getAction();
        String nfcCardData = "Tidak Ada NFC";
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            nfcCardData = buildTagViews(msgs);
        }
        return nfcCardData;
    }

    private static String buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return "";
        String values = "";
        try {
            String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
            byte[] payload = msgs[0].getRecords()[0].getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
            int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
            // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            values = GlobalHelper.decompress(text);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }catch (IndexOutOfBoundsException e){
            Log.e("UnsupportedEncoding", e.toString());
        }

        return values;
    }

    public static String decompress(String value)  {
        try {
            byte [] buf = Base64.decode(value, Base64.DEFAULT);
            ByteArrayInputStream bis = new ByteArrayInputStream(buf);
            GZIPInputStream gis = new GZIPInputStream(bis);
            BufferedReader br = new BufferedReader(new InputStreamReader(gis, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            gis.close();
            bis.close();
            return sb.toString();
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return value;
        } catch (IOException e) {
            e.printStackTrace();
            return value;
        }catch (Exception e){
            e.printStackTrace();
            return value;
        }
    }

    public static JSONObject getModule(){
        try {
            JSONObject jModule = new JSONObject();
            String value = GlobalHelper.readFileContent(GlobalHelper.PATH_SELMOD_FILE);
            if(value == null){
                return null;
            }
            JSONArray jsonArray = new JSONArray(Encrypts.decrypt(value));
            for (int i =0;i< jsonArray.length();i++){
                jModule = jsonArray.getJSONObject(i);
//                Log.e("mdlCode",jModule.getString("mdlCode"));
                if (jModule.getString("mdlCode").equals("AWM")||jModule.getString("mdlCode").equals("PSA")) {
                    return jModule;
                }
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String getUserLvl(){
        try {
            JSONObject jModule = new JSONObject();
            String value = GlobalHelper.readFileContent(GlobalHelper.PATH_SELMOD_FILE);
            if(value == null){
                return null;
            }
            JSONArray jsonArray = new JSONArray(Encrypts.decrypt(value));
            for (int i =0;i< jsonArray.length();i++){
                jModule = jsonArray.getJSONObject(i);
                if (jModule.getString("mdlCode").equals("GNM")) {
                    String a = jModule.getString("mdlAccCode");
//                    Log.e("mdlAccCode",jModule.getString("mdlAccCode"));
                    return a;
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String bytesToHexString(byte[] src) {
//        StringBuilder stringBuilder = new StringBuilder("0x");
        StringBuilder stringBuilder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }
        return stringBuilder.toString();
    }

    public static void writeFileContent(String path, String sBody){
        try{
            File fpath = new File(path);
            if(!fpath.getParentFile().exists()){
                fpath.getParentFile().mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(fpath);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos);
            outputStreamWriter.write(sBody);
            outputStreamWriter.flush();
            outputStreamWriter.close();
        }catch (IOException e){
            e.printStackTrace();

        }
    }


    public static void writeFileContent2(String path,String sBody) throws IOException {
        File fpath = new File(path);
        if(!fpath.getParentFile().exists()){
            fpath.getParentFile().mkdirs();
        }
        byte[] data = Base64.decode(sBody, Base64.DEFAULT);
        FileOutputStream fos = new FileOutputStream(fpath);
        fos.write(data);
        fos.close();
    }


    public static String getNameObject(String status){
        String _condition = "";
        switch (status) {
            case "LUS3":
                _condition = "Landuse";
                break;
            case "BRD1":
                _condition = "Jembatan";
                break;
            case "CVT1":
                _condition = "Gorong2";
                break;
            case "WGT1":
                _condition = "Pintu Air";
                break;
            case "PUM1":
                _condition = "Pompa Air";
                break;
            case "DAM1":
                _condition = "Bendungan";
                break;
            case "BUN1":
                _condition = "Weir";
                break;
            case "ZIP1":
                _condition = "Syphon";
                break;
            case "TGL2":
                _condition = "Tanggul";
                break;
            case "FLP1":
                _condition = "Flapgate";
                break;
            case "STK1E":
                _condition = "Patok HGU";
                break;
            default: _condition = "invalid Code";
        }
        return _condition;
    }

    public static String getNameAttribute(String code){
        String _condition = code;
        switch (code) {
            case "ESTNR":
                _condition = "estate";
                break;
            case "block":
                _condition = "Block";
                break;
            case "type":
                _condition = "Tipe";
                break;
            case "condition":
                _condition = "Kondisi";
                break;
            case "length":
                _condition = "Panjang (meter)";
                break;
            case "width":
                _condition = "Lebar (meter)";
                break;
            case "flow_dir":
                _condition = "Arah Aliran";
                break;
            case "built":
                _condition = "Tanggal dibuat";
                break;
            case "status":
                _condition = "Status Objek";
                break;
            case "tot_door":
                _condition = "Jumlah Pintu";
                break;
            case "brand":
                _condition = "merk";
                break;
            case "model":
                _condition = "Model";
                break;
            case "hp":
                _condition = "Horse Power (hp)";
                break;
            case "diameter":
                _condition = "Diameter (centimeter)";
                break;
            case "throughput":
                _condition = "Kapasitas per jam (meter kubik)";
                break;
            case "month":
                _condition = "Bulan di Buat";
                break;
            case "year":
                _condition = "Tahun di Buat";
                break;
            case "duration":
                _condition = "Lama pembuatan (bulan)";
                break;
            case "CompanyCode":
                _condition = "Company";
                break;
            case "GroupCompanyName":
                _condition = "Company Group";
                break;
            case "year_budget":
                _condition = "Tahun Budget";
                break;
            case "spillway_level":
                _condition = "Spillway Level (centimeter)";
                break;
            case "rekomendasi":
                _condition = "Rekomendasi";
                break;
            case "typePerbaikan":
                _condition = "Tipe Perbaikan";
                break;
            case "typeMaterial":
                _condition = "Tipe Material";
                break;
            case "PatokID":
                _condition = "ID Patok";
                break;
            case "PatokName":
                _condition = "Nama Patok";
                break;
            case "typePatok":
                _condition = "Tipe Patok";
                break;
            case "typePerawatan":
                _condition = "Tipe Perawatan";
                break;
            case "alasan":
                _condition = "Alasan";
                break;
            case "surveyor":
                _condition = "Tim Pelaksana";
                break;
            case "tot_gorong":
                _condition = "Total Gorong2";
                break;
            case "tot_weir":
                _condition = "Total Weir";
                break;
            case "tot_syphon":
                _condition = "Total Syphon";
                break;
            case "Division":
                _condition = "Afdeling";
                break;
            case "EstCode":
                _condition = "Estate";
                break;

            default: _condition = code;
        }
        return _condition;
    }

    public static String reverseToKey(String code){
        String _condition = code;
        switch (code) {
            case "estate":
                _condition = "ESTNR";
                break;
            case "Block":
                _condition = "block";
                break;
            case "Tipe":
                _condition = "type";
                break;
            case "Kondisi":
                _condition = "condition";
                break;
            case "Panjang (meter)":
                _condition = "length";
                break;
            case "Lebar (meter)":
                _condition = "width";
                break;
            case "Arah Aliran":
                _condition = "flow_dir";
                break;
            case "Tanggal dibuat":
                _condition = "built";
                break;
            case "Status Objek":
                _condition = "status";
                break;
            case "Jumlah Pintu":
                _condition = "tot_door";
                break;
            case "merk":
                _condition = "brand";
                break;
            case "Model":
                _condition = "model";
                break;
            case "Horse Power (hp)":
                _condition = "hp";
                break;
            case "Diameter (centimeter)":
                _condition = "diameter";
                break;
            case "Kapasitas per jam (meter kubik)":
                _condition = "throughput";
                break;
            case "Bulan di Buat":
                _condition = "month";
                break;
            case "Tahun di Buat":
                _condition = "year";
                break;
            case "Lama pembuatan (bulan)":
                _condition = "duration";
                break;
            case "Company":
                _condition = "CompanyCode";
                break;
            case "Company Group":
                _condition = "GroupCompanyName";
                break;
            case "Tahun Budget":
                _condition = "year_budget";
                break;
            case "Spillway Level (centimeter)":
                _condition = "spillway_level";
                break;
            case "Rekomendasi":
                _condition = "rekomendasi";
                break;
            case "Tipe Perbaikan":
                _condition = "typePerbaikan";
                break;
            case "Tipe Material":
                _condition = "typeMaterial";
                break;
            case "Tipe Perawatan":
                _condition = "typePerawatan";
                break;
            case "Alasan":
                _condition = "alasan";
                break;
            case "Tim Pelaksana":
                _condition = "surveyor";
                break;
            case "Total Gorong2":
                _condition = "tot_gorong";
                break;
            case "Total Weir":
                _condition = "tot_weir";
                break;
            case "Total Syphon":
                _condition = "tot_syphon";
                break;
            case "Afdeling":
                _condition = "Division";
                break;
            case "Estate":
                _condition = "EstCode";
                break;

            default: _condition = code;
        }
        return _condition;
    }

    public static Feature getFeature(String layerName, MapView mapView){
        Feature[] featureResult = new Feature[1];
        for (Layer layer : mapView.getMap().getOperationalLayers()) {
            if (layer instanceof FeatureLayer) {
                if (Objects.equals(layer.getName(), layer)){
                    FeatureLayer featureLayer = (FeatureLayer) layer;
                    FeatureTable featureTable = featureLayer.getFeatureTable();
                    Log.e("FeatureTable",layer.getName());
                    QueryParameters query = new QueryParameters();
                    query.setWhereClause("1=1");
                    final ListenableFuture<FeatureQueryResult> future = featureTable.queryFeaturesAsync(query);
                    future.addDoneListener(() -> {
                        try {
                            // call get on the future to get the result
                            FeatureQueryResult result = future.get();
                            Iterator<Feature> resultIterator = result.iterator();
                            featureResult[0]=resultIterator.next();
                            Log.e("Feature Result", String.valueOf(featureResult[0].getAttributes().get("GlobalID")));

                        } catch (Exception e) {
                            Log.e("ErrCounting", e.getMessage());
                        }
                    });
                }
            }
        }
        return featureResult[0];

    }

    public static File photoAddStamp(File file, double latitude, double longitude){
        Bitmap src = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap dest = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateTime = sdf.format(Calendar.getInstance().getTime());
        dateTime = dateTime +"\n" + getEstate().getEstName();
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        if(kml.exists()) {
            try {
                String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, latitude, longitude).split(";");
                dateTime = dateTime +" : " +estAfdBlock[2];
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        dateTime = dateTime +"\n" + String.format("%.5f",latitude) + " : " + String.format("%.5f",longitude);

        Canvas cs = new Canvas(dest);
//        cs.drawColor(Color.TRANSPARENT);
        cs.drawBitmap(src, 0f, 0f, null);

        Bitmap text = drawText(dateTime);
        cs.drawBitmap(text,0f, 0f, null);
        try {

            File folder = new File(file.getParent()+"/addstamp/");
            if(!folder.exists()) folder.mkdirs();
            File fotoOK = new File(folder+ "/"+ file.getName());
            dest.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(fotoOK));
            file.delete();

            ExifInterface exif = new ExifInterface(fotoOK.getAbsolutePath());
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, GpsCamera.convert(latitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, GpsCamera.latitudeRef(latitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, GpsCamera.convert(longitude));
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, GpsCamera.longitudeRef(longitude));
            exif.saveAttributes();

            return fotoOK;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap drawText(String text) {
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(ContextCompat.getColor(PSAApp.getContext(), R.color.Yellow));
        textPaint.setTextSize(Utils.convertDpToPx(PSAApp.getContext(), 7));
        StaticLayout mTextLayout = new StaticLayout(text, textPaint, Utils.convertDpToPx(PSAApp.getContext(), 80), Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
// Create bitmap and canvas to draw to
        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(), mTextLayout.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
// Draw background with transparent color
        c.drawColor(ContextCompat.getColor(PSAApp.getContext(), android.R.color.transparent), PorterDuff.Mode.CLEAR);
// Draw text with stroke (border)
        textPaint.setStyle(Paint.Style.STROKE);
        textPaint.setStrokeWidth(Utils.convertDpToPx(PSAApp.getContext(), 1.2f)); // Adjust the stroke width as needed
        textPaint.setColor(ContextCompat.getColor(PSAApp.getContext(), R.color.black)); // Set the stroke color
        mTextLayout.draw(c);
// Draw text with fill color
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(ContextCompat.getColor(PSAApp.getContext(), R.color.Yellow)); // Set the fill color
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();
        return b;
    }




//    public static List<Bitmap> getLegendImage(FeatureTable featureTable){
//        for (FeatureType type : featureTable) {
//            for (FeatureTemplate template : type.getTemplates()) {
//                Geometry geometry = //create whatever shape you want the swatch to be
//                        Feature feature = featureTable.createFeature(template, geometry);
//                Symbol symbol = featureLayer.getRenderer().getSymbol(feature);
//                ListenableFuture<Bitmap> symbolSwatch = symbol.createSwatchAsync(24, 24,
//                        density, color, feature.getGeometry());
//
//                Bitmap swatch = symbolSwatch.get();
//            }
//        }
//    }

    public static List<Optionlayer> getOptionLayers(){
        List<Optionlayer> list = new ArrayList<>();
        Optionlayer a = new Optionlayer(2, "Plan",true);
        Optionlayer b = new Optionlayer(3, "Budget",true);
        Optionlayer c = new Optionlayer(4, "PTA",true);
        Optionlayer d = new Optionlayer(1, "Existing",true);
        Optionlayer e = new Optionlayer(9, "Perlu Survey",false);
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        return  list;
    }


    public static List<String> ancakOpen(){
        List<String>open = new ArrayList<>();
        open.add("Ancak Panen");
        open.add("Bukan Ancak Panen");
        open.add("Ancak tanpa TPH");
        return open;
    }

    public static List<String> ancakClose(){
        List<String>open = new ArrayList<>();
        open.add("Ancak Panen");
        open.add("Bukan Ancak Panen");
//        open.add("Ancak tanpa TPH");
        return open;
    }


    public static List<Optionlayer> getFilterHGU(){
        List<Optionlayer> list = new ArrayList<>();
        Optionlayer a = new Optionlayer(1, "HGU",true);
        Optionlayer b = new Optionlayer(2, "Riparian",true);

        list.add(a);
        list.add(b);

        return  list;
    }

    public static List<String> getBudgetLayers(){
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        List<String> yearList = new ArrayList<>();
        yearList.add("ALL"); // Add "All" option
        // Add years for current year - 2 to current year + 2
        for (int i = currentYear - 2; i <= currentYear + 3; i++) {
            yearList.add(String.valueOf(i));
        }


        return  yearList;
    }

    public static List<String> getStatusObject(){
        List<String> list = new ArrayList<>();
        list.add("Plan");
        list.add("Budget");
        list.add("PTA");
        list.add("Existing");
        return list;
    }

    public static List<String> attributesPlan(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("block");
        list.add("remark");
        list.add("QC_remarks");
        return list;
    }


    public static List<String> attributesAncak(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("Division");
        list.add("EstCode");
        list.add("ESTNR");
        list.add("Block");
        list.add("LuasHa");
        list.add("Ha_L");
        list.add("NoAncak");
//        list.add("tphCode");
        return list;
    }

    public static List<String> attributesEditAncak(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("Division");
        list.add("EstCode");
        list.add("ESTNR");
        list.add("Block");
        list.add("LuasHa");
        list.add("NoAncak");
        list.add("remark");
        list.add("Flag");


//        list.add("tphCode");
        return list;
    }

    public static List<String> attributesHGU(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("Block");
        list.add("typeMaterial");
        list.add("condition");
        list.add("rekomendasi");
        list.add("typePerbaikan");
        list.add("typePerawatan");
        list.add("alasan");
        list.add("surveyor");
        list.add("keterangan");
        return list;
    }

    public static List<String> attributesViewHGU(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("Block");
        list.add("PatokID");
        list.add("typePatok");
        list.add("typeMaterial");
        list.add("condition");
        list.add("rekomendasi");
        list.add("typePerbaikan");
        list.add("typePerawatan");
        list.add("alasan");
        list.add("surveyor");
        list.add("keterangan");
        return list;
    }
    public static List<String> attributesHGUB(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("Block");
        list.add("condition");
        list.add("keterangan");
        return list;
    }
    public static List<String> attributesBudget(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("block");
        list.add("type");
        list.add("year_budget");
        list.add("length");
        list.add("width");
        list.add("material");
        list.add("size");
        list.add("spillway_level");
        list.add("brand");
        list.add("model");
        list.add("tipe_door");
        list.add("tot_door");
        list.add("diameter");
        list.add("hp");
        list.add("flow_dir");
        list.add("tot_gorong");
        list.add("tot_weir");
        list.add("tot_syphon");

        list.add("remark");

        return list;
    }
    public static List<String> attributesExisting(){
        List<String> list = new ArrayList<>();
        list.add("CompanyCode");
        list.add("GroupCompanyName");
        list.add("ESTNR");
        list.add("block");
        list.add("condition");
        list.add("type");
        list.add("length");
        list.add("width");
        list.add("material");
        list.add("size");
        list.add("spillway_level");
        list.add("brand");
        list.add("model");
        list.add("tipe_door");
        list.add("tot_door");
        list.add("diameter");
        list.add("hp");
        list.add("flow_dir");
//        list.add("built");
        list.add("year");
        list.add("month");
        list.add("duration");
        list.add("tot_gorong");
        list.add("tot_weir");
        list.add("tot_syphon");
        list.add("remark");


        return list;
    }


    public static String getValueCoded(Feature feature, String attr){
        String val="";
        CodedValueDomain codedValue = (CodedValueDomain) feature.getFeatureTable().getField(attr).getDomain();
        List<CodedValue> values =  codedValue.getCodedValues();
        for (CodedValue value:values){
            if (value.getCode()==feature.getAttributes().get(attr)){
                val = value.getName();

            }
        }
        return val;
    }
    public static String getAppVersion(Context context){
        String versionName= "";
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
            // Use the versionCode as needed
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    public static String joinText(String original, String add){
        return original+add;
    }

    public static int getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR);
    }


}
