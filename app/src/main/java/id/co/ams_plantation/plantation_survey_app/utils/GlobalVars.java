package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.fasterxml.jackson.databind.deser.std.DateDeserializers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobapphome.mahencryptorlib.MAHEncryptor;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import id.co.ams_plantation.plantation_survey_app.PSAApp;
import id.co.ams_plantation.plantation_survey_app.services.model.FileTypeUrlMapping;

/**
 * Created by PASPC-02009 on 25/05/2016.
 */
public class GlobalVars {

    public static final String DB_MASTER = "DBMasterAMS";
    public static final String DB_PASS = "AMS12345PALM";
    public static final String EXTERNAL_DIR_FILES = "/AMSApp/files";
    public static final String FILETYPEURL_MAPPING = "FileTypeUrlMapping";
    public static final String SETTING_MYAPPS = "SettingUserMyApps";
    public static final String APPLICATION_MODE = "ApplicationMode";
    public static final String SELECTED_USER = "User";
    public static final String SELECTED_ESTATE = "SelEstate";
    public static final String DOWNLOADED_FILES_MAP_APP_ESTATE = "DownloadedMapByAppEstate";
    public static final String EXT_PROJECTDEF = "ProjectDef" ;
    public static final String SELECTED_PROJECTDEF = "SelectedProjectDef";
    public static final String SELECTED_TPK = "SelectedTPKEnc";
    public static final String PASSWORD_ENCRYPT = "KopiNikmatGakBikinKembung";
    public static final String USER_AUTHORIZED_MAPS = "UserAuthMaps";
    public static final String FFB_ACTUAL = "FFBActual";
    public static final String FFB_BUDGET = "FFBBudget";
    public static final String DOWNLOADED_FILES_MAP = "DownloadedMap";
    public static final String GIS_ADMIN_APPS   = "id.co.ams_plantation.amsadminapps";


    public static String allCapsConverter(String string){
        String allString="";
        for (String word:string.split(" ")) {
            StringBuilder sb = new StringBuilder(word);
            for(int index=1; index<sb.length();index++){
                char c = sb.charAt(index);
                if(!Character.isLowerCase(c)){
                    sb.setCharAt(index,Character.toLowerCase(c));
                }
            }
            allString += sb.toString() + " ";
        }
        return allString;
    }

    public static SpannableString fileSizeShorter(String fileSize, boolean withSuffix){
        DecimalFormat decimalFormat = new DecimalFormat("#,###.#");
        Double fileSizeLong = Double.parseDouble(fileSize);
        Double finalSize=0.0;
        String suffix = "B";
        if(fileSizeLong >= 1024){
            if(fileSizeLong>= 1024*1024){
                if(fileSizeLong >=1024*1024*1024){
                    finalSize = fileSizeLong / (1024*1024*1024);
                    suffix = "GB";
                }else{
                    finalSize = fileSizeLong / (1024*1024);
                    suffix = "MB";
                }
            }else{
                finalSize = fileSizeLong / 1024;
                suffix = "KB";
            }
        }else{
            finalSize = fileSizeLong;
            suffix = "B";
        }
        if(withSuffix){
            SpannableString string = new SpannableString(decimalFormat.format(finalSize) + suffix);
            switch (suffix){
                case "GB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#FF0000")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "MB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#FFA500")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "KB":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#008000")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;
                case "B":
                    string.setSpan(new ForegroundColorSpan(Color.parseColor("#0000FF")),string.toString().indexOf(suffix),
                            string.toString().indexOf(suffix)+suffix.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    break;

            }
            return string;
        }else{
            SpannableString string = new SpannableString(decimalFormat.format(finalSize));
            return string;
        }

    }


    public static String getDatabasePath(){
//        Log.e("Storage",Environment.getDataDirectory() + EXTERNAL_DIR_FILES +"/");
        File file = new File(Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/" + Encrypts.encrypt("API"));
        if(!file.exists())file.mkdirs();
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/" + Encrypts.encrypt("API");
    }

    public static String getDatabaseAdminAppsPath(){
//        Log.e("Storage",Environment.getDataDirectory() + EXTERNAL_DIR_FILES +"/");
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/";
    }

    public static String getDatabasePass(){
        return Encrypts.encrypt(DB_PASS);
    }
    public static WaspHash getTableHash(String tableName){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabasePath(),
                GlobalVars.DB_MASTER,
                GlobalVars.getDatabasePass());
        WaspHash returnedTable = db.openOrCreateHash(tableName);
        return returnedTable;
    }

    public static WaspHash getProjectDefHash(){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabasePath(),
                GlobalVars.DB_MASTER,
                GlobalVars.getDatabasePass());
        WaspHash returnedTable = db.openOrCreateHash(EXT_PROJECTDEF);
        return returnedTable;
    }

    public static WaspHash getProjectIDHash(String projectID){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabasePath(),
                GlobalVars.DB_MASTER,
                GlobalVars.getDatabasePass());
        WaspHash returnedTable = db.openOrCreateHash(EXT_PROJECTDEF +
                "_" + GlobalHelper.getEstate().getEstCode() + "_"+projectID);
        return returnedTable;
    }
    public static WaspHash getProjectIDHash(WaspDb db,String projectID){
        WaspHash returnedTable = db.openOrCreateHash(EXT_PROJECTDEF +
                "_" + GlobalHelper.getEstate().getEstCode() + "_"+projectID);
        return returnedTable;
    }
    public static WaspHash getFFBActualStatisticsHash(WaspDb db){
        WaspHash returnedTable = db.openOrCreateHash(FFB_ACTUAL + "_" + GlobalHelper.getEstate().getEstCode());
        return returnedTable;
    }

    public static WaspHash getFFBBudgetStatisticsHash(WaspDb db){
        WaspHash returnedTable = db.openOrCreateHash(FFB_BUDGET + "_" + GlobalHelper.getEstate().getEstCode());
        return returnedTable;
    }

    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    public static String getFileTypeUrl(String fileType){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabaseAdminAppsPath(),
                GlobalVars.DB_MASTER,
                GlobalVars.getDatabasePass());
        WaspHash tblFileTypeUrl = db.openOrCreateHash(FILETYPEURL_MAPPING);
        if(tblFileTypeUrl.get(fileType)!=null){
            FileTypeUrlMapping model = (FileTypeUrlMapping) tblFileTypeUrl.get(fileType);

            return model.getUrl();
        }else{
            return null;
        }
    }
    public static String getFilePath(String fileName){
        File file = new File(getDatabasePath(),Encrypts.encrypt(fileName));
        return file.getAbsolutePath();
    }

    public static String getFilePath(String type,String estCode,
                                     String companyCode,
                                     String divCode,
                                     String BlockId){
        String initialPath = "";
        initialPath = Environment.getExternalStorageDirectory() +
                EXTERNAL_DIR_FILES + "/"+encryptString(type) +"/"+
                encryptString(companyCode) +
                (estCode.length()>0 ? "/" + encryptString(estCode) : "") +
                (divCode.length()>0 ? "/" + encryptString(divCode) : "") +
                (BlockId.length()>0 ? "/" + encryptString(BlockId) : "") ;
        Log.e("DirUnEcr", Environment.getExternalStorageDirectory() +
                EXTERNAL_DIR_FILES + "/"+(type) +"/"+
                (companyCode) +
                (estCode.length()>0 ? "/" + (estCode) : "") +
                (divCode.length()>0 ? "/" + (divCode) : "") +
                (BlockId.length()>0 ? "/" + (BlockId) : "") );
        Log.e("Dir",initialPath);
        return initialPath;
    }
    public static String encryptString(String encString){
        try {
            MAHEncryptor encryptor = MAHEncryptor.newInstance(PASSWORD_ENCRYPT);
            String encrypted = encryptor.encode(encString);
            String urlSafe = URLEncoder.encode(encrypted,"UTF-8");
            Log.e("URL ENCODE",urlSafe);
            return urlSafe;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void checkFileUnEncripted() {
        File baseDir, baseDir1, baseDir2, baseDir3, baseDir4, baseDir5, baseDir6, baseDir7, baseDir8;

        baseDir = new File(Environment.getExternalStorageDirectory()
                + GlobalVars.EXTERNAL_DIR_FILES);

        baseDir1 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("FOTO_UDARA_MAP"));
        baseDir2 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("KML_BLOCK"));
        baseDir3 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("VECTOR_KEBUN_MAP"));
        baseDir4 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("GEOEYE_DATA"));

        baseDir5 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("GEODATABASE"));

        baseDir6 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("VTPK"));

        baseDir7 = new File(baseDir.getAbsolutePath() + "/"
                + GlobalVars.encryptString("IND_MAP"));

        baseDir8 = new File(baseDir.getAbsolutePath()+ "/"
                + GlobalVars.encryptString("WM_MAP"));

//        if(BuildConfig.BUILD_VARIANT.equals("dev")){
////            baseDir = new File(Environment.getExternalStorageDirectory()
////                    + "/dev" + GlobalVars.EXTERNAL_DIR_FILES);
////
////            baseDir1 = new File(baseDir.getAbsolutePath() + "/"
////                    + "/dev" + GlobalVars.encryptString("FOTO_UDARA_MAP"));
////            baseDir2 = new File(baseDir.getAbsolutePath() + "/"
////                    + "/dev" + GlobalVars.encryptString("KML_BLOCK"));
////            baseDir3 = new File(baseDir.getAbsolutePath() + "/"
////                    + "/dev" + GlobalVars.encryptString("VECTOR_KEBUN_MAP"));
////
////            baseDir4 = new File(baseDir.getAbsolutePath() + "/"
////                    + "/dev" + GlobalVars.encryptString("GEOEYE_DATA"));
//            baseDir = new File(Environment.getExternalStorageDirectory()
//                    + GlobalVars.EXTERNAL_DIR_FILES);
//
//            baseDir1 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("FOTO_UDARA_MAP"));
//            baseDir2 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("KML_BLOCK"));
//            baseDir3 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("VECTOR_KEBUN_MAP"));
//
//            baseDir4 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("GEOEYE_DATA"));
//
//            baseDir5 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("GEODATABASE"));
//
//            baseDir6 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("VTPK"));
//
//            baseDir7 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("IND_MAP"));
//
//
//        }else{
//            baseDir = new File(Environment.getExternalStorageDirectory()
//                    + GlobalVars.EXTERNAL_DIR_FILES);
//
//            baseDir1 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("FOTO_UDARA_MAP"));
//            baseDir2 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("KML_BLOCK"));
//            baseDir3 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("VECTOR_KEBUN_MAP"));
//
//            baseDir4 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("GEOEYE_DATA"));
//
//            baseDir5 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("GEODATABASE"));
//
//            baseDir6 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("VTPK"));
//
//            baseDir7 = new File(baseDir.getAbsolutePath() + "/"
//                    + GlobalVars.encryptString("IND_MAP"));
//        }



        //=================================================
        Log.e("Packagers.java : ", "Cek File FOTO_UDARA_MAP");
        ArrayList<File> fileDirFUM = new ArrayList<>();
        fileDirFUM.add(baseDir1);
        boolean checkdir1 = true;
        while (checkdir1) {
            for (int i = 0; i < fileDirFUM.size(); i++) {
                if (fileDirFUM.get(i).isDirectory()) {
                    checkdir1 = false;
                    File[] cekChild = fileDirFUM.get(i).listFiles();
                    if (cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileDirFUM.get(i).getAbsolutePath() + "/" + cekChild[j].getName());
                            fileDirFUM.get(i).delete();
                            fileDirFUM.add(fileDirs);
                        }
                    } else {

                    }
                } else {
                    checkdir1 = false;
                    if (fileDirFUM.get(i).getName().contains(".")) {
                        String filePath = fileDirFUM.get(i).getAbsolutePath().
                                substring(0, fileDirFUM.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + fileDirFUM.get(i).getName());
                        new File(fileDirFUM.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileDirFUM.get(i).getName())));
                    }
                }
            }
        }

        //=================================================
        Log.e("Packagers.java : ", "Cek File KML_BLOCK");
        ArrayList<File> fileDirKML = new ArrayList<>();
        fileDirKML.add(baseDir2);
        boolean checkdir2 = true;
        while (checkdir2) {
            for (int i = 0; i < fileDirKML.size(); i++) {
                if (fileDirKML.get(i).isDirectory()) {
                    checkdir2 = false;
                    File[] cekChild = fileDirKML.get(i).listFiles();
                    if (cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileDirKML.get(i).getAbsolutePath() + "/" + cekChild[j].getName());
                            fileDirKML.get(i).delete();
                            fileDirKML.add(fileDirs);
                        }
                    } else {

                    }
                } else {
                    checkdir2 = false;
                    if (fileDirKML.get(i).getName().contains(".")) {
                        String filePath = fileDirKML.get(i).getAbsolutePath().
                                substring(0, fileDirKML.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File KML Block path ======================= " + filePath);
                        new File(fileDirKML.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileDirKML.get(i).getName())));

                    }
                }
            }
        }

        //=================================================
        Log.e("Packagers.java : ","Cek File VECTOR_KEBUN_MAP");

        ArrayList<File> fileDir = new ArrayList<>();
        fileDir.add(baseDir3);
        boolean checkDir = true;
        while(checkDir){
            for(int i=0; i < fileDir.size(); i++) {
                if (fileDir.get(i).isDirectory()) {
                    checkDir = false;
                    File[] cekChild = fileDir.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileDir.get(i).getAbsolutePath()+"/"+cekChild[j].getName());
                            fileDir.get(i).delete();
                            fileDir.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDir=false;
                    if (fileDir.get(i).getName().contains(".")) {
                        String filePath = fileDir.get(i).getAbsolutePath().
                                substring(0, fileDir.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + fileDir.get(i).getName());
                        new File(fileDir.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileDir.get(i).getName())));
                    }
                }
            }
        }

        Log.e("Packagers.java : ","Cek File GEOEYE_DATA");

        ArrayList<File> fileDirGeoEye = new ArrayList<>();
        fileDirGeoEye.add(baseDir4);
        boolean checkDirGeoEye = true;
        while(checkDirGeoEye){
            for(int i=0; i < fileDirGeoEye.size(); i++) {
                if (fileDirGeoEye.get(i).isDirectory()) {
                    checkDirGeoEye = false;
                    File[] cekChild = fileDirGeoEye.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileDirGeoEye.get(i).getAbsolutePath()+"/"+cekChild[j].getName());
                            fileDirGeoEye.get(i).delete();
                            fileDirGeoEye.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDirGeoEye=false;
                    if (fileDirGeoEye.get(i).getName().contains(".")) {
                        String filePath = fileDirGeoEye.get(i).getAbsolutePath().
                                substring(0, fileDirGeoEye.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File VKM path ======================= " + fileDirGeoEye.get(i).getName());
                        new File(fileDirGeoEye.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileDirGeoEye.get(i).getName())));
                    }
                }
            }
        }

        Log.e("Packagers.java : ","Cek File GEODATABASE");
        ArrayList<File> fileDirGeodatabase = new ArrayList<>();
        fileDirGeodatabase.add(baseDir5);

        boolean checkDirGeodatabase = true;
        while(checkDirGeodatabase){
            for(int i=0; i < fileDirGeodatabase.size(); i++) {
                if (fileDirGeodatabase.get(i).isDirectory()) {
                    checkDirGeodatabase = false;
                    File[] cekChild = fileDirGeodatabase.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileDirGeodatabase.get(i).getAbsolutePath()+"/"+cekChild[j].getName());

                            fileDirGeodatabase.get(i).delete();
                            fileDirGeodatabase.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDirGeodatabase=false;
                    if (fileDirGeodatabase.get(i).getName().contains(".")) {
                        String filePath = fileDirGeodatabase.get(i).getAbsolutePath().
                                substring(0, fileDirGeodatabase.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File Geodatabase path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File Geodatabase file ======================= " + fileDirGeodatabase.get(i).getName());
                        new File(fileDirGeodatabase.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileDirGeodatabase.get(i).getName())));
//                        Log.i(TAG, "checkFileUnEncripted: "+filePath+"/"+GlobalVars.encryptString(fileDirGeodatabase.get(i).getName()));
                    }
                }
            }
        }

        //=================================================
        Log.e("Packagers.java : ","Cek File VTPK");
        ArrayList<File> fileVTPK = new ArrayList<>();
        fileVTPK.add(baseDir6);
        boolean checkDirVTPK = true;
        while(checkDirVTPK){
            for(int i=0; i < fileVTPK.size(); i++) {
                if (fileVTPK.get(i).isDirectory()) {
                    checkDirVTPK = false;
                    File[] cekChild = fileVTPK.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileVTPK.get(i).getAbsolutePath()+"/"+cekChild[j].getName());
                            fileVTPK.get(i).delete();
                            fileVTPK.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDirVTPK=false;
                    if (fileVTPK.get(i).getName().contains(".")) {
                        String filePath = fileVTPK.get(i).getAbsolutePath().
                                substring(0, fileVTPK.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File VTPK path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File VTPK path ======================= " + fileVTPK.get(i).getName());
                        new File(fileVTPK.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileVTPK.get(i).getName())));
                    }
                }
            }
        }

        //=================================================
        Log.e("Packagers.java : ", "Cek File IND_MAP");
        ArrayList<File> fileINDMap = new ArrayList<>();
        fileINDMap.add(baseDir7);
        boolean checkDirINDMap = true;
        while(checkDirINDMap){
            for(int i=0; i < fileINDMap.size(); i++) {
                if (fileINDMap.get(i).isDirectory()) {
                    checkDirINDMap = false;
                    File[] cekChild = fileINDMap.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileINDMap.get(i).getAbsolutePath()+"/"+cekChild[j].getName());
                            fileINDMap.get(i).delete();
                            fileINDMap.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDirINDMap=false;
                    if (fileINDMap.get(i).getName().contains(".")) {
                        String filePath = fileINDMap.get(i).getAbsolutePath().
                                substring(0, fileINDMap.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File IND_MAP path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File IND_MAP path ======================= " + fileINDMap.get(i).getName());
                        new File(fileINDMap.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileINDMap.get(i).getName())));
                    }
                }
            }
        }

        //=================================================
        Log.e("Packagers.java : ", "Cek File WM_MAP");
        ArrayList<File> fileWMMap = new ArrayList<>();
        fileWMMap.add(baseDir8);
        boolean checkDirWPMap = true;
        while(checkDirWPMap){
            for(int i=0; i < fileWMMap.size(); i++) {
                if (fileWMMap.get(i).isDirectory()) {
                    checkDirWPMap = false;
                    File[] cekChild = fileWMMap.get(i).listFiles();
                    if(cekChild.length > 0) {
                        for (int j = 0; j < cekChild.length; j++) {
                            File fileDirs = new File(fileWMMap.get(i).getAbsolutePath()+"/"+cekChild[j].getName());
                            fileWMMap.get(i).delete();
                            fileWMMap.add(fileDirs);
                        }
                    }else{

                    }
                }else{
                    checkDirWPMap=false;
                    if (fileWMMap.get(i).getName().contains(".")) {
                        String filePath = fileWMMap.get(i).getAbsolutePath().
                                substring(0, fileWMMap.get(i).getAbsolutePath().lastIndexOf(File.separator));

                        Log.e("Packagers.java : ", "Cek File WM_MAP path ======================= " + filePath);
                        Log.e("Packagers.java : ", "Cek File WM_MAP path ======================= " + fileWMMap.get(i).getName());
                        new File(fileWMMap.get(i).getAbsolutePath()).renameTo(new File(filePath+"/"+GlobalVars.encryptString(fileWMMap.get(i).getName())));
                    }
                }
            }
        }

    }



    public static boolean isApplicationInOnlineMode(Context context){
//        if(context.getSharedPreferences(GlobalVars.APPLICATION_MODE, Context.MODE_PRIVATE)!=null){
//            SharedPreferences sharedPreferences = context.getSharedPreferences
//                    (GlobalVars.APPLICATION_MODE,Context.MODE_PRIVATE);
//            return sharedPreferences.getBoolean(GlobalVars.APPLICATION_MODE,false);
//        }
        try{
            ConnectivityManager cm = (ConnectivityManager) PSAApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
//            if(APIApp.getContext().getSharedPreferences(GlobalVars.APPLICATION_MODE, Context.MODE_PRIVATE)!=null){
//                SharedPreferences sharedPreferences = context.getSharedPreferences
//                        (GlobalVars.APPLICATION_MODE,Context.MODE_PRIVATE);
//                return sharedPreferences.getBoolean(GlobalVars.APPLICATION_MODE,false);
//            }
        }catch (Exception e){

        }

        return false;
    }

    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) PSAApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static Gson gsonBuilder(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializers.DateDeserializer());
        Gson gson = gsonBuilder.create();

        return gson;
    }

}
