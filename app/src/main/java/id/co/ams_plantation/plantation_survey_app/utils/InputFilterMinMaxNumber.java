package id.co.ams_plantation.plantation_survey_app.utils;

/**
 * Created by user on 8/2/2017.
 */

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterMinMaxNumber implements InputFilter {

    private float min, max;

    public InputFilterMinMaxNumber(float min, float max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMaxNumber(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            float input = Float.parseFloat(dest.toString() + source.toString());
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) { }
        return "";
    }

    private boolean isInRange(float a, float b, float c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
