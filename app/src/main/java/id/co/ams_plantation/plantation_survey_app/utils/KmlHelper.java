package id.co.ams_plantation.plantation_survey_app.utils;

import com.esri.arcgisruntime.geometry.PointCollection;
import com.esri.arcgisruntime.geometry.Polygon;
import com.esri.arcgisruntime.geometry.SpatialReferences;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Mafrukhin on 24/03/2023.
 */
public class KmlHelper {

    private static String parseKMZFile(File file){
        try {
            ZipFile kmzFile = new ZipFile(file);
            String rootFileName = null;
            //Iterate in the KMZ to find the first ".kml" file:
            Enumeration<? extends ZipEntry> list = kmzFile.entries();
            while (list.hasMoreElements() && rootFileName == null){
                ZipEntry ze = list.nextElement();
                String name = ze.getName();
                if (name.endsWith(".kml") && !name.contains("/"))
                    rootFileName = name;
            }
            String result;
            if (rootFileName != null){
                ZipEntry rootEntry = kmzFile.getEntry(rootFileName);
                InputStream stream = kmzFile.getInputStream(rootEntry);
                result = convertStreamToString(stream);
            } else {
                result = null;
            }
            kmzFile.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static HashMap<String, ArrayList<Polygon>> getLineOfPlacemarks(File KmzFile){
        String geoJSONKml = parseKMZFile(KmzFile);
        HashMap<String,ArrayList<Polygon>> hashMapPolygones = new HashMap<>();
        if(geoJSONKml==null){
            return hashMapPolygones;
        }
        try {
            Polygon polygon;
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(geoJSONKml));
            int eventType = xpp.getEventType();

            while(eventType!= XmlPullParser.END_DOCUMENT){
                if(eventType == XmlPullParser.START_TAG &&
                        xpp.getName().equalsIgnoreCase("description")){

                    while(eventType!= XmlPullParser.TEXT){
                        eventType=xpp.next();
                    }
                    String blockName = xpp.getText();
                    if(blockName!=null){
                        if(blockName.length()>0)
                            while(!(eventType == XmlPullParser.END_TAG &&
                                    xpp.getName().equalsIgnoreCase("placemark"))){
                                if(blockName.equalsIgnoreCase("Area Features")){
                                    if(eventType==XmlPullParser.START_TAG &&
                                            xpp.getName().equalsIgnoreCase("description")) {
                                        while (eventType != XmlPullParser.TEXT) {
                                            eventType = xpp.next();
                                        }
                                        blockName = xpp.getText();
                                    }
                                }else if(eventType== XmlPullParser.START_TAG &&
                                        xpp.getName().equalsIgnoreCase("LinearRing")){
                                    while(!(eventType== XmlPullParser.END_TAG &&
                                            xpp.getName().equalsIgnoreCase("LinearRing"))){
                                        if(eventType== XmlPullParser.START_TAG &&
                                                xpp.getName().equalsIgnoreCase("coordinates")){
                                            while(eventType!= XmlPullParser.TEXT){
                                                eventType=xpp.next();
                                            }
//                                    System.out.println(xpp.getText());
                                            ///Extract Polygon
                                            BufferedReader br = new BufferedReader(new StringReader(xpp.getText()));
                                            String str;
                                            ArrayList<ArrayList<Double>> arrayListPoint = new ArrayList<>();
                                            while((str=br.readLine())!=null){
                                                str = str.replace(" ","");
                                                StringTokenizer tokenizer = new StringTokenizer(str,",");
                                                if(str.length()>0 && tokenizer.hasMoreTokens()){
                                                    double lon= Double.parseDouble(tokenizer.nextToken());
                                                    double lat= Double.parseDouble(tokenizer.nextToken());

                                                    ArrayList<Double> arrayList = new ArrayList<>();
                                                    arrayList.add(lat);
                                                    arrayList.add(lon);
                                                    arrayListPoint.add(arrayList);
                                                }
                                            }
                                            double[][] points = new double[arrayListPoint.size()][2];
                                            for(int i=0;i<arrayListPoint.size();i++){
                                                ArrayList<Double> row = arrayListPoint.get(i);
//                                            Log.e("index ke - "+i,row.get(0)+" | "+row.get(1));
                                                points[i][0] = row.get(0);
                                                points[i][1] = row.get(1);
                                            }

                                            polygon = KmlHelper.createPolygon(points);
//                                        Log.e("blockName","{"+blockName+"}, "+ polygon.isValid()+"\n"+gson.toJson(points));
                                            if(hashMapPolygones.containsKey(blockName)){
//                                            Polygon newPol = KmlHelper.createPolygon(polygon,points);
                                                ArrayList<Polygon> polygons = hashMapPolygones.get(blockName);
                                                polygons.add(polygon);
                                                hashMapPolygones.put(blockName,polygons);
                                            }else{
                                                ArrayList<Polygon> polygons = new ArrayList<>();
                                                polygons.add(polygon);
                                                hashMapPolygones.put(blockName,polygons);
                                            }

                                        }
                                        eventType=xpp.next();
                                    }
                                }
                                eventType=xpp.next();
                            }
                    }


                }
//                if(eventType == XmlPullParser.START_DOCUMENT) {
//                    System.out.println("Line "+xpp.getLineNumber()+"Start document");
//                } else if(eventType == XmlPullParser.END_DOCUMENT) {
//                    System.out.println("Line "+xpp.getLineNumber()+"End document");
//                } else if(eventType == XmlPullParser.START_TAG) {
//                    System.out.println("Line "+xpp.getLineNumber()+"Start tag "+xpp.getName());
//                } else if(eventType == XmlPullParser.END_TAG) {
//                    System.out.println("Line "+xpp.getLineNumber()+"End tag "+xpp.getName());
//                } else if(eventType == XmlPullParser.TEXT) {
//                    System.out.println("Text "+xpp.getText());
//                }
//                System.out.println("Triggered next");
                eventType=xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return hashMapPolygones;
    }

    public static Polygon createPolygon(double[][] latlon) {
        if(latlon != null && latlon.length >= 3) {
            PointCollection thePointCollection = new PointCollection(SpatialReferences.getWgs84());
            thePointCollection.add(latlon[0][1], latlon[0][0]);

            for(int var2 = 1; var2 < latlon.length; ++var2) {
                thePointCollection.add(latlon[var2][1], latlon[var2][0]);
            }
            return new Polygon(thePointCollection);
        } else {
            return null;
        }
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }


}
