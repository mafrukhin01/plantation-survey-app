//package id.co.ams_plantation.plantation_survey_app.utils;
//
//import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
//
//public class NumericalAnnotationSymbol extends SimpleMarkerSymbol {
//
//    private final int number;
//    private final int textColor;
//    private final int textSize;
//
//    public NumericalAnnotationSymbol(int number, int textColor, int textSize) {
//        super(Style.CIRCLE, 20); // Set marker size
//        this.number = number;
//        this.textColor = textColor;
//        this.textSize = textSize;
//    }
//
//    @Override
//    public void draw(Graphics2D g, float x, float y, float angle) {
//        super.draw(g, x, y, angle);
//
//        // Draw numerical text
//        g.setColor(textColor);
//        g.setFont(new Font("Arial", Font.BOLD, textSize));
//        g.drawString(String.valueOf(number), x - textSize / 2, y + textSize / 2);
//    }
//}
//
