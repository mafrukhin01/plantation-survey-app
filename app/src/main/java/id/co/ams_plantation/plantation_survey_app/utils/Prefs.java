package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.zjy.pdfview.widget.ScrollSlider;

import java.util.ArrayList;
import java.util.List;

import id.co.ams_plantation.plantation_survey_app.data.entitiy.Project;

/**
 * Created by Mafrukhin on 29/04/2022.
 */
public class Prefs {
    private Prefs() {
    }
    private static SharedPreferences prefs;
    public static final String IS_LOGGED = "is_logged";
    public static final Integer PAGINATION_COUNT = 2000;
    public static final String USER_SDE = "editor1";
    public static final String PASS_SDE = "editor1123";
    public static final String LICENSE = "license";
    public static final String USERNAME = "username";
    public static final String TEMP_IMAGE_NAME = "temp_image_name";
    public static final String TEMP_IMAGE_LOC = "temp_image_loc";
    public static final String LAST_SYNC_DATA= "last_sync_data";
    public static final String ACTIVE_SESSION = "active_session";
    public static final String ACTIVE_SESSION_HGU = "active_session_hgu";
    public static final String ACTIVE_SESSION_ANCAK = "active_session_ancak";
    public static final String ADD_MODE = "add_mode";
    public static final String IS_ACTIVE_BLOCK = "is_active_block";

    public static final String SESSION_ID = "session_id";
    public static final String SESSION_ID_HGU = "session_id_hgu";
    public static final String SESSION_ID_ANCAK = "session_id_ancak";
    public static final String SESSION_NAME = "session_name";
    public static final String BLOCK = "block";
    public static final String BLOCK_ANCAK = "block_ancak";
    public static final String PELAKSANA = "pelaksana";
    public static final String SERVICE_TEST ="https://app.gis-div.com/arcgis/rest/services/pub_editing/feature_edit/FeatureServer";
    public static final String SERVICE_SDE_INVENTORY ="https://app.gis-div.com/arcgis/rest/services/pub_psa/PA_Inventory/FeatureServer";
    public static final String SERVICE_SDE_HGU ="https://app.gis-div.com/arcgis/rest/services/pub_psa/STK_2/FeatureServer";
    public static final String SERVICE_ANCAK = "https://app.gis-div.com/arcgis/rest/services/pub_psa/Ancak/FeatureServer";
    public static final String SERVICE_ANCAK2 = "https://app.gis-div.com/arcgis/rest/services/pub_psa/Ancak_V2/FeatureServer";
    public static final String BASE_URL = "https://app.gis-div.com/PSAService/api/v1/";
    public static final String BASE_URL_PDC = "https://app.gis-div.com/pdcservices/api/";
    public static final String BASE_URL_PDC_DEV = "http://dev.gis-div.com/pdcservices/api/";
    public static final String BASE_URL_HMS = "https://app.gis-div.com/hmssap/api/";
    public static final String BASE_URL_DEV_HMS = "https://dev.gis-div.com/hmssap/api/";
    public static final String BASE_URL_ADMIN_APPS = "https://app.gis-div.com/Android_Service/api/";
    public static final String BASE_URL_REPORT = "http://map.gis-div.com:234/api/";
    public static final String QC_MODE = "qc_mode";
    public static final String JOB_ID = "job_id";
    public static final String BASEMAP = "basemap";
    public static final String LOCK_ESTATE= "lock_estate";
    public static Integer EMP_QTY;
    public static final List<String> LIST_PROMO = new ArrayList<>();
    public static void init(Context context) {
        prefs = context.getSharedPreferences("psa_pref", Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor setPrefs() {
        return prefs.edit();
    }

    public static String getLicense(String key) {
        return prefs.getString(key, "{\"licenseString\":\"tF4HsX9tIL9Ns1TNPfGbCKZc04gs6HsyzU//jyDjoUg=\"}");
    }
    public static String getString(String key) {
        return prefs.getString(key, null);
    }

    public static boolean getBoolean(String key) {
        return prefs.getBoolean(key, false);
    }

    public static long getLong(String key){return  prefs.getLong(key, 0);}

    public static List<Project> objSample(){String[] parts = GlobalHelper.getEstate().getEstName().split(" ");
       List newObj = new ArrayList<>();
       Project a = new Project("AWM","Asset Water Management", "Survey Asset Water Management","2023-05-01","2023-09-01",parts[0]);
       newObj.add(a);
       return  newObj;
    }
}
