package id.co.ams_plantation.plantation_survey_app.utils;

import android.annotation.SuppressLint;


/**
 * Created by Mafrukhin on 21/04/2022.
 */
@SuppressLint("DefaultLocale")
public class SequenceNumber {
    static String stringNum;
    static int number;
    static int seq;
    static String stringSeq;
    static  int initial;
    static int seq2;
    static String stringSeq2;
    static  int initial2;
    public static  String getSequence(int integer){
        number = integer +1;
        stringNum = String.format("%03d",number);
//        Log.e("Sequence ", stringNum);
        return stringNum;
    }

    public static String getSequence4(int integer){
        number = integer +1;
        stringNum = String.format("%04d",number);
//        Log.e("Sequence ", stringNum);
        return stringNum;
    }


}
