package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import com.esri.arcgisruntime.data.Feature;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FeatureEditAdapter;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;

/**
 * Created by Mafrukhin on 10/03/2023.
 */
public class SheetAddPoint {
    private static Context context;
    LayoutInflater inflater;
    RecyclerView rv;
    Feature mData;
    BottomSheetDialog mBottomSheetDialog;
    FeatureEditAdapter adapter;
    MapReplantingActivity activity;
    String a,b,c;
    EditText surveyor, name, jenis;
    Button b1, b2;

    public SheetAddPoint(Context context1){


        context = context1;
        mBottomSheetDialog  = new BottomSheetDialog(context1);
        inflater = LayoutInflater.from(context1);

        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;

        }
    }

    public BottomSheetDialog showDialog(){
        View sheetView = inflater.inflate(R.layout.bottom_sheet, null);
        rv = sheetView.findViewById(R.id.rv);

//        ImageView imageView = sheetView.findViewById(R.id.imageView);
        b1 = sheetView.findViewById(R.id.button);
        b2 = sheetView.findViewById(R.id.button2);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        b1.setOnClickListener(view -> {
            Log.e("sizeAdapteredited", String.valueOf(adapter.getEditedFeature().size()));
//            activity.updateFeature(adapter.getEditedFeature());
            activity.reverseVertex(mData);
        });
        return mBottomSheetDialog;
    }
}
