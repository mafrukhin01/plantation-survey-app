package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FeatureEditAdapter;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.EditFeature;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;

/**
 * Created by Mafrukhin on 18/07/2022.
 */
public class ShowBottomSheetDialog implements FeatureEditAdapter.ItemClickListener{
    private static Context context;
    LayoutInflater inflater;
    RecyclerView rv;
    Feature mData;
    BottomSheetDialog mBottomSheetDialog;
    FeatureEditAdapter adapter;
    MapReplantingActivity activity;
    MapInventoryActivity activity2;
    private contextOf mState;
    String a,b,c;
    EditText surveyor, name, jenis;
    Button b1, b2;
    public ShowBottomSheetDialog(Context context1, Feature data, String s, String n, String j){
        this.mData = data;
        this.b = n;
        this.c = j;
        this.a = s;
        context = context1;
        mBottomSheetDialog  = new BottomSheetDialog(context1);
        inflater = LayoutInflater.from(context1);
        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;
            mState = contextOf.Replanting;
        }
        else {
            activity2 = (MapInventoryActivity) context;
            mState = contextOf.Inventory;
        }
    }

    public BottomSheetDialog showDialog(){
        View sheetView = inflater.inflate(R.layout.bottom_sheet, null);
        rv = sheetView.findViewById(R.id.rv);
        List<String> setAttr = new ArrayList<>();
        setAttr = GlobalHelper.attributesAncak();
        Map<String, Object> attr = mData.getAttributes();

//        Log.e("mdata",mData.getAttributes().get("JenisJembatan").toString());
        Set<String> keys = attr.keySet();
        List<EditFeature> list = new ArrayList<>();
        Log.e("LayerName",String.valueOf(mData.getFeatureTable().getTableName()));
        if (Objects.equals(mData.getFeatureTable().getTableName(), "BLK3")){

            for (String key : keys) {
                if (mData.getFeatureTable().getField(key).isEditable()) {
                    Object value = attr.get(key);
                    Log.e("key", key + " | type " + mData.getFeatureTable().getField(key).getDomain() + "\n");
                    if (mData.getFeatureTable().getField(key).getDomain() != null) {
                        CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                        List<CodedValue> values = codedValue.getCodedValues();
                        for (CodedValue fieldValue : values) {
                            if (fieldValue.getCode().equals(fieldValue)) {
                                Log.e("key", key + " | type " + fieldValue.getName() + "\n");
                                break;
                            }
                        }
                    }


//                mData.getFeatureTable().getField(key).getDomain();
//            EditFeature f = new EditFeature(key,String.valueOf(value));
//            list.add(f);
                    if (value != null) {
                        EditFeature f = new EditFeature(key, value.toString());
                        list.add(f);
                        Log.e("content", key + " | " + value + "\n");
                    }
                }

//            calloutContent.append(key + " | " + value + "\n");
            }
        }
        else {
            for (String _attr : setAttr) {

                for (String key : keys) {
                    if (_attr.equals(key)) {
                        if (mData.getFeatureTable().getField(key).isEditable()) {
                            Object value = attr.get(key);
                            Log.e("key", key + " | type " + mData.getFeatureTable().getField(key).getDomain() + "\n");
                            if (mData.getFeatureTable().getField(key).getDomain() != null) {
                                CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                                List<CodedValue> values = codedValue.getCodedValues();
                                for (CodedValue fieldValue : values) {
                                    if (fieldValue.getCode().equals(fieldValue)) {
                                        Log.e("key", key + " | type " + fieldValue.getName() + "\n");
                                        break;
                                    }
                                }
                            }


//                mData.getFeatureTable().getField(key).getDomain();
//            EditFeature f = new EditFeature(key,String.valueOf(value));
//            list.add(f);
                            if (value != null) {
                                EditFeature f = new EditFeature(key, value.toString());
                                list.add(f);
                                Log.e("content", key + " | " + value + "\n");
                            }
                        }
                    }
//            calloutContent.append(key + " | " + value + "\n");
                }
            }
        }


        Log.e("list size Adapter", String.valueOf(list.size()));
        adapter = new FeatureEditAdapter(context, list);
//        adapter.setClickListener((FeatureEditAdapter.ItemClickListener) context);
//                adapter.setData(layers);
        Log.e("adapter size", String.valueOf(adapter.getItemCount()));
        rv.setAdapter(adapter);

//        ImageView imageView = sheetView.findViewById(R.id.imageView);
        b1 = sheetView.findViewById(R.id.button);
        b2 = sheetView.findViewById(R.id.button2);
        surveyor = sheetView.findViewById(R.id.et_surveyor);
        name = sheetView.findViewById(R.id.et_name);
        jenis = sheetView.findViewById(R.id.et_jenis);
        surveyor.setText(a);
        name.setText(b);
        jenis.setText(c);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        b1.setOnClickListener(view -> {
            if (mState == contextOf.Replanting){
                Log.e("sizeAdapteredited", String.valueOf(adapter.getEditedFeature().size()));
//                activity.saveNewFeature(mData);
                mBottomSheetDialog.cancel();
            } else {
                activity2.updateFeature(adapter.getEditedFeature());
            }
//            activity.reverseVertex(mData);
        });
        return mBottomSheetDialog;
    }


    @Override
    public void onItemClick(View view, int position) {

    }

    enum contextOf {
        Inventory,
        Replanting
    }
}
