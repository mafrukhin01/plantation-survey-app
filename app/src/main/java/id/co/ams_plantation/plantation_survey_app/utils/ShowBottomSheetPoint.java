package id.co.ams_plantation.plantation_survey_app.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.SliderLayout;
import com.esri.arcgisruntime.data.ArcGISFeature;
import com.esri.arcgisruntime.data.CodedValue;
import com.esri.arcgisruntime.data.CodedValueDomain;
import com.esri.arcgisruntime.data.Feature;
import com.esri.arcgisruntime.data.Field;
import com.esri.arcgisruntime.data.QueryParameters;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.adapter.FeatureEditAdapter;
import id.co.ams_plantation.plantation_survey_app.ui.MapInventoryActivity;
import id.co.ams_plantation.plantation_survey_app.ui.MapReplantingActivity;

/**
 * Created by Mafrukhin on 10/05/2023.
 */
public class ShowBottomSheetPoint implements FeatureEditAdapter.ItemClickListener{
    private static Context context;
    private ArcGISFeature mSelectedArcGISFeature;
    public boolean iscompas = false;
    LayoutInflater inflater;
    TextView header;
    ImageView iv_marker;
    RecyclerView rv;
    Feature mData;
    ScrollView sv_object;
    BottomSheetDialog mBottomSheetDialog;
    SliderLayout sliderView;
    FeatureEditAdapter adapter;
    MapReplantingActivity activity;
    MapInventoryActivity activity2;
    private static LinearLayout viewGroup;
    private ShowBottomSheetPoint.contextOf mState;
    RelativeLayout takePicture;
    private action mAction;
    String a,b,c;
    EditText surveyor, name, jenis;
    Button b_save, b_cancel;
    public ShowBottomSheetPoint(Context context1, Feature feature, int _action){
        if (_action==0){
            mAction = action.Add;
        } else if (_action==1){
            mAction = action.show;
        }
        else {
            mAction = action.edit;
        }
        this.mData = feature;
        context = context1;
        mBottomSheetDialog  = new BottomSheetDialog(context1);
        inflater = LayoutInflater.from(context1);

        if(context instanceof MapReplantingActivity) {
            activity = (MapReplantingActivity) context;
            mState = ShowBottomSheetPoint.contextOf.Replanting;
        }
        else {
            activity2 = (MapInventoryActivity) context;
            mState = ShowBottomSheetPoint.contextOf.Inventory;
        }
    }

    public BottomSheetDialog showDialog(){
        View sheetView = inflater.inflate(R.layout.bottomsheet2, null);
        viewGroup = sheetView.findViewById(R.id.view_group);
        header = sheetView.findViewById(R.id.tv_header);
        b_save = sheetView.findViewById(R.id.bt_save);
        b_cancel = sheetView.findViewById(R.id.bt_cancel);
        sv_object = sheetView.findViewById(R.id.scroll_object);
        iv_marker = sheetView.findViewById(R.id.iv_marker);
        sliderView = (SliderLayout) sheetView.findViewById(R.id.sliderLayout);
        takePicture = sheetView.findViewById(R.id.rl_ipf_takepicture);
//        TextSliderView textSliderView = new TextSliderView(activity);
//        textSliderView.image("http://app.gis-div.com/GSAService/"+mData.getAttributes().get("image"));
//        sliderView.addSlider(textSliderView);
        header.setText(GlobalHelper.getNameObject(mData.getFeatureTable().getTableName()));
        addView();
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        BottomSheetBehavior<View> bottomSheetBehavior = BottomSheetBehavior.from((View) sheetView.getParent());
        bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
//        bottomSheetBehavior.onInterceptTouchEvent()
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.show();
        b_cancel.setOnClickListener(v -> {
            mBottomSheetDialog.cancel();
        });
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
//                activity2.openCam();

            }
        });
        b_save.setOnClickListener( v-> {
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childView = viewGroup.getChildAt(i);
                LinearLayout ll = (LinearLayout) childView;
                for (int l = 0; l < ll.getChildCount(); l++) {
                    View child = ll.getChildAt(l);
                    TextInputLayout textInputLayout = (TextInputLayout) child;
//                    View editLayout= textInputLayout.getChildAt(2);
//                    Log.e("editLayout", String.valueOf(editLayout.getClass()));
                    if(textInputLayout.getEditText() instanceof TextInputEditText) {
                        TextInputEditText editText = (TextInputEditText) textInputLayout.getEditText();
//                        Log.e("child", String.valueOf(textInputLayout.getHint()));
                        if (editText != null) {
                            String value = editText.getText().toString();
                            String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                            mData.getAttributes().put(key,value);
                            Log.e("key =" + key, "value = " + mData.getAttributes().get(key));

                        }
                    } else if ((textInputLayout.getEditText() instanceof MaterialAutoCompleteTextView) ) {
                        MaterialAutoCompleteTextView editText = (MaterialAutoCompleteTextView) textInputLayout.getEditText();
                        if (editText != null) {
                            String value = String.valueOf(editText.getText());
                            Log.e("Value ",  value);
                            String key = GlobalHelper.reverseToKey(String.valueOf(textInputLayout.getHint()));
                            Log.e("key ",  key);
                            CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                            List<CodedValue> values =  codedValue.getCodedValues();
                            for (CodedValue fieldValue : values) {
//                                Log.e("coded ",  fieldValue.getName());
                                if(fieldValue.getName().equals(value)){
                                    Log.e("codedName equals ",  fieldValue.getName());
                                    mData.getAttributes().put(key,fieldValue.getCode().toString());
                                    Log.e("key =" + key, "value = " + mData.getAttributes().get(key));
                                }
                            }

                        }
                    }
                }

            }

//            Log.e("key = GlobalID" , "value = " + mData.getAttributes().get("GlobalID"));
//
//            Log.e("key = OBJECTID" , "value = " + mData.getAttributes().get("OBJECTID"));
//            mData.getAttributes().put("survey_date",DateTime.getStringLongDate());

            if (mAction == action.Add) {
                activity2.saveNewFeature(mData);
                mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        activity2.featureTemp = mData;
                    }
                });
                mBottomSheetDialog.dismiss();
//                activity2.addFeature(mData);
//                mData.getFeatureTable().addFeatureAsync(mData);
            }else if( mAction == action.show){
//                activity2.editFeature(mData);
                activity2.moveFeatureTo(mData);
                mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        activity2.featureTemp = mData;
                    }
                });
                mBottomSheetDialog.dismiss();
            }




            Log.e("can add: ", String.valueOf(mData.getFeatureTable().canAdd()));
            QueryParameters query = new QueryParameters();
                    query.setWhereClause("1=1");
            mData.getFeatureTable().queryFeatureCountAsync(query).addDoneListener(() -> {
                try {
                    long featureCount = mData.getFeatureTable().queryFeatureCountAsync(query).get();

                    // Display the feature count
                    System.out.println("Feature count: " + featureCount);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });
        iv_marker.setOnClickListener(v-> {
                activity2.moveFeatureTo(mData);
//            activity2.moveSelectedFeatureTo(mData);
//            sheetView.setVisibility(View.GONE);

            mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    activity2.featureTemp = mData;
                }
            });
            mBottomSheetDialog.dismiss();
        });
        return mBottomSheetDialog;
    }

    private void addView() {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        Map<String, Object> attr = mData.getAttributes();
        Set<String> keys = attr.keySet();
        for (String key : keys) {
            Field.Type typeObject = mData.getFeatureTable().getField(key).getFieldType();
            List<String> listAdapter = new ArrayList<>();
            if (mData.getFeatureTable().getField(key).isEditable()&&!key.equals("survey_ID")&&!key.equals("WTR_ID")&&!key.equals("BUN_ID")&&!key.equals("BRD_ID")&&
                    !key.equals("ZIP_ID")&&!key.equals("PUM_ID")&&!key.equals("CVT_ID")&&!key.equals("DAM_ID")&&!key.equals("image")&& !key.equals("survey_date")&&
                    !key.equals("WMA_code")&&!key.equals("source")&&!key.equals("event_type")&&!key.equals("status")&&!key.equals("QC")){
                Object value = attr.get(key);
                if (value == null){
                    value = "";
                }
                Log.e("key",key + " | FieldType " + typeObject+ "\n");
                if(mData.getFeatureTable().getField(key).getDomain() != null){
                    CodedValueDomain codedValue = (CodedValueDomain) mData.getFeatureTable().getField(key).getDomain();
                    List<CodedValue> values =  codedValue.getCodedValues();
                    for (CodedValue fieldValue : values) {
                            listAdapter.add(fieldValue.getName());
                    }
                    viewGroup.addView(viewAutoComplete(GlobalHelper.getNameAttribute(key),value,listAdapter,values,linearLayout));
                }
                else {
//                    if (){
                        if(key.equals("remark")){
                            viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key),value,linearLayout,2));
                        }
                        else {
                            if(typeObject== Field.Type.TEXT){
                                viewGroup.addView(viewEditText(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                            }else if (typeObject == Field.Type.SHORT) {
                                Log.e("shortinput","true");
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                            }
                            else if (typeObject == Field.Type.INTEGER || typeObject == Field.Type.DOUBLE){
                                viewGroup.addView(viewEditNumber(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                            } else if (typeObject == Field.Type.DATE) {
                                viewGroup.addView(viewEditDate(GlobalHelper.getNameAttribute(key),value,linearLayout,1));
                            }

                        }
//                    }
                }
            }
        }
//        viewGroup.addView(linearLayout);
    }

    private View viewEditText(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }

    private View viewEditNumber(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        textInputEditText.setMaxLines(maxLine);
        textInputEditText.setText(value.toString());
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }


    private View viewEditDate(String title, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_text, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        textInputEditText.setInputType(InputType.TYPE_NULL);
        textInputEditText.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            // Create DatePickerDialog
            DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                    (view1, year1, month1, dayOfMonth1) -> {
                        // Update TextInputEditText with selected date
                        String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                        textInputEditText.setText(selectedDate);
                    }, year, month, dayOfMonth);
            // Show DatePickerDialog
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();

        });
//            textInputEditText.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME);
//            textInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
//                Calendar calendar = Calendar.getInstance();
//                int year = calendar.get(Calendar.YEAR);
//                int month = calendar.get(Calendar.MONTH);
//                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//                // Create DatePickerDialog
//                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
//                        (view1, year1, month1, dayOfMonth1) -> {
//                            // Update TextInputEditText with selected date
//                            String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
//                            textInputEditText.setText(selectedDate);
//                        }, year, month, dayOfMonth);
//                // Show DatePickerDialog
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            });
        return customView;
    }

    private View viewEditCompass(String title, Field.Type type, Object value, LinearLayout linearLayout, int maxLine){
        View customView = inflater.inflate(R.layout.item_edit_compass, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.text_title);
        textInputLayout.setHint(title);
        TextInputEditText textInputEditText = customView.findViewById(R.id.text_value);
        final ImageView iv_compas_lock = customView.findViewById(R.id.iv_compas_lock);
//        InputType inputType;
        if (type == Field.Type.TEXT){
            textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
            textInputEditText.setMaxLines(maxLine);
        } else if (type == Field.Type.DATE) {
            textInputEditText.setInputType(InputType.TYPE_NULL);
//            textInputEditText.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME);
            textInputEditText.setOnFocusChangeListener((v, hasFocus) -> {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                // Create DatePickerDialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        (view1, year1, month1, dayOfMonth1) -> {
                            // Update TextInputEditText with selected date
                            String selectedDate = dayOfMonth1 + "/" + (month1 + 1) + "/" + year1;
                            textInputEditText.setText(selectedDate);
                        }, year, month, dayOfMonth);
                // Show DatePickerDialog
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            });
        } else if (type == Field.Type.DOUBLE) {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
            textInputEditText.setMaxLines(maxLine);
        }else if (type == Field.Type.INTEGER) {
            textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            textInputEditText.setMaxLines(maxLine);
        }
        else {
            textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
            textInputEditText.setMaxLines(maxLine);
        }
//        switch (type){
//            case Field.Type.TEXT :
//                textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
//                break;
//            case Field.Type.DOUBLE:
//                textInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
//
//            default: textInputEditText.setInputType(InputType.TYPE_CLASS_TEXT);
//
//        }

        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return customView;
    }


    private View viewAutoComplete(String title, Object value, List<String> listSpin,List<CodedValue> codedValues,LinearLayout linearLayout){
        View customView = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        int selected = 0;
        for(int loop = 0; loop<codedValues.size(); loop++){
            if( value == codedValues.get(loop).getCode()){
                selected = loop;
            }
        }
//        View view = inflater.inflate(R.layout.item_edit_spinner, linearLayout, false);
        TextInputLayout textInputLayout = customView.findViewById(R.id.spin_title);
        textInputLayout.setHint(title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line,listSpin);
        MaterialAutoCompleteTextView textInputEditText = customView.findViewById(R.id.spin_value);
        textInputEditText.setAdapter(adapter);
        if (value != null){
            textInputEditText.setText(adapter.getItem(selected),false);
        }
        return customView;
    }
    @Override
    public void onItemClick(View view, int position) {

    }

    enum contextOf {
        Inventory,
        Replanting
    }

    enum action{
        Add,
        show,
        edit,
    }
}
