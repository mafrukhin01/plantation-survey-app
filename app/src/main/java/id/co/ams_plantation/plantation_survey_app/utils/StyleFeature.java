package id.co.ams_plantation.plantation_survey_app.utils;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.symbology.CompositeSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.Symbol;
import com.esri.arcgisruntime.symbology.TextSymbol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import id.co.ams_plantation.plantation_survey_app.R;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.MapPointTPH;
import id.co.ams_plantation.plantation_survey_app.data.entitiy.ancak.TPH;

public class StyleFeature {
    private static final SpatialReference wgs84 = SpatialReferences.getWgs84();
    public static void addStyleTPH(List<TPH> tphList, Context context, GraphicsOverlay graphicsOverlay, List<MapPointTPH> mapPointTPH){
        List<Symbol> symbols = new ArrayList();
        List<Symbol> symbols2 = new ArrayList();
        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, context.getColor(R.color.LimeGreen), 20);
        SimpleMarkerSymbol symbol2 = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, context.getColor(R.color.grey500), 20);
        SimpleLineSymbol blueOutlineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, context.getColor(R.color.Black), 1f);
        symbol.setOutline(blueOutlineSymbol);
        symbol2.setOutline(blueOutlineSymbol);
        symbols2.add(symbol2);
        symbols.add(symbol);
        CompositeSymbol cms = new CompositeSymbol(symbols);
        CompositeSymbol cms2 = new CompositeSymbol(symbols2);
        for (TPH tph : tphList) {
            if (tph.getAncakFlag()==1){
                Point point = new Point(tph.getLongitude(), tph.getLatitude(), wgs84);
//            SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.RED, 12);
//            Log.e("latlong ", String.valueOf(tph.getLongitude()+tph.getLatitude()));
                Graphic graph = new Graphic(point, cms);
                TextSymbol textSymbol = new TextSymbol();
                textSymbol.setSize(13);
                textSymbol.setText(tph.getNamaTph());
                textSymbol.setFontWeight(TextSymbol.FontWeight.BOLD);
                textSymbol.setColor(Color.BLACK);
                textSymbol.setHaloColor(Color.WHITE);
                textSymbol.setHaloWidth(1f);
                textSymbol.setVerticalAlignment(TextSymbol.VerticalAlignment.MIDDLE);
//            TextSymbol textSymbol = new TextSymbol(12, tph.getNamaTph(), Color.BLACK, TextSymbol.HorizontalAlignment.CENTER, TextSymbol.VerticalAlignment.BOTTOM);
                // Create a graphic with the text symbol
                Graphic textGraphic = new Graphic(point, textSymbol);
                MapPointTPH tphPoint = new MapPointTPH(graph,tph);
                mapPointTPH.add(tphPoint);
                graphicsOverlay.getGraphics().add(graph);
                graphicsOverlay.getGraphics().add(textGraphic);

            } else {
                Point point = new Point(tph.getLongitude(), tph.getLatitude(), wgs84);
//            SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.RED, 12);
//            Log.e("latlong ", String.valueOf(tph.getLongitude()+tph.getLatitude()));
                Graphic graph = new Graphic(point, cms2);
                TextSymbol textSymbol = new TextSymbol();
                textSymbol.setSize(13);
                textSymbol.setText(tph.getNamaTph());
                textSymbol.setFontWeight(TextSymbol.FontWeight.BOLD);
                textSymbol.setColor(Color.BLACK);
                textSymbol.setHaloColor(Color.WHITE);
                textSymbol.setHaloWidth(1f);
                textSymbol.setVerticalAlignment(TextSymbol.VerticalAlignment.MIDDLE);
//            TextSymbol textSymbol = new TextSymbol(12, tph.getNamaTph(), Color.BLACK, TextSymbol.HorizontalAlignment.CENTER, TextSymbol.VerticalAlignment.BOTTOM);

                // Create a graphic with the text symbol
                Graphic textGraphic = new Graphic(point, textSymbol);
                MapPointTPH tphPoint = new MapPointTPH(graph,tph);
                mapPointTPH.add(tphPoint);

                graphicsOverlay.getGraphics().add(graph);
                graphicsOverlay.getGraphics().add(textGraphic);
            }


        }

    }

    public static void addStyleTPH2(List<TPH> tphList, Context context, GraphicsOverlay graphicsOverlay, List<MapPointTPH> mapPointTPH) {
        // Buat simbol dan garis outline yang akan digunakan untuk kedua kondisi
        SimpleMarkerSymbol symbolGreen = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, context.getColor(R.color.LimeGreen), 20);
        SimpleMarkerSymbol symbolGrey = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, context.getColor(R.color.grey500), 20);
        SimpleLineSymbol blueOutlineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, context.getColor(R.color.Black), 1f);

        symbolGreen.setOutline(blueOutlineSymbol);
        symbolGrey.setOutline(blueOutlineSymbol);

        CompositeSymbol compositeGreen = new CompositeSymbol(Collections.singletonList(symbolGreen));
        CompositeSymbol compositeGrey = new CompositeSymbol(Collections.singletonList(symbolGrey));

        // Loop melalui daftar TPH
        for (TPH tph : tphList) {
            boolean isAncak = (tph.getAncakFlag() == 1);
            Point point = new Point(tph.getLongitude(), tph.getLatitude(), wgs84);
            CompositeSymbol compositeSymbol = isAncak ? compositeGreen : compositeGrey;

            // Buat Graphic untuk marker
            Graphic graph = new Graphic(point, compositeSymbol);
            graphicsOverlay.getGraphics().add(graph);

            // Buat TextSymbol untuk menampilkan nama TPH
            TextSymbol textSymbol = new TextSymbol();
            textSymbol.setSize(13);
            textSymbol.setText(tph.getNamaTph());
            textSymbol.setFontWeight(TextSymbol.FontWeight.BOLD);
            textSymbol.setColor(Color.BLACK);
            textSymbol.setHaloColor(Color.WHITE);
            textSymbol.setHaloWidth(1f);
            textSymbol.setVerticalAlignment(TextSymbol.VerticalAlignment.MIDDLE);

            Graphic textGraphic = new Graphic(point, textSymbol);
            graphicsOverlay.getGraphics().add(textGraphic);

            // Tambahkan ke daftar mapPointTPH
            mapPointTPH.add(new MapPointTPH(textGraphic, tph));
        }
    }


}
