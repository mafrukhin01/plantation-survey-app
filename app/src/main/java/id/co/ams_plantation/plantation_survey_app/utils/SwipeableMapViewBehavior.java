package id.co.ams_plantation.plantation_survey_app.utils;

import android.view.MotionEvent;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.esri.arcgisruntime.mapping.view.MapView;

import id.co.ams_plantation.plantation_survey_app.R;

/**
 * Created by Mafrukhin on 25/05/2023.
 */
public class SwipeableMapViewBehavior extends CoordinatorLayout.Behavior<MapView> {

    @Override
    public boolean onInterceptTouchEvent(CoordinatorLayout parent, MapView child, MotionEvent ev) {
        // Consume the touch event to prevent it from collapsing the BottomSheet
        return parent.findViewById(R.id.sample_sheet2).getBottom() != 0;
    }

    @Override
    public boolean onTouchEvent(CoordinatorLayout parent, MapView child, MotionEvent ev) {
        // Consume the touch event to prevent it from collapsing the BottomSheet
        return parent.findViewById(R.id.sample_sheet2).getBottom() != 0;
    }
}