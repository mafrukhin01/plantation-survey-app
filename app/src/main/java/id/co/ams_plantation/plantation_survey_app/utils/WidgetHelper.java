package id.co.ams_plantation.plantation_survey_app.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.SpannableString;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;

import id.co.ams_plantation.plantation_survey_app.R;

public class WidgetHelper {
    public static AlertDialog showOKDialog(Context context,
                                           String title,
                                           String message,
                                           DialogInterface.OnClickListener onClickListener){
//        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_upload_dialog_layout,null);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showOKDialog2(Context context,
                                           String title,
                                           SpannableString message,
                                           DialogInterface.OnClickListener onClickListener){
//        View view = (View) LayoutInflater.from(context).inflate(R.layout.custom_upload_dialog_layout,null);
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK",onClickListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static AlertDialog showOKCancelDialog(Context context,
                                                 String title,
                                                 String message,
                                                 String btnCancelTitle,
                                                 String btnOkTitle,
                                                 boolean cancelable,
                                                 DialogInterface.OnClickListener onClickListener,
                                                 DialogInterface.OnClickListener onCancelListener){
        AlertDialog alertDialog = new AlertDialog
                .Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancelable)
                .setPositiveButton(btnOkTitle,onClickListener)
                .setNegativeButton(btnCancelTitle, onCancelListener)

                .create();
        return alertDialog;
    }

    public static Snackbar showSnackBar(CoordinatorLayout view, String message){
        Snackbar bar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
        bar.show();
        return bar;
    }
    public static AlertDialog showFormDialogTransparant(AlertDialog alertDialog, View view, Context context){
        alertDialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyleTransparant)
                .setCancelable(false)
                .setView(view)
                .create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
        //alertDialog.getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(00F, 00F));
        alertDialog.show();
        return alertDialog;
    }
    public static AlertDialog showListReference(AlertDialog alertDialog,View view,Context context){
        alertDialog = new AlertDialog.Builder(context,R.style.MyAlertDialogStyle)
                .setView(view)
                .create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.UpDownAnimation;
        alertDialog.show();
        return alertDialog;
    }
}
