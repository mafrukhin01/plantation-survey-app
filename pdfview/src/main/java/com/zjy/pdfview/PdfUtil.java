package com.zjy.pdfview;

import static com.zjy.pdfview.download.DownloadService.DOWNLOAD_URL_KEY;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.zjy.pdfview.download.DownloadService;

public class PdfUtil {
    private Context context;

    public PdfUtil(Context context){
        this.context = context;
    }

    public void loadPdf(String url) {
        //contentRv.setVisibility(GONE);
        //loadingLayout.showLoading();
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith("http")) {
                //pdfUrl = url;
                Intent serviceIntent = new Intent(context, DownloadService.class);
                serviceIntent.putExtra(DOWNLOAD_URL_KEY, url);
                context.startService(serviceIntent);
            } else {
//                pdfLocalPath = url;
//                openPdf();
            }
        }

    }
}
