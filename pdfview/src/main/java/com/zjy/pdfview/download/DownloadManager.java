package com.zjy.pdfview.download;

import android.content.Context;

import androidx.annotation.NonNull;

import com.zjy.pdfview.utils.FileUtils;
import com.zjy.pdfview.utils.PdfLog;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Date: 2021/1/26
 * Author: Yang
 * Describe:
 */
public class DownloadManager {
    private String path = "";
    private String fileName = "";
    private IDownloadCallback mCallback;

    public DownloadManager(IDownloadCallback callback) {
        mCallback = callback;
    }

    public void downloadFile(final Context context, final String url) {
        final long startTime = System.currentTimeMillis();
        PdfLog.logInfo("download url=" + url);
        PdfLog.logInfo("download startTime=" + startTime);

        Request request = new Request.Builder().url(url).build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                // 下载失败
                //e.printStackTrace();
                PdfLog.logError("download failed" + e.toString());
                if (mCallback != null) {
                    mCallback.downloadFail();
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                String resultPath = "";
                try {
                    File dest = FileUtils.writeNetToFile(context, url, response);
                    PdfLog.logInfo("download totalTime=" + (System.currentTimeMillis() - startTime));
                    resultPath = dest.getAbsolutePath();
                    if (mCallback != null) {
                        mCallback.downloadSuccess(resultPath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    PdfLog.logError("download failed: " + e.getMessage());
                    if (mCallback != null) {
                        mCallback.downloadFail();
                    }
                } finally {
                    if (mCallback != null) {
                        mCallback.downloadComplete(resultPath);
                    }
                }
            }
        });

//        new OkHttpClient().newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                // 下载失败
//                //e.printStackTrace();
//                PdfLog.logError("download failed" + e.toString());
//                if (mCallback != null) {
//                    mCallback.downloadFail();
//                }
//            }
//
//            @Override
//            public void onResponse(@NonNull Call call, @NonNull Response response) {
//                String resultPath = "";
//                try {
//                    File dest = FileUtils.writeNetToFile(context, url, response);
//                    PdfLog.logInfo("download totalTime=" + (System.currentTimeMillis() - startTime));
//                    resultPath = dest.getAbsolutePath();
//                    if (mCallback != null) {
//                        mCallback.downloadSuccess(resultPath);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    PdfLog.logError("download failed: " + e.getMessage());
//                    if (mCallback != null) {
//                        mCallback.downloadFail();
//                    }
//                } finally {
//                    if (mCallback != null) {
//                        mCallback.downloadComplete(resultPath);
//                    }
//                }
//            }
//        });
    }

    public void downloadFile(final Context context, final String url, final String path, final String fileName) {
        final long startTime = System.currentTimeMillis();
        this.path = path;
        if(!new File(path).exists()){
            File file = new File(path);
            file.mkdirs();
        }
        this.fileName = fileName;
        PdfLog.logInfo("download url=" + url);
        PdfLog.logInfo("download startTime=" + startTime);
        PdfLog.logInfo("download storePath=" + path);
        PdfLog.logInfo("download fileName=" + fileName);


        Request request = new Request.Builder().url(url).build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                // 下载失败
                //e.printStackTrace();
                PdfLog.logError("download failed" + e.toString());
                if (mCallback != null) {
                    mCallback.downloadFail();
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                String resultPath = "";
                try {
                    File dest = null;
                    //File dest = FileUtils.writeNetToFile(context, url, response);
                    if(path==null){
                        dest = FileUtils.writeNetToFile(context, url, response);
                    }else{
                        dest = FileUtils.writeNetToFile(context, url,path,fileName, response);
                    }
                    PdfLog.logInfo("download totalTime=" + (System.currentTimeMillis() - startTime));
                    resultPath = dest.getAbsolutePath();
                    if (mCallback != null) {
                        mCallback.downloadSuccess(resultPath);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    PdfLog.logError("download failed: " + e.getMessage());
                    if (mCallback != null) {
                        mCallback.downloadFail();
                    }
                } finally {
                    if (mCallback != null) {
                        mCallback.downloadComplete(resultPath);
                    }
                }
            }
        });

//        new OkHttpClient().newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(@NonNull Call call, @NonNull IOException e) {
//                // 下载失败
//                //e.printStackTrace();
//                PdfLog.logError("download failed" + e.toString());
//                if (mCallback != null) {
//                    mCallback.downloadFail();
//                }
//            }
//
//            @Override
//            public void onResponse(@NonNull Call call, @NonNull Response response) {
//                String resultPath = "";
//                try {
//                    File dest = FileUtils.writeNetToFile(context, url, response);
//                    PdfLog.logInfo("download totalTime=" + (System.currentTimeMillis() - startTime));
//                    resultPath = dest.getAbsolutePath();
//                    if (mCallback != null) {
//                        mCallback.downloadSuccess(resultPath);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    PdfLog.logError("download failed: " + e.getMessage());
//                    if (mCallback != null) {
//                        mCallback.downloadFail();
//                    }
//                } finally {
//                    if (mCallback != null) {
//                        mCallback.downloadComplete(resultPath);
//                    }
//                }
//            }
//        });
    }

    public void cancel() {
        //mContext = null;

    }
}
